<?php

namespace Home\Controller;

class MemberController extends MemcomController {





    public function pay() {
        $pay_month = M("pay_month")->where("is_show = 1")->order("ord ASC")->select();

        $pay_points = M("pay_points")->where("is_check=1")->order("ord ASC")->select();
        $pay_huobi = M("pay_huobi")->where("is_check=1")->order("ord ASC")->select();
//             print_r($pay_huobi);
        $this->assign("pay_month", $pay_month);
        $this->assign("pay_points", $pay_points);
        $this->assign("pay_huobi", $pay_huobi);
        $this->assign("userid", getUserId());
        $this->assign("title", "在线充值");
        $this->display();
    }

    public function pay2() {
        $pay_month = M("pay_month")->order("ord ASC")->select();

        $pay_points = M("pay_points")->order("ord ASC")->select();
        $pay_huobi = M("pay_huobi")->order("ord ASC")->select();
//             print_r($pay_huobi);
        $this->assign("pay_month", $pay_month);
        $this->assign("pay_points", $pay_points);
        $this->assign("pay_huobi", $pay_huobi);
        $this->assign("title", "在线充值2");
        $this->display();
    }

    public function message() {
        $userid = getUserid();
        $sql = "touid = " . $userid . "";
        $count = M('messages')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, 10);
        $comments = M('messages')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order("addtime desc")->select();
        foreach ($comments as $k => $v) {
            $info = getMessageFormat($v['format_type'], $v['tid'], $v['mtype'], $v['content'], $v['uid'], $v['touid']);
            $comments[$k]['title'] = $info['title'];
            $comments[$k]['content'] = $info['content'];
        }

        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("comments", $comments);
        $this->assign("count", $count);
        $this->assign("title", "我的消息");
        $this->display();
    }

    public function topic() {
        $userid = getUserid();
        $sql = "1=1 AND uid = " . $userid . "";
        $count = M("topic")->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, 10);
        $lists = M("topic")->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order("id desc")->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("title", "我的话题");
        $this->display();
    }

    public function topic_edit() {
        $id = I("get.id");
        $detail = M("topic")->where("id = " . $id . "")->find();
        $assignArr = array(
            "title" => "编辑" . $detail['name'] . " - 素材火",
            "keywords" => "素材火话题讨论,技术讨论,技术求助,技术分享,程序猿的技术闲聊,程序猿的吐槽",
            "description" => "素材火讨论社区给大家提供一个技术圈交流的平台,您可以在这求助各种技术难题,吐槽闲聊心情,分享IT生活的各种欢乐与小烦恼,快来加入我们把！让我们大家共创素材火和谐新社区！",
        );
        $cats = M("topic_dictionary")->where("pid = 1 AND is_check = 1")->order("ord ASC")->select();
        $tags_recommend = M("topic_tags")->where("is_check = 2")->order("ord ASC")->select();
        $latest = M('topic')->where("is_check=1")->limit(10)->order("id DESC")->select();
        if ($detail['tags']) {
            $tags_detail = M("topic_tags")->where("id in (" . $detail['tags'] . ")")->select();
        }
        $this->assign("tags_detail", $tags_detail);
        $this->assign($assignArr);
        $this->assign("cats", $cats);
        $this->assign("cats_num", count($cats));
        $this->assign("tags_recommend", $tags_recommend);
        $this->assign("detail", $detail);
        $this->assign("latest", $latest);
        $this->display();
    }

    public function order() {
        $userid = getUserid();
        $sql = "uid = " . $userid . " AND status = 1";
        if ($userid == 1) {
            
        } else {
            $order_months = M("order_month")->where($sql)->order("addtime desc")->select();
        }


        $count = M('pay')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, 15);
        $lists = M('pay')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order("addtime desc")->select();
//        echo M('pay')->getlastsql();
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("order_months", $order_months);
        $this->assign("title", "充值记录");
        $this->display();
    }

    public function order2() {
//      echo 1;exit;
        $userid = getUserid();
        $sql = "uid = " . $userid . " AND status = 1";
        if ($userid == 1) {
            $sql = "status = 1";
            $times = getTimes();
            $statics = array();

            foreach ($times as $k => $v) {
                $statics[$k]['title'] = $v['title'];
                $statics[$k]['money'] = intval(M('pay')->where("addtime between " . $v['starttime'] . " AND " . $v['endtime'] . " AND status = 1")->sum("money"));
                $statics[$k]['month'] = M('order_month')->where("addtime between " . $v['starttime'] . " AND " . $v['endtime'] . " AND status = 1")->sum("money");
            }
            $days = array();
            $cha = time() - strtotime(date("Y-m-d"));
            for ($i = 0; $i <= 10; $i++) {

                $starttime = strtotime('-' . $i . ' day') - $cha;

                $endtime = strtotime('-' . ($i - 1) . ' day') - $cha;

                $days[$i]['title'] = date("Y-m-d", $starttime);
                $days[$i]['starttime'] = $starttime;
                $days[$i]['endtime'] = $endtime;
                $days[$i]['start_date'] = date("Y-m-d", $starttime);
                $days[$i]['end_date'] = date("Y-m-d", $endtime);
            }
            $statics_days = array();
            foreach ($days as $k => $v) {
                $statics_days[$k]['title'] = $v['title'];
                $statics_days[$k]['money'] = M('pay')->where("addtime between " . $v['starttime'] . " AND " . $v['endtime'] . " AND status = 1")->sum("money");
                $statics_days[$k]['month'] = M('order_month')->where("addtime between " . $v['starttime'] . " AND " . $v['endtime'] . " AND status = 1")->sum("money");
                $statics_days[$k]['total'] = $statics_days[$k]['money'] + $statics_days[$k]['month'];
            }

            $this->assign("statics", $statics);
            $this->assign("statics_days", $statics_days);
        } else {
            $order_months = M("order_month")->where($sql)->select();
        }



        $count_pay = M('pay')->where($sql)->count();    //计算总数
        $count_month = M('order_month')->where($sql)->count();
        $count = $count_pay + $count_month;

        $Page = new \Think\Page($count, 30);
        $sql = "(SELECT `out_trade_no`,`uid`,`money`,`addtime`,`update_time`,`points`,symbol,is_send_email as trade_num,huobi FROM `sucai_pay` where " . $sql . ") UNION ALL (SELECT order_num as out_trade_no,uid,money,addtime,update_time,month as money,status as symbol,trade_num,is_send_email as huobi FROM sucai_order_month where " . $sql . ") order by addtime desc LIMIT " . $Page->firstRow . "," . $Page->listRows . "";
        $lists = M('pay')->query($sql);
//   print_r($lists);
//        echo M('pay')->getlastsql();
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("order_months", $order_months);
        $this->assign("title", "充值记录");
        $this->display("Member:order2");
    }

    public function uploads() {
        $userid = getUserid();
        if ($userid == 1) {
            $sql = "1=1";
        } else {
            $sql = "uid = " . $userid . "";
        }
        $count = M("temp")->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, 10);
        $lists = M("temp")->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order("id desc")->select();
        foreach ($lists as $k => $v) {
            if (in_array($v['state'], array(1, 2))) {

                if ($v['original_id'] > 0) {
                    $table = getMtypeTable($v['mtype']);
                    $detail = M($table)->field("is_original,points_type")->where("id = " . $v['original_id'] . "")->find();
                    $download_points = M("download")->where("mtype = " . $v['mtype'] . " AND tid = " . $v['original_id'] . " AND uid_to = " . $userid . "")->sum("points");
                    $lists[$k]['is_original'] = $detail['is_original'];
                    $lists[$k]['points_type'] = $detail['points_type'];
                    $lists[$k]['download_points'] = $download_points > 0 ? $download_points : 0;
                    $lists[$k]['download_times'] = M("download")->where("mtype = " . $v['mtype'] . " AND tid = " . $v['original_id'] . "")->count();
                }
            }
        }
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("title", "我的上传");
        $this->display();
    }

    public function downloaded() {
        $userid = getUserid();
        $sql = "1=1 AND uid_to = " . $userid . "";
        $count = M("download")->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, 10);
        $lists = M("download")->field("tid,addtime,mtype,points")->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order("addtime desc")->select();
        foreach ($lists as $k => $v) {
            $table = getMtypeTable($v['mtype']);
            $info = M($table)->field("name,tags")->where("id = " . $v['tid'] . "")->find();
            $lists[$k]['title'] = $info['name'];
            $lists[$k]['tags'] = $info['tags'];
            $lists[$k]['url'] = getMtypeHref($v['tid'], $v['mtype']);
        }
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("count", $count);
        $this->assign("title", "被下载");
        $this->display();
    }

    public function invitors() {
        $userid = getUserid();
        $sql = "uid_top = " . $userid . "";
        $count = M("user")->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, 10);
        $lists = M("user")->field("id,name,nickname,money,huobi,addtime")->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order("addtime desc")->select();

        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("count", $count);
        $this->assign("title", "我的下线");
        $this->display();
    }

    public function withdraw() {
        $userid = getUserid();
        $sql = "uid = " . $userid . "";
        $count = M("withdraw")->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, 10);
        $lists = M("withdraw")->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order("addtime desc")->select();

        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("count", $count);
        $this->assign("title", "我的提现");
        $this->display();
    }

    public function withdraw_apply() {
        $this->assign("withdraw_lowest_money", C("money.withdraw_lowest_money"));
        $this->assign("title", "申请提现");
        $this->display();
    }

    public function withdraw_save() {

        $userid = getUserId();

        $money = I("post.money");
        $huobi = $money * (1 + C("money.withdraw_handling_charge_percent") / 100);

        $user = M("user")->field("huobi")->where("id = " . $userid . "")->find();
        if ($user['huobi'] < $huobi) {
            getError('您的账户最多可提现' . getWithdrawMoney($user['huobi']) . '元', 'money_not_enough');
            exit;
        } else {
            $data['uid'] = $userid;
            $data['huobi'] = $huobi;
            $data['money'] = $money;
            $data['account'] = I("post.account");
            $data['realname'] = I("post.realname");
            $data['addtime'] = time();
            $data['method'] = 'alipay';
            $lastid = M("withdraw")->add($data);
            //提现扣除币
            if ($lastid > 0) {
                addPoints("withdraw", $huobi, $userid, 0, 0, 1, 0, 0, 1);
            }
            getError('', '200');
        }
    }

    public function signDay() {

        $userid = getUserid();
        $code_key = getUnionLoginCheckcode(getUserId() . date("Y-m-d"));
//        if ($code_key != I("post.key")) {
//            echo 'key';
//            exit;
//        }
        $data['addtime'] = strtotime(date("Y-m-d 00:00:00"));
        $data['uid'] = $userid;
        $userinfo = M("user")->field("is_check,sign_month")->where("id = '" . $userid . "'")->find();
        if (in_array($userinfo['is_check'], array("-1", "-2"))) {
            echo 'is_check';
            exit;
        }
        $info = M('sign')->field("id")->where("addtime = " . $data['addtime'] . " AND uid = " . $data['uid'] . " AND status = 0")->find();

        if (empty($info)) {
            $data['money'] = getVipPoints($userid, C("points.sign_day"));
            $lastid = M('sign')->add($data);
            if ($lastid > 0) {
                addPoints("day_sign", $data['money'], $userid, 5, 1);
                echo $data['money'];
            }
            $day = date("d");
         
            if ($day >= 20 && $userinfo['sign_month'] != date("Y-m")) {
                $this->month_sign();
            }
        } else {
            echo -1;
        }
    }

    private function month_sign() {
        $userid = getUserid();
        $month_start = strtotime(date("Y-m-1"));
        $month_end = time();
        $sign_month = date("Y-m");
        $count = M("sign")->where("uid = " . $userid . " AND addtime between " . $month_start . " AND " . $month_end . "")->count();
        if ($count >= 20) {
            $award = C("points.sign_month");
            addPoints("month_sign", $award, $userid, 5, 1);
            M("user")->where("id = " . $userid . "")->save(array("sign_month" => $sign_month));
        }
    }

}
