<?php

namespace Home\Controller;

use Think\Controller;

class MemcomController extends Controller {

    function _initialize() {
        header("Content-type: text/html; charset=utf-8");
  setSessionCookie("userid", 5);
        setSessionCookie("unionCheckcode", getUnionLoginCheckcode(5));
        setSessionCookie("username", 'test');
        $action = strtolower(CONTROLLER_NAME);
        $mod = strtolower(ACTION_NAME);
        session('returnUrl', __SELF__);
        $returnUrl = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . ""; //当前页面
        $userid = getUserid();
        if (in_array($action, array("member", "order")) && $userid == '') {
            session('returnUrl', $returnUrl);
            $this->redirect("/login");
        }
        $config = getTableConfig();

        $userinfo = M("user")->field("id,name,nickname,area,money,huobi,invite_code,vip_time,job,sex,signature,times_ba,qq_num")->where("id = " . $userid . "")->find();
        $is_vip = is_vip($userinfo['vip_time']);
        if ($userinfo['area']) {
            $area = explode(",", $userinfo['area']);
            $this->assign("area", $area);
        }
        if ($userinfo['money'] >= C("points.invite") && $userinfo['invite_code'] == '') {
            $invite_code = get_invite_code();
            M("user")->where("id = " . $userid . "")->save(array("invite_code" => $invite_code));
            $userinfo['invite_code'] = $invite_code;
        }

        $collects_num = M('collects')->where("uid = " . $userid . "")->count();
        $signnum = M('sign')->where("uid = " . $userid . "")->count();
        $downloadnum = M('download')->where("uid = " . $userid . "")->count();
        if ($mod == 'message') {
            $msg_num = 0;
            M('messages')->where("touid = '" . $userid . "'")->save(array("is_read" => 1));
        } else {
            $msg_num = M("messages")->where("touid= '" . $userid . "' AND is_read = 0")->count();
        }

        //7天收入(火币)
        $seven_start = time() - 7 * 3600 * 24;
        $now = time();
        $huobi_seven = M("points")->where("uid = " . $userid . " AND fuhao = 1 AND points_type = 1 AND addtime between " . $seven_start . " AND " . $now . "")->sum("money");
        $points_seven = M("points")->where("uid = " . $userid . " AND fuhao = 1 AND points_type = 0 AND addtime between " . $seven_start . " AND " . $now . "")->sum("money");
        $assignArr = array(
            "control" => $action,
            "mod" => $mod,
            "returnUrl" => $returnUrl,
            "version" => C("version"),
            "collects_num" => $collects_num,
            "signnum" => $signnum,
            "downloadnum" => $downloadnum,
            "msg_num" => $msg_num,
        );


        $this->assign("userinfo", $userinfo);

        $this->assign($assignArr);
        $this->assign("config", $config);
        $this->assign("huobi_seven", $huobi_seven > 0 ? $huobi_seven : "0.0");
        $this->assign("points_seven", $points_seven > 0 ? $points_seven : "0.0");
        $this->assign("url_cur", __SELF__);
        $this->assign("is_vip", $is_vip);
    }

}
