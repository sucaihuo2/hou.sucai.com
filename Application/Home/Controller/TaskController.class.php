<?php

namespace Home\Controller;

use Think\Controller;

class TaskController extends Controller {

    public function delivery_notify(){
        $res_not_exist = M("log")->where("is_notify = 0")->order("id desc")->find();

        if($res_not_exist){
            $where["title"] = $res_not_exist["title"];
            $where["mtype"] = $res_not_exist["mtype"];
            $where["is_notify"] = 1;
            $res_exist = M("log")->where($where)->find();

            if(empty($res_exist)){
                $arr["is_notify" ]= 1;
                $w["title"] = $res_not_exist["title"];
                $w["mtype"] = $res_not_exist["mtype"];
                M("log")->where($w)->save($arr);
                $arr = array(
                    "title"=>"手动发货提醒",
                    "content"=> $res_not_exist["title"]."<br/>时间:".date("Y-m-d H:i",$res_not_exist["addtime"])
                );
                sendEmail('email_send_pwd', "1206995177@qq.com", $arr);
                echo 1;
            }else{

                $arr["is_notify" ]= 1;
                $w["title"] = $res_not_exist["title"];
                $w["mtype"] = $res_not_exist["mtype"];
                M("log")->where($w)->save($arr);
                echo 2;
            }
        }
    }

}