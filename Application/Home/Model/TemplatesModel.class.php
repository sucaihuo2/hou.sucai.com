<?php

namespace Home\Model;

use Think\Model;

class TemplatesModel extends Model {

    public $tableName = "user";

    function getModalsCat($field = 'modal_cat_id', $table = 'modals_dictionary', $type, $limit = "") {//模板分类
        $cat_info = M($table)->field("id")->where("code = '" . $field . "'")->find();
        if ($type == 'php') {
            $cat_info['id'] = 15;
        } else {
            if ($table == 'js_dictionary') {
                $sql_js = " AND id != 15";
            }
        }
             $cats = M($table)->field("id,name")->where("pid = " . $cat_info['id'] . $sql_js . " AND is_check=1")->order("ord ASC")->limit($limit)->select();
        return $cats;
    }

    function getModalsCatSub($cat_id, $table = 'modals_dictionary') { //模板子类
        if ($cat_id > 0) {
              $cats_sub = M($table)->field("id,name")->where("pid = " . $cat_id . " AND is_check = 1")->order("ord ASC")->select();
//              echo M($table)->getlastsql();
        }
        return $cats_sub;
    }

    function getIndexTags($dictionary_modals, $modal_cat_id, $tags_modals) {
        $modals_cats = $dictionary_modals[0]['cats']; //分类
        foreach ($modals_cats as $v) {
            if ($modal_cat_id > 0 && $v['id'] == $modal_cat_id) {
                $tagsChose = $v['tags'];
                break;
            }
        }
        $tagsArr = array();
        if ($tagsChose) {
            foreach ($tags_modals as $v) {
                if (in_array($v['id'], explode(",", $tagsChose))) {
                    $tagsArr[] = $v;
                }
            }
        } else {
            $tagsArr = $tags_modals;
        }
        return $tagsArr;
    }

    function getTagRelative($cat_id, $tags_modals) {
        $tag_info = M("modals_dictionary")->field("tags")->where("id = " . $cat_id . "")->find();
        $tag_cats = explode(",", $tag_info['tags']);
        $tag_relative = array();
        if ($tags_modals) {
            foreach ($tags_modals as $v) {
                if (in_array($v['id'], $tag_cats)) {
                    $tag_relative[] = $v;
                }
            }
        }
        return $tag_relative;
    }

    function getSiteSeo($modal_sub_pid, $cat_id, $color_id, $lay_id) {
        $t = "网址大全";
        $t_short = "网址导航";
        $table_dictionary = "modals_dictionary";
        $title = "酷站欣赏_中文网址大全-素材火";
        $keywords = "";
        $description = "";

        if ($modal_sub_pid > 0) {
            $info = M($table_dictionary)->field("name")->where("id = " . $modal_sub_pid . "")->find();
            $title .= $info['name'] . "" . $t . "_";
            $subs = M($table_dictionary)->field("name")->where("pid = " . $modal_sub_pid . "")->select();
            $description = $info['name'];
            foreach ($subs as $k => $v) {
                if ($k < 4) {
                    $keywords .= $v['name'] . "" . $t_short . ",";
                }
                $description .= $v['name'] . ",";
            }
        }
        if ($cat_id > 0) {
            $info = M($table_dictionary)->field("name")->where("id = " . $cat_id . "")->find();
            $title .= $info['name'] . "" . $t . "_";
            $keywords .= $info['name'] . "" . $t_short . ",";
            $description .= $info['name'] . ",";
        }
        if ($color_id > 0) {
            $info = M($table_dictionary)->field("name")->where("id = " . $color_id . "")->find();
            $title .= $info['name'] . "" . $t . "_";
            $keywords .= $info['name'] . "" . $t_short . ",";
            $description .= $info['name'] . ",";
        }
        if ($lay_id > 0) {
            $info = M($table_dictionary)->field("name")->where("id = " . $lay_id . "")->find();
            $title .= $info['name'] . "" . $t . "_";
            $keywords .= $info['name'] . "" . $t_short . ",";
            $description .= $info['name'] . ",";
        }
        if (!$keywords) {
            $keywords = "网址大全,网址导航,中文网址,精选网址";
        } else {
            $keywords = substr($keywords, 0, -1);
        }

        if (!$description) {
            $description = "" . $t . "分为网址大全,网址导航,行业网址,精选网址,商城网址,企业网址,个人网址,门户资讯,素材火";
        } else {
            $description = $t . "分为" . substr($description, 0, -1) . "等" . $t_short;
        }

        return array("title" => $title, "keywords" => $keywords, "description" => $description);
    }

    function getPhpSeo($modal_sub_pid, $cat_id, $easy_id) {

        $t = "PHP教程";
        $t_short = "";
        $table_dictionary = "js_dictionary";
        $title = ""; //php教程_php源码_php+ajax_php+mysql_php实例-素材火
        $keywords = "";
        $description = "";

        if ($modal_sub_pid > 0) {
            $info = M($table_dictionary)->field("name")->where("id = " . $modal_sub_pid . "")->find();
            // $title .= $info['name'] . "".$t;
            $subs = M($table_dictionary)->field("name")->where("pid = " . $modal_sub_pid . "")->select();
            $description = $info['name'];
            foreach ($subs as $k => $v) {
                if ($k < 4) {
                    $keywords .= $v['name'] . "" . $t_short . ",";
                }
                $description .= $v['name'] . ",";
            }
        }
        if ($cat_id > 0) {
            $info = M($table_dictionary)->field("name")->where("id = " . $cat_id . "")->find();
            $title .= $info['name'] . "_" . $t;
            $keywords .= $info['name'] . "" . $t_short . ",";
            $description .= $info['name'] . ",";
        }
        if ($easy_id > 0) {
            $info = M($table_dictionary)->field("name")->where("id = " . $easy_id . "")->find();
            $title .= $info['name'] . "" . $t;
            $keywords .= "PHP" . $info['name'] . "" . $t_short . ",";
            $description .= $info['name'] . ",";
        }
        if (!$title) {
            $title = "php教程_php源码_php+ajax_php+mysql_php实例-素材火";
        } else {
            $title .= "-素材火";
        }
        if (!$keywords) {
            $keywords = "php教程,php源码,jquery+ajax,php+ajax,php实例,CURL,THINKPHP";
        } else {
            $keywords = substr($keywords, 0, -1);
        }

        if (!$description) {
            $description = "为php开发人员提供PHP/MySQL方面的php教程技术文章,常用实例在线演示demo和php源码下载,在线解答AJAX,CURL,JSON,THINKPHP,插件,函数,表单,手机等问题";
        } else {
            $description = $t . "分为" . substr($description, 0, -1) . "等PHP源码" . $t_short;
        }

        return array("title" => $title, "keywords" => $keywords, "description" => $description);
    }

    function getTopicSeo($cat_name) {
        $title = "素材火话题讨论,技术讨论,技术求助,技术分享,素材火官网提供招聘求职,php工程师招聘,前端开发";
        if ($cat_name) {
            $title = $cat_name . " - 素材火";
        }

        $keywords = "PHP,网站前端,网站源码,网站模板，整站下载，CSS、JS特效、Jquery插件";
        $description = "素材火官网求助交流区,您可以在这里求助各种技术难题,如PHP,前端技术,都会有活跃技术高手为您解答";

        return array("title" => $title, "keywords" => $keywords, "description" => $description);
    }

    function getTagSeo($title) {

        $title = "标签-" . $title . "-素材火";
        $keywords = "js标签云,jquery标签库,素材火";
        $description = "" . $title . "标签库为网页前端人员提供建站常用的网页特效,包括图片代码、导航菜单、选项卡/滑动门资源、文字特效、表单代码、提示框/浮动层/弹出层等资源";

        return array("title" => $title, "keywords" => $keywords, "description" => $description);
    }

    function getSourceSeo($modal_sub_pid, $cat_id, $cat_info_name, $cat_sub_info_keywords) {
        $title = "网站源码下载_php网站源码_整站源码_免费网站源码-素材火";
        $keywords = "php网站源码,整站源码,免费网站源码,精品源码下载";
        $description = "素材火官网为PHP程序员提供网站源码在线演示,常见PHP CMS系统的二次开发,php网站源码,企业网站源码,商城源码,html5网站源码免费下载";
        if ($modal_sub_pid > 0) {
            if ($modal_sub_pid != $cat_id) {
                $keywordsArr = $cat_sub_info_keywords ? explode(",", $cat_sub_info_keywords) : "";
                $keywords = '';
                foreach ($keywordsArr as $v) {
                    $keywords .= $v . "源码,";
                }
                $title = str_replace(",", "_", $keywords) . $cat_info_name . "免费下载_素材火";
                $keywords = $keywords . $cat_info_name . "网站源码下载";
                $description = $keywords . $cat_info_name . "网站源码下载,网站源码,DIV+CSS源码,几千种免费网站源码下载尽在素材火www.sucaihuo.com";
            } else {
                $title = "" . $cat_info_name . "网站源码_" . $cat_info_name . "网站源码免费下载_素材火";
                $keywords = "" . $cat_info_name . "源码,网站源码,网站源码,DIV+CSS源码," . $cat_info_name . "网站源码下载";
                $description = "" . $cat_info_name . "网站源码、" . $cat_info_name . "网站源码免费下载," . $cat_info_name . "网站源码下载,网站源码,DIV+CSS源码,几千种免费网站源码下载尽在素材火www.sucaihuo.com";
            }
        }
        return array("title" => $title, "keywords" => $keywords, "description" => $description);
    }

    function getVideoSeo($modal_sub_pid, $cat_id, $cat_info_name, $cat_sub_info_keywords) {
        $title = "php教程_java教程_python视频教程_php开发视频教程-素材火";
        $keywords = "php视频教程,java视频教程,python教程,c语言视频教程";
        $description = "为编程人员准备的网络程序设计开发视频教程,包括php开发教程、php视频教程、java视频教程、python视频教程、c语言视频教程全套下载";
        if ($modal_sub_pid > 0) {
            if ($modal_sub_pid != $cat_id) {
                $keywordsArr = $cat_sub_info_keywords ? explode(",", $cat_sub_info_keywords) : "";
                $keywords = '';
                foreach ($keywordsArr as $v) {
                    $keywords .= $v . "源码,";
                }
                $title = str_replace(",", "_", $keywords) . $cat_info_name . "免费下载_素材火";
                $keywords = $keywords . $cat_info_name . "网站源码下载";
                $description = $keywords . $cat_info_name . "网站源码下载,网站源码,DIV+CSS源码,几千种免费网站源码下载尽在素材火www.sucaihuo.com";
            } else {
                $title = "" . $cat_info_name . "网站源码_" . $cat_info_name . "网站源码免费下载_素材火";
                $keywords = "" . $cat_info_name . "源码,网站源码,网站源码,DIV+CSS源码," . $cat_info_name . "网站源码下载";
                $description = "" . $cat_info_name . "网站源码、" . $cat_info_name . "网站源码免费下载," . $cat_info_name . "网站源码下载,网站源码,DIV+CSS源码,几千种免费网站源码下载尽在素材火www.sucaihuo.com";
            }
        }
        return array("title" => $title, "keywords" => $keywords, "description" => $description);
    }

    function getCourseSeo($modal_sub_pid, $cat_id, $cat_info_name, $cat_sub_info_keywords) {
        $title = "php教程_java教程_python教程_php开发教程-素材火";
        $keywords = "php教程,java教程,python教程,c语言教程";
        $description = "为编程人员准备的网络程序设计开发教程,包括php开发教程、php教程、java教程、python教程、c语言教程全套下载";
        if ($modal_sub_pid > 0) {
            if ($modal_sub_pid != $cat_id) {
                $keywordsArr = $cat_sub_info_keywords ? explode(",", $cat_sub_info_keywords) : "";
                $keywords = '';
                foreach ($keywordsArr as $v) {
                    $keywords .= $v . "源码,";
                }
                $title = str_replace(",", "_", $keywords) . $cat_info_name . "免费下载_素材火";
                $keywords = $keywords . $cat_info_name . "网站源码下载";
                $description = $keywords . $cat_info_name . "网站源码下载,网站源码,DIV+CSS源码,几千种免费网站源码下载尽在素材火www.sucaihuo.com";
            } else {
                $title = "" . $cat_info_name . "网站源码_" . $cat_info_name . "网站源码免费下载_素材火";
                $keywords = "" . $cat_info_name . "源码,网站源码,网站源码,DIV+CSS源码," . $cat_info_name . "网站源码下载";
                $description = "" . $cat_info_name . "网站源码、" . $cat_info_name . "网站源码免费下载," . $cat_info_name . "网站源码下载,网站源码,DIV+CSS源码,几千种免费网站源码下载尽在素材火www.sucaihuo.com";
            }
        }
        return array("title" => $title, "keywords" => $keywords, "description" => $description);
    }

    function getSoftSeo($modal_sub_pid, $cat_id, $cat_info_name, $cat_sub_info_keywords) {
        $title = "php软件_java软件_python软件_php开发软件-素材火";
        $keywords = "php软件,java软件,python软件,c语言软件";
        $description = "为编程人员准备的网络程序设计开发软件,包括php开发软件、php软件、java软件、python软件、c语言软件全套下载";
        if ($modal_sub_pid > 0) {
            if ($modal_sub_pid != $cat_id) {
                $keywordsArr = $cat_sub_info_keywords ? explode(",", $cat_sub_info_keywords) : "";
                $keywords = '';
                foreach ($keywordsArr as $v) {
                    $keywords .= $v . "源码,";
                }
                $title = str_replace(",", "_", $keywords) . $cat_info_name . "免费下载_素材火";
                $keywords = $keywords . $cat_info_name . "网站源码下载";
                $description = $keywords . $cat_info_name . "网站源码下载,网站源码,DIV+CSS源码,几千种免费网站源码下载尽在素材火www.sucaihuo.com";
            } else {
                $title = "" . $cat_info_name . "网站源码_" . $cat_info_name . "网站源码免费下载_素材火";
                $keywords = "" . $cat_info_name . "源码,网站源码,网站源码,DIV+CSS源码," . $cat_info_name . "网站源码下载";
                $description = "" . $cat_info_name . "网站源码、" . $cat_info_name . "网站源码免费下载," . $cat_info_name . "网站源码下载,网站源码,DIV+CSS源码,几千种免费网站源码下载尽在素材火www.sucaihuo.com";
            }
        }
        return array("title" => $title, "keywords" => $keywords, "description" => $description);
    }

    function getTagSql($mtype = 2, $keyword) {
        $tags_js_ids = array(); //js 标签
        if ($mtype == 15) {
            $mtype = 1;
        }
        $table = getTableInfo($mtype);
        $tags_js = M("" . $table . "_tags")->field("id,name")->where("is_check = 1")->select();
        foreach ($tags_js as $v) {
            if (strstr(strtolower($keyword), strtolower($v['name']))) {
                $tags_js_ids[] = $v['id'];
            }
        }
        $sql_js = "";
        if (!empty($tags_js_ids)) {
            foreach ($tags_js_ids as $v) {
                $sql_js .= " OR  FIND_IN_SET('" . $v . "',tags)";
            }
        }
        if ($sql_js) {
            $sql_js .= " AND is_check = 1";
        }
        return $sql_js;
    }

    function getJsSeo($modal_sub_pid, $cat_id, $easy_id) {
//        $t = "";
//        $t_short = "jQuery";
//        $table_dictionary = "js_dictionary";
//        $title = "";
//        $keywords = "";
//        $description = "";
//
//        if ($modal_sub_pid > 0) {
//            $info = M($table_dictionary)->field("name,keywords,description")->where("id = " . $modal_sub_pid . "")->find();
//
//            $subs = M($table_dictionary)->field("name")->where("pid = " . $modal_sub_pid . "")->select();
//            $description = $info['name'];
//            foreach ($subs as $k => $v) {
//                if ($k < 4) {
//                    $keywords .= $v['name'] . "" . $t_short . ",";
//                }
//                $description .= $v['name'] . ",";
//            }
//        }

        $t = "";
        $t_short = "jQuery";
        $table_dictionary = "js_dictionary";
        $title = "";
        $keywords = "";
        $description = "";

        if ($modal_sub_pid > 0) {
            $info = M($table_dictionary)->field("name,seo_title,keywords,description")->where("id = " . $modal_sub_pid . "")->find();

            $subs = M($table_dictionary)->field("name")->where("pid = " . $modal_sub_pid . "")->select();
            $description = $info['name'];
            foreach ($subs as $k => $v) {
                if ($k < 4) {
                    $keywords .= $v['name'] . "" . $t_short . ",";
                }
                $title .= $v['name'] . $t;
                $description .= $v['name'] . ",";
            }
        }
        if ($cat_id > 0) {
            $info = M($table_dictionary)->field("name,seo_title,keywords,description")->where("id = " . $cat_id . "")->find();
            $title .= $info['name'] . $t;
            $keywords .= $info['name'] . "" . $t_short . ",";
            $description .= $info['name'] . ",";
        }

        if (!empty($info['seo_title'])) {
            $title = $info['seo_title'];
            $keywords = $info['keywords'];
            $description = $info['description'];
        }
  
        if ($cat_id > 0) {
            $info = M($table_dictionary)->field("name,seo_title,keywords,description")->where("id = " . $cat_id . "")->find();
            $title .= $info['name'] . $t;
            $keywords .= $info['name'] . "" . $t_short . ",";
            $description .= $info['name'] . ",";
        }

        if (!empty($info['seo_title'])) {
            $title = $info['seo_title'];
            $keywords = $info['keywords'];
            $description = $info['description'];
        }
        if ($easy_id > 0) {
            $info = M($table_dictionary)->field("name,seo_title,keywords,description")->where("id = " . $easy_id . "")->find();
            if ($title) {
                $title = getExplodeSeoTitle($info['name'], $title);
                $keywords = getExplodeSeoTitle($info['name'], $keywords);
                $description = $info['name'] . $description . $info['description'];
            } else {
                $title .= $info['seo_title'] ? $info['seo_title'] : $info['name'] . $t;
            }

            $keywords = $keywords ? $keywords : $info['keywords'];
            $description = $description ? $description : $info['description'];
        }


        if (!$title) {
            $title = "jQuery特效_html5+css3动画_手机特效_html代码大全-素材火";
        } else {
            $title .= "-素材火";
        }
        if (!$keywords) {
            $keywords = "jQuery特效,html5特效,css3动画,手机特效,js代码,html代码大全";
        }

        if (!$description) {
            $description = "分享实用的jQuery特效,js图片特效,文字特效,滚动代码,html5+css3动画,选项卡/滑动门,导航菜单,表单代码,悬浮层/弹出层/提示框,手机特效,常用html代码大全";
        }

        return array("title" => $title, "keywords" => $keywords, "description" => $description);
    }

    function getTemplateSeo($modal_sub_pid, $cat_id, $color_id, $lay_id, $lang_id) {
        $t = "";
        $t_short = "网站模板";
        $table_dictionary = "modals_dictionary";
        $title = "";
        $keywords = "";
        $description = "";

        if ($modal_sub_pid > 0) {
            $info = M($table_dictionary)->field("name,seo_title,keywords,description")->where("id = " . $modal_sub_pid . "")->find();

            $subs = M($table_dictionary)->field("name")->where("pid = " . $modal_sub_pid . "")->select();
            $description = $info['name'];
            foreach ($subs as $k => $v) {
                if ($k < 4) {
                    $keywords .= $v['name'] . "" . $t_short . ",";
                }
                $title .= $v['name'] . $t;
                $description .= $v['name'] . ",";
            }
        }
        if ($cat_id > 0) {
            $info = M($table_dictionary)->field("name,seo_title,keywords,description")->where("id = " . $cat_id . "")->find();
            $title .= $info['name'] . $t;
            $keywords .= $info['name'] . "" . $t_short . ",";
            $description .= $info['name'] . ",";
        }

        if (!empty($info['seo_title'])) {
            $title = $info['seo_title'];
            $keywords = $info['keywords'];
            $description = $info['description'];
        }

        if ($color_id > 0) {
            $info = M($table_dictionary)->field("name,seo_title,keywords,description")->where("id = " . $color_id . "")->find();
            if ($title) {
                $title = getExplodeSeoTitle($info['name'], $title);
                $keywords = getExplodeSeoTitle($info['name'], $keywords);
                $description = $info['name'] . $description . $info['description'];
            } else {
                $title .= $info['seo_title'] ? $info['seo_title'] : $info['name'] . $t;
            }

            $keywords = $keywords ? $keywords : $info['keywords'];
            $description = $description ? $description : $info['description'];
        }
        if ($lay_id > 0) {
            $info = M($table_dictionary)->field("name,seo_title,keywords,description")->where("id = " . $lay_id . "")->find();
            if ($title && $color_id == '') {
                $title = getExplodeSeoTitle($info['name'], $title);
                $keywords = getExplodeSeoTitle($info['name'], $keywords);
                $description = $info['name'] . $description . $info['description'];
            } else {
                $title .= $info['seo_title'] ? $info['seo_title'] : $info['name'] . $t;
            }
        }
        if ($lang_id > 0) {
            $info = M($table_dictionary)->field("name,seo_title,keywords,description")->where("id = " . $lang_id . "")->find();
            if ($title && $lay_id == '' && $color_id == '') {
                $title = getExplodeSeoTitle($info['name'], $title);
                $keywords = getExplodeSeoTitle($info['name'], $keywords);
                $description = $info['name'] . $description . $info['description'];
            } else {

                $title .= $info['seo_title'] ? $info['seo_title'] : $info['name'] . $t;
                $keywords = $info['keywords'];
                $description = $info['description'];
            }
        }

        if (!$title) {
            $title = "网站模板_手机网站模板_商城模板_后台模板_html5模板下载-素材火";
        } else {
            $title .= "-素材火";
        }
        if (!$keywords) {
            $keywords = "网站模板,手机网站模板,企业网站模板,商城模板,后台模板,模板网站";
        }

        if (!$description) {
            $description = "大量优质网站模板,手机wap网站模板,html5响应式网站模板,企业网站模板,后台模板,网上商城模板,app模板,div+css网页模板免费下载";
        }

        return array("title" => $title, "keywords" => $keywords, "description" => $description);
    }

}
?>

