<?php

namespace Home\Model;

use Think\Model;

class JsModel extends Model {

    function getIndexDictionary() {//特效分类
        $dictionary_js = S("dictionary_js");
        if (empty($dictionary_js)) {
            $dictionary_js = M('js_dictionary')->field("id,name")->where("pid = 0 AND is_check = 1")->order("ord ASC")->select();
//            echo M('js_dictionary')->getlastsql();
            foreach ($dictionary_js as $k => $v) {
                $dictionary_js[$k]['cats'] = M('js_dictionary')->field("id,name")->where("pid = " . $v['id'] . " AND is_check = 1")->order("ord ASC")->select();
            }
            S("dictionary_js", $dictionary_js);
        }
        return $dictionary_js;
    }

    function getIndexTags($dictionary_js, $modal_cat_id, $tags_js) {
        $js_cats = $dictionary_js[0]['cats']; //分类
        foreach ($js_cats as $v) {
            if ($modal_cat_id > 0 && $v['id'] == $modal_cat_id) {
                $tagsChose = $v['tags'];
                break;
            }
        }
        $tagsArr = array();
        if ($tagsChose) {
            foreach ($tags_js as $v) {
                if (in_array($v['id'], explode(",", $tagsChose))) {
                    $tagsArr[] = $v;
                }
            }
        } else {
            $tagsArr = $tags_js;
        }
        return $tagsArr;
    }

    function getDetailTags($tags, $tags_js) {
        $tags_detail = array();
        if ($tags) {
            $tagsArr = explode(",", $tags);
            $tags_js = getTableFile("js_tags"); //模板标签缓存
            $i = 0;
            foreach ($tags_js as $v) {
                if (in_array($v['id'], $tagsArr)) {
                    $tags_detail[$i]['id'] = $v['id'];
                    $tags_detail[$i]['name'] = $v['name'];
                    $i++;
                }
            }
        }
        return $tags_detail;
    }

    function getTagRelative($cat_id, $tags_js) {
        $tag_info = M("js_dictionary")->field("tags")->where("id = " . $cat_id . "")->find();
        $tag_cats = explode(",", $tag_info['tags']);
        $tag_relative = array();
        foreach ($tags_js as $v) {
            if (in_array($v['id'], $tag_cats)) {
                $tag_relative[] = $v;
            }
        }
        return $tag_relative;
    }

   

}
?>

