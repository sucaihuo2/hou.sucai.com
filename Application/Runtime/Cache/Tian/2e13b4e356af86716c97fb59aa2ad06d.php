<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
    <!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>素材火 &rsaquo; 登录</title>
        <link rel='stylesheet' id='dashicons-css'  href='/other/admin/login/css/dashicons.css' type='text/css' media='all' />
        <link rel='stylesheet' id='wp-admin-css'  href='/other/admin/login/css/wp-admin.css' type='text/css' media='all' />
        <link rel='stylesheet' id='buttons-css'  href='/other/admin/login/css/buttons.css' type='text/css' media='all' />
        <link rel='stylesheet' id='colors-fresh-css'  href='/other/admin/login/css/colors.css' type='text/css' media='all' />
        <!--[if lte IE 7]>
        <link rel='stylesheet' id='ie-css'  href='/other/admin/login/css/ie.min.css' type='text/css' media='all' />
        <![endif]-->
        <meta name='robots' content='noindex,follow' />
        <script type="text/javascript">
            addLoadEvent = function (func) {
                if (typeof jQuery != "undefined") jQuery(document).ready(func);
                else if (typeof wpOnload != 'function') {
                    wpOnload = func;
                } else {
                    var oldonload = wpOnload;
                    wpOnload = function () {
                        oldonload();
                        func();
                    }
                }
            };

            function s(id, pos) {
                g(id).left = pos + 'px';
            }

            function g(id) {
                return document.getElementById(id).style;
            }

            function shake(id, a, d) {
                c = a.shift();
                s(id, c);
                if (a.length > 0) {
                    setTimeout(function () {
                        shake(id, a, d);
                    }, d);
                } else {
                    try {
                        g(id).position = 'static';
                        wp_attempt_focus();
                    } catch (e) {}
                }
            }
            addLoadEvent(function () {
                var p = new Array(15, 30, 15, 0, -15, -30, -15, 0);
                p = p.concat(p.concat(p));
                var i = document.forms[0].id;
                g(i).position = 'relative';
                shake(i, p, 20);
            });
        </script>
    </head>
    <body class="login login-action-login wp-core-ui">
        <div id="login">
            <h1 style="margin-bottom: 20px">
                <a href="" title="<?php echo ($config["title"]); ?>">
                    <img src="/other/images/logo.jpg">
                </a>
            </h1>
            <?php if($error != ''): ?><div id="login_error">	<strong>错误</strong>：<?php echo ($error); ?><br /></div><?php endif; ?>
            <form name="loginform" id="loginform" action="<?php echo U('Public/login');?>" method="post">
                <p>
                    <label for="user_login">用户名<br />
                        <input type="text" id="name" name="name" class="input" value="<?php echo ($_POST['name']); ?>" size="20" /></label>
                </p>
                <p>
                    <label for="user_pass">密码<br />
                        <input type="password" name="pwd" id="pwd" class="input" value="" size="20" /></label>
                </p>
                <p class="forgetmenot">
                    <label for="rememberme">
                        <input name="rememberme" type="checkbox" checked id="rememberme" value="1"  /> 记住我的登录信息
                    </label>
                </p>
                <p class="submit">
                    <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="登录" />
                </p>
            </form>
            <p id="nav">
                <a href="/" title="找回密码">忘记密码？</a>
            </p>
            <?php if($error != ''): ?><script type="text/javascript">
                    function wp_attempt_focus(){
                        setTimeout( function(){ try{
                                d = document.getElementById('pwd');
                                d.value = '';
                                d.focus();
                                d.select();
                            } catch(e){}
                        }, 200);
                    }
                    if(typeof wpOnload=='function')wpOnload();
                </script><?php endif; ?>
            <p id="backtoblog"><a href="/" title="不知道自己在哪？">&larr; 回到<?php echo ($config["title"]); ?></a></p>
        </div>
        <div class="clear"></div>
    </body>
</html>