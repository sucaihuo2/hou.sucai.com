<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>服务列表 - 后台管理</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="/other/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="/other/assets/css/font-awesome.min.css" />
        <!--[if IE 7]>
          <link rel="stylesheet" href="/other/assets/css/font-awesome-ie7.min.css" />
        <![endif]-->
        <link rel="stylesheet" href="/other/assets/css/jquery.gritter.css" />
        <link rel="stylesheet" href="/other/assets/css/ace.min.css" />
        <link rel="stylesheet" href="/other/assets/css/jquery-ui-1.10.3.full.min.css" />
        <!--[if lte IE 8]>
         <link rel="stylesheet" href="/other/assets/css/ace-ie.min.css" />
       <![endif]-->
        <!--[if lt IE 9]>
               <script src="/other/assets/js/html5shiv.js"></script>
               <script src="/other/assets/js/respond.min.js"></script>
               <![endif]-->
        <link rel="stylesheet" href="/other/admin/css/xuan.css" />
        <script src="/Public/js/jquery.js"></script>
        <!--        [if !IE]> 
                <script type="text/javascript">
                    window.jQuery || document.write("<script src='/other/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
                </script>
                 <![endif]
                [if IE]>
                <script type="text/javascript">
                window.jQuery || document.write("<script src='/other/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
                </script>
                <![endif]-->
        <script src="/other/assets/js/bootstrap.min.js"></script>

        <script src="/other/assets/js/jquery-ui-1.10.3.full.min.js"></script>
        <script src="/other/assets/js/jquery.ui.touch-punch.min.js"></script>

        <script src="/other/assets/js/ace-elements.min.js"></script>
        <script src="/other/assets/js/ace.min.js"></script>
        <script src="/other/js/xuan.js"></script>
        <link rel="stylesheet" href="/other/assets/css/chosen.css" />
        <script src="/other/assets/js/chosen.jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/Public/js/plugins/fancybox/fancybox.css" />
        <script type="text/javascript" src="/Public/js/plugins/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
    </head>
    <body>
        <div class="navbar navbar-default" id="navbar">

            <div class="navbar-container" id="navbar-container">
                <div class="navbar-header pull-left">
                    <a class="navbar-brand" href="/<?php echo (MODULE_NAME); ?>">
                        <small>
                            <i class="icon-leaf"></i>
                            <?php echo ($config["title"]); ?>
                        </small>
                    </a>
                </div>
                <div class="navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="/other/assets/avatars/user.jpg" alt="Jason's Photo" />
                                <span class="user-info">
                                    <small>欢迎,</small>
                                    <?php echo (session('admin_name')); ?>
                                </span>
                                <i class="icon-caret-down"></i>
                            </a>

                            <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="#">
                                        <i class="icon-user"></i>
                                        账号设置
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-cog"></i>
                                        修改密码
                                    </a>
                                </li>
                                <li class="divider"></li>

                                <li>
                                    <a href="<?php echo U('Public/logout');?>">
                                        <i class="icon-off"></i>
                                        退出
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="cl"></div>
            </div>
        </div>


<div class="main-container" id="main-container">
    <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
<div class="sidebar" id="sidebar">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <a class="btn btn-success" href=""><i class="icon-signal"></i></a>
            <button class="btn btn-info"> <i class="icon-pencil"></i> </button>
            <button class="btn btn-warning"><i class="icon-group"></i> </button>
            <button class="btn btn-danger"> <i class="icon-cogs"></i></button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div>
    <ul class="nav nav-list">
        <?php if(is_array($menus)): foreach($menus as $key=>$row): if($row["mod"] == 'index'): ?><li <?php if($row["pid"] == $menu_pid && $row["game_code"] == $game_code): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row["url"]); ?>" >
                        <i class="<?php echo ($row["icon"]); ?>"></i>
                        <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                    </a>
                </li>
                <?php else: ?>
            <li title='<?php echo ($row["id"]); ?>'  <?php if($row["id"] == $menu_pid): ?>class="active open"<?php endif; ?>>
            <a class="dropdown-toggle">
                <i class="<?php echo ($row["icon"]); ?>"></i>
                <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <?php if(is_array($row['sub'])): foreach($row['sub'] as $key=>$row2): ?><li title='<?php echo ($row2["ord"]); ?>'  <?php if($row2['control'] == $control && $row2['mod'] == $mod && $detail["id"] == ''): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row2["url"]); ?>">
                        <i class="icon-double-angle-right"></i>
                        <?php echo ($row2["name"]); ?>
                    </a>
                    </li><?php endforeach; endif; ?>
            </ul>
            </li><?php endif; endforeach; endif; ?>
    </ul>
    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>
</div>
        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li><i class="icon-home home-icon"></i><a href="/<?php echo (MODULE_NAME); ?>">首页</a></li>
                    <li><a href="<?php echo U('Service/lists');?>">服务管理</a></li>
                    <li class="active">服务列表</li>
                </ul>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="header smaller lighter blue">
                                    <div class="nav-search col-xs-10" id="nav-search">
                                        <form class="form-search" method="get">
                                            <div class="search_area">
                                             
                                              
                                               <select name="is_check" autocomplete="off">
                                                    <option value="">请选择是否开启</option>
                                              <option value="0"<?php if($is_check == 0 && $is_check != ''): ?>selected<?php endif; ?>>否</option>
                                                           <option value="1"<?php if($is_check == 1): ?>selected<?php endif; ?>>是</option>
                                            </select>
                                                <input class="form-control search-query" type="text" name="keyword" autocomplete="off"  placeholder="请输入服务名称" value="<?php echo ($_GET['keyword']); ?>">
                                                <button class="btn btn-purple btn-sm" type="submit">搜 索 <i class="icon_search icon-on-right bigger-110"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class='cl'></div>
                                </div>
                                <?php if(!empty($orderlist)): ?><div class="table-responsive">
                                        <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width='45px' class="center">ID</th>
                                                    <th>订单编号</th>
                                                    <th>服务名称</th>
                                                    <th>套餐名称</th>
                                                    <th>套餐价格</th>
                                                    <th>订单用户</th>
                                                    <th>订单金额</th>
                                                    <th>第三方交易号</th>
                                                    <th>支付类型</th>
<!--                                                    <th>处理人</th>-->
<!--                                                    <th>完成时间</th>-->
                                                    <th>订单创建时间</th>
                                                    <th>订单支付时间</th>
                                                    <th>支付状态</th>
<!--                                                    <th>处理状态</th>-->
<!--                                                    <th>操作</th>-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php if(is_array($orderlist)): foreach($orderlist as $key=>$row): ?><tr>
                                                    <td class="center"><?php echo ($row["id"]); ?></td>
                                                    <td><?php echo ($row["order_no"]); ?></td>
                                                    <td><?php echo (msubstr($row["title"],0,32,'utf-8',true)); ?></td>
                                                    <td><?php echo ($row["name"]); ?></td>
                                                    <td><?php echo ($row["price"]); ?></td>
                                                       <td> <a href="<?php echo U('User/detail',array('id'=>$row['uid']));?>" target="_blank"><?php echo (getUserName($row["uid"])); ?></a></td>
                                                    <td><?php echo ($row["order_money"]); ?></td>
                                                    <td><?php echo ($row["trade_no"]); ?></td>
                                                    <td><?php echo (paytype($row["pay_type"])); ?></td>
<!--                                                    <td onclick="setperson('<?php echo ($row["id"]); ?>')" style="color: #b3ad0d;cursor: pointer"><?php echo ($row["do_linkman"]); ?></td>-->
<!--                                                    <td><?php echo (date("m-d H:i",$row["do_finish_time"])); ?></td>-->
                                                    <td><?php echo (date("Y-m-d H:i",$row["addtime"])); ?></td>
                                                    <td><?php echo (date("Y-m-d H:i",$row["time_pay"])); ?></td>
                                                    <td><?php if($row["status"] == 1): ?><span style="color:green">已支付</span><?php else: ?>未支付<?php endif; ?></td>
<!--                                                    <td><?php echo (operation($row["do_status"])); ?></td>-->
<!--                                                    <td class="op">
                                                        <span style="color: #b3ad0d;margin-left: 10px;" onclick="orderdostatus('<?php echo ($row["id"]); ?>',0)">待审核</span>
                                                        <span style="color: #0b18b3;margin-left: 10px" onclick="orderdostatus('<?php echo ($row["id"]); ?>',1)">进行中</span>
                                                        <span style="color: #b3003e;margin-left: 10px" onclick="orderdostatus('<?php echo ($row["id"]); ?>',2)">已完成</span>
                                                        <span style="color: #00b33b;margin-left: 10px" onclick="orderdostatus('<?php echo ($row["id"]); ?>',3)">拒绝</span>
                                                    </td>-->
                                                </tr><?php endforeach; endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?php else: ?>
                                    <p class='noempty'>没有符合条件的记录！</p><?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="ace-settings-container" id="ace-settings-container">
            <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                <i class="icon-cog bigger-150"></i>
            </div>

            <div class="ace-settings-box" id="ace-settings-box">
                <div>
                    <div class="pull-left">
                        <select id="skin-colorpicker" class="hide">
                            <option data-skin="default" value="#438EB9">#438EB9</option>
                            <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                            <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                            <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                        </select>
                    </div>
                    <span>&nbsp; Choose Skin</span>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
                    <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
                    <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
                    <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
                    <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
                    <label class="lbl" for="ace-settings-add-container">
                        Inside
                        <b>.container</b>
                    </label>
                </div>
            </div>
        </div> /#ace-settings-container -->
    </div>
    <script src="/Public/plugins/layer/layer.js"></script>
    <script>
        function yesstatus(id, table, field) {
            $.post(getUrl("Ajax/yes"), {
                id: id,
                table: table,
                field: field
            }, function(data) {
                if (data == "是") {
                    $('#' + field + '_' + id).html("<a onclick=yesstatus(" + id + ",'" + table + "','" + field + "')>" + data + "</a>");
                } else {
                    $('#' + field + '_' + id).html("<a onclick=yesstatus(" + id + ",'" + table + "','" + field + "')><span style='color:red'>" + data + "</span></a>");
                }
            })
        }
        function setperson(id) {
            var info = "";
            layer.prompt({title: '请输入处理人', formType: 3}, function(name, index){
                layer.close(index);
                layer.prompt({title: '请填写联系qq，并确认', formType: 2}, function(qq, index){
                    layer.close(index);
                    info = name + '-' + qq;
                    processing_person(id,info)
                });
            });

        }
        function processing_person(id,info) {
            $.post("<?php echo U('Service/update_processing_person');?>", {
                id: id,
                do_linkman: info,
            }, function(data) {
                if(data.code == 200 ){
                    layer.open({
                        content: '处理人设置成功'
                        ,skin: 'msg'
                        ,time: 3000//2秒后自动关闭
                        ,end: function () {
                            window.location.reload()
                        }
                    });
                }else{
                    layer.alert('处理人设置失败', {
                        icon: 2,
                        skin: 'layer-ext-moon'
                    })
                }
            });
        }
        function orderdostatus(id,orderdostatus) {
            $.post("<?php echo U('Service/orderdostatus');?>", {
                id: id,
                do_status: orderdostatus,
            }, function(data) {
                if(data.code == 200 ){
                    layer.open({
                        content: '状态修改成功'
                        ,skin: 'msg'
                        ,time: 3000//2秒后自动关闭
                        ,end: function () {
                            window.location.reload()
                        }
                    });
                }else{
                    layer.alert('状态修改失败', {
                        icon: 2,
                        skin: 'layer-ext-moon'
                    })
                }
            })
        }

    </script>
</div>
<style>
    .op span{
        cursor: pointer;
    }
</style>
<div class="modal fade" id="modal_state" data-id="0" tabindex="-1" role="dialog" aria-labelledby="modal_delLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">消息提示</h4>
            </div>
            <div class="modal-body">
                <label><input type="radio" name="state_box" value="0" checked autocomplete="off"/> 等待审核</label>
                <label><input type="radio" name="state_box" value="1" autocomplete="off"/> 审核完成</label>
                <label><input type="radio" name="state_box" value="-1" autocomplete="off"/>拒绝通过</label>
                 <textarea id="reason" name="reason" rows="8" cols="60" style="margin: 10px 0 0;"></textarea>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-xs ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" role="button" aria-disabled="false" onclick="checkState($(this))" data-url="<?php echo U('Ajax/checkState');?>">
                    <span class="ui-button-text">
                        <i class="icon-ok bigger-110"></i>
                        确 定
                    </span>
                </button>
                <button class="btn btn-xs ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" role="button" aria-disabled="false" data-dismiss="modal">
                    <span class="ui-button-text">
                        <i class="icon-remove bigger-110"></i>
                        取 消
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {
        $('table th input:checkbox').on('click', function() {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function() {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });
        });
        $(".help-button").popover();
        $('[data-rel=tooltip]').tooltip();
        $(".chosen-select").chosen();
        $(".fancybox_img").fancybox({
            'transitionIn': 'elastic', //窗口显示的方式
            'transitionOut': 'elastic',
            'titlePosition': 'inside'
        });
    })
</script>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
<div class="footer" id="footer" data-url="/"> </div>
<input type="hidden" id="SITE_URL" value="/<?php echo (MODULE_NAME); ?>"/>
<div id="windown_box2"></div>
</body>
</html>