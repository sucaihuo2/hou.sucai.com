<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>用户列表 - 后台管理</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="/other/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="/other/assets/css/font-awesome.min.css" />
        <!--[if IE 7]>
          <link rel="stylesheet" href="/other/assets/css/font-awesome-ie7.min.css" />
        <![endif]-->
        <link rel="stylesheet" href="/other/assets/css/jquery.gritter.css" />
        <link rel="stylesheet" href="/other/assets/css/ace.min.css" />
        <link rel="stylesheet" href="/other/assets/css/jquery-ui-1.10.3.full.min.css" />
        <!--[if lte IE 8]>
         <link rel="stylesheet" href="/other/assets/css/ace-ie.min.css" />
       <![endif]-->
        <!--[if lt IE 9]>
               <script src="/other/assets/js/html5shiv.js"></script>
               <script src="/other/assets/js/respond.min.js"></script>
               <![endif]-->
        <link rel="stylesheet" href="/other/admin/css/xuan.css" />
        <script src="/Public/js/jquery.js"></script>
        <!--        [if !IE]> 
                <script type="text/javascript">
                    window.jQuery || document.write("<script src='/other/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
                </script>
                 <![endif]
                [if IE]>
                <script type="text/javascript">
                window.jQuery || document.write("<script src='/other/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
                </script>
                <![endif]-->
        <script src="/other/assets/js/bootstrap.min.js"></script>

        <script src="/other/assets/js/jquery-ui-1.10.3.full.min.js"></script>
        <script src="/other/assets/js/jquery.ui.touch-punch.min.js"></script>

        <script src="/other/assets/js/ace-elements.min.js"></script>
        <script src="/other/assets/js/ace.min.js"></script>
        <script src="/other/js/xuan.js"></script>
        <link rel="stylesheet" href="/other/assets/css/chosen.css" />
        <script src="/other/assets/js/chosen.jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/Public/js/plugins/fancybox/fancybox.css" />
        <script type="text/javascript" src="/Public/js/plugins/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
    </head>
    <body>
        <div class="navbar navbar-default" id="navbar">

            <div class="navbar-container" id="navbar-container">
                <div class="navbar-header pull-left">
                    <a class="navbar-brand" href="/<?php echo (MODULE_NAME); ?>">
                        <small>
                            <i class="icon-leaf"></i>
                            <?php echo ($config["title"]); ?>
                        </small>
                    </a>
                </div>
                <div class="navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="/other/assets/avatars/user.jpg" alt="Jason's Photo" />
                                <span class="user-info">
                                    <small>欢迎,</small>
                                    <?php echo (session('admin_name')); ?>
                                </span>
                                <i class="icon-caret-down"></i>
                            </a>

                            <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="#">
                                        <i class="icon-user"></i>
                                        账号设置
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-cog"></i>
                                        修改密码
                                    </a>
                                </li>
                                <li class="divider"></li>

                                <li>
                                    <a href="<?php echo U('Public/logout');?>">
                                        <i class="icon-off"></i>
                                        退出
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="cl"></div>
            </div>
        </div>

<div class="main-container" id="main-container">
    <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
<div class="sidebar" id="sidebar">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <a class="btn btn-success" href=""><i class="icon-signal"></i></a>
            <button class="btn btn-info"> <i class="icon-pencil"></i> </button>
            <button class="btn btn-warning"><i class="icon-group"></i> </button>
            <button class="btn btn-danger"> <i class="icon-cogs"></i></button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div>
    <ul class="nav nav-list">
        <?php if(is_array($menus)): foreach($menus as $key=>$row): if($row["mod"] == 'index'): ?><li <?php if($row["pid"] == $menu_pid && $row["game_code"] == $game_code): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row["url"]); ?>" >
                        <i class="<?php echo ($row["icon"]); ?>"></i>
                        <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                    </a>
                </li>
                <?php else: ?>
            <li title='<?php echo ($row["id"]); ?>'  <?php if($row["id"] == $menu_pid): ?>class="active open"<?php endif; ?>>
            <a class="dropdown-toggle">
                <i class="<?php echo ($row["icon"]); ?>"></i>
                <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <?php if(is_array($row['sub'])): foreach($row['sub'] as $key=>$row2): ?><li title='<?php echo ($row2["ord"]); ?>'  <?php if($row2['control'] == $control && $row2['mod'] == $mod && $detail["id"] == ''): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row2["url"]); ?>">
                        <i class="icon-double-angle-right"></i>
                        <?php echo ($row2["name"]); ?>
                    </a>
                    </li><?php endforeach; endif; ?>
            </ul>
            </li><?php endif; endforeach; endif; ?>
    </ul>
    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>
</div>
        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li><i class="icon-home home-icon"></i><a href="/<?php echo (MODULE_NAME); ?>">首页</a></li>
                    <li><a>任务管理</a></li>
                    <li class="active">任务列表</li>
                </ul>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="header smaller lighter blue">
                                    <div class="nav-search col-xs-10" id="nav-search">
                                        <form class="form-search" method="get" action="">
                                            <div class="search_area">
                                                <input class="form-control search-query" type="text" name="orderno" autocomplete="off"  placeholder="请输入订单编号" value="<?php echo ($_GET['orderno']); ?>">
                                                <select name="assignuid" id="assign_uid" autocomplete="off">
                                                    <option value="" >请选择指派用户</option>
                                                    <?php if(is_array($adminlist)): foreach($adminlist as $k=>$vo): ?><option value="<?php echo ($vo["id"]); ?>" <?php if($_GET['assignuid']== $vo["id"] ): ?>selected<?php endif; ?> ><?php echo ($vo["nickname"]); ?></option><?php endforeach; endif; ?>
                                                </select>
                                                <select name="status" id="status" autocomplete="off" style="width:120px;">
                                                    <option value=""  >全部</option>
                                                    <option value="0" <?php if($_GET['status']== 0 ): ?>selected<?php endif; ?> >新建</option>
                                                    <option value="1" <?php if($_GET['status']== 1 ): ?>selected<?php endif; ?> >进行中</option>
                                                    <option value="2" <?php if($_GET['status']== 2 ): ?>selected<?php endif; ?> >已解决</option>
                                                </select>
                                                <input class="form-control search-query" type="text" name="keyword" autocomplete="off"  placeholder="请输入关键字" value="<?php echo ($_GET['keyword']); ?>">
                                                <button class="btn btn-purple btn-sm" type="submit">搜 索 <i class="icon_search icon-on-right bigger-110"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class='cl'></div>
                                </div>
                                <?php if(!empty($tasklist)): ?><div class="table-responsive">
                                        <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width='20px' class="center">序号</th>
                                                    <th style="width:120px;">任务名称</th>
                                                    <th style="width:60px;">订单编号</th>
                                                               <th style="width:60px;">商品</th>
                                                    <th style="width:80px;">qq</th>
                                                    <th style="width:80px;">电话</th>
                                                    <th style="width:120px;">指派给</th>
                                                    <th style="width:130px;">任务状态</th>
                                                    <th style="width:130px;">受理时间</th>
                                                    <th style="width:130px;">创建时间</th>
                                                    <th width="120px">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php if(is_array($tasklist)): foreach($tasklist as $key=>$row): ?><tr>

                                                    <td class="center"><?php echo ($row["id"]); ?></td>
                                                     <td style="text-align: left"><?php echo ($row["task_name"]); ?></td>
                                                     <td style="text-align: left"><?php echo ($row["order_no"]); ?></td>
                                                     <td style="text-align: left"><?php if($row["goods_id"] > 0): ?><a href="https://www.sucaihuo.com/source/<?php echo ($row["goods_id"]); ?>.html" target="_blank"><?php echo ($row["goods_id"]); ?></a><?php endif; ?></td>
                                                     <td style="text-align: left"><?php echo ($row["qq"]); ?></td>
                                                     <td style="text-align: left"><?php echo ($row["mobile"]); ?></td>
                                                     <td style="text-align: left"><?php echo ($row["nickname"]); ?></td>
                                                     <td style="text-align: left"><?php echo (getTaskstatus($row["status"])); ?></td>
                                                     <td style="text-align: left"><?php echo ($row["processing_time"]); ?></td>
                                                     <td style="text-align: left"><?php echo ($row["create_time"]); ?></td>
                                                     <td style="text-align: left">
                                                         <a href="<?php echo U('Taskwork/taskdetails',array('id'=>$row['id']));?>" class="btn btn-minier btn-yellow">查看任务</a>
                                                         <a href="<?php echo U('Taskwork/edittask',array('id'=>$row['id']));?>" class="btn btn-minier btn-success">编辑任务</a>
                                                         <a onclick="deltask(this,<?php echo ($row["id"]); ?>)" class="btn btn-minier btn-pink">删除</a>
<!--                                                         <button type="button" class="btn btn-sm btn-danger" data-toggle="button">Small</button>-->


                                                     </td>

                                                </tr><?php endforeach; endif; ?>
                                            </tbody>
                                        </table>

                                    </div>
                                    <div id="grid-pager" class="ui-state-default  ui-corner-bottom" dir="ltr">
                                        <table width='100%' data-table='user'  id='modal_del_table'>
                                            <tr>
                                                <td>
<!--                                                    <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                        <a class="purple" href="<?php echo U('User/detail');?>" data-rel="tooltip" data-original-title="用户添加">
                                                            <i class="icon-plus-sign bigger-130"></i>
                                                        </a>
                                                        <a class="red" data-toggle="modal" onclick="targetDelTable(0)"  data-rel="tooltip" data-original-title="批量删除">
                                                            <i class="icon-trash bigger-130"></i>
                                                        </a>
                                                    </div>-->
                                                </td>
                                                <td class='td_pager'><div class='pager'><?php echo ($page); ?></div></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <?php else: ?>
                                    <p class='noempty'>没有符合条件的记录,立即<a href="<?php echo U('Taskwork/addtask');?>">添加</a></p><?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="ace-settings-container" id="ace-settings-container">
            <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                <i class="icon-cog bigger-150"></i>
            </div>

            <div class="ace-settings-box" id="ace-settings-box">
                <div>
                    <div class="pull-left">
                        <select id="skin-colorpicker" class="hide">
                            <option data-skin="default" value="#438EB9">#438EB9</option>
                            <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                            <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                            <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                        </select>
                    </div>
                    <span>&nbsp; Choose Skin</span>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
                    <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
                    <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
                    <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
                    <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
                    <label class="lbl" for="ace-settings-add-container">
                        Inside
                        <b>.container</b>
                    </label>
                </div>
            </div>
        </div> /#ace-settings-container -->
    </div>
</div>

<script type="text/javascript">
    jQuery(function($) {
        $('table th input:checkbox').on('click', function() {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function() {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });
        });
        $(".help-button").popover();
        $('[data-rel=tooltip]').tooltip();
        $(".chosen-select").chosen();
        $(".fancybox_img").fancybox({
            'transitionIn': 'elastic', //窗口显示的方式
            'transitionOut': 'elastic',
            'titlePosition': 'inside'
        });
    })
</script>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
<div class="footer" id="footer" data-url="/"> </div>
<input type="hidden" id="SITE_URL" value="/<?php echo (MODULE_NAME); ?>"/>
<div id="windown_box2"></div>
</body>
</html>

<script type="text/javascript" src="/Public/plugins/layer/layer.js"></script>
<script type="text/javascript">
    // function changeUserStatus(uid,status) {
    //
    //
    //
    //     $.post(getUrl('Ajax/changeUserStatus'), {uid:uid, status:status}, function(data) {
    //
    //     }, "json")
    //
    // }
    function showLoginError(tip) {
        $("#notice_error").text(tip).show();
        $("#notice_error").fadeOut(2500);
    }

    function deltask(obj,id) {
        layer.confirm('您确定要删除吗？', {
            btn: ['确定','取消'] //按钮
        }, function(){

            $.ajax({
                url:"<?php echo U('Taskwork/deltask');?>",
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    id:id,
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                beforeSend:function(xhr){
                    var index = layer.load()
                },
                success:function(data,textStatus,jqXHR){
                    if(data.code == 200) {
                        layer.closeAll();
                        $(obj).parent().parent().remove();
                        layer.msg(data.msg);
                    }else{
                        layer.closeAll()
                        layer.msg(data.msg);
                    }
                },
                error:function(xhr,textStatus){
                    // console.log('error')
                    // console.log(xhr)
                    // console.log(textStatus)
                },
                complete:function(){
                    // console.log('end')
                }
            });
        }, function(){
            layer.msg('您取消了操作', {
                time: 20000, //20s后自动关闭
            });
        });
    }
</script>