<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title><?php echo ($menu["name"]); ?> - 后台管理</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="/other/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="/other/assets/css/font-awesome.min.css" />
        <!--[if IE 7]>
          <link rel="stylesheet" href="/other/assets/css/font-awesome-ie7.min.css" />
        <![endif]-->
        <link rel="stylesheet" href="/other/assets/css/jquery.gritter.css" />
        <link rel="stylesheet" href="/other/assets/css/ace.min.css" />
        <link rel="stylesheet" href="/other/assets/css/jquery-ui-1.10.3.full.min.css" />
        <!--[if lte IE 8]>
         <link rel="stylesheet" href="/other/assets/css/ace-ie.min.css" />
       <![endif]-->
        <!--[if lt IE 9]>
               <script src="/other/assets/js/html5shiv.js"></script>
               <script src="/other/assets/js/respond.min.js"></script>
               <![endif]-->
        <link rel="stylesheet" href="/other/admin/css/xuan.css" />
        <script src="/Public/js/jquery.js"></script>
        <!--        [if !IE]> 
                <script type="text/javascript">
                    window.jQuery || document.write("<script src='/other/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
                </script>
                 <![endif]
                [if IE]>
                <script type="text/javascript">
                window.jQuery || document.write("<script src='/other/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
                </script>
                <![endif]-->
        <script src="/other/assets/js/bootstrap.min.js"></script>

        <script src="/other/assets/js/jquery-ui-1.10.3.full.min.js"></script>
        <script src="/other/assets/js/jquery.ui.touch-punch.min.js"></script>

        <script src="/other/assets/js/ace-elements.min.js"></script>
        <script src="/other/assets/js/ace.min.js"></script>
        <script src="/other/js/xuan.js"></script>
        <link rel="stylesheet" href="/other/assets/css/chosen.css" />
        <script src="/other/assets/js/chosen.jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/Public/js/plugins/fancybox/fancybox.css" />
        <script type="text/javascript" src="/Public/js/plugins/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
    </head>
    <body>
        <div class="navbar navbar-default" id="navbar">

            <div class="navbar-container" id="navbar-container">
                <div class="navbar-header pull-left">
                    <a class="navbar-brand" href="/<?php echo (MODULE_NAME); ?>">
                        <small>
                            <i class="icon-leaf"></i>
                            <?php echo ($config["title"]); ?>
                        </small>
                    </a>
                </div>
                <div class="navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="/other/assets/avatars/user.jpg" alt="Jason's Photo" />
                                <span class="user-info">
                                    <small>欢迎,</small>
                                    <?php echo (session('admin_name')); ?>
                                </span>
                                <i class="icon-caret-down"></i>
                            </a>

                            <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="#">
                                        <i class="icon-user"></i>
                                        账号设置
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-cog"></i>
                                        修改密码
                                    </a>
                                </li>
                                <li class="divider"></li>

                                <li>
                                    <a href="<?php echo U('Public/logout');?>">
                                        <i class="icon-off"></i>
                                        退出
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="cl"></div>
            </div>
        </div>


<div class="main-container" id="main-container">
    <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
<div class="sidebar" id="sidebar">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <a class="btn btn-success" href=""><i class="icon-signal"></i></a>
            <button class="btn btn-info"> <i class="icon-pencil"></i> </button>
            <button class="btn btn-warning"><i class="icon-group"></i> </button>
            <button class="btn btn-danger"> <i class="icon-cogs"></i></button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div>
    <ul class="nav nav-list">
        <?php if(is_array($menus)): foreach($menus as $key=>$row): if($row["mod"] == 'index'): ?><li <?php if($row["pid"] == $menu_pid && $row["game_code"] == $game_code): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row["url"]); ?>" >
                        <i class="<?php echo ($row["icon"]); ?>"></i>
                        <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                    </a>
                </li>
                <?php else: ?>
            <li title='<?php echo ($row["id"]); ?>'  <?php if($row["id"] == $menu_pid): ?>class="active open"<?php endif; ?>>
            <a class="dropdown-toggle">
                <i class="<?php echo ($row["icon"]); ?>"></i>
                <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <?php if(is_array($row['sub'])): foreach($row['sub'] as $key=>$row2): ?><li title='<?php echo ($row2["ord"]); ?>'  <?php if($row2['control'] == $control && $row2['mod'] == $mod && $detail["id"] == ''): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row2["url"]); ?>">
                        <i class="icon-double-angle-right"></i>
                        <?php echo ($row2["name"]); ?>
                    </a>
                    </li><?php endforeach; endif; ?>
            </ul>
            </li><?php endif; endforeach; endif; ?>
    </ul>
    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>
</div>
        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li><i class="icon-home home-icon"></i><a href="/<?php echo (MODULE_NAME); ?>">首页</a></li>
                    <li>报表管理</li>
                    <li class="active"><?php echo ($menu["name"]); ?></li>
                </ul>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="header smaller lighter blue clearfix">
                            <form class="form-search" method="get">
                                <div class="search_area" style="float: right;width:auto;margin:2px 0 0 10px">
                                    <button class="btn btn-purple btn-sm" type="submit">查 询 <i class="icon_search icon-on-right bigger-110"></i></button>
                                </div>
                                <div class='calendar_area clearfix'>
                                    <div class='calendar_dates'> 
                                        <div class="ta_date" id="div_date1">
                                            <span class="date_title" id="date1"></span>
                                            <a class="opt_sel" id="input_trigger1" href="#">
                                                <i class="i_orderd"></i>
                                            </a>
                                        </div>
                                        <div id="datePicker1"></div>
                                    </div>
                                    <input type="hidden" name="start_date" id='start_date' value='<?php echo ($start_date); ?>' autocomplete="off" />
                                    <input type="hidden" name="end_date" id='end_date' value='<?php echo ($end_date); ?>' autocomplete="off" />
                                </div>
                            </form>
                        </div>
                        <!-----------------最近10日------------------>
                        <div class="row marginBottom-10">
                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#income" data-toggle="tab">
                                                    <i class="green icon-home bigger-110"></i>
                                                    最近10日统计表
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active">
                                                <div id="chart-1" class="fusion_charts" data-json='<?php echo ($day_pays_json); ?>'></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#pay2_person_num" data-toggle="tab">
                                                    <i class="green icon-home bigger-110"></i>
                                                    最近10日二维表
                                                </a>
                                            </li>
                                        </ul>

                                        <div class="tab-content fusion_charts_table">
                                            <div class="tab-pane active" id="pay2_person_num">
                                                <table class="table table-striped table-border-no table-hover">
                                                    <thead class="thin-border-bottom">
                                                        <tr>
                                                            <th width="90px"><i class="icon-time bigger-110"></i>日期</th>
                                                            <th>金额</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if(is_array($day_pays)): foreach($day_pays as $key=>$row): ?><tr>
                                                            <td><?php echo ($row["label"]); ?></td>
                                                            <td><?php echo ($row["value"]); ?></td>
                                                        </tr><?php endforeach; endif; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hr hr32 hr-dotted"></div>
                        <!-----------------最近周统计------------------>
                        <div class="row marginBottom-10">
                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#income" data-toggle="tab">
                                                    <i class="green icon-home bigger-110"></i>
                                                    最近10周统计表
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active">
                                                <div id="chart-2" class="fusion_charts" data-json='<?php echo ($week_pays_json); ?>'></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#pay2_person_num" data-toggle="tab">
                                                    <i class="green icon-home bigger-110"></i>
                                                    最近10周二维表
                                                </a>
                                            </li>
                                        </ul>

                                        <div class="tab-content fusion_charts_table">
                                            <div class="tab-pane active" id="pay2_person_num">
                                                <table class="table table-striped table-border-no table-hover">
                                                    <thead class="thin-border-bottom">
                                                        <tr>
                                                            <th width="90px"><i class="icon-time bigger-110"></i>日期</th>
                                                            <th>金额</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if(is_array($week_pays)): foreach($week_pays as $key=>$row): ?><tr>
                                                            <td><?php echo ($row["label"]); ?></td>
                                                            <td><?php echo ($row["value"]); ?></td>
                                                        </tr><?php endforeach; endif; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hr hr32 hr-dotted"></div>
                        <!-----------------最近月统计------------------>
                        <div class="row marginBottom-10">
                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#income" data-toggle="tab">
                                                    <i class="green icon-home bigger-110"></i>
                                                    最近10月统计表
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active">
                                                <div id="chart-3" class="fusion_charts" data-json='<?php echo ($month_pays_json); ?>'></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#pay2_person_num" data-toggle="tab">
                                                    <i class="green icon-home bigger-110"></i>
                                                    最近10月二维表
                                                </a>
                                            </li>
                                        </ul>

                                        <div class="tab-content fusion_charts_table">
                                            <div class="tab-pane active" id="pay2_person_num">
                                                <table class="table table-striped table-border-no table-hover">
                                                    <thead class="thin-border-bottom">
                                                        <tr>
                                                            <th width="90px"><i class="icon-time bigger-110"></i>日期</th>
                                                            <th>金额</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if(is_array($month_pays)): foreach($month_pays as $key=>$row): ?><tr>
                                                            <td><?php echo ($row["label"]); ?></td>
                                                            <td><?php echo ($row["value"]); ?></td>
                                                        </tr><?php endforeach; endif; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hr hr32 hr-dotted"></div>
                        <!-----------------最近年统计------------------>
                        <div class="row marginBottom-10">
                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#income" data-toggle="tab">
                                                    <i class="green icon-home bigger-110"></i>
                                                    最近10年统计表
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active">
                                                <div id="chart-4" class="fusion_charts" data-json='<?php echo ($year_pays_json); ?>'></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="widget-box transparent">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#pay2_person_num" data-toggle="tab">
                                                    <i class="green icon-home bigger-110"></i>
                                                    最近10年二维表
                                                </a>
                                            </li>
                                        </ul>

                                        <div class="tab-content fusion_charts_table">
                                            <div class="tab-pane active" id="pay2_person_num">
                                                <table class="table table-striped table-border-no table-hover">
                                                    <thead class="thin-border-bottom">
                                                        <tr>
                                                            <th width="90px"><i class="icon-time bigger-110"></i>日期</th>
                                                            <th>金额</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if(is_array($year_pays)): foreach($year_pays as $key=>$row): ?><tr>
                                                            <td><?php echo ($row["label"]); ?></td>
                                                            <td><?php echo ($row["value"]); ?></td>
                                                        </tr><?php endforeach; endif; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {
        $('table th input:checkbox').on('click', function() {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function() {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });
        });
        $(".help-button").popover();
        $('[data-rel=tooltip]').tooltip();
        $(".chosen-select").chosen();
        $(".fancybox_img").fancybox({
            'transitionIn': 'elastic', //窗口显示的方式
            'transitionOut': 'elastic',
            'titlePosition': 'inside'
        });
    })
</script>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
<div class="footer" id="footer" data-url="/"> </div>
<input type="hidden" id="SITE_URL" value="/<?php echo (MODULE_NAME); ?>"/>
<div id="windown_box2"></div>
</body>
</html>