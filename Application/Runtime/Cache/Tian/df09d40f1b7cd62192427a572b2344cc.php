<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>[title] - 后台管理</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="/other/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="/other/assets/css/font-awesome.min.css" />
        <!--[if IE 7]>
          <link rel="stylesheet" href="/other/assets/css/font-awesome-ie7.min.css" />
        <![endif]-->
        <link rel="stylesheet" href="/other/assets/css/jquery.gritter.css" />
        <link rel="stylesheet" href="/other/assets/css/ace.min.css" />
        <link rel="stylesheet" href="/other/assets/css/jquery-ui-1.10.3.full.min.css" />
        <!--[if lte IE 8]>
         <link rel="stylesheet" href="/other/assets/css/ace-ie.min.css" />
       <![endif]-->
        <!--[if lt IE 9]>
               <script src="/other/assets/js/html5shiv.js"></script>
               <script src="/other/assets/js/respond.min.js"></script>
               <![endif]-->
        <link rel="stylesheet" href="/other/admin/css/xuan.css" />
        <script src="/Public/js/jquery.js"></script>
        <!--        [if !IE]> 
                <script type="text/javascript">
                    window.jQuery || document.write("<script src='/other/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
                </script>
                 <![endif]
                [if IE]>
                <script type="text/javascript">
                window.jQuery || document.write("<script src='/other/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
                </script>
                <![endif]-->
        <script src="/other/assets/js/bootstrap.min.js"></script>

        <script src="/other/assets/js/jquery-ui-1.10.3.full.min.js"></script>
        <script src="/other/assets/js/jquery.ui.touch-punch.min.js"></script>

        <script src="/other/assets/js/ace-elements.min.js"></script>
        <script src="/other/assets/js/ace.min.js"></script>
        <script src="/other/js/xuan.js"></script>
        <link rel="stylesheet" href="/other/assets/css/chosen.css" />
        <script src="/other/assets/js/chosen.jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/Public/js/plugins/fancybox/fancybox.css" />
        <script type="text/javascript" src="/Public/js/plugins/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
    </head>
    <body>
        <div class="navbar navbar-default" id="navbar">

            <div class="navbar-container" id="navbar-container">
                <div class="navbar-header pull-left">
                    <a class="navbar-brand" href="/<?php echo (MODULE_NAME); ?>">
                        <small>
                            <i class="icon-leaf"></i>
                            <?php echo ($config["title"]); ?>
                        </small>
                    </a>
                </div>
                <div class="navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="/other/assets/avatars/user.jpg" alt="Jason's Photo" />
                                <span class="user-info">
                                    <small>欢迎,</small>
                                    <?php echo (session('admin_name')); ?>
                                </span>
                                <i class="icon-caret-down"></i>
                            </a>

                            <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="#">
                                        <i class="icon-user"></i>
                                        账号设置
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-cog"></i>
                                        修改密码
                                    </a>
                                </li>
                                <li class="divider"></li>

                                <li>
                                    <a href="<?php echo U('Public/logout');?>">
                                        <i class="icon-off"></i>
                                        退出
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="cl"></div>
            </div>
        </div>

<div class="main-container" id="main-container">
    <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
<div class="sidebar" id="sidebar">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <a class="btn btn-success" href=""><i class="icon-signal"></i></a>
            <button class="btn btn-info"> <i class="icon-pencil"></i> </button>
            <button class="btn btn-warning"><i class="icon-group"></i> </button>
            <button class="btn btn-danger"> <i class="icon-cogs"></i></button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div>
    <ul class="nav nav-list">
        <?php if(is_array($menus)): foreach($menus as $key=>$row): if($row["mod"] == 'index'): ?><li <?php if($row["pid"] == $menu_pid && $row["game_code"] == $game_code): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row["url"]); ?>" >
                        <i class="<?php echo ($row["icon"]); ?>"></i>
                        <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                    </a>
                </li>
                <?php else: ?>
            <li title='<?php echo ($row["id"]); ?>'  <?php if($row["id"] == $menu_pid): ?>class="active open"<?php endif; ?>>
            <a class="dropdown-toggle">
                <i class="<?php echo ($row["icon"]); ?>"></i>
                <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <?php if(is_array($row['sub'])): foreach($row['sub'] as $key=>$row2): ?><li title='<?php echo ($row2["ord"]); ?>'  <?php if($row2['control'] == $control && $row2['mod'] == $mod && $detail["id"] == ''): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row2["url"]); ?>">
                        <i class="icon-double-angle-right"></i>
                        <?php echo ($row2["name"]); ?>
                    </a>
                    </li><?php endforeach; endif; ?>
            </ul>
            </li><?php endif; endforeach; endif; ?>
    </ul>
    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>
</div>
        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li><i class="icon-home home-icon"></i><a href="/<?php echo (MODULE_NAME); ?>">首页</a></li>
                    <li><a href="<?php echo U('Say/topic');?>">话题管理</a></li>
                    <li class="active">话题列表</li>
                </ul>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="header smaller lighter blue">
                                    <div class="nav-search col-xs-10" id="nav-search">
                                        <form class="form-search" method="get" action="<?php echo U('Say/topic');?>">
                                            <div class="search_area">

                                                <input class="form-control search-query" type="text" name="keywords" autocomplete="off"  placeholder="请输入话题名称" value="<?php echo ($_GET['keywords']); ?>">
                                                <button class="btn btn-purple btn-sm" type="submit">搜 索 <i class="icon_search icon-on-right bigger-110"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                        <a class="red" data-toggle="modal" onclick="targetDelTable(0)"  data-rel="tooltip" data-original-title="批量删除">
                                            <i class="icon-trash bigger-130"></i>
                                        </a>
                                        <a class="purple" href="<?php echo U('Topic/detail');?>" data-rel="tooltip" data-original-title="添加话题">
                                            <i class="icon-plus-sign bigger-130"></i>
                                        </a>
                                    </div>
                                    <div class='cl'></div>
                                </div>
                                <?php if(!empty($lists)): ?><div class="table-responsive">
                                        <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                            <tbody>
                                                <tr>
                                                    <th class="center" width='45px'>
                                                        <label>
                                                            <input type="checkbox" class="ace"  autocomplete="off"/>
                                                            <span class="lbl"></span>
                                                        </label>
                                                    </th>
                                                    <th width="40px">序号</th>
                                                    <th width="90px">用户</th>
                                                    <th>标题</th>
<!--                                                    <th>内容</th>-->
                                                    <th width="130px">发布时间</th>
                                                       <th>是否开启</th>
                                                    <th style="width:130px;"><input type="checkbox" id="dels" autocomplete="off" onclick="checkAll($(this));" />操作</th>
                                                </tr>
                                            <?php if(!empty($lists)): if(is_array($lists)): foreach($lists as $key=>$row): ?><tr id="del_<?php echo ($row["id"]); ?>">
                                                        <td class="center">
                                                            <label><input type="checkbox" class="ace" name="ids" value="<?php echo ($row["id"]); ?>" autocomplete="off" /><span class="lbl"></span></label>
                                                        </td>
                                                        <td><?php echo ($row["id"]); ?></td>
                                                        <td style="text-align: left"><a href="<?php echo U('User/detail',array('id'=>$row['uid']));?>"><?php echo (getSingleField($row["uid"],'user','name')); ?></a></td>
                                                        <td style="text-align: left">  <a href="https://www.sucaihuo.com/topic/<?php echo ($row["id"]); ?>.html" target=""><?php echo ($row["name"]); ?></a></td>
<!--                                                        <td style="text-align: left"><?php echo ($row["content"]); ?></td>-->
                                                        <td><?php echo (date("Y-m-d H:i:s",$row["addtime"])); ?></td>
                                                         <td id="is_check_<?php echo ($row["id"]); ?>"><label onclick="yes('<?php echo ($row["id"]); ?>', 'topic', 'is_check')">
                                                            <input name="switch-field-1" class="ace ace-switch ace-switch-5" type="checkbox"<?php if($row["is_check"] == 1): ?>checked<?php endif; ?>  autocomplete='off'/>
                                                            <span class="lbl"></span>
                                                        </label></td>
                                                        <td>
<!--                                                            <a onclick="targetDelTable('<?php echo ($row["id"]); ?>')">删除</a>-->
 <a class="green" href="<?php echo U('Topic/detail',array('id'=>$row['id']));?>" data-rel="tooltip" data-original-title="编辑"><i class="icon-pencil bigger-130"></i></a>
                                                        </td>
                                                    </tr><?php endforeach; endif; ?>
                                                <?php else: ?>
                                                <tr>
                                                    <td colspan="7">没有找到相关数据。</td>
                                                </tr><?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="grid-pager" class="ui-state-default  ui-corner-bottom" dir="ltr">
                                        <table width='100%' data-table='topic'  id='modal_del_table'>
                                            <tr>
                                                <td>

                                                </td>
                                                <td class='td_pager'><div class='pager'><?php echo ($page); ?></div></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <?php else: ?>
                                    <p class='noempty'>没有符合条件的记录！</p><?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="ace-settings-container" id="ace-settings-container">
            <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                <i class="icon-cog bigger-150"></i>
            </div>

            <div class="ace-settings-box" id="ace-settings-box">
                <div>
                    <div class="pull-left">
                        <select id="skin-colorpicker" class="hide">
                            <option data-skin="default" value="#438EB9">#438EB9</option>
                            <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                            <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                            <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                        </select>
                    </div>
                    <span>&nbsp; Choose Skin</span>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
                    <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
                    <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
                    <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
                    <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
                    <label class="lbl" for="ace-settings-add-container">
                        Inside
                        <b>.container</b>
                    </label>
                </div>
            </div>
        </div> /#ace-settings-container -->
    </div>
</div>
<div class="modal fade" id="modal_del" data-id="0" tabindex="-1" role="dialog" aria-labelledby="modal_delLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">消息提示</h4>
            </div>
            <div class="modal-body">
                确定要删除吗？
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-xs ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" role="button" aria-disabled="false" onclick="more_del($(this))" data-url="<?php echo U('Ajax/more_del');?>">
                    <span class="ui-button-text">
                        <i class="icon-trash bigger-110"></i>
                        确 定
                    </span>
                </button>
                <button class="btn btn-xs ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" role="button" aria-disabled="false" data-dismiss="modal">
                    <span class="ui-button-text">
                        <i class="icon-remove bigger-110"></i>
                        取 消
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {
        $('table th input:checkbox').on('click', function() {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function() {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });
        });
        $(".help-button").popover();
        $('[data-rel=tooltip]').tooltip();
        $(".chosen-select").chosen();
        $(".fancybox_img").fancybox({
            'transitionIn': 'elastic', //窗口显示的方式
            'transitionOut': 'elastic',
            'titlePosition': 'inside'
        });
    })
</script>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
<div class="footer" id="footer" data-url="/"> </div>
<input type="hidden" id="SITE_URL" value="/<?php echo (MODULE_NAME); ?>"/>
<div id="windown_box2"></div>
</body>
</html>