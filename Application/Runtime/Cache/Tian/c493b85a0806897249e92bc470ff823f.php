<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>服务列表 - 后台管理</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="/other/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="/other/assets/css/font-awesome.min.css" />
        <!--[if IE 7]>
          <link rel="stylesheet" href="/other/assets/css/font-awesome-ie7.min.css" />
        <![endif]-->
        <link rel="stylesheet" href="/other/assets/css/jquery.gritter.css" />
        <link rel="stylesheet" href="/other/assets/css/ace.min.css" />
        <link rel="stylesheet" href="/other/assets/css/jquery-ui-1.10.3.full.min.css" />
        <!--[if lte IE 8]>
         <link rel="stylesheet" href="/other/assets/css/ace-ie.min.css" />
       <![endif]-->
        <!--[if lt IE 9]>
               <script src="/other/assets/js/html5shiv.js"></script>
               <script src="/other/assets/js/respond.min.js"></script>
               <![endif]-->
        <link rel="stylesheet" href="/other/admin/css/xuan.css" />
        <script src="/Public/js/jquery.js"></script>
        <!--        [if !IE]> 
                <script type="text/javascript">
                    window.jQuery || document.write("<script src='/other/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
                </script>
                 <![endif]
                [if IE]>
                <script type="text/javascript">
                window.jQuery || document.write("<script src='/other/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
                </script>
                <![endif]-->
        <script src="/other/assets/js/bootstrap.min.js"></script>

        <script src="/other/assets/js/jquery-ui-1.10.3.full.min.js"></script>
        <script src="/other/assets/js/jquery.ui.touch-punch.min.js"></script>

        <script src="/other/assets/js/ace-elements.min.js"></script>
        <script src="/other/assets/js/ace.min.js"></script>
        <script src="/other/js/xuan.js"></script>
        <link rel="stylesheet" href="/other/assets/css/chosen.css" />
        <script src="/other/assets/js/chosen.jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/Public/js/plugins/fancybox/fancybox.css" />
        <script type="text/javascript" src="/Public/js/plugins/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
    </head>
    <body>
        <div class="navbar navbar-default" id="navbar">

            <div class="navbar-container" id="navbar-container">
                <div class="navbar-header pull-left">
                    <a class="navbar-brand" href="/<?php echo (MODULE_NAME); ?>">
                        <small>
                            <i class="icon-leaf"></i>
                            <?php echo ($config["title"]); ?>
                        </small>
                    </a>
                </div>
                <div class="navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="/other/assets/avatars/user.jpg" alt="Jason's Photo" />
                                <span class="user-info">
                                    <small>欢迎,</small>
                                    <?php echo (session('admin_name')); ?>
                                </span>
                                <i class="icon-caret-down"></i>
                            </a>

                            <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="#">
                                        <i class="icon-user"></i>
                                        账号设置
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-cog"></i>
                                        修改密码
                                    </a>
                                </li>
                                <li class="divider"></li>

                                <li>
                                    <a href="<?php echo U('Public/logout');?>">
                                        <i class="icon-off"></i>
                                        退出
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="cl"></div>
            </div>
        </div>


<div class="main-container" id="main-container">
    <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
<div class="sidebar" id="sidebar">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <a class="btn btn-success" href=""><i class="icon-signal"></i></a>
            <button class="btn btn-info"> <i class="icon-pencil"></i> </button>
            <button class="btn btn-warning"><i class="icon-group"></i> </button>
            <button class="btn btn-danger"> <i class="icon-cogs"></i></button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div>
    <ul class="nav nav-list">
        <?php if(is_array($menus)): foreach($menus as $key=>$row): if($row["mod"] == 'index'): ?><li <?php if($row["pid"] == $menu_pid && $row["game_code"] == $game_code): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row["url"]); ?>" >
                        <i class="<?php echo ($row["icon"]); ?>"></i>
                        <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                    </a>
                </li>
                <?php else: ?>
            <li title='<?php echo ($row["id"]); ?>'  <?php if($row["id"] == $menu_pid): ?>class="active open"<?php endif; ?>>
            <a class="dropdown-toggle">
                <i class="<?php echo ($row["icon"]); ?>"></i>
                <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <?php if(is_array($row['sub'])): foreach($row['sub'] as $key=>$row2): ?><li title='<?php echo ($row2["ord"]); ?>'  <?php if($row2['control'] == $control && $row2['mod'] == $mod && $detail["id"] == ''): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row2["url"]); ?>">
                        <i class="icon-double-angle-right"></i>
                        <?php echo ($row2["name"]); ?>
                    </a>
                    </li><?php endforeach; endif; ?>
            </ul>
            </li><?php endif; endforeach; endif; ?>
    </ul>
    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>
</div>
        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li><i class="icon-home home-icon"></i><a href="/<?php echo (MODULE_NAME); ?>">首页</a></li>
                    <li><a href="<?php echo U('Service/lists');?>">服务管理</a></li>
                    <li class="active">服务列表</li>
                </ul>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="header smaller lighter blue">
                                    <div class="nav-search col-xs-10" id="nav-search">
                                        <form class="form-search" method="get">
                                            <div class="search_area">
                                             
                                              
                                               <select name="is_check" autocomplete="off">
                                                    <option value="">请选择是否开启</option>
                                              <option value="0"<?php if($is_check == 0 && $is_check != ''): ?>selected<?php endif; ?>>否</option>
                                                           <option value="1"<?php if($is_check == 1): ?>selected<?php endif; ?>>是</option>
                                            </select>
                                                <input class="form-control search-query" type="text" name="keyword" autocomplete="off"  placeholder="请输入服务名称" value="<?php echo ($_GET['keyword']); ?>">
                                                <button class="btn btn-purple btn-sm" type="submit">搜 索 <i class="icon_search icon-on-right bigger-110"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class='cl'></div>
                                </div>
                                <?php if(!empty($lists)): ?><div class="table-responsive">
                                        <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width='45px' class="center">ID</th>
                                                    <th>服务ip</th>
                                                    <th>服务器账户</th>
                                                    <th>服务器初始密码</th>
                                                    <th width="120px">服务器类型</th>
                                                    <th width="120px">联系qq</th>
                                                    <th width="120px">联系手机</th>
                                                    <th width="120px">开始时间</th>
                                                    <th width="120px">结束时间</th>
                                                    <!--<th width="75px">是否开启</th>-->
                                                    <th width="115px">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php if(is_array($lists)): foreach($lists as $key=>$row): ?><tr>
                                                    <td class="center"><?php echo ($row["id"]); ?></td>
                                                    <td><?php echo ($row["ip"]); ?></td>
                                                    <td><?php echo ($row["account"]); ?></td>
                                                    <td><?php echo ($row["initial_password"]); ?></td>
                                                    <td style="width: 150px"><?php echo (seventype($row["servers_type"])); ?></td>
                                                    <td><?php echo ($row["qq"]); ?></td>
                                                    <td><?php echo ($row["mobile"]); ?></td>
                                                    <td><?php if(!empty($row["start_time"])): echo (date("m-d H:i",$row["start_time"])); endif; ?></td>
                                                    <td><?php if(!empty($row["end_time"])): echo (date("m-d H:i",$row["end_time"])); endif; ?></td>
                                                    <!--<td id="is_check_<?php echo ($row["id"]); ?>"><a onclick="yes('<?php echo ($row["id"]); ?>', 'service', 'is_check')"><?php if($row["is_check"] == 1): ?>是<?php else: ?><span style="color:red">否</span><?php endif; ?></a></td>-->
                                                    <td class="action-buttons">
                                                         <a class="green" href="<?php echo U('Servers/servers_detail',array('id'=>$row['id']));?>" data-rel="tooltip" data-original-title="编辑"><i class="icon-pencil bigger-130"></i></a>
                                                    </td>
                                                </tr><?php endforeach; endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="grid-pager" class="ui-state-default  ui-corner-bottom" dir="ltr">
                                        <table width='100%' data-table='modals'  id='modal_del_table'>
                                            <tr>
                                                <td>
                                                    <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                        <a class="purple" href="<?php echo U('Servers/servers_detail');?>" data-rel="tooltip" data-original-title="添加服务">
                                                            <i class="icon-plus-sign bigger-130"></i>
                                                        </a>
                                                       
                                                    </div>
                                                </td>
                                                <td class='td_pager'><div class='pager'><?php echo ($page); ?></div></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <?php else: ?>
                                    <p class='noempty'>没有符合条件的记录！</p><?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="ace-settings-container" id="ace-settings-container">
            <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                <i class="icon-cog bigger-150"></i>
            </div>

            <div class="ace-settings-box" id="ace-settings-box">
                <div>
                    <div class="pull-left">
                        <select id="skin-colorpicker" class="hide">
                            <option data-skin="default" value="#438EB9">#438EB9</option>
                            <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                            <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                            <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                        </select>
                    </div>
                    <span>&nbsp; Choose Skin</span>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
                    <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
                    <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
                    <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
                    <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
                    <label class="lbl" for="ace-settings-add-container">
                        Inside
                        <b>.container</b>
                    </label>
                </div>
            </div>
        </div> /#ace-settings-container -->
    </div>
    <script src="/Public/plugins/layer/layer.js"></script>
    <script>
        function show_packages_list(url){
            layer.open({
                type: 2,
                title: '服务套餐列表',
                shadeClose: true,
                shade: 0.8,
                area: ['800px', '60%'],
                content: url //iframe的url
            });
        }
    </script>
</div>
<div class="modal fade" id="modal_state" data-id="0" tabindex="-1" role="dialog" aria-labelledby="modal_delLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">消息提示</h4>
            </div>
            <div class="modal-body">
                <label><input type="radio" name="state_box" value="0" checked autocomplete="off"/> 等待审核</label>
                <label><input type="radio" name="state_box" value="1" autocomplete="off"/> 审核完成</label>
                <label><input type="radio" name="state_box" value="-1" autocomplete="off"/>拒绝通过</label>
                 <textarea id="reason" name="reason" rows="8" cols="60" style="margin: 10px 0 0;"></textarea>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-xs ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" role="button" aria-disabled="false" onclick="checkState($(this))" data-url="<?php echo U('Ajax/checkState');?>">
                    <span class="ui-button-text">
                        <i class="icon-ok bigger-110"></i>
                        确 定
                    </span>
                </button>
                <button class="btn btn-xs ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" role="button" aria-disabled="false" data-dismiss="modal">
                    <span class="ui-button-text">
                        <i class="icon-remove bigger-110"></i>
                        取 消
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {
        $('table th input:checkbox').on('click', function() {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function() {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });
        });
        $(".help-button").popover();
        $('[data-rel=tooltip]').tooltip();
        $(".chosen-select").chosen();
        $(".fancybox_img").fancybox({
            'transitionIn': 'elastic', //窗口显示的方式
            'transitionOut': 'elastic',
            'titlePosition': 'inside'
        });
    })
</script>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
<div class="footer" id="footer" data-url="/"> </div>
<input type="hidden" id="SITE_URL" value="/<?php echo (MODULE_NAME); ?>"/>
<div id="windown_box2"></div>
</body>
</html>