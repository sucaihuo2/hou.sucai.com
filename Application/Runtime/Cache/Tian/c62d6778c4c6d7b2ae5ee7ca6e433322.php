<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>模板详情 - 后台管理</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="/other/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="/other/assets/css/font-awesome.min.css" />
        <!--[if IE 7]>
          <link rel="stylesheet" href="/other/assets/css/font-awesome-ie7.min.css" />
        <![endif]-->
        <link rel="stylesheet" href="/other/assets/css/jquery.gritter.css" />
        <link rel="stylesheet" href="/other/assets/css/ace.min.css" />
        <link rel="stylesheet" href="/other/assets/css/jquery-ui-1.10.3.full.min.css" />
        <!--[if lte IE 8]>
         <link rel="stylesheet" href="/other/assets/css/ace-ie.min.css" />
       <![endif]-->
        <!--[if lt IE 9]>
               <script src="/other/assets/js/html5shiv.js"></script>
               <script src="/other/assets/js/respond.min.js"></script>
               <![endif]-->
        <link rel="stylesheet" href="/other/admin/css/xuan.css" />
        <script src="/Public/js/jquery.js"></script>
        <!--        [if !IE]> 
                <script type="text/javascript">
                    window.jQuery || document.write("<script src='/other/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
                </script>
                 <![endif]
                [if IE]>
                <script type="text/javascript">
                window.jQuery || document.write("<script src='/other/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
                </script>
                <![endif]-->
        <script src="/other/assets/js/bootstrap.min.js"></script>

        <script src="/other/assets/js/jquery-ui-1.10.3.full.min.js"></script>
        <script src="/other/assets/js/jquery.ui.touch-punch.min.js"></script>

        <script src="/other/assets/js/ace-elements.min.js"></script>
        <script src="/other/assets/js/ace.min.js"></script>
        <script src="/other/js/xuan.js"></script>
        <link rel="stylesheet" href="/other/assets/css/chosen.css" />
        <script src="/other/assets/js/chosen.jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/Public/js/plugins/fancybox/fancybox.css" />
        <script type="text/javascript" src="/Public/js/plugins/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
    </head>
    <body>
        <div class="navbar navbar-default" id="navbar">

            <div class="navbar-container" id="navbar-container">
                <div class="navbar-header pull-left">
                    <a class="navbar-brand" href="/<?php echo (MODULE_NAME); ?>">
                        <small>
                            <i class="icon-leaf"></i>
                            <?php echo ($config["title"]); ?>
                        </small>
                    </a>
                </div>
                <div class="navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="/other/assets/avatars/user.jpg" alt="Jason's Photo" />
                                <span class="user-info">
                                    <small>欢迎,</small>
                                    <?php echo (session('admin_name')); ?>
                                </span>
                                <i class="icon-caret-down"></i>
                            </a>

                            <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="#">
                                        <i class="icon-user"></i>
                                        账号设置
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-cog"></i>
                                        修改密码
                                    </a>
                                </li>
                                <li class="divider"></li>

                                <li>
                                    <a href="<?php echo U('Public/logout');?>">
                                        <i class="icon-off"></i>
                                        退出
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="cl"></div>
            </div>
        </div>

<script src="/Public/js/other/validate.js"></script>
<script>
    $(function(){
        $.getScript("/Public/js/plugins/kind2/kindeditor-min.js", function() {
            KindEditor.basePath = '/Public/js/plugins/kind2/';
            KindEditor.create('textarea[id="details"]');
        });
    })

        </script>
<div class="main-container" id="main-container">
    <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
<div class="sidebar" id="sidebar">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <a class="btn btn-success" href=""><i class="icon-signal"></i></a>
            <button class="btn btn-info"> <i class="icon-pencil"></i> </button>
            <button class="btn btn-warning"><i class="icon-group"></i> </button>
            <button class="btn btn-danger"> <i class="icon-cogs"></i></button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div>
    <ul class="nav nav-list">
        <?php if(is_array($menus)): foreach($menus as $key=>$row): if($row["mod"] == 'index'): ?><li <?php if($row["pid"] == $menu_pid && $row["game_code"] == $game_code): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row["url"]); ?>" >
                        <i class="<?php echo ($row["icon"]); ?>"></i>
                        <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                    </a>
                </li>
                <?php else: ?>
            <li title='<?php echo ($row["id"]); ?>'  <?php if($row["id"] == $menu_pid): ?>class="active open"<?php endif; ?>>
            <a class="dropdown-toggle">
                <i class="<?php echo ($row["icon"]); ?>"></i>
                <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <?php if(is_array($row['sub'])): foreach($row['sub'] as $key=>$row2): ?><li title='<?php echo ($row2["ord"]); ?>'  <?php if($row2['control'] == $control && $row2['mod'] == $mod && $detail["id"] == ''): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row2["url"]); ?>">
                        <i class="icon-double-angle-right"></i>
                        <?php echo ($row2["name"]); ?>
                    </a>
                    </li><?php endforeach; endif; ?>
            </ul>
            </li><?php endif; endforeach; endif; ?>
    </ul>
    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>
</div>
        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li><i class="icon-home home-icon"></i><a href="/<?php echo (MODULE_NAME); ?>">首页</a></li>
                    <li><a href="<?php echo U('Modals/lists');?>">模板管理</a></li>
                    <li class="active">模板列表</li>
                </ul>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="header smaller lighter blue">模板<?php echo (getPageType($_GET['id'])); ?></h3>
                                <form id="form" enctype="multipart/form-data" method="POST" action="<?php echo U('Modals/detail_post');?>" onsubmit="return checkJsForm()">
                                    <div class="caption">
                                        <table class="table table_detail">
                                            <tr>
                                                <td class="td_left">标题</td>
                                                <td>
                                                    <input type="text" value="<?php echo ($detail["name"]); ?>" name="name" id="name" autocomplete="off"/>
                                                    <?php if($detail["id"] > 0): ?><a href="http://www.sucaihuo.com/templates/<?php echo ($detail["id"]); ?>.html" target="_blank">详情页</a>
                                                        <a href="<?php echo (getModalsDemo($detail["id"])); ?>" target="_blank">演示页</a>
                                                        <a onclick="getMiddleLogo('<?php echo ($detail["id"]); ?>',1)">缩略图</a><?php echo ($detail["height"]); endif; ?>  
                                                </td>
                                            </tr>

                                            <?php if(is_array($modals_cat)): foreach($modals_cat as $key=>$row): ?><tr class="modals_cat">
                                                    <td class="td_left">
                                                        <label class='label_width'>
                                                            <input type='checkbox' value='<?php echo ($row["id"]); ?>'class="cat_id" name='cat_id[]' autocomplete="off" <?php echo (getChecked($detail["cat_id"],$row[id])); ?>>
                                                            <?php echo ($row["name"]); ?>
                                                        </label>
                                                    </td>
                                                    <td>

                                                        <table>

                                                            <tr>
                                                            <?php if(is_array($row['sub'])): foreach($row['sub'] as $key=>$row2): ?><td>
                                                                    <label class='label_width'>
                                                                        <input type='checkbox' value='<?php echo ($row2["id"]); ?>' class='cat_sub_id' name='cat_sub_id[]' autocomplete="off" <?php echo (getChecked($detail["cat_sub_id"],$row2[id])); ?>>
                                                                        <?php echo ($row2["name"]); ?>
                                                                    </label>
                                                                </td><?php endforeach; endif; ?>
                                                </tr>
                                        </table>              
                                        </td>
                                        </tr><?php endforeach; endif; ?>
                                        <tr>
                                            <td class="td_left">颜色</td>
                                            <td>
                                        <?php if(is_array($modals_color)): foreach($modals_color as $key=>$row): ?><label class='label_width'>
                                                <input type='checkbox' value='<?php echo ($row["id"]); ?>' name='modal_color_id[]' <?php echo (getChecked($detail["color_id"],$row['id'])); ?> autocomplete="off">
                                                <?php echo ($row["name"]); ?>
                                            </label><?php endforeach; endif; ?>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td class="td_left">布局</td>
                                            <td>
                                        <?php if(is_array($modals_lay)): foreach($modals_lay as $key=>$row): ?><label class='label_width'>
                                                <input type='radio' value='<?php echo ($row["id"]); ?>' name='modal_lay_id' <?php echo (getEqual($detail["lay_id"],$row['id'],'checked')); ?> autocomplete="off">
                                                <?php echo ($row["name"]); ?>
                                            </label><?php endforeach; endif; ?>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td class="td_left">语言</td>
                                            <td>
                                        <?php if(is_array($modals_lang)): foreach($modals_lang as $key=>$row): ?><label class='label_width'>
                                                <input type='radio' value='<?php echo ($row["id"]); ?>' name='modal_lang_id' <?php echo (getEqual($detail["lang_id"],$row['id'],'checked')); ?> autocomplete="off">
                                                <?php echo ($row["name"]); ?>
                                            </label><?php endforeach; endif; ?>
                                        </td>
                                        </tr>
                                                          <tr>
                    <td class="td_left">上传封面图</td>
                    <td>
                        <a id="big_img_area">
                            <?php if($detail["logo_big"] != ''): ?><a class="fancybox_img" title="<?php echo ($detail["name"]); ?>" href="/<?php echo ($detail["logo_big"]); ?>">
                                <img src="/<?php echo ($detail["logo_big"]); ?>" alt="封面图" class="cover_img" id="cover_img" style="max-height: 250px"/>
                                </a>
                                <?php else: ?>
                                <img src="/Public/images/cover.jpg" alt="封面图" class="cover_img" id="cover_img"/><?php endif; ?>
<!--                            <input name="logo_big" value="<?php echo ($detail["logo_big"]); ?>" type="hidden" autocomplete="off"/>-->
                        </a>
                        <a class="cover_btn" id="cover_btn_big"><span>+</span></a>
                        <span class="tip" style="margin:30px 0 0;display: inline-block">建议760*980px的图片。宽高不限</span>  
                    </td>
                </tr>
                                        <tr>
                                            <td class="td_left">上传源文件</td>
                                            <td>
                                                <a id="zips_area">
<!--                                                    <input id="zip_input"  value='<?php echo ($detail["zip"]); ?>' type="text" style="background:#ddd;width:350px;margin:20px 16px 0 0" disabled="disabled" autocomplete="off"/>-->
                                                </a>
                                                <a class="cover_btn" id="zip_btn"><span>+</span></a>
                                                <span class="tip">根目录必须含有“index.html”否则无法演示不能通过审核。</span>  
                                                <input type="hidden" name="zip" id="zip" autocomplete="off" value='<?php echo ($detail["zip"]); ?>'/>
                                        <?php if($detail["zip"] != ''): ?><a href="/<?php echo ($detail["zip"]); ?>">点击下载</a><?php endif; ?>
                                        </td>
                                        </tr>
                                                  <tr>
    <td class="td_left">原创</td>
    <td class="clearfix">
        <div class="opt_radio"> 
            <input class="magic-radio"  type="radio" name="is_original"  id="is_original_1" <?php echo (getEqual($detail["is_original"],0,'checked')); ?> value="0" autocomplete='off' >
            <label for="is_original_1">否</label>
        </div>
        <div class="opt_radio">
            <input class="magic-radio" type="radio" name="is_original"  id="is_original_2" <?php echo (getEqual($detail["is_original"],1,'checked')); ?> value="1" autocomplete='off'>
            <label for="is_original_2">是</label>
        </div>

    </td>
</tr>
<tr id='tr_points_type'>
    <td class="td_left">币种类型</td>
    <td class="clearfix">
        <div class="opt_radio"> 
            <input class="magic-radio" type="radio" onclick="checkPointsType(this.value)" name="points_type"  id="points_type_1" <?php echo (getEqual($detail["points_type"],0,'checked')); ?> value="0" autocomplete='off' >
            <label for="points_type_1">积分</label>
        </div>
        <div class="opt_radio">
            <input class="magic-radio"  type="radio"   onclick="checkPointsType(this.value)" name="points_type" id="points_type_2" <?php echo (getEqual($detail["points_type"],1,'checked')); ?> value="1" autocomplete='off'>
            <label for="points_type_2">火币</label>
        </div>

    </td>
</tr>

    <tr id="tr_download_jifen">
    <td class="td_left" id="points_title"> 下载<?php echo (getPointsUnit($detail["points_type"])); ?><span class="red">*</span></td>
    <td>
        <input type="text" value='<?php echo ($detail["points"]); ?>' name="points" id="points" autocomplete="off" style="width:80px" maxlength="6"/>
        <span class="tip" id="points_description"><?php if($detail["points_type"] == 0): ?>适当的下载积分，赚取更多积分<?php else: ?>赚取火币可以”申请提现“<?php endif; ?>。</span>  
    </td>
</tr>


                                        <tr>
                                            <td class="td_left">来源</td>
                                            <td>
                                                <input type="text" value="<?php echo ($detail["source"]); ?>" placeholder="http://www.sucaihuo.com" name="source" autocomplete="off"/>
                                                <span class="tip">链接地址如：“http://www.sucaihuo.com”</span>  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td_left">演示地址</td>
                                            <td>
                                                <input type="text" value="<?php echo ($detail["url_demo"]); ?>" name="url_demo" autocomplete="off"/>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="td_left">标签:<a onclick="addTags()">+添加标签</a></td>
                                            <td id="td_tags"> 
                                        <?php if(is_array($tags)): foreach($tags as $key=>$row): ?><input name="tag[]" type="text" class="common_txt tag" autocomplete="off" value="<?php echo ($row); ?>"/><a onclick=$(this).prev('input').remove();
                                                                                                                                         $(this).remove()>删除</a><?php endforeach; endif; ?>
                                        </td>
                                        </tr>
                                        </table>
                                    </div>
                                    <div class="course">
                                        <table class="table table_detail" id="table_course">

                                            <tr>
                                                <td class="td_left">状态</td>
                                                <td>

                                                    <label>
                                                        <input class="ace" type="radio"  name="is_check" value='0' <?php echo (getEqual($detail["is_check"],0,'checked')); ?> autocomplete='off'/>
                                                        <span class="lbl"> 关闭</span>
                                                    </label>
                                                    <label>
                                                        <input class="ace" type="radio"  name="is_check" value='1' <?php echo (getEqual($detail["is_check"],1,'checked')); ?> autocomplete='off'/>
                                                        <span class="lbl"> 开启</span>
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_left">是否推荐</td>
                                                <td>

                                                    <label>
                                                        <input class="ace" type="radio"  name="is_recommend" value='0' <?php echo (getEqual($detail["is_recommend"],0,'checked')); ?> autocomplete='off'/>
                                                        <span class="lbl"> 否</span>
                                                    </label>
                                                    <label>
                                                        <input class="ace" type="radio"  name="is_recommend" value='1' <?php echo (getEqual($detail["is_recommend"],1,'checked')); ?> autocomplete='off'/>
                                                        <span class="lbl"> 是</span>
                                                    </label>
                                                </td>
                                            </tr>
  <tr>
                                                <td class="td_left">详情页开头</td>
                                                <td>
                                                    <textarea class="textarea" name="content_first" id="content_first" autocomplete="off"><?php echo ($detail["content_first"]); ?></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_left">关键字</td>
                                                <td>
                                                    <textarea class="textarea" name="keywords" id="keywords" autocomplete="off"><?php echo ($detail["keywords"]); ?></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_left">描述</td>
                                                <td>
                                                    <textarea class="textarea" name="description" id="description" autocomplete="off"><?php echo ($detail["description"]); ?></textarea>
                                                </td>
                                            </tr>
                                               <tr>
                                                <td class="td_left">详细说明</td>
                                                <td>
                                                    <textarea class="textarea" name="details" id="details" autocomplete="off" style="height:300px"><?php echo ($detail["details"]); ?></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_left"></td>
                                                <td>
                                                    <input type='hidden' name='id' value='<?php echo ($_GET['id']); ?>'/>
                                                    <button class="btn btn-info btn-sm" type="submit">
                                                        <i class="icon-ok bigger-110 "></i>
                                                        保 存
                                                    </button>
                                                    <button class="btn btn-sm reset" type="button" onclick="goUrl(-1)">
                                                        <i class="icon-undo bigger-110"></i>
                                                        返 回
                                                    </button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="ace-settings-container" id="ace-settings-container">
            <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                <i class="icon-cog bigger-150"></i>
            </div>

            <div class="ace-settings-box" id="ace-settings-box">
                <div>
                    <div class="pull-left">
                        <select id="skin-colorpicker" class="hide">
                            <option data-skin="default" value="#438EB9">#438EB9</option>
                            <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                            <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                            <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                        </select>
                    </div>
                    <span>&nbsp; Choose Skin</span>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
                    <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
                    <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
                    <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
                    <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
                    <label class="lbl" for="ace-settings-add-container">
                        Inside
                        <b>.container</b>
                    </label>
                </div>
            </div>
        </div> /#ace-settings-container -->
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {
        $('table th input:checkbox').on('click', function() {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function() {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });
        });
        $(".help-button").popover();
        $('[data-rel=tooltip]').tooltip();
        $(".chosen-select").chosen();
        $(".fancybox_img").fancybox({
            'transitionIn': 'elastic', //窗口显示的方式
            'transitionOut': 'elastic',
            'titlePosition': 'inside'
        });
    })
</script>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
<div class="footer" id="footer" data-url="/"> </div>
<input type="hidden" id="SITE_URL" value="/<?php echo (MODULE_NAME); ?>"/>
<div id="windown_box2"></div>
</body>
</html>

<div class="modal fade" id="modal_cat" data-id="0" data-modal="position"  tabindex="-1" role="dialog" aria-labelledby="modal_catLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" ></h4>
            </div>
            <div class="modal-body">
                名称：
                <input id="cat_name" type="text" class="common_txt" value="<?php echo ($detail["ord"]); ?>" maxlength="20" autocomplete="off"/>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-xs ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="submit" role="button" aria-disabled="false" >
                    <span class="ui-button-text">
                        <i class="icon-trash bigger-110"></i>
                        确 定
                    </span>
                </button>
                <button class="btn btn-xs ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" role="button" aria-disabled="false" data-dismiss="modal">
                    <span class="ui-button-text">
                        <i class="icon-remove bigger-110"></i>
                        取 消
                    </span>
                </button>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/Public/js/plugins/plupload/plupload.full.min.js"></script>
<script type="text/javascript">

  

    var uploader3 = new plupload.Uploader({
        runtimes: 'html5,flash,silverlight,html4', //上传插件初始化选用那种方式的优先级顺序
        browse_button: ['cover_btn_big'], // 上传按钮
        url: "/Tian/Uploadreal/cover_img/id/<?php echo ($id); ?>/utype/", //远程上传地址
        flash_swf_url: 'plupload/Moxie.swf', //flash文件地址
        silverlight_xap_url: 'plupload/Moxie.xap', //silverlight文件地址
        filters: {
            max_file_size: '50mb', //最大上传文件大小（格式100b, 10kb, 50mb, 1gb）
            mime_types: [//允许文件上传类型
                {title: "files", extensions: "jpg,png,gif,jpeg"}
            ]
        },
        multi_selection: false, //true:ctrl多文件上传, false 单文件上传
        multipart_params: {
            code: 'identity',
        },
        init: {
            FilesAdded: function(up, files) { //文件上传前

                var item = "";
                plupload.each(files, function(file) { //遍历文件
                    item += "<div class='progress' id=" + file['id'] + "><span class='bar'></span><span class='percent'>0%</span></div>";
                });
                $("#big_img_area").html(item)
                uploader3.start();

            },
            UploadProgress: function(up, file) { //上传中，显示进度条
                var percent = file.percent;
                $("#" + file.id).find('.bar').css({"width": percent + "%"});
                $("#" + file.id).find(".percent").text(percent + "%");
            },
            FileUploaded: function(up, file, info) { //文件上传成功的时候触发

                var data = eval("(" + info.response + ")");//解析返回的json数据
                if (data.error != '') {
//                    alert(data.error);
                    $("#big_img_area").html("<img id='cover_img' class='cover_img' src='/Public/images/cover.jpg' alt='封面'><input name='logo' value='' autocomplete='off' type='hidden'>");
                } else {
                    $("#" + file.id).html("<input type='hidden'name='logo_big' value='" + data.src + "'/><img class='img_common'  src='/" + data.src + "'/>");//追加图片
                }
            },
            Error: function(up, err) { //上传出错的时候触发
                alert(err.message);
            }
        }
    });
    uploader3.init();
  var uploader4 = new plupload.Uploader({
        runtimes: 'html5,flash,silverlight,html4', //上传插件初始化选用那种方式的优先级顺序
        browse_button: ['cover_btn_demo'], // 上传按钮
          url: "/Tian/Uploadreal/cover_img/id/<?php echo ($id); ?>/utype/", //远程上传地址
        flash_swf_url: 'plupload/Moxie.swf', //flash文件地址
        silverlight_xap_url: 'plupload/Moxie.xap', //silverlight文件地址
        filters: {
            max_file_size: '50mb', //最大上传文件大小（格式100b, 10kb, 50mb, 1gb）
            mime_types: [//允许文件上传类型
                {title: "files", extensions: "jpg,png,gif,jpeg"}
            ]
        },
        multi_selection: false, //true:ctrl多文件上传, false 单文件上传
        multipart_params: {
            code: 'identity',
        },
        init: {
            FilesAdded: function(up, files) { //文件上传前

                var item = "";
                plupload.each(files, function(file) { //遍历文件
                    item += "<div class='progress' id=" + file['id'] + "><span class='bar'></span><span class='percent'>0%</span></div>";
                });
                $("#big_img_demo").html(item)
                uploader4.start();

            },
            UploadProgress: function(up, file) { //上传中，显示进度条
                var percent = file.percent;
                $("#" + file.id).find('.bar').css({"width": percent + "%"});
                $("#" + file.id).find(".percent").text(percent + "%");
            },
            FileUploaded: function(up, file, info) { //文件上传成功的时候触发

                var data = eval("(" + info.response + ")");//解析返回的json数据
                if (data.error != '') {
                    alert(data.error);
                    $("#big_img_demo").html("<img id='cover_demo' class='cover_img' src='<?php echo (C("url_oss")); ?>/images/cover.jpg' alt='封面'><input name='url_logo' value='' autocomplete='off' type='hidden'>");
                } else {
                    $("#" + file.id).html("<input type='hidden'name='url_logo' value='" + data.src + "'/><img class='img_common'  src='/" + data.src + "'/>");//追加图片
                }
            },
            Error: function(up, err) { //上传出错的时候触发
                alert(err.message);
            }
        }
    });
    uploader4.init();


</script>


<link rel="stylesheet" href="/Public/css/uploads.css" type="text/css" /> 

<script type="text/javascript" src="/Public/js/plugins/jquery.dragsort-0.5.2.js"></script>
<script type="text/javascript" src="/Public/js/plugins/textarea_autoheight.js"></script>
<script type="text/javascript" src="/other/js/uploads.js"></script>
<script type="text/javascript">
                                                        $(function() {
                                                            $(".modals_cat").find(".cat_sub_id").click(function() {

                                                                var length_check = $(this).parents(".modals_cat").find(".cat_sub_id:checked").length;
                                                                if (length_check > 0) {
                                                                    $(this).parents(".modals_cat").find(".cat_id").prop("checked", true)
                                                                } else {
                                                                    $(this).parents(".modals_cat").find(".cat_id").removeAttr("checked")
                                                                }
                                                            })
                                                        });





</script>
<script type="text/javascript">
                                                        $(function() {
                                                            getTagAutocomplete();
                                                        });
                                                        function getTagAutocomplete() {
                                                            $(".tag").autocomplete({
                                                                source: "/<?php echo (MODULE_NAME); ?>/Ajax/autocomplete/mtype/2",
                                                                minLength: 1
                                                            });
                                                        }
                                                        function addTags() {
                                                            var tr = "<input name='tag[]' type='text' class='common_txt tag'  value=''/>\n\
<a onclick=$(this).prev('input').remove();$(this).remove()>删除</a>";
                                                            $('#td_tags').append(tr);
                                                            getTagAutocomplete();
                                                        }
                                                        function removeContent(obj) {
                                                            obj.parent("td").parent("tr").remove();
                                                        }
                                                        function addParameter(obj) {
                                                            var tr = "<tr><td class='td_left'>参数设置 <a onclick=addParameter($(this))>+增加</a></td>\n\
    <td><label>参数：</label><input type='text' name='paras[]' class='paras'/>\n\
        <label>描述：</label><input type='text' name='descriptions[]' class='descriptions'/>\n\
        <label>默认：</label><input type='text' name='defaults[]' class='defaults'/>\n\
        <label>链接：</label><input type='text' name='urls[]' class='urls'/>\n\
        <a onclick=removeContent($(this))>删除</a></td></tr>";
                                                            obj.parent("td").parent("tr").after(tr);
                                                        }
</script>
<style>
    #td_tags .common_txt,#form .paras{width:150px}
</style>