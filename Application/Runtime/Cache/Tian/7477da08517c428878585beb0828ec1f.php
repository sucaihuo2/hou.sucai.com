<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>用户详情 - 后台管理</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="/other/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="/other/assets/css/font-awesome.min.css" />
        <!--[if IE 7]>
          <link rel="stylesheet" href="/other/assets/css/font-awesome-ie7.min.css" />
        <![endif]-->
        <link rel="stylesheet" href="/other/assets/css/jquery.gritter.css" />
        <link rel="stylesheet" href="/other/assets/css/ace.min.css" />
        <link rel="stylesheet" href="/other/assets/css/jquery-ui-1.10.3.full.min.css" />
        <!--[if lte IE 8]>
         <link rel="stylesheet" href="/other/assets/css/ace-ie.min.css" />
       <![endif]-->
        <!--[if lt IE 9]>
               <script src="/other/assets/js/html5shiv.js"></script>
               <script src="/other/assets/js/respond.min.js"></script>
               <![endif]-->
        <link rel="stylesheet" href="/other/admin/css/xuan.css" />
        <script src="/Public/js/jquery.js"></script>
        <!--        [if !IE]> 
                <script type="text/javascript">
                    window.jQuery || document.write("<script src='/other/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
                </script>
                 <![endif]
                [if IE]>
                <script type="text/javascript">
                window.jQuery || document.write("<script src='/other/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
                </script>
                <![endif]-->
        <script src="/other/assets/js/bootstrap.min.js"></script>

        <script src="/other/assets/js/jquery-ui-1.10.3.full.min.js"></script>
        <script src="/other/assets/js/jquery.ui.touch-punch.min.js"></script>

        <script src="/other/assets/js/ace-elements.min.js"></script>
        <script src="/other/assets/js/ace.min.js"></script>
        <script src="/other/js/xuan.js"></script>
        <link rel="stylesheet" href="/other/assets/css/chosen.css" />
        <script src="/other/assets/js/chosen.jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/Public/js/plugins/fancybox/fancybox.css" />
        <script type="text/javascript" src="/Public/js/plugins/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
    </head>
    <body>
        <div class="navbar navbar-default" id="navbar">

            <div class="navbar-container" id="navbar-container">
                <div class="navbar-header pull-left">
                    <a class="navbar-brand" href="/<?php echo (MODULE_NAME); ?>">
                        <small>
                            <i class="icon-leaf"></i>
                            <?php echo ($config["title"]); ?>
                        </small>
                    </a>
                </div>
                <div class="navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="/other/assets/avatars/user.jpg" alt="Jason's Photo" />
                                <span class="user-info">
                                    <small>欢迎,</small>
                                    <?php echo (session('admin_name')); ?>
                                </span>
                                <i class="icon-caret-down"></i>
                            </a>

                            <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="#">
                                        <i class="icon-user"></i>
                                        账号设置
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-cog"></i>
                                        修改密码
                                    </a>
                                </li>
                                <li class="divider"></li>

                                <li>
                                    <a href="<?php echo U('Public/logout');?>">
                                        <i class="icon-off"></i>
                                        退出
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="cl"></div>
            </div>
        </div>

<script src="/Public/js/other/validate.js"></script>
<script type="text/javascript">
    $(function() {
//        checkFengting();
        var id = '<?php echo ($detail["id"]); ?>';
        var id_url = '';
        if (id > 0) {
            id_url = "/id/" + id + "";
        }
        var mobile = $('#mobile').val();
        jQuery.validator.addMethod("isMobile", function(value, element) {
            var length = value.length;
            var mobile = /^1[3,5,8]\d{9}$/;
            return (length == 11 && mobile.exec(value)) ? true : false;
        }, "请正确填写您的手机号码");
        $("#form").validate({
            errorPlacement: function(error, element) {
                var error_td = element.parent('td');
                error_td.find('label.error').hide();
                error_td.append(error);
            },
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 15,
                    remote: {
                        url: '/<?php echo (MODULE_NAME); ?>/Ajax/check_name' + id_url,
                        type: 'post',
                        data: {
                            name: function() {
                                return $('#name').val();
                            }
                        }
                    }
                },
                pwd: {
                    minlength: 6,
                    maxlength: 20
                },
                email: {
                    email: true,
                    remote: {
                        url: '/<?php echo (MODULE_NAME); ?>/Ajax/check_email' + id_url,
                        type: 'post',
                        data: {
                            email: function() {
                                return $('#email').val();
                            }
                        }
                    }
                },
                phone: {
//                    isMobile  : true,
                    remote: {
                        url: '/<?php echo (MODULE_NAME); ?>/Ajax/check_mobile' + id_url,
                        type: 'post',
                        data: {
                            phone: function() {
                                return $('#phone').val();
                            }
                        }
                    }
                }

            },
            messages: {
                name: {
                    required: '用户名不能为空',
                    minlength: '用户名必须在3-15个字符之间',
                    maxlength: '用户名必须在3-15个字符之间',
                    remote: '该用户名已经存在'
                },
                pwd: {
                    minlength: '密码长度应在6-20个字符之间',
                    maxlength: '密码长度应在6-20个字符之间'
                },
                email: {
                    email: '这不是一个有效的电子邮箱',
                    remote: '该电子邮箱已经存在'
                },
                phone: {
                    isMobile: "请输入有效的手机号码",
                    remote: '该手机号码已经存在'
                }

            }
        });
    });
    function checkFengting() {
        var check = $("input[name=state]:checked").val();
        if (check == 0) {
            $('#fengting').hide();
        } else {
            $('#fengting').show();
        }
    }
    function get_invite_code() {
        $.post("/Ajax/get_invite_code", {},
                function(data) {
                    $("#invite_code").val(data);
                })
    }
    function zengtui_table() {
        $("#modal_zengsong").modal('toggle');
    }
    function zengtui_confirm(obj) {
    var modal = obj.parents(".modal");
    var id = $("#id").val();
     var points = $("#zengtui_points").val();
     var plus_minus = $("#zengtui_plus_minus").val();
     var type = $("#zengtui_type").val();
    $.post(obj.attr("data-url"), {
        id: id,
        points:points,
        plus_minus:plus_minus,
        type:type
        
    }, function(data) {
        location.reload();
    })
}
    
</script>
<div class="main-container" id="main-container">
    <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
<div class="sidebar" id="sidebar">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <a class="btn btn-success" href=""><i class="icon-signal"></i></a>
            <button class="btn btn-info"> <i class="icon-pencil"></i> </button>
            <button class="btn btn-warning"><i class="icon-group"></i> </button>
            <button class="btn btn-danger"> <i class="icon-cogs"></i></button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div>
    <ul class="nav nav-list">
        <?php if(is_array($menus)): foreach($menus as $key=>$row): if($row["mod"] == 'index'): ?><li <?php if($row["pid"] == $menu_pid && $row["game_code"] == $game_code): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row["url"]); ?>" >
                        <i class="<?php echo ($row["icon"]); ?>"></i>
                        <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                    </a>
                </li>
                <?php else: ?>
            <li title='<?php echo ($row["id"]); ?>'  <?php if($row["id"] == $menu_pid): ?>class="active open"<?php endif; ?>>
            <a class="dropdown-toggle">
                <i class="<?php echo ($row["icon"]); ?>"></i>
                <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <?php if(is_array($row['sub'])): foreach($row['sub'] as $key=>$row2): ?><li title='<?php echo ($row2["ord"]); ?>'  <?php if($row2['control'] == $control && $row2['mod'] == $mod && $detail["id"] == ''): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row2["url"]); ?>">
                        <i class="icon-double-angle-right"></i>
                        <?php echo ($row2["name"]); ?>
                    </a>
                    </li><?php endforeach; endif; ?>
            </ul>
            </li><?php endif; endforeach; endif; ?>
    </ul>
    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>
</div>
        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li><i class="icon-home home-icon"></i><a href="/<?php echo (MODULE_NAME); ?>">首页</a></li>
                    <li><a href="<?php echo U('User/lists');?>">用户管理</a></li>
                    <li class="active">用户<?php echo (getPageType($_GET['id'])); ?></li>
                </ul>
            </div>
            <form id="form" enctype="multipart/form-data" method="POST" action="<?php echo U('User/detail_post');?>">
                <div class="page-content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h3 class="header smaller lighter blue">用户<?php echo (getPageType($_GET['id'])); ?></h3>


                                    <table class="table table_detail">
                                        <tbody>
                                            <tr>
                                                <td class="td_left">用户名:</td>
                                                <td>
                                                    <?php echo ($detail["name"]); ?>   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_left">昵称:</td>
                                                <td>
                                                    <?php echo ($detail["nickname"]); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_left">金额:</td>
                                                <td>
                                                    积分：<?php echo ($detail["money"]); ?>   火币：<?php echo ($detail["huobi"]); ?>
                                                    <a class="red" data-toggle="modal"  onclick="zengtui_table()"data-rel="tooltip" data-original-title="赠送或退币">赠送或退币</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_left">QQ:</td>
                                                <td>
                                                    <?php echo ($detail["qq_num"]); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_left">密码:</td>
                                                <td>
                                                    <input class="common_txt" type="text" value="" name="pwd" id="pwd" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_left">安全码:</td>
                                                <td>
                                                    <input class="common_txt" type="text" value="" name="pwd_pay" id="pwd_pay" />
                                                </td>
                                            </tr>
                                           
                                            <!--                            <tr>
                                                                            <td class="td_left">手机号:</td>
                                                                            <td>
                                                                                <input class="common_txt" type="text" value="<?php echo ($detail["phone"]); ?>" name="phone"id="phone" />
                                                                            </td>
                                                                        </tr>-->
                                            <tr>
                                                <td class="td_left">email:</td>
                                                <td>
                                                    <input class="common_txt" type="text" value="<?php echo ($detail["email"]); ?>" name="email" id="email"/>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="td_left">校验码:</td>
                                                <td>
                                                    <input class="common_txt" type="text" value="" name="verify" />
                                                </td>
                                            </tr>
                                            <!--                            <tr>
                                                                            <td class="td_left">邀请码:</td>
                                                                            <td>
                                                                                <input class="common_txt" type="text" value="<?php echo ($detail["invite_code"]); ?>" name="invite_code" id="invite_code"/>
                                                                                <button class="btn btn-sm" type="button" onclick="get_invite_code()">随机获取</button>
                                                                            </td>
                                                                        </tr>-->

                                        <?php if($detail["id"] > 0): ?><tr>
                                                <td class="td_left">注册时间:</td>
                                                <td>
                                                    <?php echo (date("Y-m-d H:i",$detail["addtime"])); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_left">最后登录时间:</td>
                                                <td>
                                                    <?php echo (date("Y-m-d H:i",$detail["logintime"])); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_left">最后登录ip:</td>
                                                <td>
                                                    <?php echo ($detail["loginip"]); ?>
                                                </td>
                                            </tr>
                                            <!--                                <tr>
                                                                                <td class="td_left">是否封停:</td>
                                                                                <td>
                                                                                    <input type="radio" name="state" value="0"onclick="checkFengting()" autocomplete="off" 
                                                                            <?php if($detail["state"] == 0): ?>checked<?php endif; ?>> 否
                                                                            <input type="radio" name="state" value="1" onclick="checkFengting()" autocomplete="off" 
                                                                                   <?php if($detail["state"] == 1): ?>checked<?php endif; ?>> 是
                                                                            </td>
                                                                            </tr>
                                                                            <tr id="fengting" style="display: none">
                                                                                <td class="td_left">封停原因:</td>
                                                                                <td>
                                                                                    <textarea style="width:410px;height:50px;" name="reason"><?php echo ($log["reason"]); ?></textarea>
                                                                                </td>
                                                                            </tr>--><?php endif; ?>
                                        <tr>
                                            <td class="td_left"></td>
                                            <td>
                                                <input type='hidden' name='id' value='<?php echo ($_GET['id']); ?>' id="id" data="<?php echo ($detail["pwd_safe"]); ?>"/>

                                                <button class="btn btn-info btn-sm" type="submit">
                                                    <i class="icon-ok bigger-110 "></i>
                                                    保 存
                                                </button>

                                                <button class="btn btn-sm reset" type="button" onclick="goUrl(-1)">
                                                    <i class="icon-undo bigger-110"></i>
                                                    返 回
                                                </button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal fade" id="modal_zengsong">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">赠送或退币</h4>
                        </div>
                        <div class="modal-body">
                            <table>
                                <tr>
                                    <td class="td_left">加减类型:</td>
                                    <td>
                                        <select name="zengtui_plus_minus" id="zengtui_plus_minus" autocomplete="off">

                                            <option value="1">赠送</option>
                                            <option value="0">退币</option>
                                            </foreach>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td_left">虚拟币类型:</td>
                                    <td>
                                        <select name="zengtui_type"  id="zengtui_type" autocomplete="off">

                                            <option value="1">火币</option>
                                            <option value="0">积分</option>
                                            </foreach>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="td_left">火币:</td>
                                    <td>
                                        <input class="common_txt" style="width:100px" type="text" value="" name="zengtui_points" id="zengtui_points"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-danger btn-xs ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" role="button" aria-disabled="false" onclick="zengtui_confirm($(this))" data-url="<?php echo U('Ajax/zengtui_confirm');?>">
                                <span class="ui-button-text">
                                    <i class="icon-trash bigger-110"></i>
                                    确 定
                                </span>
                            </button>
                            <button class="btn btn-xs ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" role="button" aria-disabled="false" data-dismiss="modal">
                                <span class="ui-button-text">
                                    <i class="icon-remove bigger-110"></i>
                                    取 消
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="ace-settings-container" id="ace-settings-container">
            <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                <i class="icon-cog bigger-150"></i>
            </div>

            <div class="ace-settings-box" id="ace-settings-box">
                <div>
                    <div class="pull-left">
                        <select id="skin-colorpicker" class="hide">
                            <option data-skin="default" value="#438EB9">#438EB9</option>
                            <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                            <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                            <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                        </select>
                    </div>
                    <span>&nbsp; Choose Skin</span>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
                    <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
                    <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
                    <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
                    <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
                    <label class="lbl" for="ace-settings-add-container">
                        Inside
                        <b>.container</b>
                    </label>
                </div>
            </div>
        </div> /#ace-settings-container -->
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {
        $('table th input:checkbox').on('click', function() {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function() {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });
        });
        $(".help-button").popover();
        $('[data-rel=tooltip]').tooltip();
        $(".chosen-select").chosen();
        $(".fancybox_img").fancybox({
            'transitionIn': 'elastic', //窗口显示的方式
            'transitionOut': 'elastic',
            'titlePosition': 'inside'
        });
    })
</script>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
<div class="footer" id="footer" data-url="/"> </div>
<input type="hidden" id="SITE_URL" value="/<?php echo (MODULE_NAME); ?>"/>
<div id="windown_box2"></div>
</body>
</html>