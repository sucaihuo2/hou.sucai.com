<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title><?php echo ($menu["name"]); ?> - 后台管理</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="/other/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="/other/assets/css/font-awesome.min.css" />
        <!--[if IE 7]>
          <link rel="stylesheet" href="/other/assets/css/font-awesome-ie7.min.css" />
        <![endif]-->
        <link rel="stylesheet" href="/other/assets/css/jquery.gritter.css" />
        <link rel="stylesheet" href="/other/assets/css/ace.min.css" />
        <link rel="stylesheet" href="/other/assets/css/jquery-ui-1.10.3.full.min.css" />
        <!--[if lte IE 8]>
         <link rel="stylesheet" href="/other/assets/css/ace-ie.min.css" />
       <![endif]-->
        <!--[if lt IE 9]>
               <script src="/other/assets/js/html5shiv.js"></script>
               <script src="/other/assets/js/respond.min.js"></script>
               <![endif]-->
        <link rel="stylesheet" href="/other/admin/css/xuan.css" />
        <script src="/Public/js/jquery.js"></script>
        <!--        [if !IE]> 
                <script type="text/javascript">
                    window.jQuery || document.write("<script src='/other/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
                </script>
                 <![endif]
                [if IE]>
                <script type="text/javascript">
                window.jQuery || document.write("<script src='/other/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
                </script>
                <![endif]-->
        <script src="/other/assets/js/bootstrap.min.js"></script>

        <script src="/other/assets/js/jquery-ui-1.10.3.full.min.js"></script>
        <script src="/other/assets/js/jquery.ui.touch-punch.min.js"></script>

        <script src="/other/assets/js/ace-elements.min.js"></script>
        <script src="/other/assets/js/ace.min.js"></script>
        <script src="/other/js/xuan.js"></script>
        <link rel="stylesheet" href="/other/assets/css/chosen.css" />
        <script src="/other/assets/js/chosen.jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/Public/js/plugins/fancybox/fancybox.css" />
        <script type="text/javascript" src="/Public/js/plugins/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
    </head>
    <body>
        <div class="navbar navbar-default" id="navbar">

            <div class="navbar-container" id="navbar-container">
                <div class="navbar-header pull-left">
                    <a class="navbar-brand" href="/<?php echo (MODULE_NAME); ?>">
                        <small>
                            <i class="icon-leaf"></i>
                            <?php echo ($config["title"]); ?>
                        </small>
                    </a>
                </div>
                <div class="navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="/other/assets/avatars/user.jpg" alt="Jason's Photo" />
                                <span class="user-info">
                                    <small>欢迎,</small>
                                    <?php echo (session('admin_name')); ?>
                                </span>
                                <i class="icon-caret-down"></i>
                            </a>

                            <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="#">
                                        <i class="icon-user"></i>
                                        账号设置
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-cog"></i>
                                        修改密码
                                    </a>
                                </li>
                                <li class="divider"></li>

                                <li>
                                    <a href="<?php echo U('Public/logout');?>">
                                        <i class="icon-off"></i>
                                        退出
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="cl"></div>
            </div>
        </div>

<script src="/Public/js/other/validate.js"></script>
<script type="text/javascript">
    $(function() {
        var id = '<?php echo ($detail["id"]); ?>';
        var id_url = '';
        if (id > 0) {
            id_url = "/id/" + id + "";
        }
        var mobile = $('#mobile').val();
        jQuery.validator.addMethod("isMobile", function(value, element) {
            var length = value.length;
            var mobile = /^1[3,5,8]\d{9}$/;
            return (length == 11 && mobile.exec(value)) ? true : false;
        }, "请正确填写您的手机号码");
        $("#form").validate({
            errorPlacement: function(error, element) {
                var error_td = element.parent('td');
                error_td.find('label.error').hide();
                error_td.append(error);
            },
            rules: {
                pwd: {
                    minlength: 6,
                    maxlength: 20
                },
                phone: {
                    isMobile: true,
                }

            },
            messages: {
                pwd: {
                    minlength: '密码长度应在6-20个字符之间',
                    maxlength: '密码长度应在6-20个字符之间'
                },
                phone: {
                    isMobile: "请输入有效的手机号码",
                }

            }
        });
    });
</script>
<div class="main-container" id="main-container">
    <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
<div class="sidebar" id="sidebar">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <a class="btn btn-success" href=""><i class="icon-signal"></i></a>
            <button class="btn btn-info"> <i class="icon-pencil"></i> </button>
            <button class="btn btn-warning"><i class="icon-group"></i> </button>
            <button class="btn btn-danger"> <i class="icon-cogs"></i></button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div>
    <ul class="nav nav-list">
        <?php if(is_array($menus)): foreach($menus as $key=>$row): if($row["mod"] == 'index'): ?><li <?php if($row["pid"] == $menu_pid && $row["game_code"] == $game_code): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row["url"]); ?>" >
                        <i class="<?php echo ($row["icon"]); ?>"></i>
                        <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                    </a>
                </li>
                <?php else: ?>
            <li title='<?php echo ($row["id"]); ?>'  <?php if($row["id"] == $menu_pid): ?>class="active open"<?php endif; ?>>
            <a class="dropdown-toggle">
                <i class="<?php echo ($row["icon"]); ?>"></i>
                <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <?php if(is_array($row['sub'])): foreach($row['sub'] as $key=>$row2): ?><li title='<?php echo ($row2["ord"]); ?>'  <?php if($row2['control'] == $control && $row2['mod'] == $mod && $detail["id"] == ''): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row2["url"]); ?>">
                        <i class="icon-double-angle-right"></i>
                        <?php echo ($row2["name"]); ?>
                    </a>
                    </li><?php endforeach; endif; ?>
            </ul>
            </li><?php endif; endforeach; endif; ?>
    </ul>
    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>
</div>
        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li><i class="icon-home home-icon"></i><a href="/<?php echo (MODULE_NAME); ?>">首页</a></li>
                    <li><a href="<?php echo U('User/admin');?>">管理员管理</a></li>
                    <li class="active"><?php echo ($menu["name"]); ?></li>
                </ul>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="header smaller lighter blue"><?php echo ($menu["name"]); ?></h3>

                                <form action="<?php echo U('User/admin_detail_post');?>" method="POST" id="form">
                                    <table class="table table_detail">
                                        <tbody>
                                            <tr>
                                                <td class="td_left">用户名:</td>
                                                <td>
                                                    <input class="common_txt" type="text" value="<?php echo ($detail["name"]); ?>" name="name"id="phone" />
                                                </td>
                                            </tr>
<!--                                            <tr>
                                                <td class="td_left">密码:</td>
                                                <td>
                                                    <input class="common_txt" type="password" value="" name="pwd" id="pwd" />
                                                </td>
                                            </tr>-->
<!--                                            <tr>
                                                <td class="td_left">是否管理员:</td>
                                                <td>
                                                    <label onclick="$('#tr_powers').hide()">
                                                        <input class="ace" type="radio" name="is_admin" value='0' <?php echo (getEqual($detail["is_admin"],0,'checked')); ?> autocomplete="off"/>
                                                        <span class="lbl"> 否</span>
                                                    </label>
                                                    <label onclick="$('#tr_powers').show()">
                                                        <input class="ace" type="radio" name="is_admin" value='1' <?php echo (getEqual($detail["is_admin"],1,'checked')); ?> autocomplete="off"/>
                                                        <span class="lbl"> 是</span>
                                                    </label>
                                                </td>
                                            </tr>-->
                                            <tr id="tr_powers">
                                                <td class="td_left">权限：</td>
                                                <td>
                                                    <table class="table table_detail">
                                                        <?php if(is_array($powers)): foreach($powers as $key=>$row): ?><tr>
                                                                <td class="td_left"><?php echo ($row["name"]); ?></td>
                                                                <td>
                                                            <?php if(is_array($row['sub'])): foreach($row['sub'] as $key=>$row2): ?><label style="width:110px;display: inline-block">
                                                                    <input class="ace" type="checkbox"  name="powers[]" value="<?php echo ($row2["id"]); ?>" <?php echo (getChecked($detail["powers"],$row2[id])); ?>/>
                                                                    <span class="lbl"> <?php echo ($row2["name"]); ?></span>
                                                                </label><?php endforeach; endif; ?>
                                                            </td>
                                                            </tr><?php endforeach; endif; ?>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td_left"></td>
                                                <td>
                                                    <input type='hidden' name='id' value='<?php echo ($_GET['id']); ?>'/>
                                                    <button class="btn btn-info btn-sm" type="submit">
                                                        <i class="icon-ok bigger-110 "></i>
                                                        保 存
                                                    </button>
                                                    <button class="btn btn-sm reset" type="button" onclick="goUrl(-1)">
                                                        <i class="icon-undo bigger-110"></i>
                                                        返 回
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {
        $('table th input:checkbox').on('click', function() {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function() {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });
        });
        $(".help-button").popover();
        $('[data-rel=tooltip]').tooltip();
        $(".chosen-select").chosen();
        $(".fancybox_img").fancybox({
            'transitionIn': 'elastic', //窗口显示的方式
            'transitionOut': 'elastic',
            'titlePosition': 'inside'
        });
    })
</script>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
<div class="footer" id="footer" data-url="/"> </div>
<input type="hidden" id="SITE_URL" value="/<?php echo (MODULE_NAME); ?>"/>
<div id="windown_box2"></div>
</body>
</html>