<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>仪表盘 - 后台管理</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="/other/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="/other/assets/css/font-awesome.min.css" />
        <!--[if IE 7]>
          <link rel="stylesheet" href="/other/assets/css/font-awesome-ie7.min.css" />
        <![endif]-->
        <link rel="stylesheet" href="/other/assets/css/jquery.gritter.css" />
        <link rel="stylesheet" href="/other/assets/css/ace.min.css" />
        <link rel="stylesheet" href="/other/assets/css/jquery-ui-1.10.3.full.min.css" />
        <!--[if lte IE 8]>
         <link rel="stylesheet" href="/other/assets/css/ace-ie.min.css" />
       <![endif]-->
        <!--[if lt IE 9]>
               <script src="/other/assets/js/html5shiv.js"></script>
               <script src="/other/assets/js/respond.min.js"></script>
               <![endif]-->
        <link rel="stylesheet" href="/other/admin/css/xuan.css" />
        <script src="/Public/js/jquery.js"></script>
        <!--        [if !IE]> 
                <script type="text/javascript">
                    window.jQuery || document.write("<script src='/other/assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
                </script>
                 <![endif]
                [if IE]>
                <script type="text/javascript">
                window.jQuery || document.write("<script src='/other/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
                </script>
                <![endif]-->
        <script src="/other/assets/js/bootstrap.min.js"></script>

        <script src="/other/assets/js/jquery-ui-1.10.3.full.min.js"></script>
        <script src="/other/assets/js/jquery.ui.touch-punch.min.js"></script>

        <script src="/other/assets/js/ace-elements.min.js"></script>
        <script src="/other/assets/js/ace.min.js"></script>
        <script src="/other/js/xuan.js"></script>
        <link rel="stylesheet" href="/other/assets/css/chosen.css" />
        <script src="/other/assets/js/chosen.jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/Public/js/plugins/fancybox/fancybox.css" />
        <script type="text/javascript" src="/Public/js/plugins/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
    </head>
    <body>
        <div class="navbar navbar-default" id="navbar">

            <div class="navbar-container" id="navbar-container">
                <div class="navbar-header pull-left">
                    <a class="navbar-brand" href="/<?php echo (MODULE_NAME); ?>">
                        <small>
                            <i class="icon-leaf"></i>
                            <?php echo ($config["title"]); ?>
                        </small>
                    </a>
                </div>
                <div class="navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="/other/assets/avatars/user.jpg" alt="Jason's Photo" />
                                <span class="user-info">
                                    <small>欢迎,</small>
                                    <?php echo (session('admin_name')); ?>
                                </span>
                                <i class="icon-caret-down"></i>
                            </a>

                            <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="#">
                                        <i class="icon-user"></i>
                                        账号设置
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-cog"></i>
                                        修改密码
                                    </a>
                                </li>
                                <li class="divider"></li>

                                <li>
                                    <a href="<?php echo U('Public/logout');?>">
                                        <i class="icon-off"></i>
                                        退出
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="cl"></div>
            </div>
        </div>

<div class="main-container" id="main-container">
    <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
<div class="sidebar" id="sidebar">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <a class="btn btn-success" href=""><i class="icon-signal"></i></a>
            <button class="btn btn-info"> <i class="icon-pencil"></i> </button>
            <button class="btn btn-warning"><i class="icon-group"></i> </button>
            <button class="btn btn-danger"> <i class="icon-cogs"></i></button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div>
    <ul class="nav nav-list">
        <?php if(is_array($menus)): foreach($menus as $key=>$row): if($row["mod"] == 'index'): ?><li <?php if($row["pid"] == $menu_pid && $row["game_code"] == $game_code): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row["url"]); ?>" >
                        <i class="<?php echo ($row["icon"]); ?>"></i>
                        <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                    </a>
                </li>
                <?php else: ?>
            <li title='<?php echo ($row["id"]); ?>'  <?php if($row["id"] == $menu_pid): ?>class="active open"<?php endif; ?>>
            <a class="dropdown-toggle">
                <i class="<?php echo ($row["icon"]); ?>"></i>
                <span class="menu-text"> <?php echo ($row["name"]); ?></span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <?php if(is_array($row['sub'])): foreach($row['sub'] as $key=>$row2): ?><li title='<?php echo ($row2["ord"]); ?>'  <?php if($row2['control'] == $control && $row2['mod'] == $mod && $detail["id"] == ''): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo ($row2["url"]); ?>">
                        <i class="icon-double-angle-right"></i>
                        <?php echo ($row2["name"]); ?>
                    </a>
                    </li><?php endforeach; endif; ?>
            </ul>
            </li><?php endif; endforeach; endif; ?>
    </ul>
    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>
</div>
        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li><i class="icon-home home-icon"></i><a href="/<?php echo (MODULE_NAME); ?>">首页</a></li>
                    <li class="active">仪表盘</li>
                </ul>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="header smaller lighter blue">仪表盘</h3>

                                <div class="alert alert-block alert-success">
                                    <button class="close" data-dismiss="alert" type="button">
                                        <i class="icon-remove"></i>
                                    </button>
                                    <i class="icon-ok green"></i>
                                    Welcome to
                                    <strong class="green">
                                        Ace
                                        <small>(v1.2)</small>
                                    </strong>
                                    , the lightweight, feature-rich and easy to use admin template.
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 widget-container-span">
                                        <div class="widget-box">
                                            <div class="widget-header">
                                                <h5>最新用户</h5>

                                                <div class="widget-toolbar">
                                                    <a href="#" data-action="settings">
                                                        <i class="icon-cog"></i>
                                                    </a>

                                                    <a href="#" data-action="reload">
                                                        <i class="icon-refresh"></i>
                                                    </a>

                                                    <a href="#" data-action="collapse">
                                                        <i class="icon-chevron-up"></i>
                                                    </a>

                                                    <a href="#" data-action="close">
                                                        <i class="icon-remove"></i>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-main no-padding">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead class="thin-border-bottom">
                                                            <tr>
                                                                <th>
                                                                    <i class="icon-user"></i>
                                                                    User
                                                                </th>

                                                                <th>
                                                                    <i>@</i>
                                                                    Email
                                                                </th>
                                                                <th class="hidden-480">添加时间</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                        <?php if(is_array($users)): foreach($users as $key=>$row): ?><tr>
                                                                <td class=""><a href="<?php echo U('User/detail',array('id'=>$row['id']));?>"><?php echo ($row["name"]); ?></a></td>

                                                                <td>
                                                                    <a href="#"><?php echo ($row["email"]); ?></a>
                                                                </td>

                                                                <td class="hidden-480">
                                                                    <?php echo (date("Y-m-d H:i",$row["logintime"])); ?>
                                                                </td>
                                                            </tr><?php endforeach; endif; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-xs-12 col-sm-4 widget-container-span">
                                        <div class="widget-box">
                                            <div class="widget-header">
                                                <h5>最新评论</h5>

                                                <div class="widget-toolbar">
                                                    <a href="#" data-action="settings">
                                                        <i class="icon-cog"></i>
                                                    </a>

                                                    <a href="#" data-action="reload">
                                                        <i class="icon-refresh"></i>
                                                    </a>

                                                    <a href="#" data-action="collapse">
                                                        <i class="icon-chevron-up"></i>
                                                    </a>

                                                    <a href="#" data-action="close">
                                                        <i class="icon-remove"></i>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-main no-padding">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead class="thin-border-bottom">
                                                            <tr>
                                                                <th>
                                                                    <i class="icon-user"></i>
                                                                    用户
                                                                </th>
                                                                <th>标题</th>
                                                                <th>内容</th>
                                                                <th class="hidden-480">添加时间</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                        <!--<?php if(is_array($comments)): foreach($comments as $key=>$row): ?>-->
                                                        <!--    <tr>-->
                                                        <!--        <td><a href="<?php echo U('User/detail',array('id'=>$row['uid']));?>"><?php echo (getUserName($row["uid"])); ?></a></td>-->

                                                        <!--        <td>-->
                                                        <!--            <a href="http://www.sucaihuo.com/<?php echo (getMtype($row["mtype"])); ?>/<?php echo ($row["tid"]); ?>" target="_blank"><?php echo ($row["mtype_name"]); ?></a>-->
                                                        <!--        </td>-->
                                                        <!--        <td><?php echo ($row["content"]); ?></td>-->

                                                        <!--        <td class="hidden-480">-->
                                                        <!--            <?php echo (date("Y-m-d H:i",$row["addtime"])); ?>-->
                                                        <!--        </td>-->
                                                        <!--    </tr>-->
                                                        <!--<?php endforeach; endif; ?>-->
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-xs-12 col-sm-4 widget-container-span">
                                        <div class="widget-box">
                                            <div class="widget-header">
                                                <h5>搜索</h5>

                                                <div class="widget-toolbar">
                                                    <a href="#" data-action="settings">
                                                        <i class="icon-cog"></i>
                                                    </a>

                                                    <a href="#" data-action="reload">
                                                        <i class="icon-refresh"></i>
                                                    </a>

                                                    <a href="#" data-action="collapse">
                                                        <i class="icon-chevron-up"></i>
                                                    </a>

                                                    <a href="#" data-action="close">
                                                        <i class="icon-remove"></i>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead class="thin-border-bottom">
                                                            <tr>
                                                                <th>
                                                                    <i class="icon-user"></i>
                                                                    名称
                                                                </th>
                                                                <th>
                                                                    名称2
                                                                </th>
                                                                <th>
                                                                    次数
                                                                </th>
                                                               
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                        <?php if(is_array($searches)): foreach($searches as $key=>$row): ?><tr>
                                                                <td><?php echo ($row["name"]); ?></td>
                                                                <td><?php echo ($row["keyword2"]); ?></td>
                                                                <td><?php echo ($row["times"]); ?></td>
                                                           </tr><?php endforeach; endif; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 widget-container-span">
                                        <div class="widget-box">
                                            <div class="widget-header">
                                                <h5>积分</h5>

                                                <div class="widget-toolbar">
                                                    <a href="#" data-action="settings">
                                                        <i class="icon-cog"></i>
                                                    </a>

                                                    <a href="#" data-action="reload">
                                                        <i class="icon-refresh"></i>
                                                    </a>

                                                    <a href="#" data-action="collapse">
                                                        <i class="icon-chevron-up"></i>
                                                    </a>

                                                    <a href="#" data-action="close">
                                                        <i class="icon-remove"></i>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead class="thin-border-bottom">
                                                            <tr>
                                                                <th>
                                                                    <i class="icon-user"></i>
                                                                    类型
                                                                </th>

                                                                <th>
                                                                    积分
                                                                </th>
                                                                <th>
                                                                    <i>@</i>
                                                                    用户
                                                                </th>
                                                                <th class="hidden-480">添加时间</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                        <?php if(is_array($points)): foreach($points as $key=>$row): ?><tr>
                                                                <td class=""><?php echo ($row["title"]); echo ($row["mtype"]); ?></td>

                                                                <td>
                                                                    <a href="<?php echo ($row["website"]); ?>" target="_blank"><?php echo ($row["money"]); ?></a>
                                                                </td>
                                                                <td>
                                                                    <a href="#"><?php echo (getSingleField($row["uid"],'user','name')); ?></a>
                                                                </td>

                                                                <td class="hidden-480">
                                                                    <?php echo (date("Y-m-d H:i",$row["addtime"])); ?>
                                                                </td>
                                                            </tr><?php endforeach; endif; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 widget-container-span">
                                        <div class="widget-box">
                                            <div class="widget-header">
                                                <h5>系统日志</h5>

                                                <div class="widget-toolbar">
                                                    <a href="#" data-action="settings">
                                                        <i class="icon-cog"></i>
                                                    </a>

                                                    <a href="#" data-action="reload">
                                                        <i class="icon-refresh"></i>
                                                    </a>

                                                    <a href="#" data-action="collapse">
                                                        <i class="icon-chevron-up"></i>
                                                    </a>

                                                    <a href="#" data-action="close">
                                                        <i class="icon-remove"></i>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead class="thin-border-bottom">
                                                            <tr>
                                                                <th>
                                                                    <i class="icon-user"></i>
                                                                    内容
                                                                </th>

                                                                <th>
                                                                    类型
                                                                </th>
                                                                <th>
                                                                    时间
                                                                </th>
                                                               
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                        <?php if(is_array($logs)): foreach($logs as $key=>$row): ?><tr>
                                                                <td class=""><?php echo ($row["title"]); ?></td>
                                                                <td><?php echo ($row["mtype"]); ?></td>
                                                                <td><?php echo (date("Y-m-d H:i",$row["addtime"])); ?></td>
                                                           </tr><?php endforeach; endif; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                       <div class="col-xs-12 col-sm-4 widget-container-span">
                                        <div class="widget-box">
                                            <div class="widget-header">
                                                <h5>发货日志</h5>

                                                <div class="widget-toolbar">
                                                    <a href="#" data-action="settings">
                                                        <i class="icon-cog"></i>
                                                    </a>

                                                    <a href="#" data-action="reload">
                                                        <i class="icon-refresh"></i>
                                                    </a>

                                                    <a href="#" data-action="collapse">
                                                        <i class="icon-chevron-up"></i>
                                                    </a>

                                                    <a href="#" data-action="close">
                                                        <i class="icon-remove"></i>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead class="thin-border-bottom">
                                                            <tr>
                                                                <th>
                                                                    <i class="icon-user"></i>
                                                                    内容
                                                                </th>

                                                                <th>
                                                                    类型
                                                                </th>
                                                                <th>
                                                                    时间
                                                                </th>
                                                               
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                        <?php if(is_array($logs_send)): foreach($logs_send as $key=>$row): ?><tr>
                                                                <td class=""><?php echo ($row["title"]); ?></td>
                                                                <td><?php echo ($row["mtype"]); ?></td>
                                                                <td><?php echo (date("Y-m-d H:i",$row["addtime"])); ?></td>
                                                           </tr><?php endforeach; endif; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="ace-settings-container" id="ace-settings-container">
            <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                <i class="icon-cog bigger-150"></i>
            </div>

            <div class="ace-settings-box" id="ace-settings-box">
                <div>
                    <div class="pull-left">
                        <select id="skin-colorpicker" class="hide">
                            <option data-skin="default" value="#438EB9">#438EB9</option>
                            <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                            <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                            <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                        </select>
                    </div>
                    <span>&nbsp; Choose Skin</span>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
                    <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
                    <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
                    <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
                    <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                </div>

                <div>
                    <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
                    <label class="lbl" for="ace-settings-add-container">
                        Inside
                        <b>.container</b>
                    </label>
                </div>
            </div>
        </div> /#ace-settings-container -->
    </div>
</div>

<script type="text/javascript">
    jQuery(function($) {
        $('table th input:checkbox').on('click', function() {
            var that = this;
            $(this).closest('table').find('tr > td:first-child input:checkbox')
                    .each(function() {
                        this.checked = that.checked;
                        $(this).closest('tr').toggleClass('selected');
                    });
        });
        $(".help-button").popover();
        $('[data-rel=tooltip]').tooltip();
        $(".chosen-select").chosen();
        $(".fancybox_img").fancybox({
            'transitionIn': 'elastic', //窗口显示的方式
            'transitionOut': 'elastic',
            'titlePosition': 'inside'
        });
    })
</script>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
<div class="footer" id="footer" data-url="/"> </div>
<input type="hidden" id="SITE_URL" value="/<?php echo (MODULE_NAME); ?>"/>
<div id="windown_box2"></div>
</body>
</html>