<?php

function getZipGbk($arr) {
//    $arr = explode(",", $file);
    foreach ($arr as $v) {
        $names[] = iconv('UTF-8', 'GB2312', $v);
    }
    return implode(",", $names);
}

function replacePathExtract($zip_url, $file_path, $file_path_remove) { //解压压缩文件并且添加或移除解压后的文件路径 
    set_time_limit(0);
    import("Common.Org.PclZip");
    $zip = new PclZip($zip_url);
    $zip->extract(PCLZIP_OPT_PATH, $file_path, PCLZIP_OPT_REMOVE_PATH, $file_path_remove); //PCLZIP_OPT_PATH：添加路径，PCLZIP_OPT_REMOVE_PATH：移除原来的路径 
}

function getZip($lastid, $uniqid, $temp, $zip, $demo, $files_save) {
    deldir($demo . $lastid); //删除指定demo目录下所有文件
    deldir($zip . $lastid); //删除指定zip目录下所有压缩文件 
    $files_save = iconv("UTF-8", "GBK", $files_save);
    import("Common.Org.PclZip");
    $zip_temp = $temp . $uniqid;
    $file_zip = $zip . $lastid;
    if (!file_exists($file_zip)) {
        mkdir($file_zip);
    }
    $zip_url = $file_zip . "/" . $files_save;
//    echo $zip_temp."<hr>".$zip_url;
    rename($zip_temp, $zip_url);
    $zip = new PclZip($zip_url);
    if (($list = $zip->listContent()) == 0) {
        die("Error : " . $zip->errorInfo(true));
    }
    $names = explode(".", $files_save);
    $extract_file_first = $list[0]['filename'];
    $html_num = 0;
    foreach ($list as $v) {
        $type = strtolower(get_extension($v['filename']));
        if ($type == 'html') {
            $html_num ++;
        }
    }
    $zip->extract(PCLZIP_OPT_PATH, $demo . $lastid . "/" . $files_save);
    if (strpos($extract_file_first, "/")) {//若解压出来的第一个文件是文件夹
        $old = $demo . $lastid . "/" . $files_save . "/" . $extract_file_first;
        $new = $demo . $lastid . "/" . $names[0];
        rename($old, $new);
        deleteFiles($demo . $lastid . "/" . $files_save . "/");
    } else {//否则直接替换文件名
        $old = $demo . $lastid . "/" . $files_save;
        $new = $demo . $lastid . "/" . $names[0];
        rename($old, $new);
    }
    return $html_num;
}

function unrar($fileName, $extractTo) { //解压rar文件
    $rar_file = rar_open($fileName) or die('could not open rar');
    $list = rar_list($rar_file) or die('could not get list');

    foreach ($list as $file) {
        $pattern = '/\".*\"/';
        preg_match($pattern, $file, $matches, PREG_OFFSET_CAPTURE);
        $pathStr = $matches[0][0];
        $pathStr = str_replace("\"", '', $pathStr);
        //            print_r($pathStr);  
        $entry = rar_entry_get($rar_file, $pathStr) or die('</br>entry not found');
        $entry->extract($extractTo); // extract to the current dir  
    }
    rar_close($rar_file);
}

function showZipFilesNum($zip_url) { //列出压缩文件列表 
    set_time_limit(0);
    import("Common.Org.PclZip");
    $zip = new PclZip($zip_url);
    $num = 1;
    if (($list = $zip->listContent()) == 0) {
        $num = 0;
    }
    return $num;
}

function createReplaceZip($zip_url, $files, $name, $name_replace) { //添加文件到压缩文件,并且替换相关路径
//    $files = getZipGbk($files);
    set_time_limit(0);
    import("Common.Org.PclZip");
    $zip = new PclZip($zip_url);
    $rs = $zip->create($files, PCLZIP_OPT_REMOVE_PATH, $name, PCLZIP_OPT_ADD_PATH, $name_replace);
    if ($rs == 0) {
        die("Error : " . $zip->errorInfo(true));
    }
}

function createAddZip($zip_url, $files) { //添加文件到压缩文件 
    set_time_limit(0);
    import("Common.Org.PclZip");
    $zip = new PclZip($zip_url);
    $rs = $zip->create($files);
    if ($rs == 0) {
        die("Error : " . $zip->errorInfo(true));
    }
}

function removePathZip($zip_url, $files, $path_remove) { //添加文件到压缩文件，移除指定路径
//    $files = getZipGbk($files);
    set_time_limit(0);
    import("Common.Org.PclZip");
    $zip = new PclZip($zip_url);
    $zip->add($files, PCLZIP_OPT_REMOVE_PATH, $path_remove); //移除路径file
}

function remove_zip_path($zip_url) { //列出压缩文件列表 
    import("Common.Org.PclZip");
    $zip = new PclZip($zip_url);

    if (($list = $zip->listContent()) == 0) {
        die("Error : " . $zip->errorInfo(true));
    }
    foreach ($list as $v) {
        if ($v['index'] == 0) {
            $path_remove = $v['filename'];
        }
        if ($v['index'] == 1) {
            $file_index_1[] = $v['filename'];
            $path_remove_2 = $v['filename'];
        }
    }
    if (count($file_index_1) == 1) {
        $path_remove = $path_remove_2;
    }

    return $path_remove;
//    for ($i = 0; $i < sizeof($list); $i++) { 
//        for (reset($list[$i]); $key = key($list[$i]); next($list[$i])) { 
//     
////            echo "File " . $i . " / [" . $key . "] = " . $list[$i][$key] . "<br />"; 
//        } 
////        echo "<br />"; 
//    } 
//    print_r($list);
}

function zip_lists($zip_url) { //列出压缩文件列表 
     import("Common.Org.PclZip");
    $zip = new PclZip($zip_url);

    if (($list = $zip->listContent()) == 0) {
        die("Error : " . $zip->errorInfo(true));
    }

    for ($i = 0; $i < sizeof($list); $i++) {
        for (reset($list[$i]); $key = key($list[$i]); next($list[$i])) {
            echo "File " . $i . " / [" . $key . "] = " . $list[$i][$key] . "<br />";
        }
        echo "<br />";
    }
}

function rar_lists($fileName, $extractTo) { //解压rar文件
    $rar_file = rar_open($fileName) or die('could not open rar');
    $list = rar_list($rar_file) or die('could not get list');
    foreach ($list as $file) {
        $pattern = '/\".*\"/';
        preg_match($pattern, $file, $matches, PREG_OFFSET_CAPTURE);
        $pathStr = $matches[0][0];
        $pathStr = str_replace("\"", '', $pathStr);
        $files[] = $pathStr;
    }
    return $files;
}
