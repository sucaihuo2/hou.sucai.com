<?php

function getPicThumb($lastid, $logo, $temp, $template, $width_thumb, $height_thumb, $logo_type = 'png') {
    $old = $temp . $logo;
    if ($logo_type == 1) {
        $logo_type = strtolower(substr(strrchr($logo, '.'), 1)); //文件类型
    }
    $logo_type = "png";
    $logo_name = $lastid . "." . $logo_type; //图片名称
    $new = $template . $logo_name; //图片全路径
    rename($old, $new); //临时文件放到正式文件夹
//    if ($width_thumb > 0) {
//       
//        if (in_array($logo_type, array("jpg", 'jpeg', 'png'))) {
//            $image = new \Think\Image();
//            $image->open($new);
//            $width = $image->width(); // 返回图片的宽度
//            $height = $image->height(); // 返回图片的高度
////            $height_calculate = $height / $width * $width_thumb;
////            //  echo $width_thumb."<hr>".$height_calculate;
////            $image->thumb($width_thumb, $height_calculate, \Think\Image::IMAGE_THUMB_FIXED)->save($logo_thumb);
////            $image->open($logo_thumb);
////            $height_thumb = $height_thumb > 0 ? $height_thumb : $height_calculate;
////            $image->thumb($width_thumb, $height_thumb, \Think\Image::IMAGE_THUMB_NORTHWEST)->save($logo_thumb);
//            //850
////            if ($width > 850) {
////                $logo_thumb = $template . "" . $lastid . "_280." . $logo_type; //缩略图
////                $image->open($new);
////                $image->crop(280, 480)->save($logo_thumb);
////                $width2 = 850;
////                $height2 = $height;
////                $logo_thumb2 = $template . "" . $lastid . "_850." . $logo_type;
////                $image->open($new);
////                $image->thumb($width2, $height2, \Think\Image::IMAGE_THUMB_NORTHWEST)->save($logo_thumb2);
////                $height_crop = $height > 1200 ? 1200 : $height * 0.7;
////                $image->open($logo_thumb2);
////                $image->crop($width2, $height_crop)->save($logo_thumb2);
////            }
//        } else {
//            copy($new, $logo_thumb);
//        }
//    }
    return array("type" => $logo_type);
}

function getCharset($content) {
    $charset = preg_match("/<meta.+?charset=[^\w]?([-\w]+)/i", $content, $temp) ? strtolower($temp[1]) : "";
    return $charset;
}

function getFileName($name) {
    $arr = end(explode("/", $name));
    return $arr[0];
}

function clearTempFile() {
    $dirs = array('Application/Runtime/');
    foreach ($dirs as $v) {
        rmdirr($v);
    }
}

//处理方法
function rmdirr($dirname) {
    if (!file_exists($dirname)) {
        return false;
    }
    if (is_file($dirname) || is_link($dirname)) {
        return unlink($dirname);
    }
    $dir = dir($dirname);
    if ($dir) {
        while (false !== $entry = $dir->read()) {
            if ($entry == '.' || $entry == '..') {
                continue;
            }
            //递归
            rmdirr($dirname . DIRECTORY_SEPARATOR . $entry);
        }
    }
}

//公共函数
//获取文件修改时间
function getfiletime($file, $DataDir) {
    $a = filemtime($DataDir . $file);
    $time = date("Y-m-d H:i:s", $a);
    return $time;
}

//获取文件的大小
function getfilesize($file, $DataDir) {
    $perms = stat($DataDir . $file);
    $size = $perms['size'];
    // 单位自动转换函数
    $kb = 1024;         // Kilobyte
    $mb = 1024 * $kb;   // Megabyte
    $gb = 1024 * $mb;   // Gigabyte
    $tb = 1024 * $gb;   // Terabyte

    if ($size < $kb) {
        return $size . " B";
    } else if ($size < $mb) {
        return round($size / $kb, 2) . " KB";
    } else if ($size < $gb) {
        return round($size / $mb, 2) . " MB";
    } else if ($size < $tb) {
        return round($size / $gb, 2) . " GB";
    } else {
        return round($size / $tb, 2) . " TB";
    }
}

function getPageType($id) {
    if ($id > 0) {
        $word = "编辑";
    } else {
        $word = "添加";
    }
    return $word;
}

function getKeyUids($keyword) {
    $lists = M("user")->field("id")->where("name like '%" . $keyword . "%'")->select();
    if ($lists) {
        foreach ($lists as $v) {
            $ids .= $v['id'] . ",";
        }
        $ids = substr($ids, 0, -1);
    }
    return $ids;
}

function isClearTempFile($table) {
    $tables_clear = array("dictionary", "ads", "article_cat", "friends"
        , "news");
    if (in_array($table, $tables_clear)) {
        clearTempFile();
    }
}

function deleteFiles($files) {
    $filesArr = explode(",", $files);
    foreach ($filesArr as $v) {
        if (file_exists($v)) {
            $type = strtolower(substr(strrchr($v, '.'), 1)); //文件类型
            if (in_array($type, array("jpg", "gif", "png", "jpeg"))) {
                unlink($v);
            } else {
                deldir($v);
            }
        }
    }
}

function deldir($dir) {
    //先删除目录下的文件：
    $dh = opendir($dir);
    while ($file = readdir($dh)) {
        if ($file != "." && $file != "..") {
            $fullpath = $dir . "/" . $file;
            if (!is_dir($fullpath)) {
                unlink($fullpath);
            } else {
                deldir($fullpath);
            }
        }
    }

    closedir($dh);
    //删除当前文件夹：
    if (rmdir($dir)) {
        return true;
    } else {
        return false;
    }
}

function getState($state) {
    $word = "等待审核";
    if ($state == 1) {
        $word = "审核完成";
    } else if ($state == -1) {
        $word = "拒绝通过";
    }
    return $word;
}

function getCurlImg($img, $website) {
    $first = substr($img, 0, 1);
    if ($first == '/') {
        $img = $website . $img;
    } else {
        $first = substr($img, 0, 4);
        if ($first != 'http') {
            $img = $website . "/" . $img;
        }
    }
    $img = str_replace("../", "", $img);
    return $img;
}

function getFileCat($name) {
    $type = get_extension($name);
    if (!in_array($type, array("css", "js"))) {
        $type = "images";
    }
    return $type;
}

function putCurlFile($file_cur, $website_name) {
    $website_name = 'website/' . $website_name;
    if (!file_exists($website_name)) {
        mkdir($website_name);
    }
    $name = substr($file_cur, strrpos($file_cur, '/') + 1); //
    $type = getFileCat($file_cur);
    $imageFile = $website_name . "/" . $type . "/";
    if (!file_exists($imageFile)) {
        mkdir($imageFile);
    }
    $file_get = file_get_contents($file_cur);
    if ($type == 'css') {
        print_r($file_get);
    }

    file_put_contents($imageFile . $name, $file_get);
}

function getCurlPerFile($url, $website, $website_filepath) {
    $url = getAsk($url);
    $type = get_extension($url);
    if (in_array($type, array("css", "js", "ico", "png", "jpg", "gif", "jpeg"))) {
        $arr = explode(",", $url);

        if (count($arr) > 1) {
            $parse_url = parse_url($arr[0]);
            $website = $parse_url['scheme'] . "://" . $parse_url["host"];
            $parse_query = $parse_url["query"];
            if ($parse_query) {
                $parse_query_arr = explode("=", $parse_query);
                $website .= $parse_url['path'] . "?" . $parse_query_arr[0] . "=";
            }
            $url_arr = array();

            foreach ($arr as $k => $v) {
                if ($k == 0) {
                    $url_arr[] = $v;
                } else {
                    $first = substr($v, 0, 1);
                    if ($first == '/') {
                        $url_arr[] = $website . $v;
                    } else {
                        $url_arr[] = $website . "/" . $v;
                    }
                }
            }
        } else {

            $url_arr[0] = getFullUrl($url, $website, $website_filepath);
        }
    }

    return $url_arr;
}

function getFullUrl($url, $website, $website_filepath) {
    $first = substr($url, 0, 1);
    $two = substr($url, 0, 2);
    $four = substr($url, 0, 4);
    $url_full = '';

    if ($two == '//') {
        $url_full = "http:" . $url;
    } else {
        if ($first == '/') {
            $url_full = $website . $url;
        } elseif ($four == 'http') {
            $url_full = $url;
        } else {
            $url_full = $website . "/" . $url;
        }
    }
    $website_filepath_url = str_replace($website, $website_filepath, $url_full);
    if (file_get_contents($website_filepath_url)) {
        $url_full = $website_filepath_url;
    }
    return $url_full;
}

function str_replace_empty($content) {
    $arr = array(
        '<script type="text/javascript"></script>',
    );
    foreach ($arr as $v) {
        $content = str_replace($v, "", $content);
    }
    return $content;
}

function str_replace_common($content) {
    $arr = array(
        0 => array(
            "old" => "gbk",
            "new" => "utf-8"
        ),
        1 => array(
            "old" => "gb2312",
            "new" => "utf-8"
        ),
        2 => array(
            "old" => '<script type="text/javascript" src="<script',
            "new" => "<script"
        ),
        3 => array(
            "old" => '</script>"></script>',
            "new" => "</script>"
        )
    );
    foreach ($arr as $v) {
        $content = str_replace($v['old'], $v['new'], $content);
    }
    return $content;
}

function getimgs($str) {
    $reg = '/((http|https):\/\/)+(\w+\.)+(\w+)[\w\/\.\-]*(jpg|gif|png)/';
    $matches = array();
    preg_match_all($reg, $str, $matches);
    foreach ($matches[0] as $value) {
        $data[] = $value;
    }
    return $data;
}

function getImgFirst($images, $website) {
    if ($images) {
        foreach ($images as $v) {
            $img = getCurlImg($v, $website);
            $arr[] = getimgs($img);
        }
        foreach ($arr as $v) {
            foreach ($v as $v2) {
                $img_last[] = $v2;
            }
        }
        return array_unique($img_last);
    }
}

function getAsk($file) {
    $files = explode("?", $file);
    return $files[0];
}

function is_img($pic) {
    $type = strtolower(get_extension($pic));
    $types = explode(",", C("pic.types"));
    $i = 0;
    foreach ($types as $v) {
        $index = strstr($type, $v);
        if ($index) {
            $i++;
        }
    }
    return $i;
}

function is_img2($pic) {
    $type = get_extension($pic);
    $types = explode(",", C("pic.types"));
    $i = 0;
    if (in_array($type, $types)) {
        $i = 1;
    }
    return $i;
}

function is_img_basename($pic) {
    $types = explode(",", C("pic.types"));
    $i = 0;
    foreach ($types as $v) {
        $index = strstr($pic, $v);
        if ($index != '') {
            $i++;
        }
    }
    return $i;
}

function get_extension_basename($file) {
    $arr = explode(".", $file);
    return $arr[1];
}

function sendMail($to, $name, $subject, $body = '', $attachment = null) {
    $config = getTableFile('config');
//    $detail = json_decode(getTableField("emails", "three"), true);
//    print_r($detail);exit;
    vendor('phpmailer.class#phpmailer');
    $mail = new PHPMailer(); //PHPMailer对象
    $mail->CharSet = 'UTF-8'; //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
    $mail->IsSMTP();  // 设定使用SMTP服务
    $mail->SMTPDebug = 0;                     // 关闭SMTP调试功能
    $mail->SMTPAuth = true;                  // 启用 SMTP 验证功能
    $mail->SMTPSecure = '';                 // 使用安全协议
    $mail->Host = "smtp.163.com";  // SMTP 服务器
    $mail->Port = "";  // SMTP服务器的端口号
    $mail->Username = "hjl416148489_5@163.com";  // SMTP服务器用户名
    $mail->Password = "hjl7233163";  // SMTP服务器密码
    $mail->Subject = $subject; //邮件标题
    $mail->SetFrom("hjl416148489_5@163.com", $config['title']);
    $mail->MsgHTML($body);
    $mail->AddAddress($to, $config['title']);
    if (is_array($attachment)) { // 添加附件
        foreach ($attachment as $file) {
            is_file($file) && $mail->AddAttachment($file);
        }
    }
    return $mail->Send() ? true : $mail->ErrorInfo;
}

function getImplode($ids, $table = 'modals_dictionary') {
    if ($ids) {
        $names = "";
        $lists = M($table)->field("name")->where("id in (" . $ids . ")")->select();
        foreach ($lists as $v) {
            $names .= $v['name'] . " ";
        }
        return $names;
    }
}

function getSitemapSelected($cat, $html, $arr) {
    $chose = '';
    foreach ($arr as $k => $v) {
        foreach ($v as $v2) {
            if ($v2['html'] == $html) {
                $chose = $k;
                break;
            }
        }
    }
    if ($cat == $chose) {
        return "selected";
    }
}

function goUrl($url) {
    echo "<script language=\"javascript\">location.href = '" . $url . "';</script>";
}

function curl_get($url, $gzip = false) {
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_ENCODING, "gzip");
    $content = curl_exec($curl);
    curl_close($curl);
    return $content;
}

function getSeo($url) {

    $meta = get_meta_tags($url);
    $keywords = $meta['keywords'];
    $keywords = explode(',', $keywords);
    $keywords = array_map('trim', $keywords);
    $keywords = array_filter($keywords);
    foreach ($keywords as $k => $v) {
        $keywords[$k] = str_replace("，", ",", $v);
    }
    $keywords = implode(",", $keywords);


    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($c);
    curl_close($c);
    $pos = strpos($data, 'utf-8');
    if ($pos === false) {
        $data = iconv("gbk", "utf-8", $data);
    }
    preg_match("/<title>(.*)<\/title>/i", $data, $title);
    $title = $title[1];


    $tags = get_meta_tags($url);
    $description = $tags['description'];
    $arr = array(
        "title" => $title,
        "keywords" => $keywords,
        "description" => $description,
    );
    return $arr;
//        echo json_encode($arr);
}

function changeCharset($content, $charset) {
    if (strtolower($charset) != 'utf-8' && $charset != '') {
        $content = iconv($charset, "utf-8", $content);
    }
    return $content;
}

function getTagsNum($mtype = 2) {
    $table = getTableInfo($mtype);
    $tag_ids = M("" . $table . "_tags")->field("id")->select();
    foreach ($tag_ids as $v) {
        $tag_num = M($table)->where("FIND_IN_SET('" . $v['id'] . "',tags)")->count();
        M("" . $table . "_tags")->where("id = " . $v['id'] . "")->save(array("num" => $tag_num));
    }
}

function htmlFormat($content) {
    $url = "http://tool.oschina.net/action/format/html";
    $post_data = array("html" => $content);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_USERAGENT, _USERAGENT_);
    curl_setopt($ch, CURLOPT_REFERER, _REFERER_);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
//      curl_setopt($ch, CURLOPT_ENCODING, "gzip"); // 关键在这里
    $output = curl_exec($ch);
    curl_close($ch);
    $json = json_decode($output, true);
    return $json['fhtml'];
}

function getPageKey($k) {
    $p = I("get.p", 1, 'int');
    return ($p - 1) * C("pagenum") + $k;
}

function getOrdernum() {
    return date("YmdHis") . rand(1000, 9999);
}

function recurse_copy($src, $dst) {  // 原目录，复制到的目录
    $dir = opendir($src);
    @mkdir($dst);
    while (false !== ( $file = readdir($dir))) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if (is_dir($src . '/' . $file)) {
                recurse_copy($src . '/' . $file, $dst . '/' . $file);
            } else {
                copy($src . '/' . $file, $dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

function getTagHtml($content) {
    return htmlspecialchars_decode($content);
}

function getPsdExists($id) {
    $info = M("psd")->field("name")->where("id = " . $id . "")->find();
    $file_path = "psds/" . $id . "/";
    $arr = array("middle.jpg", "big.jpg");
    $str = "";
    foreach ($arr as $v) {
        $file_name = $file_path . $v;
        if (!file_exists($file_name)) {
            $str .= $v . "不存在&nbsp;";
        }
    }
    $lists = scandir($file_path);
    $i = 0;
    foreach ($lists as $v) {
        $type = get_extension($v);
        if ($type == 'zip') {
            $i++;
        }
    }
    if ($i == 0) {
        $str .= "zip压缩文件不存在";
    }
    return $str;
}

function checkBaidu($id) {
    $url = "http://www.sucaihuo.com/js/" . $id . ".html";
    $url = 'http://www.baidu.com/s?wd=' . $url;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $rs = curl_exec($curl);
    curl_close($curl);
    if (!strpos($rs, '没有找到')) {
        return "<span class='red'>已收录</span>";
    } else {
        return "<span>未收录</span>";
    }
}

function getHasContentFile($files) {
    foreach ($files as $v) {
        $content = file_get_contents($v);
        if (is_file($v)) {
            echo strstr($content, "404");
        }
    }
//    return $rs;
}

function getOrdMax($table, $where) {
    if (I("post.ord", 0, 'int') == 0) {
        $max = M($table)->where($where)->max("ord");
        $ord = $max + 1;
    } else {
        $ord = I("post.ord", 0, 'int');
    }
    return $ord;
}

function getDownloadState($state) {
    $rs = "未下载";
    if ($state == 1) {
        $rs = "<span style='color:green'>已下载</span>";
    } elseif ($state == -1) {
        $rs = "<span style='color:red'>下载失败</span>";
    }
    return $rs;
}

function sendToBaidu($url) {
    if ($url) {
        $urls = array(
            $url
        );
        $api = 'http://data.zz.baidu.com/urls?site=www.sucaihuo.com&token=2218HVy0qsbcEQdQ&type=mip';
        $ch = curl_init();
        $options = array(
            CURLOPT_URL => $api,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => implode("\n", $urls),
            CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
        );
        curl_setopt_array($ch, $options);
        $result = curl_exec($ch);
        return $result;
    }
}

function delInnerFiles($dir) {
    if (is_dir($dir)) {
        $files = scandir($dir);
        //打开目录 //列出目录中的所有文件并去掉 . 和 .. 
        foreach ($files as $filename) {
            if ($filename != '.' && $filename != '..') {
                if (!is_dir($dir . '/' . $filename)) {
                    if (empty($file_type)) {
                        unlink($dir . '/' . $filename);
                    } else {
                        if (is_array($file_type)) {
                            //正则匹配指定文件
                            if (preg_match($file_type[0], $filename)) {
                                unlink($dir . '/' . $filename);
                            }
                        } else {
                            //指定包含某些字符串的文件
                            if (false != stristr($filename, $file_type)) {
                                unlink($dir . '/' . $filename);
                            }
                        }
                    }
                } else {
                    delInnerFiles($dir . '/' . $filename);
                    rmdir($dir . '/' . $filename);
                }
            }
        }
    } else {
        if (file_exists($dir))
            unlink($dir);
    }
}

function makeDir($dir, $mode = 0777) {
    if (!$dir)
        return false;

    if (!file_exists($dir)) {
        mkdir($dir, $mode, true);
        return chmod($dir, $mode);
    } else {
        return true;
    }
}

function getAdminId() {
    $userid = session("admin_uid");
    $username = session("admin_name");
    if (!$userid || !$username) {
        $info = M("user")->field("name")->where("id= '" . $userid . "' AND is_admin = 1")->find();
        if (!empty($info['name'])) {
            session('admin_name', $info['name']);
            session('admin_uid', $userid);
        }
    }
    return $userid;
}

function sendMailOrderPoints() {
    $pay_info = M("pay")->field("out_trade_no,money")->where("is_send_email = 0 AND status = 1")->find();
    if ($pay_info) {
        M("pay")->where("out_trade_no = '" . $pay_info['out_trade_no'] . "'")->save(array("is_send_email" => 1));
        $small = 21;
        $big = 23;
        for ($i = $small; $i <= $big; $i++) {
            $email_accounts[$i]['account'] = 'sucaihuo_0' . $i . '@sucaihuo.com';
            $email_accounts[$i]['pwd'] = 'Sucaihuo778899';
        }
        $rand = rand($small, $big);
        $email_name = $email_accounts[$rand]['account']; //zhengzaoxia@yaxzb.com,admin@mfroad.com
        $email_pwd = $email_accounts[$rand]['pwd']; //ZX200711zx,1234！QAZ2wsx

        $detail = array(
            "smpt" => "smtp.exmail.qq.com",
            "account" => $email_name,
            "pwd" => $email_pwd,
            "port" => 25 //25
        );
        $title = "pay_points" . $pay_info['money'];
        vendor('phpmailer.class#phpmailer');
        $mail = new PHPMailer(); //PHPMailer对象
        $mail->CharSet = 'UTF-8'; //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
        $mail->IsSMTP();  // 设定使用SMTP服务
        $mail->SMTPDebug = 0;                     // 关闭SMTP调试功能
        $mail->SMTPAuth = true;                  // 启用 SMTP 验证功能
        $mail->SMTPSecure = '';                 // 使用安全协议
        $mail->Host = $detail['smpt'];  // SMTP 服务器
        $mail->Port = "25";  // SMTP服务器的端口号
        $mail->Username = $detail['account'];  // SMTP服务器用户名
        $mail->Password = $detail['pwd'];  // SMTP服务器密码
        $mail->Subject = $title; //邮件标题
        $mail->SetFrom($detail['account'], $title);
        $mail->MsgHTML($pay_info['money']);
        $mail->AddAddress("416148489@qq.com", $title);
        $result = $mail->Send() ? 'ok' : $mail->ErrorInfo;
    }
}

function sendMailOrderMonth() {
    $pay_info = M("order_month")->field("id,money")->where("is_send_email = 0 AND status = 1")->find();
    if ($pay_info) {
        M("order_month")->where("id = '" . $pay_info['id'] . "'")->save(array("is_send_email" => 1));
        $email_accounts = array();
        $small = 21;
        $big = 23;
        for ($i = $small; $i <= $big; $i++) {
            $email_accounts[$i]['account'] = 'sucaihuo_0' . $i . '@sucaihuo.com';
            $email_accounts[$i]['pwd'] = 'Sucaihuo778899';
        }
        $rand = rand($small, $big);
        $email_name = $email_accounts[$rand]['account']; //zhengzaoxia@yaxzb.com,admin@mfroad.com
        $email_pwd = $email_accounts[$rand]['pwd']; //ZX200711zx,1234！QAZ2wsx

        $detail = array(
            "smpt" => "smtp.exmail.qq.com",
            "account" => $email_name,
            "pwd" => $email_pwd,
            "port" => 25 //25
        );
        $title = "pay_month" . $pay_info['money'];
        vendor('phpmailer.class#phpmailer');
        $mail = new PHPMailer(); //PHPMailer对象
        $mail->CharSet = 'UTF-8'; //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
        $mail->IsSMTP();  // 设定使用SMTP服务
        $mail->SMTPDebug = 0;                     // 关闭SMTP调试功能
        $mail->SMTPAuth = true;                  // 启用 SMTP 验证功能
        $mail->SMTPSecure = '';                 // 使用安全协议
        $mail->Host = $detail['smpt'];  // SMTP 服务器
        $mail->Port = "25";  // SMTP服务器的端口号
        $mail->Username = $detail['account'];  // SMTP服务器用户名
        $mail->Password = $detail['pwd'];  // SMTP服务器密码
        $mail->Subject = $title; //邮件标题
        $mail->SetFrom($detail['account'], $title);
        $mail->MsgHTML($pay_info['money']);
        $mail->AddAddress("416148489@qq.com", $title);
        $mail->Send() ? 'ok' : $mail->ErrorInfo;
    }
}

function deleteHtmlFile($id, $mtype) {
    $mtype_file = getHtmlMtype($mtype);
    $filename = "Application/Html/" . $mtype_file . "/" . getFileBei($id) . $id . ".html";
//    echo $filename;exit;
    if (file_exists($filename)) {
        unlink($filename);
    }
}

function is_power($action) {
    $actions = explode("/", $action);
    if ($actions) {
        $admin_uid = getSessionCookie("admin_uid");

        $detail = M("admin_menu2")->field("id,name")->where("control = '" . $actions[0] . "' AND `mod` = '" . $actions[1] . "' AND id != 1")->find();
//        echo M("admin_menu2")->getlastsql();exit;
//        print_r($actions);
//        PRINT_r($detail);exit;
        if (empty($detail)) {
            return 1;
        }
        $user = M("user")->field("powers")->where("id = " . $admin_uid . "")->find();
        $powers = explode(",", $user['powers']);
        if (in_array($detail['id'], $powers) or $admin_uid == 1) {
            return 1;
        } else {
            return $detail['name'];
        }
    }
}

function checkAdminMenu($id, $ids) {
    $admin_uid = getSessionCookie("admin_uid");
    if ($admin_uid != 1) {
        $idsArr = explode(",", $ids);
        if (!in_array($id, $idsArr)) {
            return "style='display:none'";
        }
    }
}

function getUserCheck($state) {
    $rs = "待审核";
    if ($state == 1) {
        $rs = "<span class='green'>审核通过</span>";
    } elseif ($state == -1) {
        $rs = "<span class='red'>积分异常</span>";
    } elseif ($state == -2) {
        $rs = "<span class='red'>刷积分</span>";
    }
    return $rs;
}

function getWangpanPwd($str) {
    $public_key = '-----BEGIN PUBLIC KEY----- 
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3//sR2tXw0wrC2DySx8vNGlqt 
3Y7ldU9+LBLI6e1KS5lfc5jlTGF7KBTSkCHBM3ouEHWqp1ZJ85iJe59aF5gIB2kl 
Bd6h4wrbbHA2XE1sq21ykja/Gqx7/IRia3zQfxGv/qEkyGOx+XALVoOlZqDwh76o 
2n1vP1D+tD3amHsK7QIDAQAB 
-----END PUBLIC KEY-----';
    openssl_public_encrypt($str, $output, $public_key);
    return base64_encode($output);
}

// 支付类型获取
function paytype($paytype) {
    if ($paytype == 1) {
        return "微信";
    } else {
        return "支付宝";
    }
}

// 操作状态 0 待审核 1进行中 2已完成 3已拒绝
function operation($operation) {
    if ($operation == 1) {
        return "进行中";
    } else if ($operation == 2) {
        return "已完成";
    } else if ($operation == 3) {
        return "已拒绝";
    } else {
        return "待审核";
    }
}

//服务器系统类型
function seventype($seventype) {
    if ($seventype == 1) {
        return "Windows 2003-32位";
    } else if ($seventype == 2) {
        return "Windows 2008-64位";
    } else if ($seventype == 3) {
        return "CentOS 6.4-64位";
    } else if ($seventype == 4) {
        return "Windows 2012-64位";
    } else if ($seventype == 5) {
        return "Windows 7-64位";
    } else if ($seventype == 6) {
        return "CentOS 7.1-64位";
    } else if ($seventype == 7) {
        return "CentOS 6.8-64位";
    } else {
        return "";
    }
}

/**
 * 阿里云上传文件
 * 阿里云目标地址http://ossali.sucaihuo.com/
 *
 * */
function aliyun_upload($avatar_temp, $avatar_aliyun) { //$avatar_temp 上传文件， $avatar_aliyun 上传目标地址（比如sucaihuo/avatar/1.jpg）
    vendor('aliyun.autoload');
    $bucket = "hjl416148489";
    $accessKeyId = "QlmCSuHgioxZNKCq"; //去阿里云后台获取秘钥
    $accessKeySecret = "JQWc9Coh5R0YP49FkG936kQNwUrorh "; //去阿里云后台获取秘钥
    $endpoint = "http://oss-cn-hangzhou.aliyuncs.com/"; //你的阿里云OSS地址
    $ossClient = new \OSS\OssClient($accessKeyId, $accessKeySecret, $endpoint);
//        判断bucketname是否存在，不存在就去创建
    if (!$ossClient->doesBucketExist($bucket)) {
        $ossClient->createBucket($bucket);
    }

    try {
        $ossClient->uploadFile($bucket, $avatar_aliyun, $avatar_temp);

        unlink($avatar_temp);
        return "http://ossali.sucaihuo.com/" . $avatar_aliyun;
    } catch (OssException $e) {
        $e->getErrorMessage();
    }
}

function getRowPlus($row) {
    $rs = 0;
    foreach ($row as $v) {
        $rs +=$v;
    }
    return $rs;
}

function getTaskstatus($status) {
    if ($status == 0) {
        $statusStr = "新建";
    } elseif ($status == 1) {
        $statusStr = "进行中";
    } elseif ($status == 2) {
        $statusStr = "已解决";
    } elseif ($status == 3) {
        $statusStr = "不能受理";
    }
    return $statusStr;
}

function getAssignUser($uid) {
    if ($uid == 3) {
        $userStr = "小蔡";
    } elseif ($uid == 11) {
        $userStr = "小贵";
    } elseif ($uid == 13) {
        $userStr = "小旋";
    } else {
        $userStr = "未知";
    }
    return $userStr;
}

//获取后台用户列表
function getAdminUsers() {
    return M("user")->field("id,name,nickname,email,job")->where("is_admin = 1")->select();
}

//获取任务状态列表
function getAdminstatus() {
    return C("WORK_STATUS");
}

function check_str_type($str = '') {
    if (trim($str) == '') {
        return '';
    }
    $m = mb_strlen($str, 'utf-8');
    $s = strlen($str);
    if ($s == $m) {
        return 1;
    }
    if ($s % $m == 0 && $s % 3 == 0) {
        return 2;
    }
    return 3;
}

function getKeywords($title, $content) {
    $tags_common = array("网页源码", "网站源码", "web源码", "系统源码", "管理系统", "框架", "程序猿", "小程序", "公众号", "系统");
    $sql = "SELECT name FROM sucai_source_tags WHERE  LOCATE(name,'" . $title . "') > 0";
    $tags_query = M("source_tags")->query($sql);
    foreach ($tags_query as $v) {
        $tags_arr[] = $v['name'];
    }
    if ($tags_arr) {
        $tags_count = count($tags_arr);
    }
    if ($tags_count <= 1) {
        $sql2 = "SELECT name FROM sucai_source_tags WHERE  LOCATE(name,'" . $content . "') > 0";
        $tags_content_query = M("source_tags")->query($sql2);
        foreach ($tags_content_query as $v) {
            $tags_content_arr[] = $v['name'];
        }

        $tags_arr = $tags_content_arr;

        if (!empty($tags_arr)) {
            $tags_count = count($tags_arr);
        }
    }

    $tags_Chinese_arr = array();
    if (empty($tags_arr)) {
        return "";
    }
    foreach ($tags_arr as $v) {
        if (preg_match("/^[a-zA-Z\s]+$/", $v)) {
            $tag_English_arr[] = $v;
        } else {
            $tags_Chinese_arr[] = $v;
        }
    }

    if (count($tags_Chinese_arr) <= 1) {
        $tags = $tags_arr;
    } else {
        if ($tag_English_arr) {

            $tags[0] = $tag_English_arr[array_rand($tag_English_arr, 1)];

            foreach ($tags_Chinese_arr as $k => $v) {
                $tags[$k + 1] = $v;
            }
        } else {
            $tags = $tags_Chinese_arr;
        }
    }

    foreach ($tags_common as $v) {
        if (strstr($content, $v)) {
            $tag_common_keyword = $v;
            break;
        }
    }
    if ($tag_common_keyword == '') {
        $tag_common_keyword = '源码';
    }

    if ($tags_count >= 3) {

        $keywords_rands = array_rand($tags, 3);

        $keyword1 = $tags[$keywords_rands[0]] . $tags[$keywords_rands[1]];
        $keyword2 = $tags[$keywords_rands[1]] . $tags[$keywords_rands[2]];
        if (check_str_type($keyword1) != 3) {
            $keyword1 .=$tag_common_keyword;
        }
        if (check_str_type($keyword2) != 3) {
            $keyword2 .=$tag_common_keyword;
        }
    } elseif ($tags_count == 2) {
        $keyword1 = $tags[0] . $tag_common_keyword;
        $keyword2 = $tags[1] . $tag_common_keyword;
    } elseif ($tags_count == 1) {
        $keyword1 = $tags[0] . "源码";
        $keyword2 = $tags[0] . "管理系统";
    }
    $keywords = $keyword1 . "," . $keyword2;
    return $keywords;
}

function randomkeys($length) {
    $pattern = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ';
    for ($i = 0; $i < $length; $i++) {
        $key .= $pattern{mt_rand(0, 35)}; //生成php随机数 
    }
    return $key;
}

function getLanguageCat($title, $content) {
    $languages = array(
        1 => array("java", "jsp", "springboot", "ssm", "java", "ssh"),
        2 => array(".net", "c++", "c语言"),
        3 => array("c#"),
        4 => array("asp"),
        5 => array("python"),
        0 => array("php", "thinkphp", "tp", "织梦", "帝国", "empire", "dede", "ecshop", "wordpress", "wp", "discuz", "dz", "destoon", "laravel", "phalcon", "symfony", "codeigniter", "ci", "yii", "aura", "zend", "yaf", "swoole", "tpcms"),
        6 => array("html", '静态模板', 'vue', 'react', 'angluar', 'uniapp'),
    );
    $languages2 = array(
        6 => array("html", "css", "模板"),
    );
    $cats = array(
        259 => array("水果", "鲜花", "礼品"),
        254 => array("楼盘", "商铺", "房", "装修", "房屋", "租赁", "物流", "快递", "交通"),
        262 => array("发卡", "收银", "收款", "签名", "卡密", "卡券"),
        260 => array("单页", "活动", "营销", "报名", "抽奖", "转盘", "竞价", "投票", "预约", "答题"),
        13 => array("门户", "新闻", "资讯", "discuz", "问答"), // 72
        250 => array("crm", "erp", "办公", "进销存", "人事"),
        14 => array("电影", "影视", "音乐"),
        263 => array("文章", "文学", "博客"),
        251 => array("小说", "游戏", "动漫", "竞技"),
        16 => array("聊天", "交友", "直播", "聊天"),
        255 => array("旅游", "餐饮", "票", "外卖", "餐", "订餐", "同城", "B2B", "分类"),
        245 => array("学校", "教育", "人才", "招聘", "求职", "课", "知识", "诗", "作业", "教学", "幼儿", "图书", "学生"),
        15 => array("股票", "金融", "理财"),
        261 => array("图片", "素材", "资源", "下载平台"),
        252 => array("医院", "女人", "健康", "医生", "护士", "养生", "体育", "运动", "赛事", "健身"),
        263 => array("域名", "服务器", "空间", "导航", "网址", "查询"),
        248 => array("车", "二手", "跳蚤", "4s", "中介"),
        11 => array("企业", "公司", "政府", "集团", "机关"), // 5
        10 => array("淘客", "网店", "商城", "购物", "mall", "淘宝", "跨境", "外贸", "ecshop", "订单", "团购", "售", "电商", '微店'), // 4
    );
    $language_id = 0;
    $cat_id = 0;
    foreach ($languages as $k => $v_sub) {
        foreach ($v_sub as $k2 => $v2) {
            if (strstr(strtolower($title), strtolower($v2))) {
                $language_id = $k;
            }
        }
        if ($language_id > 0) {
            break;
        }
    }

    if ($language_id == 0) {
        foreach ($languages as $k => $v_sub) {
            foreach ($v_sub as $k2 => $v2) {
                if (strstr(strtolower($content), strtolower($v2))) {
                    $language_id = $k;
                }
            }
            if ($language_id > 0) {
                break;
            }
        }
    }
    if ($language_id == 0) {
        foreach ($languages2 as $k => $v_sub) {
            foreach ($v_sub as $k2 => $v2) {
                if (strstr(strtolower($title), strtolower($v2))) {
                    $language_id = $k;
                }
            }
            if ($language_id > 0) {
                break;
            }
        }
    }
    if ($language_id == 0) {
        foreach ($languages2 as $k => $v_sub) {
            foreach ($v_sub as $k2 => $v2) {
                if (strstr(strtolower($content), strtolower($v2))) {
                    $language_id = $k;
                }
            }
            if ($language_id > 0) {
                break;
            }
        }
    }
    foreach ($cats as $k => $v_sub) {

        foreach ($v_sub as $k2 => $v2) {
            if (strstr(strtolower($title), strtolower($v2))) {
                $cat_id = $k;
            }
        }
        if ($cat_id > 0) {
            break;
        }
    }

    if ($cat_id == 0) {
        foreach ($cats as $k => $v_sub) {
            foreach ($v_sub as $k2 => $v2) {
                if (strstr(strtolower($content), strtolower($v2))) {
                    $cat_id = $k;
                }
            }
            if ($cat_id > 0) {
                break;
            }
        }
    }
    //小写匹配
    $data['language_id'] = $language_id > 0 ? $language_id : 99; //0PHP 99 其他
    $data['color_id'] = $cat_id > 0 ? $cat_id : 259; //259 系统  
    $rs_cat_id = 9; // 其他
    if ($cat_id == 10) {
        $rs_cat_id = 4;
    } else if ($cat_id == 11) {
        $rs_cat_id = 5;
    } else if ($cat_id == 13) {
        $rs_cat_id = 72;
    }
    $data['cat_id'] = $rs_cat_id;
    $developments = array(
        117 => array("thinkphp", "tpcms"),
        120 => array("织梦", "dede", "dedecms"),
        216 => array("帝国", "empire"),
        118 => array("ecshop"),
        205 => array("wordpress"),
        119 => array("discuz"),
        244 => array("destoon"),
    );
    $development_id = 0;
    foreach ($developments as $k => $v_sub) {

        foreach ($v_sub as $k2 => $v2) {
            if (strstr(strtolower($title), strtolower($v2))) {
                $development_id = $k;
            }
        }
        if ($development_id > 0) {
            break;
        }
    }

    if ($development_id == 0) {
        foreach ($developments as $k => $v_sub) {
            foreach ($v_sub as $k2 => $v2) {
                if (strstr(strtolower($content), strtolower($v2))) {
                    $development_id = $k;
                }
            }
            if ($development_id > 0) {
                break;
            }
        }
    }
    $data['development_id'] = $development_id > 0 ? $development_id : 121; //其他
    return $data;
}

function getLanguageCatLanren($title, $content) {
    $languages = array(
        7 => array("springboot", "ssm", "java", "ssh"),
        4 => array("java", "jsp"),
        3 => array(".net", "c++", "c语言"),
        10 => array("c#"),
        9 => array("object-c"),
        1 => array("asp"),
        11 => array("python"),
        2 => array("php", "thinkphp", "tp", "织梦", "帝国", "empire", "dede", "ecshop", "wordpress", "wp", "discuz", "dz", "destoon", "laravel", "phalcon", "symfony", "codeigniter", "ci", "yii", "aura", "zend", "yaf", "swoole", "tpcms"),
        5 => array("html", '静态模板', 'vue', 'react', 'angluar', 'uniapp'),
    );
    $languages2 = array(
        5 => array("html", "css", "模板"),
    );
    $cats = array(
        18 => array("楼盘", "商铺", "房", "装修", "房屋", "租赁"),
        43 => array("发卡", "收银", "收款", "签名", "卡密", "卡券"),
        46 => array("单页", "活动", "营销", "报名", "抽奖", "转盘", "竞价", "投票", "预约", "答题"),
        45 => array("聊天", "挂机"),
        13 => array("门户", "新闻", "资讯", "discuz", "问答"),
        11 => array("导航", "网址", "查询"),
        15 => array("同城", "B2B", "分类"),
        5 => array("crm", "erp", "办公", "进销存", "人事"),
        6 => array("电影", "影视", "音乐"),
        9 => array("小说", "文章", "文学", "博客"),
        7 => array("游戏", "动漫", "竞技"),
        8 => array("聊天", "交友", "直播"),
        17 => array("旅游", "餐饮", "票", "外卖", "餐", "订餐"),
        19 => array("学校", "教育", "人才", "招聘", "求职", "课", "知识", "诗", "作业", "教学", "幼儿", "图书", "学生"),
        20 => array("股票", "金融", "理财"),
        22 => array("图片", "素材", "资源", "下载平台"),
        10 => array("医院", "女人", "健康", "医生", "护士", "养生"),
        24 => array("体育", "运动", "赛事", "健身"),
        25 => array("物流", "快递", "交通"),
        26 => array("域名", "服务器", "空间"),
        27 => array("车", "二手", "跳蚤", "4s", "中介"),
        21 => array("企业", "公司", "政府", "集团", "机关"),
        12 => array("淘客", "网店", "商城", "购物", "mall", "淘宝", "跨境", "外贸", "ecshop", "订单", "团购", "售", "电商", '微店'),
    );
    $language_id = 0;
    $cat_id = 0;
    foreach ($languages as $k => $v_sub) {
        foreach ($v_sub as $k2 => $v2) {
            if (strstr(strtolower($title), strtolower($v2))) {
                $language_id = $k;
            }
        }
        if ($language_id > 0) {
            break;
        }
    }

    if ($language_id == 0) {
        foreach ($languages as $k => $v_sub) {
            foreach ($v_sub as $k2 => $v2) {
                if (strstr(strtolower($content), strtolower($v2))) {
                    $language_id = $k;
                }
            }
            if ($language_id > 0) {
                break;
            }
        }
    }
    if ($language_id == 0) {
        foreach ($languages2 as $k => $v_sub) {
            foreach ($v_sub as $k2 => $v2) {
                if (strstr(strtolower($title), strtolower($v2))) {
                    $language_id = $k;
                }
            }
            if ($language_id > 0) {
                break;
            }
        }
    }
    if ($language_id == 0) {
        foreach ($languages2 as $k => $v_sub) {
            foreach ($v_sub as $k2 => $v2) {
                if (strstr(strtolower($content), strtolower($v2))) {
                    $language_id = $k;
                }
            }
            if ($language_id > 0) {
                break;
            }
        }
    }
    foreach ($cats as $k => $v_sub) {

        foreach ($v_sub as $k2 => $v2) {
            if (strstr(strtolower($title), strtolower($v2))) {
//                    echo $v2 . "@" . $k;
                $cat_id = $k;
            }
        }
        if ($cat_id > 0) {
            break;
        }
    }

    if ($cat_id == 0) {
        foreach ($cats as $k => $v_sub) {
            foreach ($v_sub as $k2 => $v2) {
                if (strstr(strtolower($content), strtolower($v2))) {
                    $cat_id = $k;
                }
            }
            if ($cat_id > 0) {
                break;
            }
        }
    }
    //小写匹配
    $data['language_id'] = $language_id > 0 ? $language_id : 4; //4其他
    $data['cat_id'] = $cat_id > 0 ? $cat_id : 28; //28 系统 
    return $data;
}

function get_taobao_content($data) {


    $content = stripslashes($data);
    $content = ereg_replace("<a [^>]*>|<\/a>", "", $content);

    $pattern = '/(http|https):\/\/([\w\d\-_]+[\.\w\d\-_]+)[:\d+]?([\/]?[\w\/\.?=]+)/i';
    preg_match_all($pattern, $content, $links_http);

    $links_http_arr = $links_http[0];
    foreach ($links_http_arr as $k => $v) {
        if (strstr($v, "item.htm")) {
            $content = str_replace($v, "", $content);
        }
    }
    $content1 = preg_replace("/<[^\/>]*>([\s]?)*<\/[^>]*>/s", '', $content);
//print_r($content);exit;

    $replaces = array(
        "旺旺" => "",
        "天猫" => "",
        "互站" => "",
        "淘宝" => ""
    );
    foreach ($replaces as $k => $v) {
        $content1 = str_replace($k, $v, $content1);
    }
    $content2 = preg_replace('/<div data-title="agiso_alds_desc">.*?(<\/p>|<\/div>)(\t|\r\n|\n|\s)*<\/div>/is', "", $content1);
    $rs = preg_replace('/<div data-title="dummy_taotools_kxapps_com">.*?(<\/p>|<\/table>)(\t|\r\n|\n|\s)*<\/div>/is', "", $content2);
    return $rs;
}

?>