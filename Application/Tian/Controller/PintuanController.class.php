<?php

namespace Tian\Controller;

use PclZip;

class PintuanController extends CommonController {

    public function lists() {
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND (name like '%" . $keyword . "%' or keywords like '%" . $keyword . "%')";
        }
        $modal_cat_id = I('get.modal_cat_id', 0, 'int'); //分类筛选
        if ($modal_cat_id > 0) {
            $sql .= " AND cat_id = " . $modal_cat_id . "";
        }
      


        $count = M('pintuan')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('pintuan')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
        foreach ($lists as $k => $v) {
           $lists[$k]['downloads'] = M("download")->field("uid,addtime,points")->where("tid = " . $v['id'] . " AND mtype = 50")->order("id DESC")->select();
        }
        $modal_cats = getDictionarySubSql('modal_cat_id', 'pintuan_dictionary');
        $modal_colors = getDictionarySubSql('modal_money_id', 'pintuan_dictionary');
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->assign("modal_cat_id", $modal_cat_id);
        $this->assign("modal_cats", $modal_cats);
        $this->assign("modal_money_id", $modal_money_id);
        $this->assign("modal_state_id", $modal_state_id);
        $this->assign("modal_lang_id", $modal_lang_id);
        $this->assign("modal_colors", $modal_colors);
        $this->assign("modal_lays", $modal_lays);
        $this->assign("modal_lang", $modal_lang);
        $this->display();
    }

    public function detail() {
        $templatesModel = new \Tian\Model\ModalsModel();
        $id = I('get.id', '0', 'int');
        if ($id > 0) {
            $detail = M('pintuan')->where("id =" . $id . "")->find();
            if (empty($detail)) {
                $this->error("不存在");
            }
            $jsons = json_decode($detail['content'], true);
            $parameters = json_decode($detail['parameters'], true);
            $detail['logo_url'] = getModalsLogo($id, 'middle', 'pintuan');
        } else {
            $detail['is_show'] = 1;
            $detail['points'] = 20;

            $detail['logo_big'] = 1;
        }
        if (empty($jsons)) {
            $jsons = array(
                0 => array(
                    "content" => "",
                    "html" => ""
                )
            );
        }
        if (empty($parameters)) {
            $parameters = array(
                0 => array(
                    "paras" => ""
                )
            );
        }
        if (!empty($detail['tags'])) {
            $tagsArr = explode(",", $detail['tags']);
            foreach ($tagsArr as $v) {
                $info = M("pintuan_tags")->where("id = " . $v . "")->find();
                $tags[] = $info['name'];
            }
        } else {
            $tags = array(
                0 => "",
                1 => "",
                2 => "",
                3 => "",
                4 => ""
            );
        }
        $modals_cat = $templatesModel->getPintuanDictionary('modal_cat_id'); //拼团分类
        foreach ($modals_cat as $k => $v) {
            $modals_cat[$k]['sub'] = M("pintuan_dictionary")->where("pid = " . $v['id'] . " AND is_check = 1")->order("ord ASC")->select();
        }
        $modals_money = $templatesModel->getPintuanDictionary('modal_money_id'); //拼团金额
        $modals_state = $templatesModel->getPintuanDictionary('modal_state_id'); //拼团状态
        $modals_development = $templatesModel->getPintuanDictionary('modal_development_id'); //拼团语言

        $next = M('pintuan')->field("name,id")->where("id > " . $id . "")->order("id ASC")->find();
        $prev = M('pintuan')->field("name,id")->where("id < " . $id . "")->order("id DESC")->find();
        $this->assign("next", $next);
        $this->assign("prev", $prev);
        $this->assign("modals_cat", $modals_cat);
        $this->assign("modals_money", $modals_money);
        $this->assign("modals_state", $modals_state);
        $this->assign("modals_development", $modals_development);
        $this->assign("detail", $detail);
        $this->assign("mtype", 'modals');



        $this->assign("jsons", $jsons);
        $this->assign("parameters", $parameters);
        $this->assign("tags", $tags);
        $this->display();
    }

    public function detail_post() {

        $id = I('post.id', '', 'int');
        if ($id == 0) {
            $data['uid'] = session("admin_uid");
            $data['is_check'] = 1;
            $data['addtime'] = time();
            $id = M('pintuan')->add($data);
        }
        $file_path = "pintuans/" . getFileBei($id) . $id . "/";

        checkDirExists($file_path);


        $data['name'] = trim(I("post.name"));
        $data['name_short'] = trim(I("post.name_short"));
        $data['cat_id'] = $_POST['cat_id'] ? implode(",", $_POST['cat_id']) : "";
        $data['cat_sub_id'] = I("post.cat_sub_id") ? implode(",", I("post.cat_sub_id")) : "";
        $data['money_id'] = I("post.modal_money_id") ? implode(",", I("post.modal_money_id")) : "";
        $data['state_id'] = I("post.modal_state_id", 0, 'int');

        $data['development_id'] = I("post.modal_development_id", 0, 'int');
        $data['ord'] = I("post.ord", 0, 'int');
        $data['points_type'] = 1;
            $data['is_original'] = 1;
        $data['points'] = I("post.points", 0, 'int');
        $data['points_pintuan'] = I("post.points_pintuan", 0, 'int');
        $data['points_market'] = I("post.points_market", 0, 'int');
        $data['points_pinhas'] = I("post.points_pinhas", 0, 'int');
        $data['is_over'] = I("post.is_over", 0, 'int');
        $data['content_first'] = I("post.content_first");
        $data['details'] = $_POST['details'];
        /*         * **内容**** */
        $json = array();
        $contents = $_POST['contents'];
        $types = $_POST['types'];
        $pics = uploads_pics(I("post.pics"));
        foreach ($contents as $k => $v) {
            if ($v) {
                $json[$k]['content'] = trim($v);
                $json[$k]['type'] = $types[$k];
                $json[$k]['pics'] = $pics[$k];
            }
        }
        $data['content'] = json_encode($json);
        
        /*         * **参数**** */
        $paras = $_POST['paras'];
        $descriptions = $_POST['descriptions'];
        $defaults = $_POST['defaults'];
        $urls = $_POST['urls'];
        foreach ($paras as $k => $v) {
            if ($v) {
                $parameters[$k]['paras'] = trim(htmlspecialchars($v));
                $parameters[$k]['descriptions'] = trim($descriptions[$k]);
                $parameters[$k]['defaults'] = trim(htmlspecialchars($defaults[$k]));
                $parameters[$k]['urls'] = trim(htmlspecialchars($urls[$k]));
            }
        }
        $data['parameters'] = json_encode($parameters);
        $data['keywords'] = I("post.keywords");
        $data['description'] = trim($_POST['description']);
        $data['source'] = $_POST['source'];
        $data['demo_url'] = I("post.demo_url");
        $data['url_wap'] = I("post.url_wap");
        $data['remark'] = I("post.remark");
        $data['is_short_time'] = I("post.is_short_time");
        $data['starttime'] = strtotime(I("post.starttime"));
        $data['endtime'] = strtotime(I("post.endtime"));
        $data['tags'] = transferTagsIds($_POST['tag'], 50, 1);
    $data['remark'] = I("post.remark");
        $data['is_check'] = I("post.is_check", 0, 'int');
        $data['is_recommend'] = I("post.is_recommend", 0, 'int');
        $logo_middle = $file_path . "middle.jpg";

        if (I("post.logo_middle")) {
            rename(I("post.logo_middle"), $logo_middle);
        }
        $logo_big = $file_path . "big.jpg";

        if (I("post.logo_big")) {
            rename(I("post.logo_big"), $logo_big);
        }
        $data['wangpan_url'] = I("post.wangpan_url");
        if (I("post.wangpan_pwd")) {
            $data['wangpan_pwd'] = getWangpanPwd(I("post.wangpan_pwd"));
//            echo $data['wangpan_pwd'];exit;
        }
        if ($id > 0) {


            M('pintuan')->where("id = " . $id . "")->save($data);
        }

        if ($id > 0) {
            $this->success('修改成功！', U("Pintuan/detail", array("id" => $id)));
        } else {
            $this->success('添加成功！', U('Pintuan/lists'));
        }
    }

    public function cat() {//拼团类别
        $pid = I('get.pid', '0', 'int');
        $sql = "pid = " . $pid . "";
        $keywords = trim(I('get.keywords'));
        if ($keywords) {
            $sql .= " AND name like '%" . $keywords . "%'";
        }
        $count = M('pintuan_dictionary')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('pintuan_dictionary')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('ord ASC')->select();
        foreach ($lists as $k => $v) {
            $lists[$k]['num'] = M('pintuan_dictionary')->where("pid = " . $v['id'] . "")->count(); //下级个数
            if ($v['tags']) {
                $lists[$k]['tags_num'] = count(explode(",", $v['tags']));
            } else {
                $lists[$k]['tags_num'] = 0;
            }
        }
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->assign("pid", $pid);
        $this->display();
    }

    public function cat_detail() {//拼团类别详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('pintuan_dictionary')->where("id = " . $id . "")->find();
        } else {
            $detail['pid'] = I("get.pid", 0, 'int');
            $detail['is_check'] = 1;
        }
        $cats_top = M('pintuan_dictionary')->where("pid = 0")->order('ord ASC')->select();
        foreach ($cats_top as $k => $v) {
            $cats_top[$k]['second'] = M('pintuan_dictionary')->where("pid = " . $v['id'] . "")->order('ord ASC')->select();
        }
        $this->assign("detail", $detail);
        $this->assign("cats_top", $cats_top);
        $this->display();
    }

    public function cat_detail_post() {

        $id = I("post.id", 0, 'int');
        $data['pid'] = I("post.pid", 0, 'int');
        $data['is_check'] = I("post.is_check", 0, 'int');

        $pinfo = M('pintuan_dictionary')->field("pid")->where("id = " . $data['pid'] . "")->find();
        if ($pinfo['pid'] > 0) {
            $level = 2;
        } else if ($pinfo['pid'] == 0) {
            $level = 1;
        } else {
            $level = 0;
        }
        $data['level'] = $level;
        $data['seo_title'] = trim(I("post.seo_title"));
        $data['keywords'] = trim(I("post.keywords"));
        $data['description'] = trim(I("post.description"));
        if ($id > 0) {
            $data['name'] = trim(I("post.name"));
            $data['ord'] = I("post.ord", 0, 'int');
            M('pintuan_dictionary')->where("id = " . $id . "")->save($data);
            $this->success("修改拼团类别成功！", session('QUERY_STRING'));
        } else {
            $names = array_unique(array_filter(I('post.name')));
            foreach ($names as $k => $v) {
                if ($v) {
                    $data['name'] = $v;
                    $data['ord'] = I('post.ord', '30', 'int') + $k;
                    M('pintuan_dictionary')->add($data);
                }
            }
            $this->success("添加拼团类别成功！", U("Pintuan/cat", array("pid" => $data['pid'])));
        }
    }

    public function tags() {
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND name like '%" . $keyword . "%'";
        }
        $count = M('pintuan_tags')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('pintuan_tags')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('ord ASC,id DESC')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    public function tags_detail() {//标签详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('pintuan_tags')->where("id = " . $id . "")->find();
        } else {
            $detail['pid'] = I("get.pid", 0, 'int');
            $detail['is_check'] = 1;
        }
        $this->assign("detail", $detail);
        $this->display();
    }

    public function tags_detail_post() {
        $id = I("post.id", 0, 'int');
        $data['is_check'] = I("post.is_check", 0, 'int');
        if ($id > 0) {
            $data['name'] = trim(I("post.name"));
            $data['ord'] = I("post.ord", 0, 'int');
            $data['keywords'] = strtolower(trim(I("post.keywords")));
            $data['description'] = strtolower(trim(I("post.description")));
            M('pintuan_tags')->where("id = " . $id . "")->save($data);
            $this->success("修改拼团标签成功！", session('QUERY_STRING'));
        } else {
            $names = array_unique(array_filter(I('post.name')));
            foreach ($names as $k => $v) {
                if ($v) {
                    $data['name'] = $v;
                    $data['ord'] = I('post.ord', '30', 'int') + $k;
                    M('pintuan_tags')->add($data);
                }
            }
            $this->success("添加拼团标签成功！", U("Pintuan/tags"));
        }
    }

}

?>
    