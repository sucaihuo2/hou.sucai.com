<?php

namespace Tian\Controller;


class ServersController extends CommonController {
   //套餐类别
    public function cats() {
         $s_admin_uid = getAdminId();
         $sql = "1=1";
        
       if($s_admin_uid != 1){
           $sql .= " AND uid = ".$s_admin_uid."";
       }
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND (name like '%" . $keyword . "%' or keywords like '%" . $keyword . "%')";
        }
        $is_check = I("get.is_check");
        if ($is_check >= 0 && $is_check != '') {
            $sql .= " AND is_check = " . $is_check . "";
        }

        $count = M('servers')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, 20);
        $lists = M('servers')->field('id,name,cpu,memory,disk,ip,wide,price,price_orginal,price_cost,ord')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    //套餐编辑
    public function cats_detail(){
        $id = I('get.id', '0', 'int');
        if ($id > 0) {
            $detail = M('service')->where("id =" . $id . "")->find();
            if (empty($detail)) {
                $this->error("不存在");
            }
            $content_problems = json_decode($detail['content_problems'], true);
        }

        $cats = M("service_dictionary")->field("id,name")->where("is_check=1")->order("ord ASC")->select(); //服务分类
        $this->assign("cats", $cats);
        $this->assign("detail", $detail);
        $this->assign("mtype", 'service');
        $this->assign("content_problems", $content_problems);
        $this->display();

    }
     //服务器订单
     public function orders(){
         $sql  = "time_pay>0";
         $count = M('servers_order')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, 30);
        $lists = M('servers_order')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
         $this->display();
     }

    //服务器信息列表
    public function lists() {
        $s_admin_uid = getAdminId();
        $sql = "1=1";

        if($s_admin_uid != 1){
            $sql .= " AND uid = ".$s_admin_uid."";
        }
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND (name like '%" . $keyword . "%' or keywords like '%" . $keyword . "%')";
        }
        $is_check = I("get.is_check");
        if ($is_check >= 0 && $is_check != '') {
            $sql .= " AND is_check = " . $is_check . "";
        }

        $count = M('servers_list')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, 20);
        $lists = M('servers_list')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    //添加服务器信息
    public function servers_detail(){
        $id = I('get.id', '0', 'int');
        if ($id > 0) {
            $detail = M('servers_list')->where("id =" . $id . "")->find();
            if (empty($detail)) {
                $this->error("不存在");
            }
            $content_problems = json_decode($detail['content_problems'], true);
        }
        $this->assign("detail", $detail);
        $this->assign("mtype", 'service');
        $this->assign("content_problems", $content_problems);
        $this->display();
    }

    //服务器信息提交
    public function servers_detail_post(){
        $id = I('post.id', '', 'int');
        if ($id) {
            $data['id'] = $id;
        }
        $data['ip'] = I("post.ip");
        $data['account'] = I("post.account");
        $data['source'] = I("post.source");
        $data['initial_password'] = I("post.initial_password");
        $data['servers_type'] = I("post.servers_type");
        $data['qq'] = I("post.qq");
        $data['mobile'] = I("post.mobile");
        if(!empty(I("post.start_time")) && !empty(I("post.start_time"))){
            $data['start_time'] = strtotime(I("post.start_time"));
            $data['end_time'] = strtotime(I("post.end_time"));
        }


        if ($id > 0) {
            $res = M('servers_list')->where("id = " . $id . "")->save($data);
            if ($res) {
                $this->success('修改成功！', U("Servers/lists"));
            } else {
                $this->error('修改失败！', U("Servers/servers_detail", array("id" => $id)));
            }
        } else {
            $res = M('servers_list')->add($data);
            if ($res) {
                $this->success('添加成功！', U('Servers/lists'));
            } else {
                $this->error('添加失败！', U('Servers/servers_detail'));
            }
        }
    }

    public function detail() {
        $id = I('get.id', '0', 'int');
        if ($id > 0) {
            $detail = M('service')->where("id =" . $id . "")->find();
            if (empty($detail)) {
                $this->error("不存在");
            }
            $content_problems = json_decode($detail['content_problems'], true);
        }
        $cats = M("service_dictionary")->field("id,name")->where("is_check=1")->order("ord ASC")->select(); //服务分类
        $this->assign("cats", $cats);
        $this->assign("detail", $detail);
        $this->assign("mtype", 'service');
        $this->assign("content_problems", $content_problems);
        $this->display();
    }

   //服务提交
    public function detail_post()
    {
        $id = I('post.id', '', 'int');
        if ($id) {
            $data['id'] = $id;
        }
        $file_path = "uploads/service/" . getFileBei($id) . $id . "/";
           checkDirExists($file_path);
        $data['title'] = I("post.title");
        $data['cat_id'] = I("post.cat_id");

        
        $data['source'] = I("post.source");
        $data['is_check'] = I("post.is_check");
        $data['keywords'] = I("post.keywords");
        $data['description'] = I("post.description");
        $data['content_goods'] = I("post.content_goods");
        $data['content_cases'] = I("post.content_cases");
        $faq = I("post.faq");
        if (!empty($faq)) {
            $data['content_problems'] = json_encode($faq, true);
        }
        $data['logo'] = $file_path . "big.jpg";

        if (I("post.logo_big") && I("post.logo_big") != $data['logo']) {
            rename(I("post.logo_big"), $data['logo']);
        }
        if ($id > 0) {
            $res = M('service')->where("id = " . $id . "")->save($data);
            if ($res) {
                $this->success('修改成功！', U("Service/lists"));
            } else {
                $this->error('修改失败！', U("Service/detail", array("id" => $id)));
            }

        } else {
            $res = M('service')->add($data);
            if ($res) {
                $this->success('添加成功！', U('Service/lists'));
            } else {
                $this->error('添加失败！', U('Service/detail'));
            }
        }
    }
    //根据服务ID关联套餐列表
    public function servers_list(){
          $id = I('get.id', '', 'int');
          if($id){
              $res = M('servers_list')->where("id = " . $id . "")->select();
              $this->assign('servers_list', $res);
              $this->assign('service_id', $id);
          }else{
              echo "调用失败";
          }
          $this->display();
    }
    //创建套餐
    public function create_packages(){
        $pid = I('post.pid');
        if(!empty($pid)){
            $data['name'] = I('post.name');
            $data['ord'] = I('post.ord');
            $data['price'] = I('post.price');
            $data['pid'] = I('post.pid');
            $data['is_check'] = 1; //默认开启
            $res = M('service_taocan')->add($data);
            if(!empty($res)){
                $this->packages_section($pid);
                $msg = array(
                    'code' => 200,
                    'mag' =>"添加成功"
                );
            }else{
                $msg = array(
                    'code' => 400,
                    'mag' =>"添加失败"
                );
            }
            $this->ajaxReturn($msg);
        }else{
            $msg = array(
                'code' => 400,
                'mag' =>"添加失败"
            );
            $this->ajaxReturn($msg);
        }
    }
    //编辑套餐
    public function edit_packages(){
        $pid = I('post.pid');
        $id = I('post.id');
        if(!empty($pid) && !empty($id)){
            $data['name'] = I('post.name');
            $data['ord'] = I('post.ord');
            $data['price'] = I('post.price');
            $w = array(
               'pid' => $pid,
               'id' => $id
            );
            $res = M('service_taocan')->where($w)->save($data);
            if(!empty($res)){
                $this->packages_section($pid);
                $msg = array(
                    'code' => 200,
                    'mag' =>"修改成功"
                );
            }else{
                $msg = array(
                    'code' => 400,
                    'mag' =>"修改失败"
                );
            }
            $this->ajaxReturn($msg);
        }else{
            $msg = array(
                'code' => 400,
                'mag' =>"修改失败"
            );
            $this->ajaxReturn($msg);
        }
    }



    public function orderdostatus(){

        $id = I('post.id');
        $do_status = I('post.do_status');

        if(!empty($id)){
            $w = array(
                'id' => $id
            );
            $data['do_status'] = $do_status;
            $res = M('service_orders')->where($w)->save($data);
            if(!empty($res)){
                $msg = array(
                    'code' => 200,
                    'mag' =>"修改成功"
                );
            }else{
                $msg = array(
                    'code' => 400,
                    'mag' =>"修改失败"
                );
            }
            $this->ajaxReturn($msg);
        }else{
            $msg = array(
                'code' => 400,
                'mag' =>"修改失败"
            );
            $this->ajaxReturn($msg);
        }
    }
    //处理人分配
    public function update_servers_id(){
        $id = I('post.id');
        $servers_id = I('post.servers_id');

        if(!empty($id)){
            $w = array(
                'id' => $id
            );
            $data['servers_id'] = $servers_id;
            $res = M('servers_order')->where($w)->save($data);
            if(!empty($res)){
                $msg = array(
                    'code' => 200,
                    'mag' =>"修改成功"
                );
            }else{
                $msg = array(
                    'code' => 400,
                    'mag' =>"修改失败"
                );
            }
            $this->ajaxReturn($msg);
        }else{
            $msg = array(
                'code' => 400,
                'mag' =>"修改失败"
            );
            $this->ajaxReturn($msg);
        }
    }

}

?>
    