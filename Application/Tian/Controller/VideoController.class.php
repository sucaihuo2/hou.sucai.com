<?php

namespace Tian\Controller;

use PclZip;

class VideoController extends CommonController {

    public function lists() {
//         M("video")->where("id>0")->save(array("times_view" => 0,'times_download'=>0));
//         $addtime = time()-3600*24*90;
//           M("download")->where("id>0 and mtype = 30 AND addtime<".$addtime."")->delete();
//            M("points")->where("id>0 and mtype = 'video' AND addtime<".$addtime."")->delete();
//           
//            M("collects")->where("id>0 and mtype = 30")->delete();
         
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));                                                              
        if (!empty($keyword)) {
            $sql .= " AND (name like '%" . $keyword . "%' or keywords like '%" . $keyword . "%')";
        }
        $modal_cat_id = I('get.modal_cat_id', 0, 'int'); //分类筛选
        if ($modal_cat_id > 0) {
            $sql .= " AND cat_id = " . $modal_cat_id . "";
        }
        $is_download = I('get.is_download', 0, 'int'); //是否可下载筛选
        if ($is_download !='' && in_array($is_download,array(0,-1))) {
            $sql .= " AND is_download = " . $is_download . "";
        }
        if (is_numeric($keyword) == 1) {
            $sql = "id = '" . $keyword . "'";
        }

        $count = M('video')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('video')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
//        echo M('video')->getlastsql();
        foreach($lists as $k=>$v){
           // $lists[$k]['downloads'] = M("download")->field("uid,addtime,points")->where("tid = ".$v['id']." AND mtype = 30")->select();
        }
        $modal_cats = getDictionarySubSql('modal_cat_id', 'video_dictionary');
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->assign("modal_cat_id", $modal_cat_id);
        $this->assign("modal_cats", $modal_cats);
        $this->display();
    }

    public function detail() {
        $templatesModel = new \Tian\Model\ModalsModel();
        $id = I('get.id', '0', 'int');
        if ($id > 0) {
            $detail = M('video')->where("id =" . $id . "")->find();
            if (empty($detail)) {
                $this->error("不存在");
            }
            $jsons = json_decode($detail['content'], true);
            $parameters = json_decode($detail['parameters'], true);
            $detail['logo_url'] = getModalsLogo($id, 'middle', 'video');
        } else {
            $detail['is_show'] = 1;
            $detail['points'] = 20;

            $detail['logo_big'] = 1;
        }
        if (empty($jsons)) {
            $jsons = array(
                0 => array(
                    "content" => "",
                    "html" => ""
                )
            );
        }
        if (empty($parameters)) {
            $parameters = array(
                0 => array(
                    "paras" => ""
                )
            );
        }
        if (!empty($detail['tags'])) {
            $tagsArr = explode(",", $detail['tags']);
            foreach ($tagsArr as $v) {
                $info = M("video_tags")->where("id = " . $v . "")->find();
                $tags[] = $info['name'];
            }
        } else {
            $tags = array(
                0 => "",
                1 => "",
                2 => "",
                3 => "",
                4 => ""
            );
        }
        $video_cat = $templatesModel->getVideoDictionary('modal_cat_id'); //视频分类

        foreach ($video_cat as $k => $v) {
            $video_cat[$k]['sub'] = M("video_dictionary")->where("pid = " . $v['id'] . " AND is_check = 1")->order("ord ASC")->select();
        }

        $next = M('video')->field("name,id")->where("id > " . $id . "")->order("id ASC")->find();
        $prev = M('video')->field("name,id")->where("id < " . $id . "")->order("id DESC")->find();
        $this->assign("next", $next);
        $this->assign("prev", $prev);
        $this->assign("video_cat", $video_cat);
        $this->assign("detail", $detail);
        $this->assign("mtype", 'video');

          $downloads = M("download")->field("uid,addtime,points")->where("tid = " . $id . " AND mtype = 30")->order("id DESC")->select();
                $this->assign("downloads", $downloads);

        $this->assign("jsons", $jsons);
        $this->assign("parameters", $parameters);
        $this->assign("tags", $tags);
        $this->display();
    }

    public function detail_post() {

        $id = I('post.id', '', 'int');
        if ($id == 0) {
            $data['uid'] = session("admin_uid");
            $data['is_check'] = 1;
            $data['addtime'] = time();
            $id = M('video')->add($data);
        }
        $file_path = "videos/" . getFileBei($id) . $id . "/";
//        $file_path_demo = $file_path . "demo/";
        checkDirExists($file_path);
        $data['is_original'] = I("post.is_original") == 1 ? 1 : 0;
        $data['is_recommend'] = I("post.is_recommend");
        $data['name'] = trim(I("post.name"));
        $data['cat_id'] = $_POST['cat_id'] ? implode(",", $_POST['cat_id']) : "";
        $data['cat_sub_id'] = I("post.cat_sub_id") ? implode(",", I("post.cat_sub_id")) : "";

        $data['ord'] = I("post.ord", 0, 'int');
        $data['points'] = I("post.points", 0, 'int');
        $data['points_type'] = I("post.points_type", 0, 'int');
        $data['content_first'] = I("post.content_first");
        /*         * **内容**** */
        $json = array();
        $contents = $_POST['contents'];
        $types = $_POST['types'];
        $pics = uploads_pics(I("post.pics"));
        foreach ($contents as $k => $v) {
            if ($v) {
                $json[$k]['content'] = trim($v);
                $json[$k]['type'] = $types[$k];
                $json[$k]['pics'] = $pics[$k];
            }
        }
        $data['content'] = json_encode($json);
          $data['details'] = $_POST['details'];
        /*         * **参数**** */
        $paras = $_POST['paras'];
        $descriptions = $_POST['descriptions'];
        $defaults = $_POST['defaults'];
        $urls = $_POST['urls'];
        foreach ($paras as $k => $v) {
            if ($v) {
                $parameters[$k]['paras'] = trim(htmlspecialchars($v));
                $parameters[$k]['descriptions'] = trim($descriptions[$k]);
                $parameters[$k]['defaults'] = trim(htmlspecialchars($defaults[$k]));
                $parameters[$k]['urls'] = trim(htmlspecialchars($urls[$k]));
            }
        }
        $data['parameters'] = json_encode($parameters);
        $data['keywords'] = I("post.keywords");
        $data['description'] = trim($_POST['description']);
        $data['logo_big'] = I("post.logo_big");
        $data['video'] = $_POST['video'];
        $data['remark'] = I("post.remark");
        $data['is_download'] = I("post.is_download");
        
        $cat_id = $_POST['cat_id'];
        if (count($cat_id) == 1 && $cat_id[0] == 15) {
            $data['type'] = 1;
        } else {
            $data['type'] = 0;
        }
        $data['tags'] = transferTagsIds($_POST['tag'], 30, 1);

        $data['is_check'] = I("post.is_check", 0, 'int');
        $data['is_recommend'] = I("post.is_recommend", 0, 'int');
        $zip_real = $file_path . $data['name'] . ".zip";
        if (I("post.zip") && I("post.zip") != $zip_real) {
            rename(I("post.zip"), $zip_real);
        }

        $data['logo_big'] = $file_path . "big.jpg";
        $data['wangpan_url'] = I("post.wangpan_url");
        if (I("post.wangpan_pwd")) {
            $data['wangpan_pwd'] = getWangpanPwd(I("post.wangpan_pwd"));
//            echo $data['wangpan_pwd'];exit;
        }
        if (I("post.logo_big") && I("post.logo_big") != $data['logo_big']) {
            rename(I("post.logo_big"), $data['logo_big']);
            $height = getMiddleLogo($data['logo_big']);
            if ($height > 0) {
                M('video')->where("id = " . $id . "")->save(array("height" => $height));
            }
        }
        
        if ($id > 0) {
            M('video')->where("id = " . $id . "")->save($data);
//            echo M('video')->getlastsql();exit;
//            $state = $data['is_check'] == 0 ? 2 : 1;
//            M("temp")->where("original_id = " . $id . " AND mtype =50")->save(array("state" => $state));
        } else {
            
        }
        include_once 'common/detail_post_zip.php';

        //解压压缩文件over
//        getTagsNum(2); //统计标签
        
        
        
        if ($id > 0) {
            deleteHtmlFile($id, 30); //删除缓存
            $this->success('修改成功！', U("Video/detail", array("id" => $id)));
        } else {
            $this->success('添加成功！', U('Video/lists'));
        }
    }
     public function cat() {//视频类别
        $pid = I('get.pid', '0', 'int');
        $sql = "pid = " . $pid . "";
        $keywords = trim(I('get.keywords'));
        if ($keywords) {
            $sql .= " AND name like '%" . $keywords . "%'";
        }
        $count = M('video_dictionary')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('video_dictionary')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('ord ASC')->select();
    
        foreach ($lists as $k => $v) {
            $lists[$k]['num'] = M('video_dictionary')->where("pid = " . $v['id'] . "")->count(); //下级个数
            if ($v['tags']) {
                $lists[$k]['tags_num'] = count(explode(",", $v['tags']));
            } else {
                $lists[$k]['tags_num'] = 0;
            }
        }
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->assign("pid", $pid);
        $this->display();
    }

    public function cat_detail() {//视频类别详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('video_dictionary')->where("id = " . $id . "")->find();
        } else {
            $detail['pid'] = I("get.pid", 0, 'int');
            $detail['is_check'] = 1;
        }
        $cats_top = M('video_dictionary')->where("pid = 0")->order('ord ASC')->select();
        foreach ($cats_top as $k => $v) {
            $cats_top[$k]['second'] = M('video_dictionary')->where("pid = " . $v['id'] . "")->order('ord ASC')->select();
        }
        $this->assign("detail", $detail);
        $this->assign("cats_top", $cats_top);
        $this->display();
    }

    public function cat_detail_post() {

        $id = I("post.id", 0, 'int');
        $data['pid'] = I("post.pid", 0, 'int');
        $data['is_check'] = I("post.is_check", 0, 'int');

        $pinfo = M('video_dictionary')->field("pid")->where("id = " . $data['pid'] . "")->find();
        if ($pinfo['pid'] > 0) {
            $level = 2;
        } else if ($pinfo['pid'] == 0) {
            $level = 1;
        } else {
            $level = 0;
        }
        $data['level'] = $level;
                 $data['seo_title'] = trim(I("post.seo_title"));
        $data['keywords'] = trim(I("post.keywords"));
        $data['description'] = trim(I("post.description"));
        if ($id > 0) {
            $data['name'] = trim(I("post.name"));
            $data['ord'] = I("post.ord", 0, 'int');
            M('video_dictionary')->where("id = " . $id . "")->save($data);
            $this->success("修改视频类别成功！", session('QUERY_STRING'));
        } else {
            $names = array_unique(array_filter(I('post.name')));
            foreach ($names as $k => $v) {
                if ($v) {
                    $data['name'] = $v;
                    $data['ord'] = I('post.ord', '30', 'int') + $k;
                    M('video_dictionary')->add($data);
                }
            }
            $this->success("添加视频类别成功！", U("Video/cat", array("pid" => $data['pid'])));
        }
    }

    public function tags() {
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND name like '%" . $keyword . "%'";
        }
        $count = M('video_tags')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, 500);
        $lists = M('video_tags')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('ord ASC,id DESC')->select();
        foreach($lists as $k=>$v){
           $num =  M('video')->where("FIND_IN_SET('" . $v['id'] . "',tags)")->count();
            $lists[$k]['num'] = $num;
            M('video_tags')->where("id = '" . $v['id'] . "'")->save(array("num"=>$num));
        }
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    public function tags_detail() {//标签详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('video_tags')->where("id = " . $id . "")->find();
        } else {
            $detail['pid'] = I("get.pid", 0, 'int');
            $detail['is_check'] = 1;
        }
        $this->assign("detail", $detail);
        $this->display();
    }

    public function tags_detail_post() {
        $id = I("post.id", 0, 'int');
        $data['is_check'] = I("post.is_check", 0, 'int');
        if ($id > 0) {
            $data['name'] = trim(I("post.name"));
            $data['ord'] = I("post.ord", 0, 'int');
            $data['keywords'] = strtolower(trim(I("post.keywords")));
            $data['description'] = strtolower(trim(I("post.description")));
            M('video_tags')->where("id = " . $id . "")->save($data);
            $this->success("修改视频标签成功！", session('QUERY_STRING'));
        } else {
            $names = array_unique(array_filter(I('post.name')));
            foreach ($names as $k => $v) {
                if ($v) {
                    $data['name'] = $v;
                    $data['ord'] = I('post.ord', '30', 'int') + $k;
                    M('video_tags')->add($data);
                }
            }
            $this->success("添加视频标签成功！", U("Source/tags"));
        }
    }


}

?>
    