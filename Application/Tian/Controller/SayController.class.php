<?php
namespace Tian\Controller;
class SayController extends CommonController {

    public function lists() {
        $sql = "1=1";
        
        $keywords = trim(I('get.keywords'));
        if ($keywords) {
            $sql .= " AND (content like '%" . $keywords . "%' or uid = '".$keywords."')";
        }
        
    
       $count = M("comment")->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count,C("pagenum"));
        $lists = M("comment")->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id desc')->select();
        foreach($lists as $k=>$v){
//          $lists[$k]['num'] = M("comment")->where("pid = ".$v['id']."")->count();
             $tinfo = getMtypeInfo($v['tid'], $v['mtype']);
            $lists[$k]['tname'] = $tinfo['name'];
                   $lists[$k]['thref'] = getMtypeHref($v['tid'], $v['mtype']);
        }
//       echo M("comment")->getLastSql();
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->display();
    }

    public function detail() {
        $id = I('get.id', '', 'int');
        $detail = M("comment")->where("id = " . $id . "")->find();
        $lists = M("comment")->where("pid = " . $id . "")->order('id desc')->select();
       
        $this->assign("lists", $lists);
        $this->assign("num", count($lists));
        $this->assign("detail", $detail);
        $this->display();
    }

    public function detail_post() {
        $id = I('post.id', '', 'int');

        $data['content'] = I('post.content');
        if ($id > 0) {
            M("comment")->where("id = " . $id . "")->save($data);
            $this->success('修改成功！',session('QUERY_STRING'));
        }
    }
    public function topic() {
        $sql = "1=1";
        
        $keywords = trim(I('get.keywords'));
        if ($keywords) {
            $sql .= " AND (content like '%" . $keywords . "%' or uid = '".$keywords."')";
        }
        
    
       $count = M("topic")->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count,C("pagenum"));
        $lists = M("topic")->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id desc')->select();
        foreach($lists as $k=>$v){
        }
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->display();
    }
     public function log() {
        $sql = "mtype =15";
        
        $keywords = trim(I('get.keywords'));
        if ($keywords) {
            $sql .= " AND (title like '%" . $keywords . "%')";
        }
        
    
       $count = M("log")->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count,C("pagenum"));
        $lists = M("log")->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id desc')->select();
       
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->display();
    }
}

?>
