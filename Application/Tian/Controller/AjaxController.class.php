<?php

namespace Tian\Controller;

use Think\Controller;
use QueryList;

class AjaxController extends Controller {

    public function update_cache() {
        $id = I("post.id");
        $mtype = I("post.mtype") ? I("post.mtype") : 15;
        $rs = getUrlJson("https://www.sucaihuo.com/Ajax/front_cache/id/" . $id . "/mtype/" . $mtype . "");
        echo $rs;
    }

    public function delPic() {
        $id = I("post.id");
        $table = I("post.table");
        $field = I("post.field");
        M($table)->where("id = " . $id . "")->save(array($field => ''));
    }

    public function changeOrdConfirm() {
        $field = I("post.field") ? I("post.field") : "ord";
        if ($field == 'undefined') {
            $field = 'ord';
        }
        if (session("admin_uid") == '') {
            echo -1;
            exit;
        }
//        print_r($_POST);
//        echo $field;
        if (!in_array($field, array("ord", "times_download", "times_view", "times_pintuan", "points_pinhas", "source"))) {
            echo 'no_field';
            exit;
        }
        $table = I("post.table");
        $id = I("post.id");
        $data[$field] = I("post.ord");

        M($table)->where("id = " . $id . "")->save($data);
//        isClearTempFile($table);
    }

    public function changeFieldConfirm() {
        $table = I("post.table");
        $id = I("post.id");
        $data[I("post.field")] = I("post.val");
        M($table)->where("id = " . $id . "")->save($data);
    }

    public function more_del() {
        $week_start = time() - 3600 * 24 * 7;
        $week_end = time();
        if (session("admin_uid") > 0) {
            $ids = I("post.ids");
            $is_delete = I("post.is_delete", 0, 'int');
            if (!empty($ids)) {
                $ids = array_filter(explode(",", $ids));
                $table = I("post.table");
                if ($table == 'source') {
                    foreach ($ids as $v) {
                        M("download")->where("tid = " . $v . " and mtype = 15")->delete();
                        M("points")->where("tid = " . $v . " and mtype = 'source'")->delete();
                        M("comment")->where("tid = " . $v . " and mtype = 15")->delete();
                        M("collects")->where("tid = " . $v . " and mtype = 15")->delete();
                        M("source")->where("id = " . $v . "")->delete();
                        M("browse")->where("tid = " . $v . "")->delete();
                    }
                } else {
                    foreach ($ids as $v) {
                        if ($is_delete == -1) {
                            M($table)->where("id = " . $v . "")->limit(1)->save(array("is_deleted" => -1));
                        } else if ($is_delete == 1) {
                            M($table)->where("id = " . $v . "")->limit(1)->save(array("is_deleted" => 0));
                        }else if ($is_delete == 2) {
                            M($table)->where("id = " . $v . "")->save(array("is_delete" => 1));
                        } else {
//                        echo $table;exit;
                            if ($table == 'topic') {
                                M($table)->where("id = " . $v . " AND addtime between " . $week_start . " AND " . $week_end . "")->limit(1)->delete();
                            } elseif ($table == 'hours') {
                                M($table)->where("id = " . $v . " AND status = 0")->limit(1)->delete();
                            } elseif ($table == 'taobao_collect') {
                              M($table)->where("id = " . $v . "")->limit(1)->save(array("is_deleted" =>1));
                            } else {
                                M($table)->where("id = " . $v . "")->limit(1)->delete();
                            }
                        }
                    
                    }
                }

//                isClearTempFile($table);
            }
        }
    }

    public function yes() {
        $id = I("post.id", '0', 'int');
        $field = I("post.field") ? I("post.field") : "is_check";
        $table = I('post.table');
        $info = M($table)->where("id = " . $id . "")->find();
        if ($info[$field] == 1) {
            $data[$field] = 0;
            echo $word = '否';
        } else {
            $data[$field] = 1;
            echo $word = '是';
        }
        M($table)->where("id = " . $id . "")->save($data);
    }

    public function yes_url_logo() {
        $id = I("post.id", '0', 'int');
        $data['url_logo'] = 0;
//         $info = M("source")->where("id = " . $id . "")->find();
//         if ($info['url_logo'] == 1) {
//            $data['url_logo'] = 0;
//            echo $word = '否';
//        } else {
//            $data['url_logo'] = 1;
//            echo $word = '是';
//        }
        M("source")->where("id = " . $id . "")->save($data);
        echo M("source")->getlastsql();
    }

    public function no() {
        $id = I("post.id", '0', 'int');
        $field = I("post.field") ? I("post.field") : "is_check";
        $table = I('post.table');
        $info = M($table)->where("id = " . $id . "")->find();
        if ($info[$field] == 1) {
            $data[$field] = 0;
            echo $word = '是';
        } else {
            if ($field == 'is_download') {
                $data[$field] = -1;
            } else {
                $data[$field] = 1;
            }

            echo $word = '否';
        }

        M($table)->where("id = " . $id . "")->save($data);
    }

    public function no_download() {
        $id = I("post.id", '0', 'int');
        $field = "is_download";
        $table = I('post.table');
        $info = M($table)->where("id = " . $id . "")->find();
        if ($info[$field] == -1) {
            $data[$field] = 0;
            echo $word = '是';
        } else {
            $data[$field] = -1;

            echo $word = '否';
        }

        M($table)->where("id = " . $id . "")->save($data);
    }

    public function check_name() {
        $name = I('post.name');
        $id = I('get.id', '', 'int');
        if ($id > 0) {
            $sql = " AND id != " . $id . "";
        }
        $info = M("user")->where("name='" . $name . "' " . $sql . "")->find();
        if (empty($info)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function check_email() {
        $email = I('post.email');
        $id = I('get.id', '', 'int');
        if ($id > 0) {
            $sql = " AND id != " . $id . "";
        }
        $info = M("user")->where("email='" . $email . "' " . $sql . "")->find();
        if (empty($info)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function check_mobile() {
        $phone = I('post.phone');
        $id = I('get.id', '', 'int');
        if ($id > 0) {
            $sql = " AND id != " . $id . "";
        }
        $info = M("user")->where("phone='" . $phone . "' " . $sql . "")->find();
        if (empty($info)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function clearTempFile() {
        clearTempFile();
    }

    public function checkState() {
        $id = I("post.id", 0, 'int');
        $state = I("post.state", 0, 'int');
        $table = I("post.table");
        $reason = I("post.reason");
        M($table)->where("id = " . $id . "")->save(array("state" => $state, "reason" => $reason, "checktime" => time()));
//        echo M($table)->getlastsql();
    }

    public function getCatTags() {
        $mtype = I("get.mtype") ? I("get.mtype") : "modals";
        $type = I("get.type") ? I("get.type") : "radio";
        $id = I("get.id", 0, 'int');
        $cat_id = I("get.cat_id", 0, 'int');
        $cat_sub_id = I("get.cat_sub_id");
        $tags_return = '';
        $dictionary = M("" . $mtype . "_dictionary")->field("tags,pid")->where("id = " . $cat_id . "")->find();
        $detail = M($mtype)->field("tags")->where("id = " . $id . "")->find();
        if ($dictionary['tags']) {
            $tagsArr = M("" . $mtype . "_tags")->field("id,name")->where("id in (" . $dictionary['tags'] . ")")->select();
            foreach ($tagsArr as $v) {
                $check = '';
                if ($detail['tags']) {
                    if (in_array($v['id'], explode(",", $detail['tags']))) {
                        $check = 'checked';
                    }
                }
                $tags_return .="<label style='margin-right:6px;'><input type='checkbox' name='tags[]' value=" . $v['id'] . " " . $check . "> " . $v['name'] . "</label>";
            }
        }

        $cats_sub = M("" . $mtype . "_dictionary")->field("id,name")->where("pid = " . $cat_id . "")->select();
        $cat_sub_return = '';
        foreach ($cats_sub as $v) {
            $check = '';
            if ($type == 'checkbox') {
                $cat_sub_idArr = explode(",", $cat_sub_id);
                if (in_array($v['id'], $cat_sub_idArr)) {
                    $check = 'checked';
                }
                $name = 'cat_sub_id[]';
            } else {
                if ($cat_sub_id == $v['id']) {
                    $check = 'checked';
                }
                $name = 'cat_sub_id';
            }
            $cat_sub_return .="<label style='margin-right:6px;'><input type='" . $type . "' name='" . $name . "' value=" . $v['id'] . " " . $check . "> " . $v['name'] . "</label>";
        }
        $arr = array("tags_return" => $tags_return, "cat_sub_return" => $cat_sub_return);
        echo json_encode($arr);
    }

    public function hideUrl() {
        $id = I("get.id");
        $code = I("get.code");
        $is_show = I("get.is_show", 0, 'int');
        $detail = M('website')->where("id = " . $id . "")->find();
        $jsons = json_decode($detail['json'], true);
        foreach ($jsons as $k => $v) {
            if ($v['codes'] == $code) {
                $jsons[$k]['is_show'] = $is_show;
            }
        }
        $data['json'] = json_encode($jsons);
        M('website')->where("id = " . $id . "")->save($data);
        echo M('website')->getlastsql();
    }

    public function sitemapCat() {
        $sitemap_cat = trim(I("get.sitemap_cat"));
        $id = I("get.id");
        M('modals')->where("id = " . $id . "")->save(array("sitemap_cat" => $sitemap_cat));
        echo M('modals')->getlastsql();
    }

    public function getSeo() {
        header("Content-type: text/html; charset=utf-8");
        $url = I("post.url");
        $arr = getSeo($url);
        echo json_encode($arr);
    }

    public function autocomplete() {
        $mtype = I("get.mtype");
        $table = getTableInfo($mtype);
        $name = strtolower(I("get.term"));
        $lists = M("" . $table . "_tags")->where("name like '%" . $name . "%'")->limit(30)->select();
//        echo M("js_tags")->getlastsql();
        $rs = array();
        foreach ($lists as $k => $v) {
            $rs[$k]['id'] = $v['id'];
            $rs[$k]['label'] = $v['name'];
        }
        echo json_encode($rs);
    }

    public function changeLang() {
        $id = I("post.id");
        $data['lang_id'] = I("post.lang_id");
        M("modals")->where("id = " . $id . "")->save($data);
    }

    public function changeMtypes() {
        $id = I("post.id");
        $data['mtype'] = I("post.mtype");
        M("temp")->where("id = " . $id . "")->save($data);
    }

    public function sendToBaidu() {
        $url = I("post.url");
        $rs = sendToBaidu($url);
        echo json_encode($rs);
    }

    //http://www.sucaihuo.com/Tian/Ajax/getMiddleLogo
    function getMiddleLogo() {
        @ini_set("memory_limit", "1024M");
        $id = $_POST['id'];
        $height = 0;
        $mtype = I("post.mtype") ? I("post.mtype") : 2;
        if ($mtype == 1) {
            $logo = getModalsBigImage($id);
        } elseif ($mtype == 15) {
            $logo = getSourceBigImage($id);
        } else {
            $logo = getJqueryBigImage($id);
        }
        $table = getMtypeTable($mtype);


        if (file_exists($logo)) {
            $fileArr = pathinfo($logo);
            $dirname = $fileArr['dirname'];
            $extension = $fileArr['extension'];
            $image_info = getimagesize($logo);
            $width = 268;
            $width_real = $image_info[0];
            $height_real = $image_info[1];
            if ($width_real > 0 && $height_real > 0) {
                $height = $width * ($height_real / $width_real);
                $name_thumb = $dirname . "/" . "middle" . "." . $extension;
                if (file_exists($logo)) {
                    $image = new \Think\Image();
                    $image->open($logo);
                    $image->thumb($width, $height)->save($name_thumb);


                    $height = $height > $height_real ? $height_real : $height;
                } else {
                    $height = 0;
                }
            }
        }
        if ($height > 0) {

            M($table)->where("id = " . $id . "")->save(array("height" => $height));
        }
        echo $height;
    }

    //http://www.sucaihuo.com/Tian/Ajax/order_email
    public function order_email() {
        sleep(1);
        sendMailOrderPoints();
        sleep(1);
        sendMailOrderMonth();
//        $this->month_sign();
        //  getUrlJson('http://www.ebb34.cn/coller.html');
    }

//
//http://admin5.sucaihuo.com/Tian/Ajax/month_sign

    public function month_sign() {
//        if (in_array(date("d"), array(2)) && intval(date("H")) == 7) {

        $month_last_start = mktime(0, 0, 0, date('m') - 1, 1, date('Y'));
        $month_last_end = strtotime(date("Y-m-01 00:00:00"));
        $signs = M("sign")->field("uid")->where("addtime between " . $month_last_start . " AND " . $month_last_end . "")->select();
        foreach ($signs as $v) {
            $users[] = $v['uid'];
        }

        $users_unique = array_unique($users);
        $sign_month = date("Y-m", $month_last_start);
        if ($users_unique) {
            $month_last_start = strtotime(date('Y-m-01', strtotime('-1 month')));
            $month_last_end = strtotime(date('Y-m-t', strtotime('-1 month')));
            foreach ($users_unique as $v) {
//                if ($v == 154) {
                $user = M("user")->field("sign_month")->where("id = " . $v . "")->find();
                if ($user['sign_month'] != $sign_month) {

                    $count = M("sign")->where("uid = " . $v . " AND addtime between " . $month_last_start . " AND " . $month_last_end . "")->count();
                    if ($count >= 20) {
                        $award = C("points.sign_month");
                        //"" . $sign_month . "月连续签到20次以上获得" . $award . "积分"
                        addPoints("month_sign", $award, $v, 5, 1);
                        $userinfo = M("user")->field("id")->where("id = " . $v . "")->find();
                        if ($userinfo) {
                            M("user")->where("id = " . $v . "")->save(array("sign_month" => $sign_month));
                        } else {
                            M("sign")->where("uid = " . $v . "")->delete();
                            M("points")->where("uid = " . $v . "")->delete();
                        }
                    }
                }
//                }
            }
        }
    }

    //http://www.sucaihuo.com/Tian/Ajax/update_demo_url
    public function update_demo_url() {
        $arr = array(
            1
        );
        foreach ($arr as $v) {
            //  M("js")->where("id = ".$v."")->save(array("demo_url"=>"http://demo.sucaihuo.com/".$v.""));
        }
    }

    //http://www.sucaihuo.com/Tian/Ajax/download_times
    public function download_times() {
        $users = M("user")->field("id")->limit(30)->where("download_times = 0")->select();
        foreach ($users as $v) {
            $count = M("download")->where("uid = " . $v['id'] . "")->count();
            if ($count == 0) {
                $download_times = -1;
            } else {
                $download_times = $count;
            }
            M("user")->where("id = " . $v['id'] . "")->save(array("download_times" => $download_times));
            echo M("user")->getlastsql();
        }
    }

    public function changeUserStatus() {
        $uid = I("post.uid");
        $status = I("post.status", 0, 'int');
        if ($status == 1) {
            $data['check_sign'] = md5($uid . "_sucaihuo");
        }
        $data['is_check'] = $status;
        M("user")->where("id = '" . $uid . "'")->save($data);
    }

    public function templates_map() {
        $path_demo = I("post.path_demo");

        $files = scandir($path_demo);
        $files_html = array();
        foreach ($files as $v) {
            $extention = get_extension($v);
            if (in_array($extention, array("html", "htm"))) {
                $files_html[] = codeAuto($v);
            }
        }
        echo json_encode($files_html);
    }

    public function getShortUrl() {
        $url_short = I("post.url_short");
        $url_short_decode = urlencode($url_short);
        $url = "http://api.3w.cn/api.htm?url=" . $url_short_decode . "&key=5d77638ad046966be6c579e9@42dfd705aaa6633a9b680eb6d4c019a5&expireDate=2080-12-12";
        $rs = getUrlJson($url);
        $r["url_short"] = $rs;
        echo json_encode($r);
    }

    public function push_source() {
        $ids = I("post.ids");
        if (!empty($ids)) {
            $ids = array_filter(explode(",", $ids));
            foreach ($ids as $v) {
                $urls[] = 'https://www.sucaihuo.com/source/' . $v . '.html';
            }
            $api = 'http://data.zz.baidu.com/urls?site=www.sucaihuo.com&token=9NiUymHT2xKAO5HF';
            $ch = curl_init();
            $options = array(
                CURLOPT_URL => $api,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS => implode("\n", $urls),
                CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
            );
            curl_setopt_array($ch, $options);
            $result = curl_exec($ch);

            echo json_encode($result);
        }
    }

    public function show_source() {
        $ids = I("post.ids");
        if (!empty($ids)) {
            $ids = array_filter(explode(",", $ids));
            foreach ($ids as $v) {
                M("source")->where("id = " . $v . "")->save(array("is_show" => 1));
            }
        }
    }

    public function start_source() {
        $ids = I("post.ids");
//              print_r($ids);
        if (!empty($ids)) {
            $ids = array_filter(explode(",", $ids));

            foreach ($ids as $v) {
                M("source")->where("id = " . $v . "")->save(array("is_check" => 1));
            }
        }
    }

    //http://www.sucai_houtai.com/Tian/Ajax/update_uid2
//http://admin7788.sucaihuo.com/Tian/Ajax/update_uid2
    public function update_uid2() {
        $s_admin_uid = getAdminId();
        $id_max = 5395;
        $uids = array(3, 13, 11);
        if (!in_array($s_admin_uid, $uids)) {
            echo '-1';
            exit;
        }

        $info = M("source")->field("id")->where("id>=" . $id_max . " AND uid =1 AND is_check =0 AND points<=30")->order("id DESC")->limit(1)->find();


        if ($info['id'] > 0) {
            echo "更新成功，新id:" . $info['id'];
            M("source")->where("id>=" . $info['id'] . " AND uid = 1")->save(array("uid" => $s_admin_uid));
        } else {
            echo 0;
        }
    }

    //http://www.sucai_houtai.com/Tian/Ajax/update_weiqing
//http://admin7788.sucaihuo.com/Tian/Ajax/update_weiqing
    public function update_weiqing() {
        $lists = M("source")->field("name,id,description")->where("description like '%微擎%'")->limit(50)->select();
//          $lists = M("source")->field("name,id,content,keywords,description,details")->where("id=144")->limit(2)->select();
        foreach ($lists as $v) {
//            $name = $v['name'];
//            $content = $v['content'];
//            $keywords = $v['keywords'];
//            $description = $v['description'];
//            $details = $v['details'];
//            $name_new = str_replace("微擎", "PHP", $name);
//            $content_new = str_replace("微擎", "PHP", $content);
//            $keywords_new = str_replace("微擎", "PHP", $keywords);
//            $description_new = str_replace("微擎", "PHP", $description);
//            $details_new = str_replace("微擎", "PHP", $details);
            $description = $v['description'];
            $description = str_replace("微擎", "", $description);
//            echo $description_new;
            echo $description . "<hr>";
            M("source")->where("id = " . $v['id'] . "")->save(array("description" => $description));
        }
    }

    public function clear_ids($id) {

        if ($id > 0) {

            echo "下载" . M("download")->where("tid = " . $id . " and mtype = 15")->count();
            echo "<hr>";
            echo "积分" . M("points")->where("tid = " . $id . " and mtype = 'source'")->count();
            echo "<hr>";
            echo "评论" . M("comment")->where("tid = " . $id . " and mtype = 15")->count();
            echo "<hr>";
            echo "收藏" . M("collects")->where("tid = " . $id . " and mtype = 15")->count();
            echo "<hr>";

            M("download")->where("tid = " . $id . " and mtype = 15")->delete();
            M("points")->where("tid = " . $id . " and mtype = 'source'")->delete();
            M("comment")->where("tid = " . $id . " and mtype = 15")->delete();
            M("collects")->where("tid = " . $id . " and mtype = 15")->delete();
            file_put_contents("a.txt", $id);
        }
    }

    //http://www.sucai_houtai.com/Tian/Ajax/clear_downloads
    public function clear_downloads() {
        $id = file_get_contents("a.txt");
        if ($id == '') {
            $id = 1;
        }
        if ($id > 0) {
            $info = M("source")->field("id,name")->where("is_download = '-1' AND id>" . $id . "")->limit(1)->find();
            $this->clear_ids($info['id']);
            echo $info['id'] . $info['name'];
            echo "<hr>";
        }
//       $lists =  M("source")->field("id,name")->where("is_download = '-1'")->limit(1)->select();
//       
//       foreach($lists as $v){
//           echo $v['name']."<hr>";
//           $this->clear_ids($v['id']);
//       }
    }

    public function zengtui_confirm() {
        $uid = I("post.id", '0', 'int');
        $points = I("post.points", '0', 'int');
        $plus_minus = I("post.plus_minus", '0', 'int');
        $type = I("post.type", '0', 'int');
        if ($points > 0) {
            if ($plus_minus == 1) {
                addPoints("renpay", $points, $uid, 5, 1, 0, 0, 0, $type);
            } else {
                $field_money = ($type == 1) ? 'huobi' : 'money';
                M('user')->where("id=" . $uid . "")->setDec($field_money, $points); //setDec减
            }
        }
    }

    // http://admin7788.sucaihuo.com/Tian/ajax/clear_source
    public function clear_source() {
        // 删除视频教程 相关记录  
        // 删除模板最近一年相关记录
        // 九鸟删除没有记录的
//        $rs = M("points")->alias('A')->join("LEFT JOIN sucai_video B on B.id=A.tid AND A.mtype ='video'")
//                        ->field('B.id')->order("A.id DESC")->limit(1)->find();
////        print_r($rs);
//        $id = $rs['id'];
//        echo M("points")->getlastsql();
//        echo $id;
//        if ($id > 0) {
        $end = strtotime("2021-2-27");
            M("download")->where("mtype = 1 AND addtime <= '".$end."'")->delete();
            M("points")->where("mtype = 'modals' AND addtime <= '".$end."'")->delete();
//            M("comment")->where("mtype = 1")->delete();
//            M("collects")->where("mtype = 1")->delete();
//        }
    }

}

?>
