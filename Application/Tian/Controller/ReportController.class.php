<?php

namespace Tian\Controller;

use PclZip;

class ReportController extends CommonController {

    public function lists() {
        $sql = "1=1";
        $count = M('report_reason')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('report_reason')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();

        $modal_cats = getDictionarySubSql('modal_cat_id', 'video_dictionary');
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("modal_cats", $modal_cats);
        $this->display();
    }

    public function sethandle(){
        $id = I("id");
        if($id){
            $data['is_handle'] = 1;
            M('report_reason')->where("id = $id")->save($data);
            $data=[
                "code"=>200,
                "msg"=>"success",
            ];
        }else{
            $data=[
                "code"=>201,
                "msg"=>"error",
            ];

        }
        $this->ajaxReturn($data);
    }


}

?>
