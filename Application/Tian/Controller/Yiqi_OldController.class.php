<?php

namespace Tian\Controller;

use PclZip;

class YiqiController extends CommonController {

    public function lists() {
        $sql = "is_show=0";
        $keyword = trim(I('get.keyword'));

        if ($keyword) {
            
        }


        $count = M("17")->where($sql)->count();    //计算总数

        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M("17")->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
//    echo D()->db(1, "DB_CONFIG2")->getLastSql();exit;
        foreach ($lists as $k => $v) {
            
        }
        $mtypes = array(
            0 => array(
                "mtype" => "1",
                "name" => "网站模板"
            ),
            2 => array(
                "mtype" => "2",
                "name" => "网页特效"
            ),
            3 => array(
                "mtype" => "20",
                "name" => "PHP"
            ),
            3 => array(
                "mtype" => "20",
                "name" => "PHP"
            ),
            4 => array(
                "mtype" => "15",
                "name" => "网站源码"
            ),
            5 => array(
                "mtype" => "4",
                "name" => "网站psd"
            ),
            6 => array(
                "mtype" => "3",
                "name" => "精选网址"
            ),
        );
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->assign("count", $count);
        $this->assign("mtypes", $mtypes);
        $this->display();
    }

    public function push() {
        $templatesModel = new \Home\Model\TemplatesModel();
        $id = I("post.id");
        $mtype = I("post.mtype");
        $detail = M("17")->where("id = " . $id . "")->find();

   $table = getTableInfo($mtype);
        $huo_id = $detail['huo_id'];

        if($huo_id>0){
             $detail = M($table)->where("id = " . $huo_id . "")->find();
//             echo M($table)->getlastsql();
             $detail['title'] = $detail['name'];
              $dictionary = M("".$table."_dictionary")->where("pid = ".$detail['cat_id']."")->find();
              print_r($dictionary);
        }

        if ($mtype == 1) {//网页模板
            $cats = $templatesModel->getModalsCat('modal_cat_id'); //模板分类
            $Model = new \Tian\Model\ModalsModel();
            $modals_color = $Model->getModalsDictionary('modal_color_id'); //模板颜色
            $modals_lay = $Model->getModalsDictionary('modal_lay_id'); //模板布局
            $modals_lang = $Model->getModalsDictionary('modal_lang_id'); //模板语言
        } else {
            $cats = $templatesModel->getModalsCat('js_cat_id', 'js_dictionary', 'js'); //js分类
            $js_easy = getDictionarySubSql('js_easy_id', 'js_dictionary');
        }

     
        if ($huo_id == 0) {
           
            if ($detail['is_original'] == 1) {
                $detail['points'] = $detail['points'] * 10;
            } else {
           
            }
          

            $detail['lang_id'] = 96;
            $detail['easy_id'] = 4;
            $detail['lay_id'] = 45;
            
        } else {
            if ($mtype == 2) {
                $url_demo = getJqueryDemo($huo_id);
                $url_detail = "http://www.sucaihuo.com/js/" . $huo_id . "";
                $url_admin_detail = "http://www.sucaihuo.com/Tian/Jquery/detail/id/" . $huo_id . "";
                $detail['middle'] = getJqueryDemoImages($huo_id, 'middle');
            } else {
                $url_demo = getModalsDemo($huo_id);
                $url_detail = "http://www.sucaihuo.com/js/" . $huo_id . "";
                $url_admin_detail = "http://www.sucaihuo.com/Tian/Jquery/detail/id/" . $huo_id . "";
                $detail['middle'] = getModalsLogo($huo_id, 'middle');
            }
//            $new = M("" . $table)->where("id = " . $huo_id . "")->find();
//            if ($new['tags']) {
//                $tags = M("" . $table . "_tags")->field("name")->where("id in (" . $new['tags'] . ")")->select();
//            }
        }

        $detail['zip'] = "../project/17sucai/" . $detail['pins_id'] . ".zip";

        $this->assign("js_easy", $js_easy);
        $this->assign("detail", $detail);
        $this->assign("cats", $cats);
        $this->assign("mtype", $mtype);
        $this->assign("modals_color", $modals_color);
        $this->assign("modals_lay", $modals_lay);
        $this->assign("modals_lang", $modals_lang);
        $this->assign("id", $id);
        $this->assign("huo_id", $huo_id);
        $this->assign("table", $table);
        $this->assign("url_demo", $url_demo);
        $this->assign("url_detail", $url_detail);
        $this->assign("url_admin_detail", $url_admin_detail);
        $this->display();
    }

    //http://localhost/sucai/Tian/Jquery/delete_rubish_files
    public function delete_rubish_files($demo_path, $zip_path) {
        $files = scandir($demo_path);
        $delete_files = array("说明.htm", "璇存??.htm");
//        foreach($delete_files as $k=>$v){
//            $delete_files[$k] = getGb2312($v);
//        }
        foreach ($files as $v) {
            $type_file = get_extension($v);
            $file = $demo_path . $v;
            if (in_array($type_file, array("txt", "url"))) {
                unlink($file);
            }
            if (in_array($type_file, array("htm", "html"))) {
                $content_file = file_get_contents($file);
                $content = str_replace("17sucai", "sucaihuo", $content_file);
                $content = str_replace("17素材", "素材火", $content);
                file_put_contents($file, $content);
            }

            if (in_array($v, $delete_files)) {
                unlink($file);
            }
        }
        $file_demo = $demo_path . "demo.html";
        $file_index = $demo_path . "index.html";
        if (file_exists($file_demo) && !file_exists($file_index)) {
            rename($file_demo, $file_index);
        }
        if (file_exists($zip_path)) {
            unlink($zip_path);
        }
    }

    public function push_sucai_post() {
        set_time_limit(0);
        $id = I("post.id", 0, "int");
        if ($id == 0) {
            echo json_encode(array("code" => "id_null", "error" => "id为0"));
            exit;
        }
        $mtype = I("post.mtype", 0, "int");

        $table = getTableInfo($mtype);
        $table = "sucai_" . $table;
        $data['name'] = I("post.name");
        $data['cat_id'] = I("post.cat_id", 0, "int");
        $data['cat_sub_id'] = I("post.cat_sub_id", 0, "int");
        $data['is_recommend'] = I("post.is_recommend", 0, "int");
        $data['is_original'] = I("post.is_original", 0, "int");

        $data['is_check'] = I("post.is_check", 0, "int");
        $data['points'] = I("post.points", 0, "int");
        $data['keywords'] = I("post.keywords");
        $data['description'] = I("post.description");
        $tags = array_unique(explode(",", I("post.tags")));
        $data['tags'] = transferTagsIds($tags, $mtype, 1);

        if ($mtype == '1') { //模板
            $data['color_id'] = I("post.color_ids") ? implode(",", array_filter(explode(",", I("post.color_ids")))) : "";
            $data['lay_id'] = I("post.lay_id", 0, 'int');
            $data['lang_id'] = I("post.lang_id", 0, 'int');
        } else {
            $data['easy_id'] = I("post.easy_id", 0, "int");
        }
        $detail = M("17")->field("huo_id")->where("id = " . $id . "")->find();
        $huo_id = $detail['huo_id'];
        if ($huo_id > 0) {
            M($table)->where("id = " . $huo_id . "")->save($data);
        } else {
            $data['addtime'] = time();
            $data['uid'] = 1;
            $huo_id = M($table)->add($data);
            M("17")->where("id = " . $id . "")->save(array("huo_id" => $huo_id));
        }
//        print_r($detail);
        if ($detail['huo_id'] == 0) {
            $file_path = getFileInfo($mtype) . "/" . getFileBei($huo_id) . $huo_id . "/";
            makeDir($file_path);
            $demo_path = $file_path . "demo/";

            delInnerFiles($demo_path);

            $temp_path = "uploads/demo/";
            $logo_big = I("post.logo_big");
            $logo_big_save = $file_path . "big.jpg";

            if (strstr($logo_big, "17sucai")) {

                file_put_contents($logo_big_save, file_get_contents($logo_big));
                $height = getMiddleLogo($logo_big_save);
                if ($height > 0) {
                    M($table)->where("id = " . $huo_id . "")->save(array("height" => $height));
                }
            }
            if ($logo_big && file_exists($logo_big)) {
                rename($logo_big, $logo_big_save);
            }
            $zip = getGb2312(I("post.zip"));
            $zip_post = getGb2312(I("post.zip"));

            $type = get_extension($zip);

            if (!strstr($zip, "temp")) {
                $zip = "uploads/temp/" . time() . "." . $type;
                if ($_SERVER['HTTP_HOST'] == 'localhost') {
                    $zip_content = file_get_contents($zip_post);
                    file_put_contents($zip, $zip_content);
                } else {
                    copy($zip_post, $zip);
                }
            }

            if (!file_exists($zip)) {
                echo json_encode(array("code" => "zip_not_exists", "error" => "压缩文件不存在"));
                exit;
            }
            //首先解压rar 或 zip压缩文件
            if ($type == 'rar') {

                unrar($zip, $temp_path);
                $base_name = basename($zip, ".rar");
            } else {
                replacePathExtract($zip, $temp_path);
                $base_name = basename($zip, ".zip");
            }



            $files = scandir($temp_path);
            if (count($files) == 3) { //判断是否有多余文件夹路径
                $files_first = $files[2];
            }



            $zip_new = "uploads/zip/" . $base_name . ".zip";

            removePathZip($zip_new, $temp_path, "uploads/demo");    // createAddZip($zip, $temp_path);
            if ($files_first == 'index.html') {
                $files_first = '';
            }
            replacePathExtract($zip_new, $demo_path, $files_first); //解压新的压缩文件夹

            delInnerFiles($temp_path); //删除临时uploads/demo里的文件路径
            $name = getGb2312($data['name']); //is_Gb2312 getUtf8
//            $name2 = getGb2312($data['name']);
            $this->delete_rubish_files($demo_path);



            createReplaceZip("" . $name . ".zip", $demo_path, $demo_path, $name); //添加新的压缩文件
            rename($name . ".zip", $file_path . $name . ".zip");
            unlink($zip_new);
            unlink($zip);
        } else {
            
        }
        if ($mtype == 2) {
            $url_demo = getJqueryDemo($huo_id);
            $url_detail = "http://www.sucaihuo.com/js/" . $huo_id . "";
            $url_admin_detail = "http://www.sucaihuo.com/Tian/Jquery/detail/id/" . $huo_id . "";
        } else {
            $url_demo = getModalsDemo($huo_id);
            $url_detail = "http://www.sucaihuo.com/templates/" . $huo_id . "";
            $url_admin_detail = "http://www.sucaihuo.com/Tian/Modals/detail/id/" . $huo_id . "";
        }
        echo json_encode(array("code" => "200", "url_demo" => $url_demo, "url_detail" => $url_detail, "url_admin_detail" => $url_admin_detail));
        exit;
    }

    public function changeMtypes() {
        $id = I("post.id");
        $data['mtype'] = I("post.mtype");
        M("17")->where("id = " . $id . "")->save($data);
    }
     public function yes_17() {
        $id = I("post.id", '0', 'int');
        $field = I("post.field") ? I("post.field") : "is_show";
        $table = I('post.table');
        $info = M($table)->where("id = " . $id . "")->find();
        if ($info[$field] == 0) {
            $data[$field] = 1;
            echo $word = '否';
        } else {
            $data[$field] = 0;
            echo $word = '是';
        }
        M($table)->where("id = " . $id . "")->save($data);
//        print_r(D()->db(1, "DB_CONFIG2"));
    }

}

?>
    