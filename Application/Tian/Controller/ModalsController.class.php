<?php

namespace Tian\Controller;

use PclZip;

class ModalsController extends CommonController {

    public function lists() {
        if ($_SERVER['SERVER_NAME'] == 'www.sucaihuo.com') {
//            echo 'new';
//            exit;
        }

        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND (name like '%" . $keyword . "%' or keywords like '%" . $keyword . "%')";
        }
        $modal_cat_id = I('get.modal_cat_id', 0, 'int'); //分类筛选
        if ($modal_cat_id > 0) {
            $sql .= " AND cat_id = " . $modal_cat_id . "";
        }
        $modal_color_id = I('get.modal_color_id', 0, 'int'); //颜色筛选
        if ($modal_color_id > 0) {
            $sql .= " AND FIND_IN_SET('" . $modal_color_id . "',color_id)";
        }
        $modal_lay_id = I('get.modal_lay_id', 0, 'int'); //布局筛选
        if ($modal_lay_id > 0) {
            $sql .= " AND lay_id = " . $modal_lay_id . "";
        }
        $modal_lang_id = I('get.modal_lang_id', 0, 'int'); //布局筛选
        if ($modal_lang_id > 0) {
            $sql .= " AND lang_id = " . $modal_lang_id . "";
        }
        $ord = I("get.ord");
        if ($ord == 1) {
            $sql .= " AND ord >0";
        }
        $is_check = I("get.is_check");
        if ($is_check >= 0 && $is_check != '') {
            $sql .= " AND is_check = " . $is_check . "";
        }
        $count = M('modals')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('modals')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
        foreach ($lists as $v) {
            $id = $v['id'];
            if ($v['files_num'] == 0) {
                $file_path = getGb2312("modals/" . getFileBei($id) . $id . "/demo/");
                $filesScandir = scandir($file_path);
                $i = 0;
                foreach ($filesScandir as $k => $v2) {
                    $type = get_extension($v2);
                    if (in_array($type, array("html", "htm")) && $v2 != 'sitemap.html') {
                        $i++;
                    }
                }
                M("modals")->where("id = " . $id . "")->save(array("files_num" => $i));
            }
        }

        $modal_cats = getDictionarySubSql('modal_cat_id', 'modals_dictionary');
        $modal_colors = getDictionarySubSql('modal_color_id', 'modals_dictionary');
        $modal_lays = getDictionarySubSql('modal_lay_id', 'modals_dictionary');
        $modal_lang = getDictionarySubSql('modal_lang_id', 'modals_dictionary');
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->assign("modal_cat_id", $modal_cat_id);
        $this->assign("modal_cats", $modal_cats);
        $this->assign("modal_color_id", $modal_color_id);
        $this->assign("modal_lay_id", $modal_lay_id);
        $this->assign("modal_lang_id", $modal_lang_id);
        $this->assign("modal_colors", $modal_colors);
        $this->assign("modal_lays", $modal_lays);
        $this->assign("modal_lang", $modal_lang);
        $this->assign("ord", $ord);
        $this->display();
    }

    public function detail() {
        if ($_SERVER['SERVER_NAME'] == 'www.sucaihuo.com') {
//            echo 'new';
//            exit;
        }
        $templatesModel = new \Tian\Model\ModalsModel();
        $id = I('get.id', '0', 'int');
        if ($id > 0) {
            $detail = M("modals")->where("id =" . $id . "")->find();
//            if ($detail['files_num'] == 0) {
//                $file_path = getGb2312("modals/" . getFileBei($id) . $id . "/demo/");
//                $filesScandir = scandir($file_path);
//                $i = 0;
//                foreach ($filesScandir as $k => $v) {
//                    $type = get_extension($v);
//                    if (in_array($type,array("html","htm")) && $v != 'sitemap.html') {
//                        $i++;
//                    }
//                }
//                M("modals")->where("id = ".$id."")->save(array("files_num"=>$i));
//            }
        } else {
            $detail['modal_lang_id'] = 96;
        }
        if (!empty($detail['tags'])) {
            $tagsArr = explode(",", $detail['tags']);
            foreach ($tagsArr as $v) {
                $info = M("modals_tags")->where("id = " . $v . "")->find();
                $tags[] = $info['name'];
            }
        } else {
            $tags = array(
                0 => "",
                1 => "",
                2 => "",
                3 => "",
                4 => ""
            );
        }
        $detail['logo'] = getModalsLogo($id);
        $detail['logo_big'] = getModalsLogo($id, 'big');
        $modals_cat = $templatesModel->getModalsDictionary('modal_cat_id'); //模板分类
        foreach ($modals_cat as $k => $v) {
            $modals_cat[$k]['sub'] = M("modals_dictionary")->where("pid = " . $v['id'] . " AND is_check = 1")->order("ord ASC")->select();
        }
        $modals_color = $templatesModel->getModalsDictionary('modal_color_id'); //模板颜色
        $modals_lay = $templatesModel->getModalsDictionary('modal_lay_id'); //模板布局
        $modals_lang = $templatesModel->getModalsDictionary('modal_lang_id'); //模板语言
        $next = M('modals')->field("name,id")->where("id > " . $id . "")->order("id ASC")->find();
        $prev = M('modals')->field("name,id")->where("id < " . $id . "")->order("id DESC")->find();
        $this->assign("next", $next);
        $this->assign("prev", $prev);
        $this->assign("modals_cat", $modals_cat);
        $this->assign("modals_color", $modals_color);
        $this->assign("modals_lay", $modals_lay);
        $this->assign("modals_lang", $modals_lang);
        $this->assign("detail", $detail);
        $this->assign("mtype", 'modals');
        $this->assign("tags", $tags);
        $this->display();
    }

    public function detail_post() {
        import("Common.Org.PclZip");

        $id = I('post.id', '', 'int');
        if ($id == 0) {
            $data['uid'] = session("admin_uid");
            $data['is_check'] = 1;
            $data['addtime'] = time();
            $id = M("modals")->add($data);
        }

        $file_path = "modals/" . getFileBei($id) . $id . "/";
        $file_path_demo = $file_path . "demo/";
        checkDirExists($file_path_demo);
        $data['name'] = replace_specialChar(trim(I("post.name")));
        $data['cat_id'] = $_POST['cat_id'] ? implode(",", $_POST['cat_id']) : "";
        $data['cat_sub_id'] = $_POST['cat_sub_id'] ? implode(",", $_POST['cat_sub_id']) : "";
        $data['color_id'] = I("post.modal_color_id") ? implode(",", I("post.modal_color_id")) : "";
        $data['lay_id'] = I("post.modal_lay_id", 0, 'int');
        $data['lang_id'] = I("post.modal_lang_id", 0, 'int');

        $data['keywords'] = I("post.keywords");
        $data['description'] = trim(I("post.description"));
        $data['source'] = I("post.source");
        $data['content_first'] = I("post.content_first");

        $data['points_type'] = I("post.points_type", 0, 'int');
        $data['points'] = I('post.points', '0', 'int');
        $data['is_check'] = I("post.is_check", 0, 'int');
        $data['is_recommend'] = I("post.is_recommend", 0, 'int');
        $data['is_original'] = I("post.is_original", 0, 'int');
        $file_zip = $file_path . getZipMd5($id) . "/";
        checkDirExists($file_zip);
        $zip_real = is_Gb2312($file_zip . $data['name'] . ".zip");
        


        if (I("post.zip") && I("post.zip") != $zip_real) {
            
            rename(I("post.zip"), $zip_real);
        }
        
        $info = M("modals")->field("name")->where("id = " . $id . "")->find();
               if($info['name'] != '' &&  $info['name'] != $data['name']){
                    $zip_old = is_Gb2312($file_zip . $info['name'] . ".zip");
                     $zip_new = is_Gb2312($file_zip . $data['name'] . ".zip");
                   rename($zip_old,$zip_new);
               }
        

        $data['logo_big'] = $file_path . "big.jpg";
        $data['url_demo'] = $_POST['url_demo'];
        if (I("post.logo_big") && I("post.logo_big") != $data['logo_big']) {
            rename(I("post.logo_big"), $data['logo_big']);
            $height = getMiddleLogo($data['logo_big']);
            if ($height > 0) {
                M("modals")->where("id = " . $id . "")->save(array("height" => $height));
            }
        }
        $tagsArr = $_POST['tag'];
        $tag_i = 0;
        foreach ($tagsArr as $v) {
            if (!empty($v)) {
                $info = M("modals_tags")->field("id")->where("name = '" . $v . "'")->find();
                if (empty($info)) {
                    $tag_i++;
                    $tag_max_ord = M("modals_tags")->max('ord') + $tag_i;
                    $tag_lastid = M("modals_tags")->add(array("name" => $v, "ord" => $tag_max_ord, "is_check" => 1));
                } else {
                    $tag_lastid = $info['id'];
                }
                $tags[] = $tag_lastid;
            }
        }
        $data['tags'] = !empty($tags) ? implode(",", $tags) : "";
        if (I("post.files_num") > 0) {
            $data['files_num'] = I("post.files_num", 0, 'int');
        }
        $files_save = $data['name'] . ".zip";
        $data['details'] = $_POST['details'];
        if ($id > 0) {
//            include_once 'common/detail_post_zip.php';
            M("modals")->where("id = " . $id . "")->save($data);
            $lastid = $id;
            $state = $data['is_check'] == 0 ? 2 : 1;
            M("temp")->where("original_id = " . $id . " AND mtype =1")->save(array("state" => $state));
        }
        $uniqid = I("post.files_save"); //压缩文件
        if ($uniqid) {
            $files_num = getZip($lastid, $uniqid, C("modals_file.temp"), C("modals_file.zip"), C("modals_file.demo"), $files_save);
            M("modals")->where("id = " . $lastid . "")->save(array("files_num" => $files_num));
        }
//        getTagsNum(1);
        if ($id > 0) {
            //  deleteHtmlFile($id, 1); //删除缓存
            $curl_update = array("id" => $id, "mtype" => 1);
            curlPost("http://www.sucaihuo.com/Ajax/update_html_type", $curl_update);

            goUrl(U('Modals/detail', array("id" => $id)));
        } else {
            goUrl(U('Modals/lists'));
        }
    }

    public function cat() {//模板类别
        $pid = I('get.pid', '0', 'int');
        $sql = "pid = " . $pid . "";
        $keywords = trim(I('get.keywords'));
        if ($keywords) {
            $sql .= " AND name like '%" . $keywords . "%'";
        }
        $count = M('modals_dictionary')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('modals_dictionary')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('is_check DESC,ord ASC')->select();
        foreach ($lists as $k => $v) {
            $lists[$k]['num'] = M('modals_dictionary')->where("pid = " . $v['id'] . "")->count(); //下级个数
            if ($v['tags']) {
                $lists[$k]['tags_num'] = count(explode(",", $v['tags']));
            } else {
                $lists[$k]['tags_num'] = 0;
            }
        }
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->assign("pid", $pid);
        $this->display();
    }

    public function cat_detail() {//模板类别详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('modals_dictionary')->where("id = " . $id . "")->find();
        } else {
            $detail['pid'] = I("get.pid", 0, 'int');
            $detail['is_check'] = 1;
        }
        $cats_top = M('modals_dictionary')->where("pid = 0")->order('ord ASC')->select();
        foreach ($cats_top as $k => $v) {
            $cats_top[$k]['second'] = M('modals_dictionary')->where("pid = " . $v['id'] . "")->order('ord ASC')->select();
        }
        $this->assign("detail", $detail);
        $this->assign("cats_top", $cats_top);
        $this->display();
    }

    public function cat_detail_post() {
        $id = I("post.id", 0, 'int');
        $data['pid'] = I("post.pid", 0, 'int');
        $data['is_check'] = I("post.is_check", 0, 'int');

        $pinfo = M('modals_dictionary')->field("pid")->where("id = " . $data['pid'] . "")->find();
        if ($pinfo['pid'] > 0) {
            $level = 2;
        } else if ($pinfo['pid'] == 0) {
            $level = 1;
        } else {
            $level = 0;
        }
        $data['level'] = $level;
        $data['seo_title'] = trim(I("post.seo_title"));
        $data['keywords'] = trim(I("post.keywords"));
        $data['description'] = trim(I("post.description"));
        if ($id > 0) {

            $data['name'] = trim(I("post.name"));
            $data['ord'] = I("post.ord", 0, 'int');
            M('modals_dictionary')->where("id = " . $id . "")->save($data);
            $this->success("修改模板类别成功！", session('QUERY_STRING'));
        } else {
            $names = array_unique(array_filter(I('post.name')));
            foreach ($names as $k => $v) {
                if ($v) {
                    $data['name'] = $v;
                    $data['ord'] = I('post.ord', '30', 'int') + $k;
                    M('modals_dictionary')->add($data);
                }
            }
            $this->success("添加模板类别成功！", U("Modals/cat", array("pid" => $data['pid'])));
        }
    }

    public function tags() {
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND name like '%" . $keyword . "%'";
        }
        $count = M('modals_tags')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('modals_tags')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('ord ASC,id DESC')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    public function tags_detail() {//标签详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('modals_tags')->where("id = " . $id . "")->find();
        } else {
            $detail['pid'] = I("get.pid", 0, 'int');
            $detail['is_check'] = 1;
        }
        $this->assign("detail", $detail);
        $this->display();
    }

    public function tags_detail_post() {
        $id = I("post.id", 0, 'int');
        $data['is_check'] = I("post.is_check", 0, 'int');
        if ($id > 0) {
            $data['name'] = trim(I("post.name"));
            $data['ord'] = I("post.ord", 0, 'int');
            $data['keywords'] = strtolower(trim(I("post.keywords")));
            $data['description'] = strtolower(trim(I("post.description")));
            M('modals_tags')->where("id = " . $id . "")->save($data);
            $this->success("修改模板标签成功！", session('QUERY_STRING'));
        } else {
            $names = array_unique(array_filter(I('post.name')));
            foreach ($names as $k => $v) {
                if ($v) {
                    $data['name'] = $v;
                    $data['ord'] = I('post.ord', '30', 'int') + $k;
                    M('modals_tags')->add($data);
                }
            }
            $this->success("添加模板标签成功！", U("Modals/tags"));
        }
    }

}

?>
    