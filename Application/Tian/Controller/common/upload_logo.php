<?php

$logo = I("post.logo");

if ($logo) {
    echo 1;
    $dir = C("uploads.upload") . $table;
    checkDirExists(C("uploads.upload") . $table);
    $logo_old = getSingleField($id, $table, "logo");
    unlink($logo_old);
    if ($logo == -1) {
        $data['logo'] = "";
    } else {
        $old = C("uploads.temp") . basename($logo);
        $new = $dir. basename($logo);
        rename($old, $new);
        $data['logo'] = $new;
    }
}
?>