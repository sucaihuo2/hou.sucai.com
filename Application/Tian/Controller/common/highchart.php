<?php

$shops = M("shop")->order("ord ASC")->select();
$shop_id = I("get.shop_id", 0, 'int');
$year = I("get.year", 0, 'int');
$month = I("get.month", 0, 'int');
$day = I("get.day", 0, 'int');
$type = I("get.type", 0, 'int');

if ($month) {
    if (strlen($month) == 1) {
        $month = "0" . $month;
    }
    $day_max = getMonthLastDay($month, $year);
    for ($i = 1; $i <= $day_max; $i++) {
        $days[] = $i;
    }
}
if ($year) {
    if ($month) {
        if ($day) {
            for ($i = 1; $i <= 23; $i++) {
                if (strlen($i) == 1) {
                    $i = "0" . $i;
                }
                $start = date("" . $year . "-" . $month . "-" . $day . " " . $i . ":00:00");
                $end = date("" . $year . "-" . $month . "-" . $day . " " . $i . ":59:59");
                $t_start = strtotime($start);
                $t_end = strtotime($end);
                $statics_cat[] = $i;
                $nums[] = getHighchartsData($mtype, $t_start, $t_end, $shop_id);
            }
            $title_day = $day . "日";
        } else {
            /*             * *****月里面的天********* */
            for ($i = 1; $i <= $day_max; $i++) {
                if (strlen($i) == 1) {
                    $i = "0" . $i;
                }
                $j = $i + 1;
                if (strlen($j) == 1) {
                    $j = "0" . $j;
                }
                $start = date("" . $year . "-" . $month . "-" . $i . "");
                $end = date("" . $year . "-" . $month . "-" . $j . "");
                $t_start = strtotime($start);
                $t_end = strtotime($end);
                $statics_cat[] = $i;
                $nums[] = getHighchartsData($mtype, $t_start, $t_end, $shop_id);
            }
        }
        $title_month = $month . "月";
    } else {
        /*         * *****年里面的月********* */
        for ($i = 1; $i <= 12; $i++) {
            if (strlen($i) == 1) {
                $i = "0" . $i;
            }
            $start = date("" . $year . "-" . $i . "-01 00:00:00");
            $end_day = getMonthLastDay($start); //某月最后一天
            $end = date("" . $year . "-" . $i . "-" . $end_day . " 23:59:59");
            $t_start = strtotime($start);
            $t_end = strtotime($end);
            $statics_cat[] = $i;
            $nums[] = getHighchartsData($mtype, $t_start, $t_end, $shop_id);
        }
    }
    $title_year = $year . "年";
} else {
    for ($i = date("Y") - 3; $i <= date("Y"); $i++) {
        $statics_cat[] = $i;
        $start = date("" . $i . "-01-01");
        $end = date("" . $i . "-12-31");
        $t_start = strtotime($start);
        $t_end = strtotime($end);
        $nums[] = getHighchartsData($mtype, $t_start, $t_end, $shop_id);
    }
}

/* * *****公共区域*** */
for ($i = date("Y") - 3; $i <= date("Y"); $i++) {
    $years[] = $i;
}
if ($year) {
    for ($i = 1; $i <= 12; $i++) {
        $months[] = $i;
    }
}

/* * ************* */

if ($type == 2) {
    $pie = array();
    foreach ($statics_cat as $k => $v) {
        $pie[$k]['y'] = intval($nums[$k]);
        $pie[$k]['name'] = $v;
    }
    $nums = $pie;
}
if ($shop_id) {
    $shop = M("shop")->field("name")->where("id = " . $shop_id . "")->find();
    $title_shop = $shop['name'];
}
if($mtype == 'users'){
    $title = "用户量";
}elseif($mtype == 'order'){
    $title = "订单量";
}else{
    $title = "营业额";
}
$ctitle = "" . $title_year . $title_month . $title_day . " " . $title_shop . " ".$title."";
$order_data = array(
    0 => array(
        "name" => $ctitle,
        "data" => $nums,
    )
);
if ($day) {
    $statics_cat = array();
    for ($i = 1; $i <= 23; $i++) {
        if (strlen($i) == 1) {
            $i = "0" . $i;
        }
        $start = date("" . $year . "-" . $month . "-" . $day . " " . $i . ":00:00");
        $end = date("" . $year . "-" . $month . "-" . $day . " " . $i . ":59:59");
        $t_start = strtotime($start);
        $t_end = strtotime($end);
        $statics_cat[] = $i;
    }
}
$lists = array();
foreach ($statics_cat as $k => $v) {
    if ($year > 0) {
        if ($month > 0) {
            if ($day > 0) {
                $start = date("" . $year . "-" . $month . "-" . $day . " " . $v . ":00:00");
                $end = date("" . $year . "-" . $month . "-" . $day . " " . $v . ":59:59");
            } else {
                $start = date("" . $year . "-" . $month . "-" . $v . " 00:00:00");
                $end = date("" . $year . "-" . $month . "-" . $v . " 23:59:59");
            }
        } else {//year
            $start = date("" . $year . "-" . $v . "-01");
            $end = date("" . $year . "-" . $v . "-31");
        }
    } else {
        $start = date("" . $v . "-01-01");
        $end = date("" . $v . "-12-31");
    }
    $t_start = strtotime($start);
    $t_end = strtotime($end);
    $lists[] = getHighchartsLists($mtype, $t_start, $t_end, $shop_id);
    $lists[$k]['t'] = $v;
}
$this->assign("order_data", json_encode($order_data));
$this->assign("year", $year);
$this->assign("years", $years);
$this->assign("months", $months);
$this->assign("month", $month);
$this->assign("days", $days);
$this->assign("day", $day);
$this->assign("type", $type);
$this->assign("mtype", $mtype);
$this->assign("ctitle", $ctitle);
$this->assign("statics_cat", json_encode($statics_cat));
$this->assign("shops", $shops);
$this->assign("shop_id", $shop_id);
$this->assign("lists", $lists);
?>
