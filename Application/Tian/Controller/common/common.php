<?php
header("Content-type: text/html; charset=utf-8");
$control = CONTROLLER_NAME;
        $mod = ACTION_NAME;
        $modReturn = array("lists", "cat",'friends','tags');
        if (in_array($mod, $modReturn)) {
            $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            session('QUERY_STRING', $url);
        }
        $menus = M("admin_menu2")->where("pid = 0 AND is_check = 1")->order("ord ASC")->select();
        foreach ($menus as $k => $v) {
            $menus[$k]['url'] = U($v['control'] . "/" . $v['mod']);
            $menus[$k]['sub'] = M("admin_menu2")->where("pid = " . $v['id'] . " AND is_check = 1")->order("ord ASC")->select();
            foreach ($menus[$k]['sub'] as $k2 => $v2) {
                $menus[$k]['sub'][$k2]['url'] = __APP__ . "/" . MODULE_NAME . "/" . ucfirst($v2['control']) . "/" . strtolower($v2['mod']) . $v2['url_other'];
            }
        }
        $menu = M("admin_menu2")->field("pid")->where("control = '" . $control . "' AND `mod` = '" . $mod . "'")->find();
        $three = getTableFile('three');
        $this->assign("three", $three[0]);
        $this->assign("config", getTableConfig());
        $this->assign("control", $control);
        $this->assign("mod", $mod);
        $this->assign("menus", $menus);
        $this->assign("menu_pid", $menu['pid']);
        $this->assign("s_admin_name", $s_admin_name);
