<?php

$zip = I("post.zip");
if ($zip) {
    $dir = C("uploads.upload") . "zip";
    checkDirExists($dir);
    $new = $dir . "/" . basename($zip);
    if ($id > 0) {
        $zip_old = getSingleField($id, $table, "zip");

        if ($zip_old) {
            unlink($zip_old);
        }
    }
    $zip_relative_path = C("uploads.temp") . basename($zip);
    rename($zip_relative_path, $new);
    $data['zip'] = $new;
}

if ($zip == -1) {
    $data['zip'] = '';
}
?>
