<?php

$id = I("post.id", 0, 'int');
$info = M("website")->field("json,website,nodata")->where("id = " . $id . "")->find();
$json = json_decode($info['json'], true);

require 'QueryList.class.php';
$website = I("post.website_chose")?I("post.website_chose"):$info['website'];
$parse_url = parse_url($website);
$website = $parse_url['scheme'] . "://" . $parse_url["host"];
$websiteArr = explode(".", $parse_url["host"]);
$website_name = $websiteArr[1];
if (in_array($website_name, array("com", "cn", "net"))) {
    $website_name = $websiteArr[0];
}
$dir_website = "website/" . $website_name;
if (!is_dir($dir_website)) {
    mkdir($dir_website);
}
?>
