<?php

namespace Tian\Controller;

use PclZip;

class TopicController extends CommonController {

    public function lists() {
        $sql = "is_deleted in (0,-2)"; //0未删除 -2 用户删除
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND (name like '%" . $keyword . "%' or keywords like '%" . $keyword . "%')";
        }
        $cat_id = I('get.cat_id', 0, 'int'); //分类筛选
        if ($cat_id > 0) {
            $sql .= " AND cat_id = " . $cat_id . "";
        }
        $easy_id = I('get.easy_id', 0, 'int'); //难易筛选
        if ($easy_id > 0) {
            $sql .= " AND easy_id = " . $easy_id . "";
        }
        $count = M('topic')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('topic')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
        $topic_cats = getDictionarySubSql('topic_cat_id', 'topic_dictionary');
        $topic_easy = getDictionarySubSql('topic_easy_id', 'topic_dictionary');
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->assign("cat_id", $cat_id);
        $this->assign("easy_id", $easy_id);
        $this->assign("topic_cats", $topic_cats);
        $this->assign("topic_easy", $topic_easy);
        $this->display();
    }

    public function detail() {
        $cats = M("topic_dictionary")->where("pid = 1 AND is_check = 1")->order("ord ASC")->select();
        $id = I('get.id', '0', 'int');
        if ($id > 0) {
            $detail = M("topic")->where("id =" . $id . "")->find();

//            print_r($cats);
            $detail['content'] = htmlspecialchars_decode($detail['content']);
        }


        $this->assign("cats", $cats);
        $this->assign("detail", $detail);
        $this->display();
    }

    public function detail_post() {
        if (get_magic_quotes_gpc()) {

            function stripslashes_deep($value) {
                $value = is_array($value) ?
                        array_map('stripslashes_deep', $value) :
                        stripslashes($value);
                return $value;
            }

            $_POST = array_map('stripslashes_deep', $_POST);
        }
        $id = I("post.id", 0, 'int');

        $data['name'] = trim(I("post.title"));
        $data['cat_id'] = I("post.cat_id", 0, "int");
        $data['content'] = I('post.content');


        if ($id > 0) {
            M("topic")->where("id =" . $id . " AND uid = 1")->save($data);
            $this->success('修改成功！', U("Topic/detail", array("id" => $id)));
        } else {
            $data['uid'] = 1;
            $data['addtime'] = time();
            $info = M("topic")->where("name ='" . $data['name'] . "' AND uid = 1")->find();
            if (empty($info)) {
                M("topic")->add($data);
                $this->success('添加成功！', U('Say/topic'));
            } else {
                $this->error('标题重复');
            }
        }
    }

    public function cat() {//话题类别
        $pid = I('get.pid', '0', 'int');
        $sql = "pid = " . $pid . "";
        $keywords = trim(I('get.keywords'));
        if ($keywords) {
            $sql .= " AND name like '%" . $keywords . "%'";
        }
        $count = M('topic_dictionary')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('topic_dictionary')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('is_check DESC,ord ASC')->select();
        foreach ($lists as $k => $v) {
            $lists[$k]['num'] = M('topic_dictionary')->where("pid = " . $v['id'] . "")->count(); //下级个数
            if ($v['tags']) {
                $lists[$k]['tags_num'] = count(explode(",", $v['tags']));
            } else {
                $lists[$k]['tags_num'] = 0;
            }
        }
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->assign("pid", $pid);
        $this->display();
    }

    public function cat_detail() {//话题类别详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('topic_dictionary')->where("id = " . $id . "")->find();
        } else {
            $detail['pid'] = I("get.pid", 0, 'int');
            $detail['is_check'] = 1;
        }
        $cats_top = M('topic_dictionary')->where("pid = 0")->order('ord ASC')->select();
        foreach ($cats_top as $k => $v) {
            $cats_top[$k]['second'] = M('topic_dictionary')->where("pid = " . $v['id'] . "")->order('ord ASC')->select();
        }
        $this->assign("detail", $detail);
        $this->assign("cats_top", $cats_top);
        $this->display();
    }

    public function cat_detail_post() {
        $id = I("post.id", 0, 'int');
        $data['pid'] = I("post.pid", 0, 'int');
        $data['is_check'] = I("post.is_check", 0, 'int');

        $pinfo = M('topic_dictionary')->field("pid")->where("id = " . $data['pid'] . "")->find();
        if ($pinfo['pid'] > 0) {
            $level = 2;
        } else if ($pinfo['pid'] == 0) {
            $level = 1;
        } else {
            $level = 0;
        }

        $data['level'] = $level;
        if ($id > 0) {
            $data['keywords'] = trim(I("post.keywords"));
            $data['description'] = trim(I("post.description"));
            $data['name'] = trim(I("post.name"));
            $data['ord'] = I("post.ord", 0, 'int');
            M('topic_dictionary')->where("id = " . $id . "")->save($data);
            $this->success("修改话题类别成功！", session('QUERY_STRING'));
        } else {
            $names = array_unique(array_filter(I('post.name')));
            foreach ($names as $k => $v) {
                if ($v) {
                    $data['name'] = $v;
                    $data['ord'] = I('post.ord', '30', 'int') + $k;
                    M('topic_dictionary')->add($data);
                }
            }
            $this->success("添加话题类别成功！", U("Topic/cat", array("pid" => $data['pid'])));
        }
    }

    public function tags() {
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND name like '%" . $keyword . "%'";
        }
        $count = M('topic_tags')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('topic_tags')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('ord ASC,id DESC')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    public function tags_detail() {//标签详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('topic_tags')->where("id = " . $id . "")->find();
        } else {
            $detail['pid'] = I("get.pid", 0, 'int');
            $detail['is_check'] = 1;
            $detail['ord'] = M("topic_tags")->max('ord') + 1;
        }

        $this->assign("detail", $detail);
        $this->display();
    }

    public function tags_detail_post() {

        $id = I("post.id", 0, 'int');
        $data['is_check'] = I("post.is_check", 0, 'int');
        if ($id > 0) {
            $data['name'] = strtolower(trim(SI("post.name")));
            $data['ord'] = I("post.ord", 0, 'int');
            $data['keywords'] = strtolower(trim(I("post.keywords")));
            $data['description'] = strtolower(trim(I("post.description")));
            M('topic_tags')->where("id = " . $id . "")->save($data);
            $this->success("修改话题标签成功！", session('QUERY_STRING'));
        } else {
            $names = array_unique(array_filter(I('post.name')));
            foreach ($names as $k => $v) {
                if ($v) {
                    $data['name'] = $v;
                    $data['ord'] = I('post.ord', '30', 'int') + $k;
                    M('topic_tags')->add($data);
                }
            }
            $this->success("添加话题标签成功！", U("Topic/tags"));
        }
    }
    public function topic_img() {
        $userid = getUserid();
        if (empty($userid)) {
            $arr = array(
                'errno' => 1,
                'data' => array(''),
                'msg' => "亲,你还没登陆哦！快去登录吧！"
            );
            echo json_encode($arr);
            exit();
        }
        //判断是否在允许文件类型
        $rootPath = "Public/Uploads/";
        $savePath = "topic/";
        $upload = new \Think\Upload(); // 实例化上传类
        $upload->maxSize = 3145728; // 设置附件上传大小
        $upload->exts = array('jpg', 'gif', 'png', 'jpeg'); // 设置附件上传类型
        $upload->rootPath = $rootPath; // 设置附件上传根目录
        $upload->savePath = $savePath; // 设置附件上传（子）目录
        $upload->autoSub = false;
        $upload->saveExt = "jpg";
        // 上传文件
        $info = $upload->upload();
        $filePatha = array();
        if (!$info) {// 上传错误提示错误信息
            $arr = array(
                'errno' => 1,
                'data' => array(''),
                'msg' => $upload->getError()
            );
            echo json_encode($arr);
            exit();
        } else {// 上传成功
            $infoKey = array_keys($info);
            $infoKey = $infoKey[0];
            $savename = $info[$infoKey]['savename'];
            $savepath = $info[$infoKey]['savepath'];
            $pic = "./" . $rootPath . $savepath . $savename;
            $avatar_aliyun = $savepath . $savename;
            $ossPic = aliyun_upload($pic, $avatar_aliyun);
            $arr = array(
                'errno' => 0,
                'data' => array($ossPic)
            );
            echo json_encode($arr);
            exit();
        }
    }

}

?>
    