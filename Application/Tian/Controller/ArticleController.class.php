<?php

namespace Tian\Controller;

class ArticleController extends CommonController {

    public function lists() {
        $sql = "1=1";
        $keywords = trim(I('get.keywords'));
        $pid = I('get.pid', '', 'int');
        if ($keywords) {
            $sql .= " AND title like '%" . $keywords . "%'";
        }
        $count = M('article')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('article')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('ord ASC,id desc')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->assign("pid", $pid);
//        $this->assign("cats", getArticleCat());
        $this->display();
    }

    public function detail() {
        $id = I('get.id', '', 'int');
        if($id>0){
        $detail = M("article")->where("id = " . $id . "")->find();
        }
        $this->assign("detail", $detail);
        $cats = M("article_cat")->where("pid = 0")->select();
        foreach ($cats as $k => $v) {
            $cats[$k]['sub'] = M("article_cat")->where("pid = " . $v['id'] . "")->select();
        }
        $this->assign("cats", $cats);
        $this->display();
    }

    public function detail_post() {
        if (get_magic_quotes_gpc()) {

            function stripslashes_deep($value) {
                $value = is_array($value) ?
                        array_map('stripslashes_deep', $value) :
                        stripslashes($value);
                return $value;
            }

            $_POST = array_map('stripslashes_deep', $_POST);
        }
        $id = I('post.id', '', 'int');
        $data['title'] = I('post.title');
        $data['code'] = I('post.code');
        $data['pid'] = I('post.pid', '', 'int');
        $data['ord'] = I('post.ord', '30', 'int');
        $data['keywords'] = I('post.keywords');
        $data['description'] = I('post.description');
        $data['content'] = trim($_POST['content']);
        $data['addtime'] = time();
        if ($id > 0) {
            M("article")->where("id = " . $id . "")->save($data);
            $this->success('修改成功！', session('QUERY_STRING'));
        } else {
            M("article")->add($data);
            $this->success('添加成功！', U('Article/lists'));
        }
    }

    public function cat() {
        $sql = "1=1 AND pid = 0";
        $keywords = trim(I('get.keywords'));
        if ($keywords) {
            $sql .= " AND title like '%" . $keywords . "%'";
        }
        $count = M('article_cat')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('article_cat')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('ord asc')->select();
        foreach ($lists as $k => $v) {
            $lists[$k]['sub'] = M('article_cat')->where("pid =" . $v['id'] . "")->order('ord ASC')->select();
        }
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);

        $this->display();
    }

    public function cat_detail() {
        $id = I('get.id', '', 'int');
        $detail = M("article_cat")->where("id = " . $id . "")->find();
        $cats = M("article_cat")->where("pid = 0")->select();
        $this->assign("detail", $detail);
        $this->assign("cats", $cats);
        $this->display();
    }

    public function cat_detail_post() {
        $id = I("post.id");
        $data['title'] = I('post.title');
        $data['ord'] = I('post.ord', '30', 'int');
        $data['pid'] = I('post.pid', '', 'int');
        if ($id > 0) {
            M("article_cat")->where("id = " . $id . "")->save($data);
            $this->success('修改成功！', session('QUERY_STRING'));
        } else {
            M("article_cat")->add($data);
            $this->success('添加成功！', U('Article/cat'));
        }
    }

    public function single() {
        $count = M('news')->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('news')->limit($Page->firstRow . ',' . $Page->listRows)->order('id desc')->select();
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->display();
    }

    public function single_detail() {
        $id = I('get.id', '', 'int');
        $detail = M('news')->where("id = " . $id . "")->find();
        $this->assign("detail", $detail);
        $this->display();
    }

    public function single_detail_post() {
        if (get_magic_quotes_gpc()) {

            function stripslashes_deep($value) {
                $value = is_array($value) ?
                        array_map('stripslashes_deep', $value) :
                        stripslashes($value);
                return $value;
            }

            $_POST = array_map('stripslashes_deep', $_POST);
        }
        $id = I('post.id', '', 'int');
        $data['title'] = I('post.title');
        $data['ord'] = I('post.ord', '30', 'int');
        $data['keywords'] = I('post.keywords');
        $data['description'] = I('post.description');
        $data['content'] = $_POST['content'];
        $data['addtime'] = time();
        if ($id > 0) {
            M('news')->where("id = " . $id . "")->save($data);
            $this->success('修改成功！', session('QUERY_STRING'));
        } else {
            M('news')->add($data);
            $this->success('添加成功！', U('News/lists'));
        }
    }

}

?>
