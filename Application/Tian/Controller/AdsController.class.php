<?php

namespace Tian\Controller;

class AdsController extends CommonController {

    public function lists() {
        $sql = "1=1";
        if(C("DB_PWD") == ''){
            $keywords = trim(strip_tags(htmlspecialchars(strtolower($_GET['keyword']))));
        }else{
            $keywords = trim(strip_tags(htmlspecialchars(strtolower(iconv("gb2312","UTF-8",$_GET['keyword'])))));
        }
        $pid = I('get.pid', '', 'int');
        if ($keywords) {
            $sql .= " AND title like '%" . $keywords . "%'";
        }
        if ($pid) {
            $sql .= " AND pid = '" . $pid . "'";
        }
        $count = M('publish')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('publish')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('pid ASC,is_show DESC,ord ASC')->select();
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->assign("pid", $pid);
        $this->display();
    }

    public function detail() {
        $id = I('get.id', '', 'int');
        if ($id > 0) {
            $detail = M('publish')->where("id = " . $id . "")->find();
            if ($detail['is_start'] == 0) {
                $detail['starttime'] = date("Y-m-d");
                $detail['endtime'] = date("Y-m-d", strtotime("+1 day"));
            } else {
                $detail['starttime'] = date("Y-m-d", $detail['starttime']);
                $detail['endtime'] = date("Y-m-d", $detail['endtime']);
            }
        } else {
            $detail['starttime'] = date("Y-m-d");
            $detail['endtime'] = date("Y-m-d", strtotime("+1 day"));
            $detail['is_show'] = 1;
        }
        $cats = M("ads_cat")->select();
        $this->assign("cats", $cats);
        if ($id == '') {
            $detail['pid'] = $cats[0]['id'];
        }
        $cat = M("ads_cat")->where("id = " . $detail['pid'] . "")->find();
          $mtypes = array("1"=>"模板","2"=>"js","30"=>"视频","15"=>"源码","50"=>"拼团");
       
        $this->assign("cat", $cat);
        $this->assign("detail", $detail);
                $this->assign("mtypes", $mtypes);
        $this->display();
    }

    public function detail_post() {
        $id = I('post.id', '', 'int');
        /*         * *******logo上传 *********** */
        $table = "publish";
        include_once("common/logo.php");

        $data['title'] = I('post.title');
        $data['pid'] = I('post.pid', '', 'int');
        $data['ord'] = getOrdMax("publish", "pid = " . $data['pid'] . "");
        $data['url'] = I('post.url');
        $data['code'] = I('post.code');
        $data['content'] = I('post.content');
        $data['is_show'] = I('post.is_show', 0, 'int');
         $data['type'] = I('post.type', 0, 'int');
          $data['tid'] = I('post.tid', 0, 'int');

        if ($id > 0) {
            M($table)->where("id = " . $id . "")->save($data);

            $this->success('修改成功！', session('QUERY_STRING'));
        } else {
            M($table)->add($data);
            $this->success('添加成功！', U('Ads/lists'));
        }
        clearTempFile();
    }

    public function delpic($id, $field, $table) {
        $table = I("post.table") ? I("post.table") : $table;
        $field = I("post.field") ? I("post.field") : $field;
        $id = I("post.id", 0, 'int') ? I("post.id") : $id;
        $info = M($table)->field($field)->where("id = " . $id . "")->find();
        if ($info) {
            M($table)->where("id = " . $id . "")->save(array($data[$field] => ''));
        }
    }

    public function cat() {
        $sql = "1=1";
        $keywords = trim(I('get.keywords'));
        if ($keywords) {
            $sql .= " AND title like '%" . $keywords . "%'";
        }
        $count = M('ads_cat')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('ads_cat')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id desc')->select();
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->display();
    }

    public function cat_detail() {
        $id = I('get.id', '', 'int');
        if ($id > 0) {
            $detail = M("ads_cat")->where("id = " . $id . "")->find();
        }
        $this->assign("detail", $detail);
        $this->display();
    }

    public function cat_detail_post() {
        $id = I('post.id', '', 'int');
        $data['title'] = I('post.title');
        $data['ord'] = I('post.ord', '30', 'int');
        $data['code'] = I('post.code');
        if ($id > 0) {
            M("ads_cat")->where("id = " . $id . "")->save($data);
            $this->success('修改成功！', session('QUERY_STRING'));
        } else {
            M("ads_cat")->add($data);
            $this->success('添加成功！', U('Ads/cat'));
        }
        clearTempFile();
    }

}

?>
