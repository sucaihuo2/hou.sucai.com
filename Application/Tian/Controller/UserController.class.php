<?php

namespace Tian\Controller;

class UserController extends CommonController {

    public function lists() {
        $sql = "1=1 AND id != 1";
        $keyword = trim(I('get.keyword'));
        if ($keyword) {
            $sql .= " AND name like '%" . $keyword . "%' or id = '" . $keyword . "' or loginip =  '" . $keyword . "' or nickname like '%" . $keyword . "%' or email ='" . $keyword . "'";
        }
        $count = M('user')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('user')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id desc')->select();

        $month_last_start = mktime(0, 0, 0, date('m') - 1, 1, date('Y'));   //上个月初

        $month_last_end = mktime(0, 0, 0, date('m'), 1, date('Y')) - 24 * 3600;   //上个月末
        $month_start = mktime(0, 0, 0, date("m"), 1, date("Y"));
        $month_end = time();
        foreach ($lists as $k => $v) {
            $lists[$k]['download_times'] = M("download")->where("uid = " . $v['id'] . "")->count();
            $lists[$k]['last_month_sign'] = M("sign")->where("uid = " . $v['id'] . " AND addtime between " . $month_last_start . " AND " . $month_last_end . "")->count();
            $lists[$k]['month_sign'] = M("sign")->where("uid = " . $v['id'] . " AND addtime between " . $month_start . " AND " . $month_end . "")->count();
        }
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    public function detail() {
        $id = I('get.id', '', 'int');
        if ($id > 0) {
            $detail = M("user")->where("id = " . $id . "")->find();
            if ($detail['state'] == 1) {
                $log = M("admin_log")->where("uid = " . $detail['id'] . "")->find();
                $this->assign("log", $log);
            }
            $this->assign("detail", $detail);
        }
        $this->display();
    }

    public function detail_post() {
        if (I("post.verify") != 'sucaihuo6') {
//            $this->error('暂未开发');
        }
        $id = I('post.id', '', 'int');
//        $data['state'] = I('post.state');
//        if ($id == 0) {
//            $data['addtime'] = time();
//        }
//        $data['name'] = I('post.name');
        $pwd = I('post.pwd');
        if ($pwd) {
            $data['pwd'] = md5($pwd);
        }
        $pwd_pay = I('post.pwd_pay');
        if ($pwd_pay) {
            $data['pwd_pay'] = md5($pwd_pay);
        }
        $data['email'] = I('post.email');
        if ($data['email']) {
            $email_info = M("user")->where("email = '" . $data['email'] . "' AND id != " . $id . "")->find();
            if ($email_info) {
                $this->error('该邮箱已存在');
            }
        }

//        $data['phone'] = I('post.phone');
//        $data['invite_code'] = I('post.invite_code');
        if ($id > 0) {
            M("user")->where("id = " . $id . "")->save($data);
            $this->success('修改成功！', session('QUERY_STRING'));
        } else {

            $this->error('不能添加！', U('User/lists'));
        }
    }

    public function points() {
        $mtypes = getPointsType();
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        $points = trim(I('get.points'));
        $mtype = I('get.mtype');
        $ptype =  I('get.ptype');
        if ($keyword) {
            $userinfo = M("user")->where("name='" . $keyword . "'")->find();
            if ($userinfo) {
                $sql .= " AND uid = " . $userinfo['id'] . "";
            } else {
                $sql .= " AND uid = -1";
            }
//           $uids = getKeyUids($keyword);
//           if($uids){
//               $sql .= " AND uid in (".$uids.")";
//           }else{
//               $sql .= " AND uid = -1";
//           }
        }
        if ($points > 0) {
            $sql .= " AND money >= '" . $points . "'";
        }
        if ($mtype) {
            $sql .= " AND mtype = '" . $mtype . "'";
        }
        if ($ptype ==1) {
            $sql .= " AND points_type = 0";
        }elseif ($ptype ==2) {
            $sql .= " AND points_type = 1";
        }
        $count = M('points')->where($sql)->count();    //计算总数
//        echo M('points')->getlastsql();
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('points')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id desc')->select();

        foreach ($lists as $k => $v) {
            $lists[$k]['is_check'] = getSingleField($v['uid'], 'user', 'is_check');
            $mtype = 0;
            if ($v['mtype'] == 'source') {
                $mtype = 15;
            } elseif ($v['mtype'] == 'js') {
                $mtype = 2;
            } elseif ($v['mtype'] == 'modals') {
                $mtype = 1;
            } elseif ($v['mtype'] == 'video') {
                $mtype = 30;
            }
            
            $user_info = M("user")->field("huobi,name,nickname,loginip")->where("id = " . $v['uid'] . "")->find();
            $lists[$k]['huobi'] = $user_info['huobi'];
            $lists[$k]['username'] = getNickname($user_info['name'],$user_info['nickname']);
            $lists[$k]['loginip'] = $user_info['loginip'];
//            $downloads = M("download")->where("uid = " . $v['uid'] . "")->order("addtime DESC")->select();
////                  echo M("download")->getlastsql();
//            foreach ($downloads as $k2 => $v2) {
//                $table = getTableInfo($v2['mtype']);
//
//                $downloads[$k2]['table'] = $table;
//                $info = M($table)->field("name,tags")->where("id = " . $v2['tid'] . "")->find();
//                $downloads[$k2]['title'] = $info['name'];
//            }
//
//            $lists[$k]['downloads'] = $downloads;


//            if ($mtype > 0) {
//                $download_info = M("download")->field("ip")->where("uid = " . $v['uid'] . " AND tid = " . $v['tid'] . " AND mtype = " . $mtype . "")->find();
//                $lists[$k]['ip'] = $download_info['ip'];
//            }
        }
        
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("mtype", I('get.mtype'));
        $this->assign("mtypes", $mtypes);
            $this->assign("ptype", $ptype);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    public function vip() {

        $sql = "status = 1";
        $keyword = trim(I('get.keyword'));
        if ($keyword) {
            $uids = getKeyUids($keyword);
            if ($uids) {
                $sql .= " AND uid in (" . $uids . ")";
            } else {
                $sql .= " AND uid = -1";
            }
        }

        $count = M('order_month')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('order_month')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('addtime desc')->select();
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    public function accounts() {
        $lists = M('accounts')->order('ord ASC,id desc')->select();
        $this->assign("lists", $lists);
        $this->display();
    }

    public function accounts_detail() {
        $id = I('get.id', '', 'int');
        $detail = M("accounts")->where("id = " . $id . "")->find();
        $threeLogin = threeLogin();
        $this->assign("detail", $detail);
        $this->assign("threeLogin", $threeLogin);
        $this->display();
    }

    public function accounts_detail_post() {
        $id = I('post.id', '', 'int');
        $data['name'] = I('post.name');
        $data['key'] = I('post.key');
        $data['secret'] = I('post.secret');
        $data['code'] = I('post.code');
        $data['is_check'] = I('post.is_check');
        $data['ord'] = I('post.ord', '30', 'int');
        if ($id > 0) {
            M("accounts")->where("id = " . $id . "")->save($data);
            $this->success('修改成功！', U('User/accounts'));
        } else {
            M("accounts")->add($data);
            $this->success('添加成功！', U('User/accounts'));
        }
        clearTempFile();
    }

    public function admin() {
        $sql = "1=1 AND id != 1 AND is_admin = 1";
        $keyword = trim(I('get.keyword'));
        if ($keyword) {
            $sql .= " AND name like '%" . $keyword . "%'";
        }
        $count = M("user")->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M("user")->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id desc')->select();
//        echo M("user")->getlastsql();
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    public function admin_add() { //管理员添加
        $powers = M("admin_menu2")->where("pid = 0 AND id != 1")->order("ord ASC")->select();
        foreach ($powers as $k => $v) {
            $powers[$k]['sub'] = M("admin_menu2")->where("pid = " . $v['id'] . "")->order("ord ASC")->select();
        }
        $detail['is_admin'] = 1;
        $this->assign("detail", $detail);
        $this->assign("powers", $powers);
        $this->display("User:admin_detail");
    }

    public function admin_edit() { //管理员编辑
        $id = I('get.id', '', 'int');
        if ($id > 0) {
            $powers = M("admin_menu2")->where("pid = 0 AND id != 1 AND is_show in (0,1)")->order("ord ASC")->select();
            foreach ($powers as $k => $v) {
                $powers[$k]['sub'] = M("admin_menu2")->where("pid = " . $v['id'] . " AND is_show in (0,1)")->order("ord ASC")->select();
            }
            $detail = M("user")->where("id = " . $id . "")->find();

            $this->assign("detail", $detail);
            $this->assign("powers", $powers);
        }
        $this->display("User:admin_detail");
    }

    public function admin_detail_post() {

        $id = I('post.id', '0', 'int');
        if ($id == 1) {
            $this->error("您没有该权限！");
            exit;
        }
//        $data['name'] = I('post.phone');
//        if ($id == 0) {
//            $user = M("user")->field("id")->where("name = '" . $data['name'] . "'")->find();
//            if ($user) {
//                $id = $user['id'];
//            } else {
//                $data['addtime'] = time();
//            }
//        }
//        $pwd = I('post.pwd');
//        if ($pwd) {
//            $data['pwd'] = md5($pwd);
//        }

        $data['name'] = I('post.name');

//        $data['is_admin'] = I("post.is_admin");
//        if ($data['is_admin'] == 1) {
        $data['powers'] = I('post.powers') ? implode(",", I('post.powers')) : "";
//        } else {
//            $data['powers'] = "";
//        }

        M("user")->where("id = " . $id . "")->save($data);
        $this->success('修改成功！', session('QUERY_STRING'));
    }

    public function abnormal_post() {
        $data['user_freeze'] = I("post.user_freeze");
        $user_freeze_arr = explode(",", $data['user_freeze']);
        $config = M("config")->field("user_freeze")->where('id=1')->find();
        $user_freeze_config = explode(",", $config['user_freeze']);
        foreach ($user_freeze_config as $v) {
            if (!in_array($v, $user_freeze_arr)) {
                M("user")->where("name = '" . $v . "'")->save(array("login_error" => 0));
            }
        }
        $data['user_banned'] = I("post.user_banned");
        $data['ip_out'] = I("post.ip_out");
        M("config")->where("id = 1")->save($data);
        $this->success('修改成功！', U('User/abnormal'));
    }

    public function getPointsType() {
        $arr = array(
            "1" => "下载模板",
            "30" => "下载视频",
            "2" => "下载js",
            "15" => "下载源码",
        );
        return $arr;
    }

    public function downloads() {
        $s_admin_uid = getAdminId();
        $mtypes = $this->getPointsType();
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        $points = trim(I('get.points', 0, 'int'));
        $points2 = trim(I('get.points2', 0, 'int'));
        $order_num = trim(I('get.order_num'));
        $mtype = 15;
        if ($keyword) {
            $userinfo = M("user")->where("name='" . $keyword . "'")->find();
            if ($userinfo) {
                $sql .= " AND uid = " . $userinfo['id'] . "";
            } else {
                $sql .= " AND ip ='" . $keyword . "'";
            }
//           $uids = getKeyUids($keyword);
//           if($uids){
//               $sql .= " AND uid in (".$uids.")";
//           }else{
//               $sql .= " AND uid = -1";
//           }
        }
        $ord = trim(I('get.ord'));
        $sql_order = "addtime desc";
        if ($ord == 1) {
            $sql_order = "edittime desc";
        }
        if ($points > 0 && $points2 > 0) {

            $sql .= " AND points between '" . $points . "' AND '" . $points2 . "' ";
        } else {
            if ($points > 0) {
                $sql .= " AND points >= '" . $points . "'";
            }
            if ($points2 > 0) {
                $sql .= " AND points <= '" . $points2 . "'";
            }
        }
        if ($mtype) {
            $sql .= " AND mtype = '" . $mtype . "'";
        }
        if ($order_num) {
            $sql .= " AND order_num = '" . $order_num . "'";
        }
        $count = M('download')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        if ($s_admin_uid == 1 or $order_num != '' or $keyword != '') {


            $lists = M('download')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order($sql_order)->select();

            foreach ($lists as $k => $v) {
                $table = getMtypeTable($v['mtype']);
                $info = M($table)->field("name,tags")->where("id = " . $v['tid'] . "")->find();
                $lists[$k]['title'] = $info['name'];
            }
        }

        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("mtype", $mtype);
        $this->assign("mtypes", $mtypes);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    public function downloads2() {
        $mtypes = $this->getPointsType();
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        $points = trim(I('get.points'));
        $order_num = trim(I('get.order_num'));
        $mtype = 15;
        if ($keyword) {
            $userinfo = M("user")->where("name='" . $keyword . "'")->find();
            if ($userinfo) {
                $sql .= " AND uid = " . $userinfo['id'] . "";
            } else {
                $sql .= " AND ip ='" . $keyword . "'";
            }
//           $uids = getKeyUids($keyword);
//           if($uids){
//               $sql .= " AND uid in (".$uids.")";
//           }else{
//               $sql .= " AND uid = -1";
//           }
        }
        if ($points > 0) {
            $sql .= " AND money >= '" . $points . "'";
        }
        if ($mtype) {
            $sql .= " AND mtype = '" . $mtype . "'";
        }
        if ($order_num) {
            $sql .= " AND order_num = '" . $order_num . "'";
        }

        $count = M('download')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('download')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('edittime desc')->select();

        foreach ($lists as $k => $v) {
            $table = getMtypeTable($v['mtype']);
            $info = M($table)->field("name,tags")->where("id = " . $v['tid'] . "")->find();
            $lists[$k]['title'] = $info['name'];
        }
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("mtype", $mtype);
        $this->assign("mtypes", $mtypes);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    //http://admin7788.sucaihuo.com/Tian/User/d_update
    public function d_update() {
//        $lists = M('download')->where("order_num is null")->limit(20000)->order('id desc')->select();
//        foreach ($lists as $v) {
//            $data['order_num'] = date("YmdHis", $v['addtime']) . rand(10000, 99999);
//            M('download')->where("id = '" . $v['id'] . "'")->save($data);
//        }
    }

}

?>
