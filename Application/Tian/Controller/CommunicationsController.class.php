<?php

namespace Tian\Controller;

class CommunicationsController extends CommonController {

    public function shorts() {
        $sql = "1=1";
        $count = M('shorts')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('shorts')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('addtime desc')->select();
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->display();
    }

    public function shorts_more_post() {
        $content = I('post.content');
        $phones = I('post.phones');
        $phones = array_unique(explode("\n", $phones));
        $rs = sendMobile(implode(",", $phones), $content, 'moresend');
        $word = getMessageResult($rs);
        if ($rs == 0) {
            $this->error($word, U('Shorts/send'));
        } else {
            $this->success('发送成功！', U('Shorts/send'));
        }
    }

    public function shorts_set() {
        $info = M("three")->where("id = 1")->find();
        $detail = json_decode($info['shortMessage'], true);
        $num = M("user")->where("is_shorts")->count();
        $this->assign("num", $num);
        $this->assign("detail", $detail);
        $this->display();
    }

    public function shorts_set_post() {
        $arr = array();
        $arr['account'] = I('post.account');
        if (I('post.pwd')) {
            $arr['pwd'] = md5(I('post.pwd'));
        } else {
            $info = M("three")->where("id = 1")->find();
            $detail = json_decode($info['shortMessage'], true);
            $arr['pwd'] = $detail['pwd'];
        }
        $arr['is_start'] = I('post.is_start');
        $arr['send_method'] = I('post.send_method');
        $arr['send_suc'] = I('post.send_suc');
        $arr['order_cancel'] = I("post.order_cancel");
        $arr['is_order_cancel'] = I("post.is_order_cancel");

        $arr['users'] = I('post.users');
        $arr['is_auto'] = I('post.is_auto');
        $arr['phones'] = I('post.phones');
        $arr['is_phone_bind'] = I("post.is_phone_bind");
        $arr['phone_bind'] = I("post.phone_bind");
        $arr['order_cancel_times'] = I("post.order_cancel_times");
        $arr['phone_bind_times'] = I("post.phone_bind_times");
        $arr['send_suc_times'] = I("post.send_suc_times");
        $arr['order_cancel'] = I("post.order_cancel");
        $order_find_receiver = I("post.order_find_receiver");
        if ($order_find_receiver) {
            $arr['order_find_receiver'] = implode(",", $order_find_receiver);
        } else {
            $arr['order_find_receiver'] = '';
        }
        M("three")->where("id = 1")->save(array("shortMessage" => json_encode($arr)));
        clearTempFile();
        $this->success('修改成功！', U('Communications/shorts_set'));
    }

    public function email() {
        $sql = "1=1 AND id>22573";
        $keywords = trim(I('get.keywords'));
        if ($keywords) {
            $sql .= " AND title like '%" . $keywords . "%'";
        }
        $count = M('email_record')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('email_record')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id desc')->select();
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->display();
    }

    public function email_set() {
        $info = M("three")->where("id = 1")->find();
        $detail = json_decode($info['emails'], true);
        $num = M("user")->where("is_shorts")->count();
        $this->assign("num", $num);
        $this->assign("detail", $detail);
        $this->display();
    }

    public function email_set_post() {
        $arr = array();
        $arr['account'] = I('post.account');
        $arr['smpt'] = I('post.smpt');
        $arr['pwd'] = I('post.pwd');
        $arr['is_start'] = I('post.is_start');
        $arr['send_method'] = I('post.send_method');
        $arr['send_suc'] = I('post.send_suc');
        $arr['is_auto'] = I('post.is_auto');
        $arr['emails'] = I('post.emails');
        M("three")->where("id = 1")->save(array("emails" => json_encode($arr)));
        clearTempFile();
        $this->success('修改成功！', U('Communications/email_set'));
    }

    public function email_more_post() {
        $content = I('post.content');
        $emails = I('post.emails');
        $emails = array_unique(explode("\n", $emails));
        foreach ($emails as $v) {
            $state = sendEmail($v, $content, 'moresend');
        }
        if ($state == 1) {
            $this->success('发送成功！', U('Communications/email_more'));
        } else {
            $this->error($state, U('Communications/email_more'));
        }
    }

    public function send_no() {
        $id = I("post.id");
//        if($id>22567){
//            
//        }else{
//            echo '超过';exit;
//        }
        $email_record = M("email_record")->where("id = " . $id . "")->find();
        if($email_record['second_email_from']){
            echo '已二次发送';exit;
        }

        $email = $email_record['accounts'];
//                print_r($email);exit;

        $paras = array();
        $paras['content'] = $email_record['content'];
        $paras['title'] = $email_record['title'];
        $rs = sendEmail('send_no', $email, $paras);
        M("email_record")->where("id = " . $id . "")->save(array("second_email_from"=>$rs['email_from'],"second_result"=>$rs['result']));
        print_r($rs);
    }
    

}

?>
