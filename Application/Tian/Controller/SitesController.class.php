<?php

namespace Tian\Controller;

class SitesController extends CommonController {

    public function lists() {
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND (name like '%" . $keyword . "%' or keywords like '%" . $keyword . "%')";
        }
        $modal_cat_id = I('get.modal_cat_id', 0, 'int'); //分类筛选
        if ($modal_cat_id > 0) {
            $sql .= " AND cat_id = " . $modal_cat_id . "";
        }
        $modal_color_id = I('get.modal_color_id', 0, 'int'); //颜色筛选
        if ($modal_color_id > 0) {
            $sql .= " AND FIND_IN_SET('" . $modal_color_id . "',color_id)";
        }
        $modal_lay_id = I('get.modal_lay_id', 0, 'int'); //布局筛选
        if ($modal_lay_id > 0) {
            $sql .= " AND lay_id = " . $modal_lay_id . "";
        }
        $count = M('sites')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('sites')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
//        echo M('sites')->getlastsql();
        $modal_cats = getDictionarySubSql('modal_cat_id', 'sites_dictionary');
        $modal_colors = getDictionarySubSql('modal_color_id', 'sites_dictionary');
        $modal_lays = getDictionarySubSql('modal_lay_id', 'sites_dictionary');
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->assign("modal_cat_id", $modal_cat_id);
        $this->assign("modal_cats", $modal_cats);
        $this->assign("modal_color_id", $modal_color_id);
        $this->assign("modal_lay_id", $modal_lay_id);
        $this->assign("modal_colors", $modal_colors);
        $this->assign("modal_lays", $modal_lays);
        $this->display();
    }

    public function detail() {
        $templatesModel = new \Tian\Model\ModalsModel();
        $id = I('get.id', '0', 'int');
        if ($id > 0) {
            $detail = M("sites")->where("id =" . $id . "")->find();
        } else {
            $detail['lay_id'] = 45;
        }
        if (!empty($detail['tags'])) {
                $tagsArr = explode(",", $detail['tags']);
                foreach ($tagsArr as $v) {
                    $info = M("sites_tags")->where("id = " . $v . "")->find();
                    $tags[] = $info['name'];
                }
            }else{
                $tags = array(
                  0=>"",
                   1=>"",
                    2=>"",
                    3=>"",
                    4=>""
                );
            }
        $modals_cat = $templatesModel->getModalsDictionary('modal_cat_id'); //模板分类
        foreach ($modals_cat as $k => $v) {
            $modals_cat[$k]['sub'] = M("sites_dictionary")->where("pid = " . $v['id'] . " AND is_check = 1")->select();
        }
        $modals_color = $templatesModel->getModalsDictionary('modal_color_id'); //模板颜色
        $modals_lay = $templatesModel->getModalsDictionary('modal_lay_id'); //模板布局
        $next = M('sites')->field("name,id")->where("id > " . $id . "")->order("id ASC")->find();
$prev = M('sites')->field("name,id")->where("id < " . $id . "")->order("id DESC")->find();
$this->assign("next", $next);
$this->assign("prev", $prev);
        $this->assign("modals_cat", $modals_cat);
        $this->assign("modals_color", $modals_color);
        $this->assign("modals_lay", $modals_lay);
        $this->assign("detail", $detail);
        $this->assign("mtype", '3');
        $this->assign("id", $id);
        $this->assign("tags", $tags);
        $this->display();
    }

    public function detail_post() {
        $id = I('post.id', '', 'int');
        $data['name'] = trim(I("post.name"));
        $data['cat_id'] = $_POST['cat_id'] ? implode(",", $_POST['cat_id']) : "";
        $data['cat_sub_id'] = $_POST['cat_sub_id'] ? implode(",", $_POST['cat_sub_id']) : "";
        $data['color_id'] = I("post.modal_color_id") ? implode(",", I("post.modal_color_id")) : "";
        $data['lay_id'] = I("post.modal_lay_id", 0, 'int');
        $data['ord'] = I("post.ord", 0, 'int');
        $data['keywords'] = I("post.keywords");
        $data['description'] = trim(I("post.description"));
        $data['source'] = I("post.source");
        $data['is_check'] = 1;
        $tagsArr = $_POST['tag'];
        $tag_i = 0;
        foreach ($tagsArr as $v) {
            if (!empty($v)) {
                $info = M("sites_tags")->field("id")->where("name = '" . $v . "'")->find();
                if (empty($info)) {
                    $tag_i++;
                    $tag_max_ord = M("sites_tags")->max('ord') + $tag_i;
                    $tag_lastid = M("sites_tags")->add(array("name" => $v, "ord" => $tag_max_ord, "is_check" => 1));
                } else {
                    $tag_lastid = $info['id'];
                }
                $tags[] = $tag_lastid;
            }
        }
        $data['tags'] = !empty($tags) ? implode(",", $tags) : "";

        if ($id > 0) {

            M("sites")->where("id = " . $id . "")->save($data);
            $lastid = $id;
        } else {
            $data['uid'] = session("admin_uid");
            $data['addtime'] = time();
            $lastid = M("sites")->add($data);
        }
        getTagsNum(3); //统计标签
        if ($id > 0) {
            goUrl(U('Sites/detail', array("id" => $id)));
        } else {
            goUrl(U('Sites/lists'));
        }
    }

    public function detail_post2() {
        set_time_limit(0);
        header("Content-type: text/html; charset=UTF-8");
        $sources = array_unique(array_filter(I('post.source')));
        $titles = array_unique(array_filter(I('post.titles')));
        $keywords = array_unique(array_filter(I('post.keywords')));
        $descpritions= array_unique(array_filter(I('post.descpritions')));
        if ($sources) {
            foreach ($sources as $k => $v) {
                $len = strlen($v) - 1;
                if ($v{$len} == '/') {
                    $v = substr($v, 0, -1);
                
                }
                if ($v) {
                    $data['source'] = $v;
                  
                    $data['name'] = $titles[$k];
                    $data['keywords'] = $keywords[$k];
                    $data['description'] = $descpritions[$k];
                    $info = M("source")->field("id")->where("source = '" . $v . "'")->find();
                    $data['is_check'] = 1;
                    $data['addtime'] = time();
                    if (empty($info)) {
               M("sites")->add($data);
                    }
                }
            }
        }
      $this->success("添加参考网站成功！", U("Sites/detail2"));
    }

    public function getUrlInfo() {
        ini_set("max_execution_time", "3"); 
        set_time_limit(1);
        header("Content-type: text/html; charset=UTF-8");
        $data['source'] = I('post.source');
        $url = $data['source'];
        $content = file_get_contents($url);
        $charset = getCharset($content);
        $data['charset'] = $charset ? $charset : "utf-8";
        $arr = getSeo($url);
        $data['title'] = changeCharset($arr['title'], $charset);
        $data['keywords'] = changeCharset($arr['keywords'], $charset);
        $data['descprition'] = changeCharset($arr['description'], $charset);

        echo json_encode($data);
    }
    public function tags() {
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND name like '%" . $keyword . "%'";
        }
        $count = M('sites_tags')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('sites_tags')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('ord ASC,id DESC')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    public function tags_detail() {//标签详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('sites_tags')->where("id = " . $id . "")->find();
        } else {
            $detail['pid'] = I("get.pid", 0, 'int');
            $detail['is_check'] = 1;
            $detail['ord'] = M("sites_tags")->max('ord') + 1;
        }

        $this->assign("detail", $detail);
        $this->display();
    }

    public function tags_detail_post() {
        $id = I("post.id", 0, 'int');
        $data['is_check'] = I("post.is_check", 0, 'int');
        if ($id > 0) {
            $data['name'] = strtolower(trim(SI("post.name")));
            $data['ord'] = I("post.ord", 0, 'int');
            $data['keywords'] = strtolower(trim(I("post.keywords")));
            $data['description'] = strtolower(trim(I("post.description")));
            M('sites_tags')->where("id = " . $id . "")->save($data);
            $this->success("修改特效标签成功！", session('QUERY_STRING'));
        } else {
            $names = array_unique(array_filter(I('post.name')));
            foreach ($names as $k => $v) {
                if ($v) {
                    $data['name'] = $v;
                    $data['ord'] = I('post.ord', '30', 'int') + $k;
                    M('sites_tags')->add($data);
                }
            }
            $this->success("添加特效标签成功！", U("Sites/tags"));
        }
    }
     public function cat() {//网址类别
        $pid = I('get.pid', '0', 'int');
        $sql = "pid = " . $pid . "";
        $keywords = trim(I('get.keywords'));
        if ($keywords) {
            $sql .= " AND name like '%" . $keywords . "%'";
        }
        $count = M('sites_dictionary')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('sites_dictionary')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('ord ASC')->select();
        foreach ($lists as $k => $v) {
            $lists[$k]['num'] = M('sites_dictionary')->where("pid = " . $v['id'] . "")->count(); //下级个数
            if ($v['tags']) {
                $lists[$k]['tags_num'] = count(explode(",", $v['tags']));
            } else {
                $lists[$k]['tags_num'] = 0;
            }
        }
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->assign("pid", $pid);
        $this->display();
    }

    public function cat_detail() {//网址类别详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('sites_dictionary')->where("id = " . $id . "")->find();
        } else {
            $detail['pid'] = I("get.pid", 0, 'int');
            $detail['is_check'] = 1;
        }
        $cats_top = M('sites_dictionary')->where("pid = 0")->order('ord ASC')->select();
        foreach ($cats_top as $k => $v) {
            $cats_top[$k]['second'] = M('sites_dictionary')->where("pid = " . $v['id'] . "")->order('ord ASC')->select();
        }
        $this->assign("detail", $detail);
        $this->assign("cats_top", $cats_top);
        $this->display();
    }

    public function cat_detail_post() {

        $id = I("post.id", 0, 'int');
        $data['pid'] = I("post.pid", 0, 'int');
        $data['is_check'] = I("post.is_check", 0, 'int');

        $pinfo = M('sites_dictionary')->field("pid")->where("id = " . $data['pid'] . "")->find();
        if ($pinfo['pid'] > 0) {
            $level = 2;
        } else if ($pinfo['pid'] == 0) {
            $level = 1;
        } else {
            $level = 0;
        }
        $data['level'] = $level;
                 $data['seo_title'] = trim(I("post.seo_title"));
        $data['keywords'] = trim(I("post.keywords"));
        $data['description'] = trim(I("post.description"));
        if ($id > 0) {
            $data['name'] = trim(I("post.name"));
            $data['ord'] = I("post.ord", 0, 'int');
            M('sites_dictionary')->where("id = " . $id . "")->save($data);
            $this->success("修改网址类别成功！", session('QUERY_STRING'));
        } else {
            $names = array_unique(array_filter(I('post.name')));
            foreach ($names as $k => $v) {
                if ($v) {
                    $data['name'] = $v;
                    $data['ord'] = I('post.ord', '30', 'int') + $k;
                    M('sites_dictionary')->add($data);
                }
            }
            $this->success("添加网址类别成功！", U("Sites/cat", array("pid" => $data['pid'])));
        }
    }


}

?>
    