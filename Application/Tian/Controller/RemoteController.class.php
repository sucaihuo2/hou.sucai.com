<?php

namespace Tian\Controller;

use Think\Controller;
use PclZip;
use QueryList;

class RemoteController extends Controller {

    public function getDatas() { //http://localhost/sucai/Tian/Remote/getDatas
        $sevice_max_id = M("js")->max("id") + 1;
        $max_id = M("js")->max("id");
        for ($i = $sevice_max_id; $i <= $max_id; $i++) {
            $info = M("js")->where("id = " . $i . "")->find();
            $data = array();
            foreach ($info as $k => $v) {
                $data[$k] = $v;
            }
            $lastid = M("js")->add($data);

            echo $lastid . "<hr>";
        }

        $sevice_max_id = M("modals")->max("id") + 1;
        $max_id = M("modals")->max("id");
        for ($i = $sevice_max_id; $i <= $max_id; $i++) {
            $info = M("modals")->where("id = " . $i . "")->find();
            $data = array();
            foreach ($info as $k => $v) {
                $data[$k] = $v;
            }
            $lastid = M("modals")->add($data);

            echo $lastid . "<hr>";
        }
    }

    //从服务器拉最新数据
    public function addDatas() { //http://localhost/sucai/Tian/Remote/addDatas
        $sevice_max_id = M("js")->max("id");
        $max_id = M("js")->max("id") + 1;
        for ($i = $max_id; $i <= $sevice_max_id; $i++) {
            $info = M("js")->where("id = " . $i . "")->find();
            $data = array();
            foreach ($info as $k => $v) {
                $data[$k] = $v;
            }
            $lastid = M("js")->add($data);

            echo $lastid . "<hr>";
        }

        $sevice_max_id = M("modals")->max("id");
        $max_id = M("modals")->max("id") + 1;
        for ($i = $max_id; $i <= $sevice_max_id; $i++) {
            $info = M("modals")->where("id = " . $i . "")->find();
            $data = array();
            foreach ($info as $k => $v) {
                $data[$k] = $v;
            }
            $lastid = M("modals")->add($data);

            echo $lastid . "<hr>";
        }
    }

    public function getZipJs() {//http://localhost/sucai/Tian/Remote/getZipJs 错误
        header("Content-type: text/html; charset=utf8");
        set_time_limit(0);
        import("Common.Org.PclZip");

        $file_help = "素材火帮助.txt";
        $file_help = getGb2312($file_help);
        $bei = 9;
        $file_path = "jquery/" . $bei . "/";
        $lists = scandir($file_path);


        foreach ($lists as $v) {

            if ($v > 0) {
                $info = M("js")->field("name,ord")->where("id = " . $v . "")->find();

                $name = getGb2312($info['name']);
                $file_v = $file_path . $v . "/";
                $file_v_demo = $file_v . "/demo/";
                $file_v_zip = $file_v . "/" . $name . ".zip";

                file_put_contents($file_v_demo . $file_help, file_get_contents($file_help));
                echo $v . "<hr>";
                $images = scandir($file_v);
                $i = 0;


                $files = array(
                    0 => $file_v_demo,
                    1 => $file_help
                );
//                print_r($files);
                if ($info['ord'] != -3 && $info['ord'] != 3) {
                    $rs = createReplaceZip($file_v_zip, $files, $file_v_demo, $name);
                }


                foreach ($images as $v2) {
                    $type = strtolower(get_extension($v2));
//            echo $v2."<hr>";
                    if (in_array($type, array("png", "jpg")) && !in_array($v2, array("middle.jpg", "big.jpg", "small.jpg"))) {
                        $file_name = $file_v . $v2;
                        $size = getimagesize($file_name);
                        $width = $size[0];
                        if ($width == 238) {
                            $name_last = 'middle.jpg';
                        } else if ($width == 233) {
                            $name_last = 'small.jpg';
                        } else {
                            $name_last = 'big.jpg';
                        }

                        if (file_exists($file_name)) {
                            $file_get = file_get_contents($file_name);
                            file_put_contents($file_v . $name_last, $file_get);
                            unlink($file_name);
                        } else {
                            echo "middle.jpg下载文件不存在";
                        }
                    }
                }
                unlink($file_v_demo . $file_help);
                sleep("0.1");
            }
        }
        echo "<script>location.href='http://localhost/sucai/Tian/Remote/getZipModal2';</script>";
    }

    public function updateJs() { //http://localhost/sucai/Tian/Remote/updateJs
//     $json = getUrlJson("http://www.sucaihuo.com/Remote/getJsLists");
        $json = getUrlJson("http://www.sucaihuo.com/Tian/Remote/getJsLists");
        $json = str_replace("﻿", "", $json);
        $arr = json_decode($json, true);
        foreach ($arr as $v) {
            $data5['times_view'] = $v['times_view'];
            $data5['times_comments'] = $v['times_comments'];
            $data5['times_download'] = $v['times_download'];
            M("js")->where("id = " . $v['id'] . "")->save($data5);
            echo M("js")->getlastsql();
        }

        $json = getUrlJson("http://www.sucaihuo.com/Tian/Remote/getModalsLists");
        $json = str_replace("﻿", "", $json);
        $arr = json_decode($json, true);
        foreach ($arr as $v) {
            $data2['times_view'] = $v['times_view'];
            $data2['times_comments'] = $v['times_comments'];
            $data2['times_download'] = $v['times_download'];
            M("modals")->where("id = " . $v['id'] . "")->save($data2);
//            echo M("modals")->getlastsql();
        }

        $json = getUrlJson("http://www.sucaihuo.com/Tian/Remote/getSitesLists");
        $json = str_replace("﻿", "", $json);
        $arr = json_decode($json, true);
//        print_r($arr);
        foreach ($arr as $v) {
            $data3['times_view'] = $v['times_view'];
            $data3['times_comments'] = $v['times_comments'];
            M("sites")->where("id = '" . $v['id'] . "'")->save($data3);
            echo M("sites")->getlastsql();
        }
        //延迟发布时间
        //模板
        $id_modal = 700;
        $info = M("modals")->field("addtime,id")->where("id = " . $id_modal . "")->find();
        $lists = M("modals")->field("id,addtime")->where("id > " . $id_modal . "")->order("id ASC")->select();
        foreach ($lists as $k => $v) {

            $data6['addtime'] = time();
            M("modals")->where("id = " . $v['id'] . "")->save($data6);
            echo date("Y-m-d H:i", $data6['addtime']) . "<hr>";
        }
        //js
        $id_js = 900;
        $info = M("js")->field("addtime,id")->where("id = " . $id_js . "")->find();
        $lists = M("js")->field("id,addtime,type")->where("id > " . $id_js . "")->order("id ASC")->select();
        foreach ($lists as $k => $v) {
            if ($v['type'] == 0) {
                $data7['addtime'] = time();
//                $data7['addtime'] = time() - rand(1, 3600 * 30);
                M("js")->where("id = " . $v['id'] . "")->save($data7);
                echo date("Y-m-d H:i", $data7['addtime']) . "<hr>";
            }
        }
        $url = __APP__ . "/Tian/Bak/index/Action/backup";
        echo "<script>location.href='" . $url . "'</script>";
    }

    public function getJsLists() {
        $lists = M("js")->field("id,times_view,times_comments,times_download")->select();

        echo json_encode($lists);
    }

    public function getModalsLists() {
        $lists = M("modals")->field("id,times_view,times_comments,times_download")->select();

        echo json_encode($lists);
    }

    public function getSitesLists() {
        $lists = M("sites")->field("id,times_view,times_comments")->select();

        echo json_encode($lists);
    }

    public function getImg() { //http://localhost/sucai/Tian/Remote/getImg
        header("Content-type: text/html; charset=utf-8");
        $file = "C:\Users\Administrator\Downloads";
        $images = scandir($file);
        $file2 = "sites";
        $images2 = scandir($file2);
        $emptyImg = '';
//        print_r($images2);
        $nums = array();
        foreach ($images2 as $v) {
            if ($v != '..' && $v != '.') {
                $info = scandir($file2 . "/" . $v);
                $nums[] = $v;
//                 echo $v."<hr>";
                if ($info[2] == '') {
                    $emptyImg = $file2 . "/" . $v . "/";
                    break;
                }
            }
        }
        $max = max($nums);
        if ($emptyImg == '') {
            $emptyImg = $file2 . "/" . ($max + 1) . "/";
            mkdir($emptyImg);
        }
        $i = 0;
        foreach ($images as $v) {
            $type = strtolower(get_extension($v));
            if ($type == 'png') {
                $i++;
//                $v = iconv('GB2312', 'UTF-8', $v);
//                $v = iconv('UTF-8', 'GB2312', $v);
                $file_name = $file . "/" . $v;
                $size = getimagesize($file_name);
                $width = $size[0];
                if ($width == 238) {
                    if (file_exists($file_name)) {
                        $file_get = file_get_contents($file_name);
                        file_put_contents($emptyImg . "middle.jpg", $file_get);
                        echo $file_name . "存放到：" . $emptyImg . "<hr>";
                        unlink($file_name);
                    } else {
                        echo "middle.jpg下载文件不存在";
                    }
                } else {
                    if (file_exists($file_name)) {
                        $file_get = file_get_contents($file_name);
                        file_put_contents($emptyImg . "big.jpg", $file_get);
                        echo $file_name . "存放到：" . $emptyImg . "<hr>";
                        unlink($file_name);
                    } else {
                        echo "big.jpg下载文件不存在";
                    }
                }
            }
        }
        if ($i == 0) {
            echo "无图片！";
        }
    }

    public function getCaiji() {//http://localhost/sucai/Tian/Remote/getCaiji
        require 'common/QueryList.class.php';
        $arr = array();
        $pattern = array("content" => array("table.dataintable  td", "text"));
        $url = "http://www.w3school.com.cn/jquery/jquery_ref_selectors.asp";
        $qy = new QueryList($url, $pattern, '', '', 'utf-8');
        $rs = $qy->jsonArr;
//print_r($rs);
        foreach ($rs as $k => $v) {
            $ceil = ceil(($k + 1) / 3);
            $bi = $k % 3;
            if ($bi == 0) {
                $arr[$ceil]['name'] = $v['content'];
            }
            if ($bi == 2) {
                $arr[$ceil]['desc'] = $v['content'];
            }
            if ($bi == 1) {
                $arr[$ceil]['default'] = $v['content'];
            }
        }
        $i = 0;
        foreach ($arr as $k => $v) {
//      echo strlen($v['name'])."<hr>";
            if (strlen($v['name']) == 2 && strlen($v['desc']) == 2 && strlen($v['default']) == 2) {

                $arr[$k]['name'] = "";
//                $i++;
            }
        }
        print_r($arr);
        $parameters = array();
        foreach ($arr as $k => $v) {
            $parameters[$k]['paras'] = trim($v['name']);
            $parameters[$k]['descriptions'] = trim($v['desc']);
            $parameters[$k]['defaults'] = trim($v['default']);
        }

        $parameters_json = json_encode($parameters);
        M("js")->where("id = 0")->save(array("parameters" => $parameters_json));
    }

    public function getCaijiDowe() {//http://www.sucaihuo.com/Tian/Remote/getCaijiDowe
        require 'common/QueryList.class.php';
        $arr = array();
        $url = "http://www.dowebok.com/127.html";
        $pattern = array("content" => array("table:not('.browsers') tbody td", "html"));
        $qy = new QueryList($url, $pattern, '', '', 'utf-8');
        $rs = $qy->jsonArr;

        foreach ($rs as $k => $v) {
            $ceil = ceil(($k + 1) / 4);
            $bi = $k % 4;
            if ($bi == 0) {
                $arr[$ceil]['name'] = $v['content'];
            }
            if ($bi == 3) {
                $arr[$ceil]['desc'] = $v['content'];
            }
            if ($bi == 2) {
                $arr[$ceil]['default'] = $v['content'];
            }
        }
        foreach ($arr as $k => $v) {
//      echo strlen($v['name'])."<hr>";
            if (strlen($v['name']) == 2 && strlen($v['desc']) == 2 && strlen($v['default']) == 2) {

                $arr[$k]['name'] = "";
//                $i++;
            }
        }
        print_r($arr);
        $parameters = array();
        foreach ($arr as $k => $v) {
            $parameters[$k]['paras'] = trim($v['name']);
            $parameters[$k]['descriptions'] = trim($v['desc']);
            $parameters[$k]['defaults'] = trim($v['default']);
        }

        $parameters_json = json_encode($parameters);
        M("js")->where("id = 1456")->save(array("parameters" => $parameters_json));
    }

    public function getModals() {//http://localhost/sucai/Tian/Remote/getModals
        header("Content-type: text/html; charset=utf8");
        set_time_limit(0);
        import("Common.Org.PclZip");

        $file_help = "素材火帮助.txt";
        $file = "modals/demo/0/";
        $lists = scandir("modals/demo/0/");
        foreach ($lists as $v) {
            if ($v > 0) {
//                echo $v."<hr>";
                $imgs = scandir($file . $v);

                foreach ($imgs as $v2) {
                    $type = get_extension($v2);
                    if (!in_array($type, array("jpg", "zip")) && strlen($v2) > 2 && $v > 0) {
                        rename($file . $v . "/" . $v2, $file . $v . "/demo");
                    }
                }
            }
        }
    }

    public function getSitesImg() { //http://localhost/sucai/Tian/Remote/getSitesImg
        header("Content-type: text/html; charset=utf8");

        $bei = 2;
        $file = "sites/" . $bei . "/";
        $lists = scandir($file);

        foreach ($lists as $v) {
            if ($v > 0) {

                $images = scandir($file . $v);
                print_r($images);
                foreach ($images as $v2) {
                    $type = strtolower(get_extension($v2));
                    echo $type . "<hr>";
                    if ($type == 'png') {
                        $file_name = $file . $v . "/" . $v2;
                        $size = getimagesize($file_name);
                        $width = $size[0];
                        if ($width == 238) {
                            $name_last = 'middle.jpg';
                        } else if ($width == 233) {
                            $name_last = 'small.jpg';
                        } else {
                            $name_last = 'big.jpg';
                        }
//                echo $file_name."<hr>";
                        if (file_exists($file_name)) {
                            $file_get = file_get_contents($file_name);
                            file_put_contents($file . $v . "/" . $name_last, $file_get);
                            unlink($file_name);
                        } else {
                            echo "middle.jpg下载文件不存在";
                        }
                    }
                }
                sleep("0.1");
            }
        }
    }

    public function getCaijiHello() {//http://localhost/sucai/Tian/Remote/getCaijiHello
        header("Content-type: text/html; charset=utf-8");
        require 'common/QueryList.class.php';
        $arr = array();
        $pattern = array("content" => array("table.main_table td", "text"));
        $url = "http://www.helloweba.com/view-blog-415.html";
//        $url = "http://localhost/sucai/1.html";
        $qy = new QueryList($url, $pattern, '', '', 'utf-8');
        $rs = $qy->jsonArr;
//print_r($rs);
        foreach ($rs as $k => $v) {
            $ceil = ceil(($k + 1) / 3);
            $bi = $k % 3;
            if ($bi == 0) {
                $arr[$ceil]['name'] = $v['content'];
            }
            if ($bi == 1) {
                $arr[$ceil]['default'] = $v['content'];
            }
            if ($bi == 2) {
                $arr[$ceil]['desc'] = $v['content'];
            }
        }
        $i = 0;
        foreach ($arr as $k => $v) {
//      echo strlen($v['name'])."<hr>";
            if (strlen($v['name']) == 2 && strlen($v['desc']) == 2 && strlen($v['default']) == 2) {

                $arr[$k]['name'] = "";
//                $i++;
            }
        }

        $parameters = array();
//        print_r($arr);
        foreach ($arr as $k => $v) {
            if ($v['name'] != '参数') {
                $parameters[$k]['paras'] = trim($v['name']);
                $parameters[$k]['descriptions'] = trim($v['desc']);
                $parameters[$k]['defaults'] = trim($v['default']);
            }
        }
        print_r($parameters);
        $parameters_json = json_encode($parameters);
//        print_r($parameters);
        M("js")->where("id = 1638")->save(array("parameters" => $parameters_json));
    }

    public function getZipModal2() {//http://localhost/sucai/Tian/Remote/getZipModal2
        header("Content-type: text/html; charset=utf8");
        set_time_limit(0);
        import("Common.Org.PclZip");

        $file_help = "素材火帮助.txt";
        $file_help = getGb2312($file_help);
        $bei = 7;
        $file_path = "modals/" . $bei . "/";
        $lists = scandir($file_path);


        foreach ($lists as $v) {

            if ($v > 0) {
                $info = M("modals")->field("name")->where("id = " . $v . "")->find();
                $name = getGb2312($info['name']);


                $file_v = $file_path . $v . "/";
                $file_v_demo = $file_v . "/demo/";
                $file_v_zip = $file_v . "/" . $name . ".zip";

                file_put_contents($file_v_demo . $file_help, file_get_contents($file_help));
                echo $v . "<hr>";




                $images = scandir($file_v);
                $i = 0;


                $files = array(
                    0 => $file_v_demo,
                    1 => $file_help
                );
//                print_r($files);

                $rs = createReplaceZip($file_v_zip, $files, $file_v_demo, $name);


                foreach ($images as $v2) {
                    $type = strtolower(get_extension($v2));
//            echo $v2."<hr>";
                    if (in_array($type, array("png", "jpg")) && !in_array($v2, array("middle.jpg", "big.jpg", "small.jpg"))) {
                        $file_name = $file_v . $v2;
                        $size = getimagesize($file_name);
                        $width = $size[0];
                        if ($width == 238) {
                            $name_last = 'middle.jpg';
                        } else if ($width == 233) {
                            $name_last = 'small.jpg';
                        } else {
                            $name_last = 'big.jpg';
                        }

                        if (file_exists($file_name)) {
                            $file_get = file_get_contents($file_name);
                            file_put_contents($file_v . $name_last, $file_get);
                            unlink($file_name);
                        } else {
                            echo "middle.jpg下载文件不存在";
                        }
                    }
                }
                unlink($file_v_demo . $file_help);
                sleep("0.1");
            }
        }

        echo "<script>location.href='http://localhost/sucai/Tian/Remote/getDatas';</script>";
    }

    public function get_tags() {//http://localhost/sucai/Tian/Remote/get_tags
        header("Content-type: text/html; charset=utf8");
        $table = "js";
        $table_tags = "js_tags";

        $table_dictionary = "js_dictionary";
        $lists = M($table_tags)->where("keywords  is null or keywords = '' or description =''")->select();
//        echo M("js_tags")->getlastsql();
//     print_r($lists);
        foreach ($lists as $v) {
            $id = $v['id'];
            $subs = M($table)->field("keywords")->where("FIND_IN_SET('" . $id . "',tags)")->select();
            if (empty($subs)) {
                $dictionary = M($table_dictionary)->where("name = '" . $v['name'] . "' ")->find();
                $subs = M($table)->field("keywords")->where("FIND_IN_SET('" . $dictionary['id'] . "',cat_id) OR FIND_IN_SET('" . $dictionary['id'] . "',cat_sub_id)")->select();
            }

            if (empty($subs)) {
                $subs = M($table)->field("keywords")->where("name like '%" . $v['name'] . "%' ")->select();
//                echo M($table)->getlastsql() . "<hr>";
            }
            print_r($subs) . "<hr>";
//    echo $id."<hr>";
            $keywords = "";
            foreach ($subs as $v2) {
                if ($v2['keywords'] != '') {
                    $keywords .= trim($v2['keywords']) . ",";
                }
            }
            if ($keywords) {
                $keywordsExplode = explode(",", $keywords);
                $keywordsArr = array_unique(array_filter($keywordsExplode));
            }
            if ($keywordsArr) {

                $num = count($keywordsArr);
                if ($num == 1) {
                    $rands[0] = 0;

                    $rands2[0] = 0;
                } else {
                    $rands = array_rand($keywordsArr, $num > 3 ? 3 : $num);

                    $rands2 = array_rand($keywordsArr, $num > 5 ? 5 : $num);
                }

                $tags_keywords = $keywordsArr[$rands[0]] . "," . $keywordsArr[$rands[1]] . "," . $keywordsArr[$rands[2]];
                $tags_keywords2 = $keywordsArr[$rands2[0]] . "," . $keywordsArr[$rands2[1]] . "," . $keywordsArr[$rands2[2]] . "," . $keywordsArr[$rands2[3]] . "," . $keywordsArr[$rands2[4]];

                $tagsKeywordsArr = array_unique(array_filter(explode(",", $tags_keywords)));
                $tagsDescriptionArr = array_unique(array_filter(explode(",", $tags_keywords2)));
                $data['keywords'] = implode(",", $tagsKeywordsArr);
                if ($data['keywords']) {
                    $data['description'] = $v['name'] . "分为" . implode(",", $tagsDescriptionArr);
                    M($table_tags)->where("id = " . $id . "")->save($data);
                }
            }

            set_time_limit(1);
        }
//        $lists = M("js_tags")->select();
//        foreach($lists as $v){
//            if($v['name']."分为" ==  $v['description']){
//                $data['description'] = "";
//                M("js_tags")->where("id = " . $v['id'] . "")->save($data);
//            }
//        }
    }

    public function getCaijiZhangxinxu() {//http://localhost/sucai/Tian/Remote/getCaijiZhangxinxu
        header("Content-type: text/html; charset=utf-8");
        require 'common/QueryList.class.php';
        $arr = array();
        $pattern = array("content" => array("table td", "text"));
        $url = "http://www.zhangxinxu.com/jq/jcarousel_zh/#Configuration";
        $qy = new QueryList($url, $pattern, '', '', 'utf-8');
        $rs = $qy->jsonArr;
        print_r($rs);
        foreach ($rs as $k => $v) {
            $ceil = ceil(($k + 1) / 4);
            $bi = $k % 4;
            if ($bi == 0) {
                $arr[$ceil]['name'] = $v['content'];
            }
            if ($bi == 2) {
                $arr[$ceil]['default'] = $v['content'];
            }
            if ($bi == 3) {
                $arr[$ceil]['desc'] = $v['content'];
            }
        }
        $i = 0;
        foreach ($arr as $k => $v) {
//      echo strlen($v['name'])."<hr>";
            if (strlen($v['name']) == 2 && strlen($v['desc']) == 2 && strlen($v['default']) == 2) {

                $arr[$k]['name'] = "";
//                $i++;
            }
        }

        $parameters = array();
        foreach ($arr as $k => $v) {
            $parameters[$k]['paras'] = trim($v['name']);
            $parameters[$k]['descriptions'] = trim($v['desc']);
            $parameters[$k]['defaults'] = trim($v['default']);
        }

        $parameters_json = json_encode($parameters);
        M("js")->where("id = 445")->save(array("parameters" => $parameters_json));
    }

    public function format() {
        // 一直以为mysql随机查询几条数据，就用  
        // SELECT * FROM `table` ORDER BY RAND() LIMIT 5  
        // 但是真正测试一下才发现这样效率非常低。一个15万余条的库，查询5条数据，居然要8秒以上
//SELECT * FROM `table` ORDER BY RAND() LIMIT 5  
//查看官方手册，也说rand()放在ORDER BY 子句中会被执行多次，自然效率及很低。   
        //搜索Google，网上基本上都是查询max(id) * rand()来随机获取数据。   
//        $table = "sucai_js";
//        $sql = "SELECT *  
//FROM `" . $table . "` AS A JOIN (SELECT ROUND(RAND() * (SELECT MAX(id) FROM `" . $table . "`)) AS id) AS B  
//WHERE A.id >= B.id  
//ORDER BY A.id ASC LIMIT 5;   ";
//        //但是这样会产生连续的5条记录。解决办法只能是每次查询一条，查询5次。即便如此也值得，因为15万条的表，查询只需要0.01秒不到。  
//        
//        SELECT *  
//FROM `table` AS t1 JOIN (SELECT ROUND(RAND() * ((SELECT MAX(id) FROM `table`)-(SELECT MIN(id) FROM `table`))+(SELECT MIN(id) FROM `table`)) AS id) AS t2  
//WHERE t1.id >= t2.id  
//ORDER BY t1.id LIMIT 1;   
    }

    public function jqueryZip() { //http://localhost/sucai/Tian/Remote/jqueryZip
        header("Content-type: text/html; charset=utf-8");
        $bei = 7;
        $file_path = "jquery/" . $bei . "/";



        $lists = scandir($file_path);
        $max = '746';
        $i = 0;
        $deleteFiles = array("readme.html");
        $deleteSuffix = array("url", "txt");
        foreach ($lists as $v) {
            $type = get_extension($v);
            if ($type == 'zip') {
                $zip = $file_path . $v;
                $id = $max + $i;


                $file_demo = $file_path . $id . "/demo/";

//                  echo $file_demo."<hr>";
                $num = showZipFilesNum($zip);
                if ($num == 1) {
                    replacePathExtract($zip, $file_demo, basename($zip, ".zip")); ////参数2 要压缩的文件(多个文件逗号隔开) 参数3：移除的文件路径，参数4,：添加的文件路径
                } else {
//                        replacePathExtract($zip, $file_demo, "");
                    echo "丢失：" . $zip;
                }

                $demos = scandir($file_demo);
//                print_r($demos.$zip);
                foreach ($demos as $v2) {
                    $demo_file = $file_demo . $v2;
                    $type_demo = get_extension($v2);
                    if (in_array($type_demo, $deleteSuffix)) {
                        unlink($demo_file);
                    }
                    if (in_array($v2, $deleteFiles)) {
                        unlink($demo_file);
                    }
                }
//                  print_r($demos);

                $i++;
            }
        }
    }

    public function modalZip() { //http://localhost/sucai/Tian/Remote/modalZip
        header("Content-type: text/html; charset=utf-8");
        $bei = 7;
        $file_path = "modals/" . $bei . "/";



        $lists = scandir($file_path);
        $max = '799';
        $i = 0;
        $deleteFiles = array("readme.html");
        $deleteSuffix = array("url", "txt");
        foreach ($lists as $v) {
            $type = get_extension($v);
            if ($type == 'zip') {
                $zip = $file_path . $v;
                $id = $max + $i;


                $file_demo = $file_path . $id . "/demo/";

//                  echo $file_demo."<hr>";
                $num = showZipFilesNum($zip);
                if ($num == 1) {
                    replacePathExtract($zip, $file_demo, basename($zip, ".zip")); ////参数2 要压缩的文件(多个文件逗号隔开) 参数3：移除的文件路径，参数4,：添加的文件路径
                } else {
//                        replacePathExtract($zip, $file_demo, "");
                    echo "丢失：" . $zip;
                }

                $demos = scandir($file_demo);
//                print_r($demos.$zip);
                foreach ($demos as $v2) {
                    $demo_file = $file_demo . $v2;
                    $type_demo = get_extension($v2);
                    if (in_array($type_demo, $deleteSuffix)) {
                        unlink($demo_file);
                    }
                    if (in_array($v2, $deleteFiles)) {
                        unlink($demo_file);
                    }
                }
//                  print_r($demos);

                $i++;
            }
        }
    }

    //http://www.sucaihuo.com/Tian/Remote/getCaijiBaidu/num/5/name/%E4%BA%9A%E6%8B%89%E5%B7%B4%E9%A9%AC%E5%B7%9E
    public function getCaijiBaidu() {//http://localhost/sucai/Tian/Remote/getCaijiBaidu/name/%E4%BA%9A%E6%8B%89%E5%B7%B4%E9%A9%AC%E5%B7%9E
        header("Content-type: text/html; charset=utf-8");
        $num = I("get.num", 6, 'int');
        $name = I("get.name");
        require 'common/QueryList.class.php';
        $url = "https://baike.baidu.com/item/" . $name . "";
        $table = "table.jquery-tablesorter td";
        if ($name == '科罗拉多州') {

            $url = "https://baike.baidu.com/item/科罗拉多州/3288702";
            $table = "table.jquery-tablesorter td";
        } else {
            //  $table = "table.log-set-param td";
        }
        if ($name == '肯塔基州') {
            $table = "table.log-set-param  th";
            $num = 1;
        }
        if (I("get.table") == 2) {
            $table = "table.log-set-param td";
        }
        $pattern = array("content" => array($table, "text"));

//        $url = "http://localhost/sucai/1.html";
        $qy = new QueryList($url, $pattern, '', '', 'utf-8');
        $rs = $qy->jsonArr;
        $s_k = 0;
        if ($name == '肯塔基州') {
//             print_r($rs);
            $s_k = -1;
        }
        $strs = "";


        foreach ($rs as $k => $v) {
            if ($k % $num == 0 && $k > $s_k) {

                $strs .= trim($v['content']) . "@";
            }
        }
        echo $strs;
    }

    public function getCaijiHuzhan() {//http://localhost/sucai/Tian/Remote/getCaijiHuzhan
        header("Content-type: text/html; charset=utf-8");
        require 'common/QueryList.class.php';
     
        $pattern = array("content" => array("body", "text"));
        $url = "https://www.huzhan.com";
//        $url = "http://localhost/sucai/1.html";
        $qy = new QueryList($url, $pattern, '', '', 'utf-8');
        $rs = $qy->jsonArr;
     
        $rs = file_get_contents($url);
           print_r($rs);
    }

}

?>