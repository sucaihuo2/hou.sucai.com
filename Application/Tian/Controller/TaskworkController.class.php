<?php

namespace Tian\Controller;

class TaskworkController extends CommonController {

    public function index(){
        echo 111;
    }
     //管理员任务列表
    public function liststask(){

        $sql = "1=1 AND is_del=0 ";

        $keywords = trim(I('get.keyword'));
        $assignuid = trim(I('get.assignuid'));
        $orderno = trim(I('get.orderno'));
        $status = trim(I('get.status'));
        if ($keywords) {
            $sql .= " AND (a.task_name like '%" . $keywords . "%' or a.task_content = '".$keywords."')";
        }
        if($assignuid){
           $sql .= " AND assign_uid =".$assignuid." ";
        }
        if($orderno){
            $sql .= " AND order_no =$orderno ";
        }
        if($status){
            $sql .= " AND status =$status ";
        }
        
        $count = M("task_work")->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count,C("pagenum"));
        $resTaskList = M("task_work")
             ->alias("a")
            ->field("a.id,a.task_name,a.order_no,a.qq,a.mobile,a.assign_uid,a.status,a.processing_time,a.create_time,a.qq,a.mobile,a.goods_id,b.nickname")
            ->join("__USER__ b ON a.assign_uid = b.id")
            ->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('a.id desc')->select();
            if($_GET['a'] == 1){
            //	ECHO $sql;
           // echo $assignuid.M("task_work")->getlastsql();;exit;	
            }
        $page = $Page->show();
        $adminlist = C("ADMIN");
        $this->assign("page", $page);
        $userList = getAdminUsers();
        $this->assign("adminlist", $userList);
        $this->assign("tasklist",$resTaskList);
        $this->display();
    }
     //非管理员列表
    public function worklist(){
        $userid = $_SESSION["userid"];
        if($userid){
            $sql = "1=1 AND is_del=0  AND  assign_uid=$userid ";

            $keywords = trim(I('get.keyword'));
            $assignuid = trim(I('get.assignuid'));
            $orderno = trim(I('get.orderno'));
            $status = trim(I('get.status'));
            if ($keywords) {
                $sql .= " AND (a.task_name like '%" . $keywords . "%' or a.task_content = '".$keywords."')";
            }
            if($assignuid){
                $sql .= " AND a.assign_uid =$assignuid ";
            }
            if($orderno){
                $sql .= " AND a.order_no =$orderno ";
            }
            if($status){
                $sql .= " AND a.status =$status ";
            }
            $count = M("task_work")->where($sql)->count();    //计算总数
            $Page = new \Think\Page($count,C("pagenum"));
            $resTaskList = M("task_work")
                ->alias("a")
                ->field("a.id,a.task_name,a.order_no,a.qq,a.mobile,a.assign_uid,a.status,a.processing_time,a.create_time,a.qq,a.mobile,b.nickname")
                ->join("__USER__ b ON a.assign_uid = b.id")
                ->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('a.id desc')->select();
            $page = $Page->show();
            $adminlist = C("ADMIN");
            $this->assign("page", $page);
            $this->assign("adminlist", $adminlist);
            $this->assign("tasklist",$resTaskList);
            $this->display();
        }else{
            $this->error("您还没有登录哦");
        }


    }

     //添加任务
    public function addtask(){
        IF(IS_POST){
            $task_name = I("task_name");
            $order_no = I("order_no");
            $task_content = I("task_content");
            $tracking_description = I("tracking_description");
            $status = I("status");
            $qq = I("qq");
            $mobile = I("mobile");
            $assign_uid = I("assign_uid");
//            echo $task_name;
//            echo $assign_uid;
//            exit;
            if($task_name && $assign_uid){
                $data["task_name"] = $task_name;
                $data["order_no"] = $order_no;
                $data["task_content"] = $task_content;
                $data["tracking_description"] = $tracking_description;
                $data["status"] = $status;
                $data["qq"] = $qq;
                $data["mobile"] = $mobile;
                $data["assign_uid"] = $assign_uid;
                $data["goods_id"] = I("goods_id");
                $res = M("task_work")->add($data);
                if($res){
                    $this->success("创建成功",U("Taskwork/liststask"));
                }else{
                    $this->error("创建失败");
                }
            }else{
                $this->error("参数不全");
            }

        }else{
            $userList = getAdminUsers();
            $adminstatus = getAdminstatus();
            $this->assign("adminlist", $userList);
            $this->assign("adminstatus", $adminstatus);
            $this->display();
        }

    }
     //编辑任务
    public function edittask(){
        IF(IS_POST){
            $task_name = I("task_name");
            $order_no = I("order_no");
            $task_content = I("task_content");
            $tracking_description = I("tracking_description");
            $status = I("status");
            $qq = I("qq");
            $mobile = I("mobile");
            $assign_uid = I("assign_uid");
            $id = I("id");
            if($task_name && $assign_uid && $id){
                $data["task_name"] = $task_name;
                $data["order_no"] = $order_no;
                $data["task_content"] = $task_content;
                $data["tracking_description"] = $tracking_description;
                $data["status"] = $status;
                $data["assign_uid"] = $assign_uid;
                $data["qq"] = $qq;
                $data["mobile"] = $mobile;
                 $data["goods_id"] = I("goods_id");
                $condition["id"] =$id;
                $res = M("task_work")->where($condition)->save($data);
                if($res){
                    $this->success("更新成功",U("Taskwork/liststask"));
                }else{
                    $this->error("更新失败");
                }
            }else{
                $this->error("参数不全");
            }

        }ELSE{
            $id = I("id");
            if($id){
                $w["id"] = $id;
                $restaskdetails = M("task_work")->where($w)->find();
                $this->assign("taskdetails", $restaskdetails);
                $userList = getAdminUsers();
                $adminstatus = getAdminstatus();
                $this->assign("adminlist", $userList);
                $this->assign("adminstatus", $adminstatus);
                $this->display();
            }else{
                $this->redirect(U("Taskwork/addtask"));
            }
        }
    }
   //删除任务
    public function deltask(){
        $id = I("id");
        if($id){
            $condition["id"] = $id;
            $data["is_del"] = 1;
            $resDetails = M("task_work")->where($condition)->save($data);
            if($resDetails){
                $arr = array(
                    "code" => 200,
                    "msg" => "删除成功",
                );
            }else{
                $arr = array(
                    "code" => 201,
                    "msg" => "删除失败",
                );
            }
            $this->ajaxReturn($arr);
        }else{
            $arr = array(
                "code" => 201,
                "msg" => "参数不全",
            );
            $this->ajaxReturn($arr);
        }
    }
    //任务详情
    public function  taskdetails(){
        $id = I("id");
        $userid = $_SESSION["userid"];
        if($id){
            $condition["a.is_del"] = 0;
            $condition["a.id"] = $id;
            $condition["a.assign_uid"] = $userid;
            $resDetails = M("task_work")
                ->alias("a")
                ->field("a.id,a.task_name,a.order_no,a.qq,a.mobile,a.assign_uid,a.task_content,a.tracking_description,a.update_time,a.status,a.processing_time,a.create_time,a.qq,a.processing_time,a.mobile,b.nickname")
                ->join("__USER__ b ON a.assign_uid = b.id")
                ->where($condition)->find();
            $adminstatus = getAdminstatus();
            $this->assign("adminstatus", $adminstatus);
            $this->assign("taskdetails",$resDetails);
            $this->display();
        }else{
            $this->error("参数不全");
        }

    }
    //设置追踪描述
    public function settracking(){
         $id = I("id");
         $tracking_description = I("tracking_description");
        if($id){
           if($tracking_description){
               $w["id"] = $id;
               $data["tracking_description"] = $tracking_description;
               $resTask = M("task_work")->where($w)->save($data);
               if($resTask){
                   $arr = array(
                       "code" => 200,
                       "msg" => "更新成功",
                   );
               }else{
                   $arr = array(
                       "code" => 201,
                       "msg" => "更新失败",
                   );
               }
           }else{
               $arr = array(
                   "code" => 201,
                   "msg" => "进度描述不能为空哦",
               );
           }
            $this->ajaxReturn($arr);
        }else{
            $arr = array(
                "code" => 201,
                "msg" => "参数不全",
            );
            $this->ajaxReturn($arr);
        }
    }
    //状态设置
    public function setstatus(){
        $id = I("id");
        $status = I("status");
        if($id && $status){
            $w["id"] = $id;
            $data["status"] = $status;
            if($status == 1){
                $data["processing_time"] = date("Y-m-d H:i:s");
            }
            $resTask = M("task_work")->where($w)->save($data);
            if($resTask){
                $arr = array(
                    "code" => 200,
                    "msg" => "更新成功",
                );
            }else{
                $arr = array(
                    "code" => 201,
                    "msg" => "更新失败",
                );
            }
            $this->ajaxReturn($arr);
        }else{
            $arr = array(
                "code" => 201,
                "msg" => "参数不全",
            );
            $this->ajaxReturn($arr);
        }
    }


}

?>
