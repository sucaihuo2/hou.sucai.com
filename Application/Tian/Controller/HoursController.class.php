<?php

namespace Tian\Controller;

class HoursController extends CommonController {

    public function lists() {
        $s_admin_uid = getAdminId();
        $sql = "1=1 AND uid = " . $s_admin_uid . "";
        $keywords = trim(strip_tags(htmlspecialchars(strtolower($_GET['keyword']))));

        if ($keywords) {
            $sql .= " AND content like '%" . $keywords . "%'";
        }

        $count = M('hours')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('hours')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->display();
    }

    public function detail() {
        $id = I('get.id', '', 'int');
        if ($id > 0) {
            $detail = M('hours')->where("id = " . $id . "")->find();
            $hour = intval($detail['hour']);
            $minutes = ($detail['hour'] - intval($detail['hour'])) * 60;
            if ($detail['status'] != 0) {
                $this->error("已审核，不可操作！");
            }
        }
        $today = date("Y-m-d");
        $this->assign("detail", $detail);
        $this->assign("hour", $hour);
        $this->assign("minutes", $minutes);
        $this->assign("today", $today);
        $this->display();
    }

    public function detail_post() {
        $id = I('post.id', '', 'int');
        $data['content'] = I('post.content');
        $data['workdate'] = date("Y-m-d");
        $data['addtime'] = time();
        $data['uid'] = getAdminId();
        $minutes = I("post.minutes");
        $data['hour'] = I("post.hour") + $minutes / 60;


        if ($id > 0) {
            M("hours")->where("id = " . $id . "")->save($data);

            $this->success('修改成功！', session('QUERY_STRING'));
        } else {
            M("hours")->add($data);
            $this->success('添加成功！', U('Hours/lists'));
        }
    }

    public function checks() {
        $sql = "1=1";
        $keywords = trim(strip_tags(htmlspecialchars(strtolower($_GET['keyword']))));

        if ($keywords) {
            $sql .= " AND content like '%" . $keywords . "%'";
        }

        $count = M('hours')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('hours')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->display();
    }

    public function check_detail() {
        $id = I('get.id', '', 'int');
        if ($id > 0) {
            $detail = M('hours')->where("id = " . $id . "")->find();
            $hour = intval($detail['hour']);
            $minutes = ($detail['hour'] - intval($detail['hour'])) * 60;
            if ($detail['status'] != 0) {
                $this->error("已审核，不可操作！");
            }
        }
        $today = date("Y-m-d");
        $this->assign("detail", $detail);
        $this->assign("hour", $hour);
        $this->assign("minutes", $minutes);
        $this->assign("today", $today);
        $this->display();
    }

    public function check_detail_post() {
        $id = I('post.id', '', 'int');
        $data['check_info'] = I('post.check_info');
        $data['check_time'] = time();
        $data['check_uid'] = getAdminId();
        $detail = M('hours')->where("id = " . $id . "")->find();
        if (empty($detail)) {
            $this->error("不存在");
        }
        if ($detail['status'] != 0) {
            $this->error("已审核");
        }
        $data['status'] = I("post.status", 0, 'int');
        
        if($data['status'] == 0){
                $this->error("状态不可以为待审核");
        }
        if ($id > 0) {
            M("hours")->where("id = " . $id . "")->save($data);

            $this->success('修改成功！', session('QUERY_STRING'));
        } else {
            M("hours")->add($data);
            $this->success('添加成功！', U('Hours/lists'));
        }
    }

}

?>
