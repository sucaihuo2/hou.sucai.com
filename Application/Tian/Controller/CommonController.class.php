<?php

/**
 * Anthor : xushuai
 * Date : 20140-7-*
 */

namespace Tian\Controller;

use Think\Controller;

class CommonController extends Controller {

    public function _initialize() {
        header("Content-type: text/html; charset=utf-8");
        $s_admin_uid = getAdminId();
        $ip = getIp();
        if ($_GET['ip']) {
            echo $ip;
            exit;
        }
        if ($_GET['name'] == 'sucaihuo') {
            
        } else {
            //$ip = '113.75.106.99';
//        $response = file_get_contents('http://ip.taobao.com/service/getIpInfo.php?ip=' . $ip);
//        $result = json_decode($response, true);
////print_r($result);
//        $city = $result['data']['city'];
//        if (!in_array($city, array("马鞍山", "河源","南京","内网IP","合肥"))) {
//            echo '兄弟，你来错地方了吧';
//                 print_r($result);
//            exit;
//        }else{
//       
//        }
        }



        if (session("admin_uid") == '') {
            session("admin_uid", $s_admin_uid);
        }
        $s_admin_name = session("admin_name") ? session("admin_name") : cookie("admin_name");
        if (session("admin_name") == '') {
            session("admin_name", $s_admin_name);
        }
//        echo $s_admin_uid;
        if ($s_admin_uid == '') {
            echo "<script>document.location.href='" . U("Public/login") . "';</script>";
            exit;
        }
        $control = CONTROLLER_NAME;
        $mod = ACTION_NAME;
        $modReturn = array("lists", "cat", 'friends', 'tags', 'brand', 'rubbish', 'single', 'rent', 'messages', 'withdraw',
            'decoration', 'admin', 'templates', 'points_abnormal', 'favor', 'checks');
        if (in_array($mod, $modReturn)) {
            $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            session('QUERY_STRING', $url);
        }


        if ($s_admin_uid != 1) {
            $user = M("user")->field("powers")->where("id = '" . $s_admin_uid . "'")->find();
            $powers_ids = explode(",", $user['powers']);

            if ($powers_ids) {

                $menus_power = M("admin_menu2")->field("pid")->where("is_check = 1 AND id in (" . implode(",", $powers_ids) . ")")->select();

                foreach ($menus_power as $v) {
                    array_push($powers_ids, $v['pid']);
                }
                $powers_ids = array_unique($powers_ids);
            }

            $sql_power = " AND id in (" . implode(",", $powers_ids) . ")";
        }
        $menus_all = M("admin_menu2")->where("is_check = 1 " . $sql_power . "")->order("ord ASC")->select();

        $menus = array();
        foreach ($menus_all as $v) {
            if ($v['pid'] == 0) {
                $menus[] = $v;
            }
        }
        foreach ($menus as $k => $v) {
            foreach ($menus_all as $v2) {
                if ($v['id'] == $v2['pid']) {
                    $menus[$k]['sub'][] = $v2;
                }
            }
        }

        foreach ($menus as $k => $v) {
            $menus[$k]['url'] = U($v['control'] . "/" . $v['mod']);

            foreach ($v['sub'] as $k2 => $v2) {
                $menus[$k]['sub'][$k2]['url'] = __APP__ . "/" . MODULE_NAME . "/" . ucfirst($v2['control']) . "/" . strtolower($v2['mod']) . $v2['url_other'];
            }
        }

        $menu = M("admin_menu2")->field("pid,name")->where("control = '" . $control . "' AND `mod` = '" . $mod . "'")->find();


        $power_check = is_power($control . "/" . $mod);

        if ($power_check != 1) {
            if (strtolower($control) == 'index' && strtolower($mod) == 'index') {
                
            } else {
                $this->error("您没有" . $power_check . " 相关权限！");
            }
        }

        $start_date = I("get.start_date") ? I("get.start_date") : date("Y-m-d");
        $end_date = I("get.end_date") ? I("get.end_date") : date("Y-m-d");
        $this->access_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2NTIwMTIyMDEsImV4cCI6MTk2NzM3MjIwMSwidV9pZCI6ODYzMTE3LCJzeXNfdHlwZSI6Inl5IiwicF9pZCI6NjcwODgyLCJqeF9waWQiOjY3MDg4Miwicm9sZSI6InBlcnNvbmFsQSIsIm5hbWUiOiJoamwxODAwNTE1MTUzOCIsImVuZF90aW1lIjoiMjg1NS0wNy0wOCAyMTozODoxOSIsImRldmljZV9tYXgiOjUsImlzX21vYmlsZSI6ZmFsc2UsInBob25lX2luZm8iOiIiLCJwaG9uZV9uYW1lIjoiXHU2NzJhXHU1NDdkXHU1NDBkXHU4YmJlXHU1OTA3In0.yvvCfKdNvPDvceaeveCJE5slvrpjzqvqtIVENR4_AK4";

        $this->assign("control", $control);
        $this->assign("mod", $mod);
        $this->assign("menus", $menus);
        $this->assign("menu_pid", $menu['pid']);
        $this->assign("menu", $menu);
        $this->assign("start_date", $start_date);
        $this->assign("end_date", $end_date);
        $this->assign("s_admin_uid", $s_admin_uid);
    }

}
