<?php

namespace Tian\Controller;

class StaticsController extends CommonController {

    //充值积分
    public function pay_points() {
        $start_date = I("get.start_date") ? I("get.start_date") : date("Y-m-d", strtotime("-9 day"));
        $end_date = I("get.end_date") ? I("get.end_date") : date("Y-m-d");
        
        
        // $start_date = '2018-10-01';
        //$end_date = '2018-10-07';
        
        $staticsModel = new \Tian\Model\StaticsModel();
        $state = I("get.state");
        $sql_add = " AND status =1";

        $datas = array(
            "sql_where" => $sql_add,
            "table" => "pay",
            "field" => "money",
            "addtime" => "addtime"
        );

        $day_pays = $staticsModel->getDayLists($datas);
//        print_r($day_pays);
        $week_pays = $staticsModel->getWeekLists($datas);
        $month_pays = $staticsModel->getMonthLists($datas);
        $year_pays = $staticsModel->getYearLists($datas);

        $this->assign("day_pays", $day_pays);
        $this->assign("day_pays_json", json_encode($day_pays));

        $this->assign("week_pays", $week_pays);
        $this->assign("week_pays_json", json_encode($week_pays));
        $this->assign("month_pays", $month_pays);
        $this->assign("month_pays_json", json_encode($month_pays));
        $this->assign("year_pays", $year_pays);
        $this->assign("year_pays_json", json_encode($year_pays));
        $this->assign("state", $state);
        $this->assign("start_date", $start_date);
        $this->assign("end_date", $end_date);
        $this->display();
    }

    //充值会员
    public function order_month() {
        $start_date = I("get.start_date") ? I("get.start_date") : date("Y-m-d", strtotime("-9 day"));
        $end_date = I("get.end_date") ? I("get.end_date") : date("Y-m-d");
        $staticsModel = new \Tian\Model\StaticsModel();
        $state = I("get.state");
        $sql_add = " AND status =1";

        $datas = array(
            "sql_where" => $sql_add,
            "table" => "order_month",
            "field" => "money",
            "addtime" => "addtime"
        );

        $day_pays = $staticsModel->getDayLists($datas);
//        print_r($day_pays);
        $week_pays = $staticsModel->getWeekLists($datas);
        $month_pays = $staticsModel->getMonthLists($datas);
        $year_pays = $staticsModel->getYearLists($datas);

        $this->assign("day_pays", $day_pays);
        $this->assign("day_pays_json", json_encode($day_pays));

        $this->assign("week_pays", $week_pays);
        $this->assign("week_pays_json", json_encode($week_pays));
        $this->assign("month_pays", $month_pays);
        $this->assign("month_pays_json", json_encode($month_pays));
        $this->assign("year_pays", $year_pays);
        $this->assign("year_pays_json", json_encode($year_pays));
        $this->assign("state", $state);
        $this->assign("start_date", $start_date);
        $this->assign("end_date", $end_date);
        $this->display();
    }

    //js上传素材
    public function user_js() {
        $keyword = I("get.keyword");
        $userinfo = M("user")->field("id")->where("name='" . $keyword . "'")->find();
        $userid = $userinfo['id'];
        $start_date = I("get.start_date") ? I("get.start_date") : date("Y-m-d", strtotime("-9 day"));
        $end_date = I("get.end_date") ? I("get.end_date") : date("Y-m-d");
        $staticsModel = new \Tian\Model\StaticsModel();
        $state = I("get.state");
        $sql_add = " AND is_check = 1";
        if ($userid > 0) {
            $sql_add .= " AND uid = " . $userid . "";
        }
        $datas = array(
            "sql_where" => $sql_add,
            "table" => "js",
            "field" => "id",
            "addtime" => "addtime",
            "mtype" => "count"
        );

        $day_pays = $staticsModel->getDayLists($datas);
//        print_r($day_pays);
        $week_pays = $staticsModel->getWeekLists($datas);
        $month_pays = $staticsModel->getMonthLists($datas);
        $year_pays = $staticsModel->getYearLists($datas);

        $this->assign("day_pays", $day_pays);
        $this->assign("day_pays_json", json_encode($day_pays));

        $this->assign("week_pays", $week_pays);
        $this->assign("week_pays_json", json_encode($week_pays));
        $this->assign("month_pays", $month_pays);
        $this->assign("month_pays_json", json_encode($month_pays));
        $this->assign("year_pays", $year_pays);
        $this->assign("year_pays_json", json_encode($year_pays));
        $this->assign("state", $state);
        $this->assign("start_date", $start_date);
        $this->assign("end_date", $end_date);
        $this->display();
    }

    //模板上传素材
    public function user_templates() {
        $keyword = I("get.keyword");
        $userinfo = M("modals")->field("id")->where("name='" . $keyword . "'")->find();
        $userid = $userinfo['id'];
        $start_date = I("get.start_date") ? I("get.start_date") : date("Y-m-d", strtotime("-9 day"));
        $end_date = I("get.end_date") ? I("get.end_date") : date("Y-m-d");
        $staticsModel = new \Tian\Model\StaticsModel();
        $state = I("get.state");
        $sql_add = " AND is_check = 1";
        if ($userid > 0) {
            $sql_add .= " AND uid = " . $userid . "";
        }
        $datas = array(
            "sql_where" => $sql_add,
            "table" => "modals",
            "field" => "id",
            "addtime" => "addtime",
            "mtype" => "count"
        );

        $day_pays = $staticsModel->getDayLists($datas);
//        print_r($day_pays);
        $week_pays = $staticsModel->getWeekLists($datas);
        $month_pays = $staticsModel->getMonthLists($datas);
        $year_pays = $staticsModel->getYearLists($datas);

        $this->assign("day_pays", $day_pays);
        $this->assign("day_pays_json", json_encode($day_pays));

        $this->assign("week_pays", $week_pays);
        $this->assign("week_pays_json", json_encode($week_pays));
        $this->assign("month_pays", $month_pays);
        $this->assign("month_pays_json", json_encode($month_pays));
        $this->assign("year_pays", $year_pays);
        $this->assign("year_pays_json", json_encode($year_pays));
        $this->assign("state", $state);
        $this->assign("start_date", $start_date);
        $this->assign("end_date", $end_date);
        $this->display();
    }

    //用户数量
    public function users() {

        $start_date = I("get.start_date") ? I("get.start_date") : date("Y-m-d", strtotime("-9 day"));
        $end_date = I("get.end_date") ? I("get.end_date") : date("Y-m-d");
        $staticsModel = new \Tian\Model\StaticsModel();
        $state = I("get.state");
        $sql_add = " ";

        $datas = array(
            "sql_where" => $sql_add,
            "table" => "user",
            "field" => "id",
            "addtime" => "addtime",
            "mtype" => "count"
        );
        $day_pays = $staticsModel->getDayLists($datas);

        $week_pays = $staticsModel->getWeekLists($datas);

//        $month_pays = $staticsModel->getMonthLists($datas);
//      
//        $year_pays = $staticsModel->getYearLists($datas);

        $this->assign("day_pays", $day_pays);
        $this->assign("day_pays_json", json_encode($day_pays));

        $this->assign("week_pays", $week_pays);
        $this->assign("week_pays_json", json_encode($week_pays));
        $this->assign("month_pays", $month_pays);
        $this->assign("month_pays_json", json_encode($month_pays));
        $this->assign("year_pays", $year_pays);
        $this->assign("year_pays_json", json_encode($year_pays));
        $this->assign("state", $state);
        $this->assign("start_date", $start_date);
        $this->assign("end_date", $end_date);

        $this->display();
    }

    public function source_num($lists, $uids, $date) {
//        print_r($lists);
//        echo $date."<hr>";
        $rs = array();
        foreach ($uids as $v) {
            $num = 0;
            foreach ($lists as $v2) {
                if ($v2['date'] == $date && $v == $v2['uid']) {
                    $num++;
                }
            }
            $rs[$v] = $num;
        }
        return $rs;
    }

    //源码上传素材
    public function user_source() {



        $start_date = date("Y-m-d", strtotime("-30 day"));//显示几天
        $end_date = date("Y-m-d");
        $start_time = strtotime($start_date);
        $end_time = time();
        $lists = M("source")->field("time_create,uid")->where("time_create between " . $start_time . " AND " . $end_time . "")->select();
        foreach ($lists as $k => $v) {
            $lists[$k]['date'] = date("Y-m-d", $v['time_create']);
        }
//        print_r($lists);
        $start = $end_time / 3600 / 24;

        $end = $start_time / 3600 / 24;
        $days = array();

        $uids = array(1, 3, 13,11);
        $j = 0;
        for ($i = $start; $i > $end; $i--) {
            $date = date("Y-m-d", ($start - $j) * 3600 * 24);

            $days[$date] = $this->source_num($lists, $uids, $date);
            $j++;
        }
//        print_r($days);
//        $days = array(
//            "2019-06-08" => array(
//                "3" => 10,
//                "13" => 20
//            ),
//            "2019-06-07" => array(
//                "3" => 11,
//                "13" => 8,
//            ),
//        );
//        print_r($news);
        //最近10月
        $month_num = 10; //显示几月
        $month_start_time = strtotime(date('Y-m-01', strtotime('-' . ($month_num - 1) . ' month')));
        $month_end_time = time();
        $lists = M("source")->field("time_create,uid")->where("time_create between " . $month_start_time . " AND " . $month_end_time . "")->select();
   
        foreach ($lists as $k => $v) {
            $lists[$k]['date'] = date("Y-m", $v['time_create']);
        }
//             print_r($lists);

        $month_start = $month_end_time / 3600 / 24/30;

        $month_end = $month_start_time / 3600 / 24/30;
        $months = array();


        $j = 0;
        for ($i = 0; $i <$month_num; $i++) {
            $month = date("Y-m", ($month_start - $i) * 3600 * 24*30);

            $months[$month] = $this->source_num($lists, $uids, $month);
            $j++;
        }
       // print_r($month_start."**".$month_end);


        $this->assign("start_date", $start_date);
        $this->assign("end_date", $end_date);
        $this->assign("days", $days);
        $this->assign("uids", $uids);
           $this->assign("months", $months);
        $this->display();
    }
    //下载统计
      public function download_statistics(){
        //本月
        $sql1 = 'SELECT B.uid,COUNT(A.id) AS sum_download_goods,SUM(B.points) AS sum_download_money, FROM_UNIXTIME(B.time_create,\'%Y-%m\')as create_time FROM sucai_download AS A LEFT JOIN sucai_source AS B ON (A.tid = B.id) WHERE FROM_UNIXTIME( B.time_create, \'%Y%m\' ) = DATE_FORMAT( CURDATE( ) , \'%Y%m\' ) =1 AND A.mtype = 15 GROUP BY B.uid';
        $resthismonth = M("download")->query($sql1);
        $thismonthArr = [];
        foreach ($resthismonth as $key => $value){
            if ($value['uid'] == 3){
                $thismonthArr[$key]["name"] ="小蔡";
            }elseif ($value['uid'] == 11){
                $thismonthArr[$key]["name"] ="小贵";
            }elseif ($value['uid'] == 13){
                $thismonthArr[$key]["name"] ="小旋";
            }else{
                $thismonthArr[$key]["name"] ="系统";
            }
            $thismonthArr[$key]["y"] =intval($value["sum_download_goods"]);

        }

        $thismonthArra = [];
        foreach ($resthismonth as $key => $value){
            if ($value['uid'] == 3){
                $thismonthArra[$key]["name"] ="小蔡";
            }elseif ($value['uid'] == 11){
                $thismonthArra[$key]["name"] ="小贵";
            }elseif ($value['uid'] == 13){
                $thismonthArra[$key]["name"] ="小旋";
            }else{
                $thismonthArra[$key]["name"] ="系统";
            }
            $thismonthArra[$key]["y"] =intval($value["sum_download_money"]);

        }

        $this->assign('thismonthlist',json_encode($thismonthArr,true));
        $this->assign('thismonthmoneylist',json_encode($thismonthArra,true));

        //上个月
        $sql= 'SELECT B.uid,COUNT(A.id) AS sum_download_goods,SUM(B.points) AS sum_download_money, FROM_UNIXTIME(B.time_create,\'%Y-%m\')as create_time FROM sucai_download AS A LEFT JOIN sucai_source AS B ON (A.tid = B.id) WHERE PERIOD_DIFF( date_format( now( ) , \'%Y%m\' ) , FROM_UNIXTIME( B.time_create, \'%Y%m\' ) ) =1 AND A.mtype = 15 GROUP BY B.uid';
        $reslastmonth = M("download")->query($sql);

        $lastmonthArr = [];
        foreach ($reslastmonth as $key => $value){
            if ($value['uid'] == 3){
                $lastmonthArr[$key]["name"] ="小蔡";
            }elseif ($value['uid'] == 11){
                $lastmonthArr[$key]["name"] ="小贵";
            }elseif ($value['uid'] == 13){
                $lastmonthArr[$key]["name"] ="小旋";
            }else{
                $lastmonthArr[$key]["name"] ="系统";
            }
            $lastmonthArr[$key]["y"] =intval($value["sum_download_goods"]);

        }
        $lastmonthArra = [];
        foreach ($reslastmonth as $key => $value){
            if ($value['uid'] == 3){
                $lastmonthArra[$key]["name"] ="小蔡";
            }elseif ($value['uid'] == 11){
                $lastmonthArra[$key]["name"] ="小贵";
            }elseif ($value['uid'] == 13){
                $lastmonthArra[$key]["name"] ="小旋";
            }else{
                $lastmonthArra[$key]["name"] ="系统";
            }
            $lastmonthArra[$key]["y"] =intval($value["sum_download_money"]);

        }
        $this->assign('lastmonthlist',json_encode($lastmonthArr,true));
        $this->assign('lastmonthmoneylist',json_encode($lastmonthArra,true));

        $this->display();
    }
}

?>
