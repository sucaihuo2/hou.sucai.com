<?php

namespace Tian\Controller;

class UploadrealController extends CommonController {

    public function files() {
        $path = C("modals_file.temp");
        $upload = new \Think\Upload(); // 实例化上传类
        $upload->rootPath = $path; // 设置附件上传目录
        $upload->saveName = array('uniqid', '');
        $upload->autoSub = false;
        $upload->replace = true;
        $upload->exts = array('jpg', 'gif', 'jpeg', 'png', 'zip', 'rar'); // 设置附件上传类型
        $info = $upload->uploadOne($_FILES['file']);
        if ($info) {
            $logo = $info['savename']; //数据库保存名
            $arr = array(
                'src' => $path . $logo,
                'name' => $logo, //数据库保存名
                'size' => $info['size'],
            );
        } else {
            $arr['error'] = $upload->getError();
        }
        echo json_encode($arr);
    }

    public function cover_img() {
        $path = "uploads/temp/";
        $id = I("get.id");
        $upload = new \Think\Upload(); // 实例化上传类
        $upload->rootPath = $path; // 设置附件上传目录
        $upload->saveName = array('uniqid', '');
        $upload->autoSub = false;
        $upload->replace = true;
        $upload->exts = array('jpg', 'gif', 'jpeg', 'png'); // 设置附件上传类型
        $info = $upload->uploadOne($_FILES['file']);

        if ($info) {
            $logo = $info['savename']; //数据库保存名

            $max_width = I("post.max_width");
            $max_height = I("post.max_height");
            $min_width = I("post.min_width");
            $min_height = I("post.min_height");
            $src = $path . $logo;
            checkMuma($src);
            $pic_info = getimagesize($src);
            if ($max_width && $max_height) {
                if ($pic_info[0] > $max_width) {
                    echo json_encode(array('error' => '图片宽度超过' . $max_width . 'px'));
                    exit;
                }
                if ($pic_info[1] > $max_height) {
                    echo json_encode(array('error' => '图片高度超过' . $max_height . 'px'));
                    exit;
                }
            }

            if ($min_width && $min_height) {

                if ($pic_info[0] < $min_width) {
                    echo json_encode(array('error' => '图片宽度小于' . $min_width . 'px'));
                    exit;
                }
                if ($pic_info[1] < $min_height) {
                    echo json_encode(array('error' => '图片高度小于' . $min_height . 'px'));
                    exit;
                }
            }
            $pic_width = $pic_info[0];

            if ($pic_width > 300) {
                $font_size = 14;
                if ($pic_width >= 500) {
                    $font_size = 18;
                }
                $utype = I("get.utype");
                if ($id > 0 && $utype == 'source') {
//                    $scandir = scandir('./fonts/3/');
//
//                    $image = new \Think\Image();
//                    foreach ($scandir as $k => $v) {
//                        if($k>2){
//                        $image->open($src)->text('https://www.sucaihuo.com/source/' . $id . '', "./fonts/3/".$v."", 24, '#FF0000', \Think\Image::IMAGE_WATER_CENTER)->save($path .$v. $logo);
//                    }
//                    }
                   $image = new \Think\Image();
                   $image->open($src)->text('https://www.sucaihuo.com/source/' . $id . '', "./fonts/Octanis-SlabRoundedItalic.ttf", $font_size, '#FF0000', \Think\Image::IMAGE_WATER_CENTER)->save($path . $logo);
                }
            }


            $arr = array(
                'src' => $src,
                'name' => $logo, //数据库保存名
                'size' => $info['size'],
                'error' => ''
            );
        } else {
            $arr['error'] = $upload->getError();
        }
        echo json_encode($arr);
    }

    public function zips() {
        $path = "uploads/temp/";
        $upload = new \Think\Upload(); // 实例化上传类
        $upload->rootPath = $path; // 设置附件上传目录
        $upload->exts = array('zip', 'rar');
        $upload->saveName = array('uniqid', '');
        $upload->autoSub = false;
        $upload->replace = true;
        $upload->maxSize = 5000 * 1024 * 1024; // 设置附件上传大小
        $info = $upload->uploadOne($_FILES['file']);

//        print_r($_FILES);
        if ($info) {
            $logo = $info['savename']; //数据库保存名
            $src = $path . $logo;
//            $zips = getZipFileLists($src);
////            print_r($zips);
//            $is_index = 0;
//            foreach ($zips as $v) {
//                if (strstr($v['filename'], "index.html") or strstr($v['filename'], "index.php")) {
//                    $is_index ++;
//                }
//            }
//            if ($is_index == 0) {
//                echo json_encode(array('error' => '缺少index.html文件'));
//                exit;
//            }

            $arr = array(
                'src' => $path . $logo,
                'name' => $logo, //数据库保存名
                'name_original' => $_FILES['file']['name'], //数据库保存名
                'size' => $info['size'],
                'error' => ''
            );
        } else {
            $arr['error'] = $upload->getError();
        }
        echo json_encode($arr);
    }

}

?>