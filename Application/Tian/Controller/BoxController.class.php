<?php

namespace Tian\Controller;

use Page;

class BoxController extends CommonController {

    public function tags_lists() {//成员详情
        $tags = trim(I('get.tags'));
        $id = I('get.id', 0, 'int');
        import("Common.Org.PageAjax");
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND name like '%" . $keyword . "%'";
        }
        $count = M('modals_tags')->where($sql)->count();    //计算总数
        $Page = new Page($count, 10);
        $lists = M('modals_tags')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('ord ASC,id DESC')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->assign("id", $id);
        $this->assign("tags", $tags);
        $this->display();
    }

    public function choseTagsBoxConfirm() {
        $id = I("get.id", 0, 'int');
        $tagsGet = I("get.tags");
        $num = 0;
        if ($tagsGet) {
            $tagsArr = explode(",", $tagsGet);
            $tagsFilter = array_filter($tagsArr);
            $num = count($tagsFilter);
            $tags = implode(",", $tagsFilter);
        } else {
            $tags = '';
        }
        M("modals_dictionary")->where("id = " . $id . "")->save(array("tags" => $tags));
        echo $num;
//        echo M("modals_dictionary")->getlastsql();
    }

    public function tags_add() {//增加标签
        $data['name'] = trim(I('get.name'));
        $data['ord'] = I("get.ord", 0, 'int');
        $data['is_check'] = 30;
        $tags = M("modals_tags")->add($data);
        $id = I("get.id", 0, 'int');
        $modals_dictionary = M("modals_dictionary")->field("tags")->where("id = " . $id . "")->find();
        if ($modals_dictionary['tags']) {
            $tags = $modals_dictionary['tags'] . "," . $tags;
        }
        M("modals_dictionary")->where("id = " . $id . "")->save(array("tags" => $tags));
    }
       public function downloads() {
        $sql = "1=1";
        $tid = I('get.tid', '', 'int');
          $mtype = I('get.mtype', '', 'int');
          if($tid>0){
              $sql .= " AND tid = ".$tid."";
          }
          if($mtype>0){
              $sql .= " AND mtype = ".$mtype."";
          }
        $count = M('download')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('download')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
        foreach ($lists as $k => $v) {
            $table = getMtypeTable($v['mtype']);
            $info = M($table)->field("name")->where("id = " . $v['tid'] . "")->find();
            $lists[$k]['title'] = $info['name'];
        }
//        print_r($lists);
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
 
        $this->assign("tid", $tid);
        $this->display();
    }

}

?>
