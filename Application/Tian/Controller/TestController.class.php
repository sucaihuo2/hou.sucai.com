<?php

namespace Home\Controller;

use Think\Controller;

class TestController extends Controller {

    public function getCatId($id) {
        $rs = 0;
        if ($id === 21) {
            $rs = 11;
        } else if ($id === 12) {
            $rs = 10;
        } else if ($id === 13) {
            $rs = 13;
        } else if ($id === 6) {
            $rs = 14;
        } else if ($id === 9) {
            $rs = 251;
        } else if ($id === 7) {
            $rs = 251;
        } else if ($id === 8) {
            $rs = 16;
        } else if ($id === 10) {
            $rs = 246;
        } else if ($id === 11) {
            $rs = 263;
        } else if ($id === 15) {
            $rs = 255;
        } else if ($id === 17) {
            $rs = 19;
        } else if ($id === 18) {
            $rs = 254;
        } else if ($id === 19) {
            $rs = 20;
        } else if ($id === 20) {
            $rs = 262;
        } else if ($id === 22) {
            $rs = 261;
        } else if ($id === 24) {
            $rs = 252;
        } else if ($id === 25) {
            $rs = 254;
        } else if ($id === 26) {
            $rs = 263;
        } else if ($id === 27) {
            $rs = 248;
        } else if ($id === 5) {
            $rs = 250;
        } else if ($id === 43) {
            $rs = 262;
        } else if ($id === 28) {
            $rs = 269;
        } else if ($id === 46) {
            $rs = 18;
        }
        return $rs;
    }

    public function getLanguageId($id) {
        $rs = 0;
        if ($id === 2) {
            $rs = 1;
        } else if ($id === 3) {
            $rs = 2;
        }
        return $rs;
    }

    // http://www.sucai.com/Test/getMacangGoods
    public function getMacangGoods() {
        $lists = M("goods")->field('taobao_id,title,keywords,description,price,cat_id,language_id,content,download_url,download_url_pwd,taobao_slides,taobao_attributes,taobao_store_link')->order("id ASC")->where('sch_goods_id = 0')->limit(1)->select();
        $data = array();
        foreach ($lists as $v) {
            $data['taobao_id'] = $v['taobao_id'];
            $data['name'] = $v['title'];
            $data['keywords'] = $v['keywords'];
            $data['description'] = $v['description'];
            $data['points'] = $v['price'] * 2;
            $data['points_type'] = 1;
            $data['cat_id'] = $this->getCatId($v['cat_id']);
            $data['language'] = $this->getLanguageId($v['language_id']);
            $data['content'] = $v['content'];
            $data['download_url'] = $v['download_url'];
            $data['download_url_pwd'] = $v['download_url_pwd']; // ？？
            $data['taobao_slides'] = $v['taobao_slides']; // ？？
            $data['taobao_attributes'] = $v['taobao_attributes']; // ？？
            $data['remark'] = "https://item.taobao.com/item.htm?id=".$v['taobao_id'].""."/n".$v['taobao_store_link']; // ？？
        }
        print_r($data);
//        print_r($lists);
    }

}

?>
