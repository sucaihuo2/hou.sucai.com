<?php

namespace Tian\Controller;

use PclZip;

class JqueryController extends CommonController {

    public function lists() {
        if($_SERVER['SERVER_NAME'] == 'www.sucaihuo.com'){
//            echo 'new';exit;
        }
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND (name like '%" . $keyword . "%' or keywords like '%" . $keyword . "%')";
        }
        $cat_id = I('get.cat_id', 0, 'int'); //分类筛选
        if ($cat_id > 0) {
            $sql .= " AND cat_id = " . $cat_id . "";
        }
        $easy_id = I('get.easy_id', 0, 'int'); //难易筛选
        if ($easy_id > 0) {
            $sql .= " AND easy_id = " . $easy_id . "";
        }
        $ord = I("get.ord");
        if ($ord == 1) {
            $sql .= " AND ord >0";
        }
        $is_check = I("get.is_check");
        if ($is_check != '') {
            $sql .= " AND is_check =" . $is_check . "";
        }
        $s_admin_uid = getAdminId();
        if($s_admin_uid != 1){
            $sql .= " AND uid  = ".$s_admin_uid."";
        }
        $count = M('js')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('js')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
        $js_cats = getDictionarySubSql('js_cat_id', 'js_dictionary');
        $js_easy = getDictionarySubSql('js_easy_id', 'js_dictionary');
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->assign("cat_id", $cat_id);
        $this->assign("easy_id", $easy_id);
        $this->assign("js_cats", $js_cats);
        $this->assign("js_easy", $js_easy);
        $this->assign("ord", $ord);
        $this->assign("is_check", $is_check);
        $this->display();
    }

    public function detail() {
        if($_SERVER['SERVER_NAME'] == 'www.sucaihuo.com'){
//            echo 'new';exit;
        }
        $templatesModel = new \Tian\Model\ModalsModel();
        $id = I('get.id', '0', 'int');
        if ($id > 0) {
            $detail = M("js")->where("id =" . $id . "")->find();
            if (empty($detail)) {
                $this->error("不存在");
            }
            $s_admin_uid = getAdminId();
        if($s_admin_uid != 1 && $s_admin_uid != $detail['uid']){
              $this->error("无权限");
        }
            $jsons = json_decode($detail['content'], true);
            $parameters = json_decode($detail['parameters'], true);
            $detail['logo_url'] = getModalsLogo($id, 'middle', 'js');
            
        } else {
            $detail['is_show'] = 1;
            $detail['points'] = 20;

            $detail['logo_big'] = 1;
        }
        if (empty($jsons)) {
            $jsons = array(
                0 => array(
                    "content" => "",
                    "html" => ""
                )
            );
        }
        if (empty($parameters)) {
            $parameters = array(
                0 => array(
                    "paras" => ""
                )
            );
        }
        if (!empty($detail['tags'])) {
            $tagsArr = explode(",", $detail['tags']);
            foreach ($tagsArr as $v) {
                $info = M("js_tags")->where("id = " . $v . "")->find();
                $tags[] = $info['name'];
            }
        } else {
            $tags = array(
                0 => "",
                1 => "",
                2 => "",
                3 => "",
                4 => ""
            );
        }
        $js_cat = $templatesModel->getModalsDictionary('js_cat_id', 'js_dictionary'); //特效分类
        foreach ($js_cat as $k => $v) {
            $js_cat[$k]['sub'] = M("js_dictionary")->where("pid = " . $v['id'] . " AND is_check = 1")->order("ord ASC")->select();
        }
        $js_easy = $templatesModel->getModalsDictionary('js_easy_id', 'js_dictionary'); //特效布局
        $next = M('js')->field("name,id")->where("id > " . $id . "")->order("id ASC")->find();
        $prev = M('js')->field("name,id")->where("id < " . $id . "")->order("id DESC")->find();
        $this->assign("next", $next);
        $this->assign("prev", $prev);
        $this->assign("js_cat", $js_cat);
        $this->assign("js_easy", $js_easy);
        $this->assign("detail", $detail);
        $this->assign("mtype", 'js');
        $this->assign("jsons", $jsons);
        $this->assign("parameters", $parameters);
        $this->assign("tags", $tags);
        $this->display();
    }

    public function detail_post() {
        
    header("Content-type: text/html; charset=utf-8");
   
        $id = I('post.id', '', 'int');
        if ($id == 0) {
            $data['uid'] = session("admin_uid");
            $data['is_check'] = 1;
            $data['addtime'] = time();
            $id = M("js")->add($data);
        }
        $file_path = "jquery/" . getFileBei($id) . $id . "/";
        $file_path_demo = $file_path . "demo/";
        checkDirExists($file_path_demo);
        $data['is_original'] = I("post.is_original") == 1 ? 1 : 0;
        $data['is_recommend'] = I("post.is_recommend");
        $data['name'] = trim(I("post.name"));
        $data['cat_id'] = $_POST['cat_id'] ? implode(",", $_POST['cat_id']) : "";
        $data['cat_sub_id'] = I("post.cat_sub_id") ? implode(",", I("post.cat_sub_id")) : "";
        $data['easy_id'] = I("post.easy_id", 0, 'int');
        $data['ord'] = I("post.ord", 0, 'int');
        $data['points'] = I("post.points", 0, 'int');
        $data['points_type'] = I("post.points_type", 0, 'int');
        $data['content_first'] = I("post.content_first");
        /*         * **内容**** */
        $json = array();
        $contents = $_POST['contents'];
        $types = $_POST['types'];
        $pics = uploads_pics(I("post.pics"));
        foreach ($contents as $k => $v) {
            if ($v) {
                $json[$k]['content'] = trim($v);
                $json[$k]['type'] = $types[$k];
                $json[$k]['pics'] = $pics[$k];
            }
        }
        $data['content'] = json_encode($json);
        /*         * **参数**** */
        $paras = $_POST['paras'];
        $descriptions = $_POST['descriptions'];
        $defaults = $_POST['defaults'];
        $urls = $_POST['urls'];
        foreach ($paras as $k => $v) {
            if ($v) {
                $parameters[$k]['paras'] = trim(htmlspecialchars($v));
                $parameters[$k]['descriptions'] = trim($descriptions[$k]);
                $parameters[$k]['defaults'] = trim(htmlspecialchars($defaults[$k]));
                $parameters[$k]['urls'] = trim(htmlspecialchars($urls[$k]));
            }
        }
        $data['parameters'] = json_encode($parameters);
        $data['keywords'] = I("post.keywords");
        $data['description'] = trim($_POST['description']);
        $data['logo_big'] = I("post.logo_big");
        $data['source'] = I("post.source");
        $data['demo_url'] = I("post.demo_url");

        $data['remark'] = I("post.remark");
        $cat_id = $_POST['cat_id'];
        if (count($cat_id) == 1 && $cat_id[0] == 15) {
            $data['type'] = 1;
        } else {
            if(count($cat_id)>=2 && in_array(15,$cat_id)){
                  $data['type'] = 2;
            }else{
                $data['type'] = 0;
            }
            
        }
        $data['tags'] = transferTagsIds($_POST['tag']);

        $data['is_check'] = I("post.is_check", 0, 'int');
        $data['is_recommend'] = I("post.is_recommend", 0, 'int');
         $file_zip = $file_path.getZipMd5($id)."/";
         checkDirExists($file_zip);
        $zip_real = is_Gb2312($file_zip . $data['name'] . ".zip");
           
        if (I("post.zip") && I("post.zip") != $zip_real) {
            rename(I("post.zip"), $zip_real);
        }
         $info = M("js")->field("name")->where("id = " . $id . "")->find();
               if($info['name'] != '' &&  $info['name'] != $data['name']){
                    $zip_old = is_Gb2312($file_zip . $info['name'] . ".zip");
                     $zip_new = is_Gb2312($file_zip . $data['name'] . ".zip");
                   rename($zip_old,$zip_new);
               }

        $data['logo_big'] = $file_path . "big.jpg";

        if (I("post.logo_big") && I("post.logo_big") != $data['logo_big']) {
            rename(I("post.logo_big"), $data['logo_big']);
            $height = getMiddleLogo($data['logo_big']);
            if ($height > 0) {
                M("js")->where("id = " . $id . "")->save(array("height" => $height));
            }
        }
        if ($id > 0) {
            $data['is_sitemap'] = I("post.is_sitemap", 0, 'int');
            if ($data['is_sitemap'] == 1) {

                $filesScandir = scandir($file_path_demo);

                foreach ($filesScandir as $k => $v) {
                    $type = get_extension($v);
                    if ($type == 'html') {
                        $htmls[] = $v;
                    }
                }
                $data['sitemap'] = json_encode($htmls);
            } else {
                $data['sitemap'] = "";
            }

            M("js")->where("id = " . $id . "")->save($data);

            $state = $data['is_check'] == 0 ? 2 : 1;
            M("temp")->where("original_id = " . $id . " AND mtype =2")->save(array("state" => $state));
        } else {
            
        }
//        include_once 'common/detail_post_zip.php';

        //解压压缩文件over
//        getTagsNum(2); //统计标签

        if ($id > 0) {
            deleteHtmlFile($id,2);//删除缓存
            $this->success('修改成功！', U("Jquery/detail", array("id" => $id)));
        } else {
            $this->success('添加成功！', U('Jquery/lists'));
        }
    }

    public function cat() {//特效类别
        $pid = I('get.pid', '0', 'int');
        $sql = "pid = " . $pid . "";
        $keywords = trim(I('get.keywords'));
        if ($keywords) {
            $sql .= " AND name like '%" . $keywords . "%'";
        }
        $count = M('js_dictionary')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('js_dictionary')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('is_check DESC,ord ASC')->select();
        foreach ($lists as $k => $v) {
            $lists[$k]['num'] = M('js_dictionary')->where("pid = " . $v['id'] . "")->count(); //下级个数
            if ($v['tags']) {
                $lists[$k]['tags_num'] = count(explode(",", $v['tags']));
            } else {
                $lists[$k]['tags_num'] = 0;
            }
        }
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->assign("pid", $pid);
        $this->display();
    }

    public function cat_detail() {//特效类别详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('js_dictionary')->where("id = " . $id . "")->find();
        } else {
            $detail['pid'] = I("get.pid", 0, 'int');
            $detail['is_check'] = 1;
        }
        $cats_top = M('js_dictionary')->where("pid = 0")->order('ord ASC')->select();
        foreach ($cats_top as $k => $v) {
            $cats_top[$k]['second'] = M('js_dictionary')->where("pid = " . $v['id'] . "")->order('ord ASC')->select();
        }
        $this->assign("detail", $detail);
        $this->assign("cats_top", $cats_top);
        $this->display();
    }

    public function cat_detail_post() {
        $id = I("post.id", 0, 'int');
        $data['pid'] = I("post.pid", 0, 'int');
        $data['is_check'] = I("post.is_check", 0, 'int');

        $pinfo = M('js_dictionary')->field("pid")->where("id = " . $data['pid'] . "")->find();
        if ($pinfo['pid'] > 0) {
            $level = 2;
        } else if ($pinfo['pid'] == 0) {
            $level = 1;
        } else {
            $level = 0;
        }
        $data['level'] = $level;
        if ($id > 0) {
            $data['keywords'] = trim(I("post.keywords"));
            $data['description'] = trim(I("post.description"));
                $data['seo_title'] = trim(I("post.seo_title"));
            $data['name'] = trim(I("post.name"));
            $data['ord'] = I("post.ord", 0, 'int');
            M('js_dictionary')->where("id = " . $id . "")->save($data);
            $this->success("修改特效类别成功！", U("Jquery/cat", array("pid" => $data['pid'])));
        } else {
            $names = array_unique(array_filter(I('post.name')));
            foreach ($names as $k => $v) {
                if ($v) {
                    $data['name'] = $v;
                    $data['ord'] = I('post.ord', '30', 'int') + $k;
                    M('js_dictionary')->add($data);
                }
            }
            $this->success("添加特效类别成功！", U("Jquery/cat", array("pid" => $data['pid'])));
        }
    }

    public function tags() {
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND name like '%" . $keyword . "%'";
        }
        $count = M('js_tags')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('js_tags')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('ord ASC,id DESC')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    public function tags_detail() {//标签详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('js_tags')->where("id = " . $id . "")->find();
        } else {
            $detail['pid'] = I("get.pid", 0, 'int');
            $detail['is_check'] = 1;
            $detail['ord'] = M("js_tags")->max('ord') + 1;
        }

        $this->assign("detail", $detail);
        $this->display();
    }

    public function tags_detail_post() {
        $id = I("post.id", 0, 'int');
        $data['is_check'] = I("post.is_check", 0, 'int');
        if ($id > 0) {
            $data['name'] = strtolower(trim(I("post.name")));
            $data['keywords'] = strtolower(trim(I("post.keywords")));
            $data['description'] = strtolower(trim(I("post.description")));
            $data['ord'] = I("post.ord", 0, 'int');
            M('js_tags')->where("id = " . $id . "")->save($data);
            $this->success("修改特效标签成功！", session('QUERY_STRING'));
        } else {
            $names = array_unique(array_filter(I('post.name')));

            foreach ($names as $k => $v) {
                if ($v) {
                    $data['name'] = $v;
                    $data['ord'] = I('post.ord', '30', 'int') + $k;
                    M('js_tags')->add($data);
                }
            }
            $this->success("添加特效标签成功！", U("Jquery/tags"));
        }
    }

    public function check() {
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND (name like '%" . $keyword . "%' or name like '%" . $keyword . "%')";
        }
        $state = I('get.state'); //分类筛选
        if ($state >= 0 && $state != '') {

            $sql .= " AND state = " . $state . "";
        }
        $mtypes = array(
            0 => array(
                "mtype" => "1",
                "name" => "网站模板"
            ),
            2 => array(
                "mtype" => "2",
                "name" => "网页特效"
            ),
            3 => array(
                "mtype" => "20",
                "name" => "PHP"
            ),
            3 => array(
                "mtype" => "20",
                "name" => "PHP"
            ),
            4 => array(
                "mtype" => "15",
                "name" => "网站源码"
            ),
            5 => array(
                "mtype" => "4",
                "name" => "网站psd"
            ),
            6 => array(
                "mtype" => "3",
                "name" => "精选网址"
            ),
        );

        $count = M("temp")->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M("temp")->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
        $js_cats = getDictionarySubSql('js_cat_id', 'js_dictionary');
        $js_easy = getDictionarySubSql('js_easy_id', 'js_dictionary');
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->assign("state", $state);
        $this->assign("js_cats", $js_cats);
        $this->assign("js_easy", $js_easy);
        $this->assign("mtypes", $mtypes);
        $this->display();
    }

    public function check_detail() {

        $id = I("get.id", 0, "int");
        $detail = M("temp")->where("id = " . $id . "")->find();

        if (empty($detail)) {
            $this->error("该文章不存在");
        }
        $table = getTableInfo($detail['mtype']);
//             $mtype = $detail['mtype']== 20?2:$detail['mtype'];
        $templatesModel = new \Home\Model\TemplatesModel();
        if ($table == 'modals') {
            $cats = $templatesModel->getModalsCat(); //分类
        } elseif ($table == 'php') {
            $table = 'js';
        } else {
            $cats = $templatesModel->getModalsCat('' . $table . '_cat_id', '' . $table . '_dictionary', $table); //分类
        }
//        echo $detail['cat_id'];exit;
        if ($detail['cat_id'] > 0) {
            $cats_sub = M("" . $table . "_dictionary")->field("id,name")->where("pid = " . $detail['cat_id'] . "")->select();
        }

        $jsons = json_decode($detail['content'], true);
        if (empty($jsons)) {
            $jsons = array(0 => array());
        }

        $tags = explode(",", $detail['tags']);
        $js_easy = getDictionarySubSql('js_easy_id', 'js_dictionary');
        $modal_colors = getDictionarySubSql('modal_color_id', 'modals_dictionary');
        $modal_lays = getDictionarySubSql('modal_lay_id', 'modals_dictionary');
        $this->assign("js_easy", $js_easy);
        $this->assign("cats", $cats);
        $this->assign("detail", $detail);
        $this->assign("cats_sub", $cats_sub);
        $this->assign("jsons", $jsons);
        $this->assign("tags", $tags);
        $this->assign("modal_colors", $modal_colors);
        $this->assign("modal_lays", $modal_lays);
        $this->assign("table", $table);
        $this->display();
    }

    public function check_post() {
        $rules = array(
            array('name', 'require', '标题不能为空！'),
        );
        if (!D("Temp")->validate($rules)->create()) {
            $this->error(D("Temp")->getError());
        }
        $id = I('post.id', '', 'int');
        $data['name'] = trim(I("post.name"));
        $data['description'] = trim(I("post.description"));
        $data['cat_id'] = I("post.cat_id");
        $data['cat_sub_id'] = I("post.cat_sub_id");
        $data['points'] = I("post.points", 0, 'int');
        $data['is_original'] = I("post.is_original") == 1 ? 1 : 0;
        $data['points_type'] = I("post.points_type", 0, 'int');
        $data['source'] = I("post.source");
        $data['tags'] = I("post.tags") ? implode(",", array_filter(explode(",", I("post.tags")))) : "";
        $data['logo'] = uploads_file(I("post.logo"));
        $data['zip'] = uploads_file(I("post.zip"), ".zip");
        /*         * **内容**** */
        $json = array();
        $contents = $_POST['contents'];
        $pics = uploads_pics(I("post.pics"));
        $types = $_POST['types'];
        foreach ($contents as $k => $v) {
            if ($v) {
                $json[$k]['content'] = trim($v);
                $json[$k]['type'] = $types[$k];
                $json[$k]['pics'] = $pics[$k];
            }
        }
        $data['content'] = $json ? json_encode($json) : "";

        $data['easy_id'] = I("post.easy_id");
        $data['lay_id'] = I("post.lay_id");
        $data['color_id'] = I("post.color_id") ? implode(",", I("post.color_id")) : "";
        $state = I("post.state");

        $temp = M("temp")->field("original_id,mtype,uid")->where("id = " . $id . "")->find();
        $temp['mtype'] = $temp['mtype'] == 20 ? 2 : $temp['mtype'];
//        if ($temp['original_id'] > 0) {
//            $this->error("已推送");
//        }
        if ($temp['mtype'] == 1) { //模板
            $data_real['lay_id'] = I("post.lay_id");
            $data_real['lang_id'] = 96;
            $data_real['color_id'] = I("post.color_id") ? implode(",", I("post.color_id")) : "";
        } else if (in_array($temp['mtype'], array(2, 20))) { //特效,php
            $data_real['easy_id'] = I("post.easy_id");
        }
        if ($state == 2) { //通过审核后
            $table = getTableInfo($temp['mtype']);
            $data_official = $data;
            unset($data_official['tags']);

            if ($temp['original_id'] > 0) {
                M($table)->where("id = " . $id . "")->save($data);
            } else {
                $data_official['tags'] = '';

                $data_official['keywords'] = I("post.tags") ? implode(",", array_filter(explode(",", I("post.tags")))) : "";

                $data_official['addtime'] = time();
                $data_official['uid'] = $temp['uid'];
                $data['original_id'] = M($table)->add($data_official);

                $original_id = $data['original_id'];
                $file_path = getFileInfo($temp['mtype']) . "/" . getFileBei($original_id) . $original_id . "/";

                checkDirExists($file_path);

                $zip = $file_path . $data['name'] . ".zip";
//                echo codeAuto($zip);exit;
                if (I("post.zip") && file_exists(I("post.zip")) && I("post.zip") != $zip) {
                    rename(I("post.zip"), codeAuto($zip));
                }

                $logo = $file_path . "middle.jpg";
                if (I("post.logo") && file_exists(I("post.logo")) && I("post.logo") != $logo) {
                    rename(I("post.logo"), $logo);
                }
                $logo_big = $file_path . "big.jpg";
                if (I("post.logo_big") && file_exists(I("post.logo_big")) && I("post.logo_big") != $logo_big) {
                    rename(I("post.logo_big"), $logo_big);
                }
                $data_real['logo'] = $logo;
                $data_real['logo_big'] = $logo_big;
                $data_real['zip'] = $zip;
                M($table)->where("id = " . $original_id . "")->save($data_real);

//                echo M($table)->getlastsql();exit;
            }
        }
        $data['reason'] = I("post.reason");

        $data['state'] = $state;
        M("temp")->where("id = " . $id . "")->save($data);
//       echo M("temp")->getlastsql();exit;
        if ($state == 2) {
            if ($temp['mtype'] == '2') {
                $url_mod = "Jquery";
            } else {
                $url_mod = "modals";
            }
            $this->success("推送成功！", U("" . $url_mod . "/detail", array("id" => $original_id)));
        } else {
            $this->success("提交成功");
        }
    }
    

}

?>
    