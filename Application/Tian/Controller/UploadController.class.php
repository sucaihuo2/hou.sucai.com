<?php

namespace Tian\Controller;

class UploadController extends CommonController {

    public function zip_modals() {//模板提交压缩文件
        $mtype = I("get.mtype") ? I("get.mtype") : "modals";
        $path = C("" . $mtype . "_file.temp");
        $upload = new \Think\Upload(); // 实例化上传类
        $upload->rootPath = $path; // 设置附件上传目录
        $upload->saveName = 'uniqid';
        $upload->autoSub = false;
        $upload->replace = true;
        $info = $upload->uploadOne($_FILES['file']);
//        print_r($info);
        if (empty($info)) {
            $arr['error'] = $upload->getError();
        } else {
            $arr = array(
                'url' => $path . $info['savename'],
                'name' => $info['savename'], //数据库保存名
                'size' => $info['size'],
            );
        }
//        print_r($arr['error']);
        echo json_encode($arr);
    }

    public function sites() {//图片
        $mtype = I("get.mtype") ? I("get.mtype") : "3";
        $id = I("get.id", 0, "int");
        $table = getTableInfo($mtype);
        if ($id == 0) {
            $id = M($table)->max('id') + 1;
        }

        $path = getFileInfo($mtype) . "/" . $id . "/";
        if (!file_exists($path)) {
            mkdir($path);
        }
        $upload = new \Think\Upload(); // 实例化上传类
        $upload->rootPath = $path; // 设置附件上传目录
        $upload->saveName = array('uniqid', '');
        $upload->autoSub = false;
        $upload->replace = true;
        $info = $upload->uploadOne($_FILES['file']);
        if ($info) {
            $pics = array("jpg", "png", "jpeg", "gif");

            $logo = $info['savename']; //数据库保存名

            $file_name = $path . $logo;
            if (in_array($info['ext'], $pics)) {
                $size = getimagesize($file_name);
                $width = $size[0];
                $img_size = "big.jpg";
                if ($width == 238) {
                    $img_size = "middle.jpg";
                }
                if (file_exists($file_name)) {
                    $middle_img = $path . $img_size;
                    rename($file_name, $middle_img);
                } else {
                    echo "middle.jpg下载文件不存在";
                }
                $arr = array(
                    'src' => __APP__ . "/" . $middle_img,
                    'name' => $logo, //数据库保存名
                    'size' => $info['size'],
                );
            } else {
                $arr = array(
                    'src' => "",
                    'name' => $logo, //数据库保存名
                    'size' => $info['size'],
                );
                rename($file_name, $path . "1.zip");
            }
        } else {
            $arr['error'] = $upload->getError();
        }
        echo json_encode($arr);
    }

    public function logo() {//图片
        $path = C("uploads.temp");
        mkdir($path);
        $upload = new \Think\Upload(); // 实例化上传类
        $upload->rootPath = $path; // 设置附件上传目录
        $upload->saveName = array('uniqid', '');
        $upload->autoSub = false;
        $upload->replace = true;
        $info = $upload->uploadOne($_FILES['file']);
        if ($info) {
            $logo = $info['savename']; //数据库保存名
            $savename = $info['savename'];
            $savepath = $info['savepath'];
            $pic =   "./" .$path . $savename;
            $avatar_aliyun = 'msucaihuo/banners/'.$savename;
            $ossPic =  aliyun_upload($pic, $avatar_aliyun);
            $arr = array(
                'src' => $ossPic,
                'name' => $logo, //数据库保存名
                'size' => $info['size'],
                'error' => ""
            );
        } else {
            $arr['error'] = $upload->getError();
        }
        echo json_encode($arr);
    }

    public function pic_single() {//图片
        $path = C("uploads.temp");
        mkdir($path);
        $upload = new \Think\Upload(); // 实例化上传类
        $upload->rootPath = $path; // 设置附件上传目录
        $upload->saveName = array('uniqid', '');
        $upload->autoSub = false;
        $upload->replace = true;
        $info = $upload->uploadOne($_FILES['file']);
        if ($info) {
            $logo = __APP__ . "/" . $path . $info['savename']; //数据库保存名
            echo "<img src='" . $logo . "'/><input type='hidden' name='sku_pic[]' value='" . $info['savename'] . "'>";
        } else {
            echo $upload->getError();
        }
    }

    public function pic_goods() {//图片
        $path = C("uploads.temp");
        mkdir($path);
        $upload = new \Think\Upload(); // 实例化上传类
        $upload->rootPath = $path; // 设置附件上传目录
        $upload->saveName = array('uniqid', '');
        $upload->autoSub = false;
        $upload->replace = true;
        $info = $upload->uploadOne($_FILES['file']);
        if ($info) {
            $logo = __APP__ . "/" . $path . $info['savename']; //数据库保存名
            echo "<img src='" . $logo . "'/><input type='hidden' name='pic_goods' value='" . $info['savename'] . "'>";
        } else {
            echo $upload->getError();
        }
    }

    public function zip() {//模板提交压缩文件
        $path = C("uploads.temp");
//        print_r($_FILES);exit;
        mkdir($path);
        $upload = new \Think\Upload(); // 实例化上传类
        $upload->rootPath = $path; // 设置附件上传目录
        $upload->maxSize = 999999145728;
        $upload->saveName = 'uniqid';

        $upload->autoSub = false;
        $upload->replace = true;
        $info = $upload->uploadOne($_FILES['file']);

        if (empty($info)) {
            $arr['error'] = $upload->getError();
        } else {
            $arr = array(
                'url' => $path . $info['savename'],
                'name' => $info['savename'], //数据库保存名
                'size' => $info['size'],
            );
        }
        echo json_encode($arr);
    }

}

?>