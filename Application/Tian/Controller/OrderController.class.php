<?php

namespace Xuan \Controller;

class OrderController extends CommonController {

    public function lists() {
        $sql = "1=1";
        $month_last = last_month_today(strtotime(date("Y-m-d")));
        $starttime = strtotime(I('get.starttime', $month_last));
        $endtime = strtotime(I('get.endtime', date("Y-m-d H:i:s")));
        $shop_id = I('get.shop_id', 0, 'int');
        $keyword = trim(I('get.keyword'));
        $state = I('get.state');
        if ($starttime && $endtime) {
            $sql .= " AND addtime between " . $starttime . " AND " . $endtime . " ";
        }
        if ($keyword) {
            $sql .= " AND (truename like '%" . $keyword . "%' or phone like '%" . $keyword . "%' or addr like '%" . $keyword . "%' or ordernum like '%" . $keyword . "%')";
        }
        if ($state != '') {
            $sql .= " AND state = " . $state . "";
        }
        if ($shop_id > 0) {
            $sql .= " AND sid = '" . $shop_id . "'";
        }
        $count = M("order")->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M("order")->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('state ASC,id desc')->select();
//        echo M("order")->getlastsql();
        foreach ($lists as $k => $v) {
            $sub = M("order_goods")->field("gname,gnum")->where("pid = " . $v['id'] . "")->select();
            $num = count($sub);
            $str_goods = '';
            foreach ($sub as $k2 => $v2) {
                if ($num != $k2 + 1) {
                    $dunhao = "、";
                } else {
                    $dunhao = "";
                }
                $str_goods .= $sub[$k2]['gname'] . $sub[$k2]['gnum'] . "份" . $dunhao . "";
            }
            $lists[$k]['goods'] = $str_goods;
            $lists[$k]['phone_shop'] = getSingleField($v['sid'], 'shop', 'phone');
            $lists[$k]['email'] = getSingleField($v['sid'], 'shop', 'email');
        }
        $shops = M("shop")->order("ord ASC")->select();
        $this->assign("shop_id", $shop_id);
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->assign("state", $state);
        $this->assign("starttime", date("Y-m-d", $starttime));
        $this->assign("endtime", date("Y-m-d", $endtime));
        $short = json_decode(getTableField("shortMessage", "three"), true);
        $email = json_decode(getTableField("emails", "three"), true);
        $this->assign("short", $short);
        $this->assign("email", $email);
        $this->assign("shops", $shops);

        $this->display();
    }

    public function detail() {
        $id = I('get.id', '', 'int');
        $detail = M("order")->where("id = " . $id . "")->find();
        $goods = M("order_goods")->where("pid  = " . $id . "")->select();
        if ($detail['state'] != 0) {
            $logs = M("admin_log")->where("pid  = " . $id . " AND mtype = 1")->order("id ASC")->select();
        }
        $this->assign("goods", $goods);
        $this->assign("detail", $detail);
        $this->assign("logs", $logs);
        $this->display();
    }

    public function detail_post() {
        $id = I('post.id', '', 'int');
        $detail = M("order")->where("id = " . $id . "")->find();
        $data['state'] = I('post.state');
        if ($data['state'] == -1) {
            $data['reason'] = I("post.reason");
            $data['reasonTime'] = time();
        }
        if ($detail['state'] != $data['state']) {
            $data2['addtime'] = time();
            $data2['pid'] = $id;
            $data2['uid'] = session("admin_uid");
            $data2['content'] = getOrderState($detail['state']) . "=>" . getOrderState($data['state']);
            if ($data['state'] == -1 && $data['reason'] != '') {
                $data2['content'] .= "，失效原因：" . $data['reason'] . "";
            }
            $data2['mtype'] = 1;
            M("admin_log")->add($data2);
        }
        if ($id > 0) {
            M("order")->where("id = " . $id . "")->save($data);
            if ($data['state'] == 5) {
                M("user")->where("id = " . $detail['uid'] . "")->save(array("is_shorts" => 1));
                if ($detail['state'] != 5) {
                    addPoints("order_suc", $detail['realMoney'], session("userid"), 5, 1);
                }
            }
            if ($data['state'] == 1) {
                sendMobileOrder($id);
            }
            $this->success('操作成功！', session('QUERY_STRING'));
        }
    }

    public function set() {
        $info = M("three")->where("id = 1")->find();
        $detail = json_decode($info['order_set'], true);
        $this->assign("detail", $detail);
        $this->display();
    }

    public function set_post() {

        $arr = array();
        $arr['is_start'] = I('post.is_start', 0, 'int');
        $arr['seconds'] = I('post.seconds', 0, 'int');
        $upload = new \Think\Upload(); // 实例化上传类
        $upload->maxSize = C('upload_max_size'); // 设置附件上传大小
        $upload->rootPath = 'Public/video/'; // 设置附件上传目录
        $upload->autoSub = false;
        $info = $upload->upload();
        $three = M("three")->where("id = 1")->find();
        $detail = json_decode($three['order_set'], true);
        if ($info) {
            $uploadInfo = $info['video'];
            if ($uploadInfo) {
                $arr['video'] = $uploadInfo['savename']; //数据库保存名
            }
           unlink('Public/video/'.$detail['video'].'');
        }else{
            $arr['video'] = $detail['video'];
        }
        M("three")->where("id = 1")->save(array("order_set" => json_encode($arr)));
        clearTempFile();
        $this->success('修改成功！', U('Order/set'));
    }

}

?>
