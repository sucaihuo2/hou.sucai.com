<?php

namespace Tian\Controller;

class YiqiController extends CommonController {

    //充值积分
    public function orders() {
       
        $staticsModel = new \Tian\Model\YiqiModel();
        $state = I("get.state");
        $sql_add = " AND order_status ='支付宝已付款'";

        $datas = array(
            "sql_where" => $sql_add,
            "table" => "17sucai_orders",
            "field" => "rmb",
            "addtime" => "order_time"
        );

        $day_pays = $staticsModel->getDayLists($datas);
//        print_r($day_pays);
//        $week_pays = $staticsModel->getWeekLists($datas);
        $month_pays = $staticsModel->getMonthLists($datas);
//        $year_pays = $staticsModel->getYearLists($datas);

        $this->assign("day_pays", $day_pays);
        $this->assign("day_pays_json", json_encode($day_pays));

     
        $this->assign("week_pays_json", json_encode($week_pays));
        $this->assign("month_pays", $month_pays);
        $this->assign("month_pays_json", json_encode($month_pays));
     
    
        $this->assign("state", $state);
        $this->display();
    }
     //会员
    public function users() {
    $staticsModel = new \Tian\Model\YiqiModel();
        $state = I("get.state");
        $sql_add = "";

        $datas = array(
            "sql_where" => $sql_add,
            "table" => "17sucai_users",
            "field" => "id",
            "mtype"=>"count",
            "addtime" => "createat"
        );

        $day_pays = $staticsModel->getDayLists($datas);
//        print_r($day_pays);
//        $week_pays = $staticsModel->getWeekLists($datas);
        $month_pays = $staticsModel->getMonthLists($datas);
//        $year_pays = $staticsModel->getYearLists($datas);

        $this->assign("day_pays", $day_pays);
        $this->assign("day_pays_json", json_encode($day_pays));

        $this->assign("week_pays", $week_pays);
        $this->assign("week_pays_json", json_encode($week_pays));
        $this->assign("month_pays", $month_pays);
        $this->assign("month_pays_json", json_encode($month_pays));
        $this->assign("year_pays", $year_pays);
        $this->assign("year_pays_json", json_encode($year_pays));
        $this->assign("state", $state);
        $this->display();
    }
   
    
}

?>
