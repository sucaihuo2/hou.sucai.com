<?php

namespace Tian\Controller;

class IndexController extends CommonController {

    public function index() {

        $SERVER_NAME = $_SERVER['SERVER_NAME'];

        if ($SERVER_NAME != 'www.sucaihuo.com' && $SERVER_NAME != 'www.hou.com') {
            if ($SERVER_NAME != 'admin7788.sucaihuo.com') {
                echo 'wrong';
                exit;
            }
//            echo 'new';exit;
        }



        $config = M("config")->where("id = 1")->find();
        $users = M("user")->order("id DESC")->limit(25)->select();
        $comments = M("comment")->order("id DESC")->limit(25)->select();
        foreach ($comments as $k => $v) {
            $table = getTableInfo($v['mtype']);
            $info = M($table)->field("name")->where("id = " . $v['tid'] . "")->find();
            $comments[$k]['mtype_name'] = $info['name'];
        }
        $points = M("points")->order("id DESC")->limit(25)->select();
        $searches = M("search")->order("id DESC")->limit(25)->select();
        $logs = M("log")->where("mtype !=15")->order("id DESC")->limit(100)->select();
         $logs_send = M("log")->where("mtype =15")->order("id DESC")->limit(100)->select();
//           $kanfens = M("kanfen")->order("id DESC")->limit(100)->select();
        $users_count = M("user")->count();
        $this->assign("users", $users);
        $this->assign("users_count", $users_count);
        $this->assign("config", $config);
        $this->assign("comments", $comments);
        $this->assign("points", $points);
        $this->assign("searches", $searches);
        $this->assign("logs", $logs);
        $this->assign("logs_send", $logs_send);
//           $this->assign("kanfens", $kanfens);
        $this->display();
    }

    public function info() {
        $config = M("config")->where("id = 1")->find();
        $this->assign("config", $config);
        $this->display("Index:info");
    }

    public function info_post() {
//        $data['is_vip'] = I("post.is_vip");
        $data['source_qq'] = I("post.source_qq");
        $data['qq_send'] = I("post.qq_send");
        $data['qun_source'] = I("post.qun_source");
        M("config")->where("id = 1")->save($data);
        $this->redirect("Index/info");
    }

    public function email() {
        $info = M("three")->where("id = 1")->find();
        $detail = json_decode($info['emails'], true);
        $this->assign("detail", $detail);
        $this->display();
    }

    public function email_post() {
        $arr = array();
        $arr['smpt'] = I('post.smpt');
        $arr['email'] = I('post.email');
        $arr['pwd'] = I('post.pwd');
        $data['emails'] = json_encode($arr);
        M("three")->where("id = 1")->save($data);
        $this->success('修改成功！', U('Index/email'));
    }

    public function menu_detail() {
        $id = I('get.id', '0', 'int');
        if ($id > 0) {
            $detail = M("admin_menu2")->where("id =" . $id . "")->find();
        } else {
            $detail['is_check'] = 1;
        }
        $this->assign("detail", $detail);
        $this->display();
    }

    public function menu_detail_post() {
        $id = I('post.id', '', 'int');
        $data['name'] = I('post.name');
        $data['pid'] = I('post.pid', 0, 'int');
        $data['control'] = I('post.control');
        $data['mod'] = I('post.mod');
        $data['icon'] = I('post.icon');
        $data['is_check'] = I('post.is_check', '0', 'int');
        $data['ord'] = I('post.ord', '30', 'int');
        if ($id > 0) {
            M("admin_menu2")->where("id = " . $id . "")->save($data);
            $this->success('修改成功！', U('Index/menus'));
        } else {
            M("admin_menu2")->add($data);
            $this->success('添加成功！', U('Index/menus'));
        }
    }

    public function friends() {//源码
        $count = M('friends')->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('friends')->limit($Page->firstRow . ',' . $Page->listRows)->order('is_check DESC,ord ASC,id DESC')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->display();
    }

    public function friends_detail() {//源码详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('friends')->where("id = " . $id . "")->find();
        } else {
            $detail['is_check'] = 1;
        }
        $this->assign("detail", $detail);
        $this->display();
    }

    public function friends_detail_post() {
        $id = I("post.id", 0, 'int');
        $data['name'] = trim(I("post.name"));
        $data['ord'] = I("post.ord", 0, 'int');


        $data['url'] = trim(I("post.url"));
//        $data['is_check'] = I("post.ord", 0, 'is_check');
//        $table = 'friends';
//        include_once("common/zip.php");
        if ($id > 0) {

            M('friends')->where("id = " . $id . "")->save($data);
            $dirs = array('Application/Html/Index/');
            foreach ($dirs as $v) {
                rmdirr($v);
            }
            $this->success("修改友情链接成功！", session('QUERY_STRING'));
        } else {
            M('friends')->add($data);
            $dirs = array('Application/Html/Index/');
            foreach ($dirs as $v) {
                rmdirr($v);
            }
            $this->success("添加友情链接成功！", U("Index/friends"));
        }
    }

    public function generateCode() {
        echo 1;exit;
        $codes = explode(",", I("post.codes"));
        $codes_num = count($codes);
        $times = trim(I("post.times"));
        $rs = "";
        $table = "js";
        $mtype = "js";
        $sql = "SELECT A.id,A.name,A.description  FROM `sucai_" . $table . "` AS A JOIN (SELECT ROUND(RAND() * (SELECT MAX(id) FROM `sucai_" . $table . "`)) AS id) AS B  
WHERE A.id >= B.id  ORDER BY A.id ASC LIMIT " . $times . ";";
        $lists = M($table)->query($sql);
        $max = I("post.max", 0, 'int');
//        print_r($lists);
        for ($i = 1; $i < $times; $i++) {
            $content = trim(I("post.content"));
            $id = $lists[$i]['id'];
            $name = $lists[$i]['name'];
            $description = $lists[$i]['description'];

            if ($max > $i) {
                $num = $i;
            } else {
                $num = mt_rand(1, $max);
            }
            if ($codes_num > $i) {
                $codes_str = $codes[($i - 1)];
            } else {
                $codes_str = $codes[(mt_rand(1, $codes_num))];
            }

            $num2 = strlen($num) >= 2 ? $num : "0" . $num;
            $content = str_replace("{id}", $id, $content);
            $content = str_replace("{num}", $num, $content);
            $content = str_replace("{num2}", $num2, $content);
            $content = str_replace("{title}", $name, $content);
            $content = str_replace("{description}", $description, $content);
            $content = str_replace("{codes}", $codes_str, $content);
            $content = str_replace("{url}", "http://www.sucaihuo.com/" . $mtype . "/" . $id . ".html", $content);
            $content = str_replace("{logo}", "http://www.sucaihuo.com/jquery/" . getFileBei($id) . "/" . $id . "/middle.jpg", $content);
            $rs .= $content . "\n";
        }
        echo $rs;
    }

    public function friends3() {//源码
        $count = M('friends3')->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('friends3')->limit($Page->firstRow . ',' . $Page->listRows)->where("is_check=1")->order('ord ASC,id DESC')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->display();
    }

    public function friends3_detail() {//源码详情
        $id = I("get.id", 0, 'int');
        if ($id > 0) {
            $detail = M('friends3')->where("id = " . $id . "")->find();
        } else {
            $detail['is_check'] = 1;
        }
        $this->assign("detail", $detail);
        $this->display();
    }

    public function friends3_detail_post() {
        $id = I("post.id", 0, 'int');
        $data['name'] = trim(I("post.name"));
        $data['ord'] = I("post.ord", 0, 'int');
        $data['remark'] = trim(I("post.remark"));
        $data['demo'] = trim(I("post.demo"));
        $data['source'] = trim(I("post.source"));
//        $data['is_check'] = I("post.ord", 0, 'is_check');
        if ($id > 0) {

            M('friends3')->where("id = " . $id . "")->save($data);
            $this->success("修改第三方链接成功！", session('QUERY_STRING'));
        } else {
            M('friends3')->add($data);
            $this->success("添加第三方链接成功！", U("Index/friends3"));
        }
    }
    public function search() {//源码
        $count = M('search')->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('search')->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->display();
    }

}
