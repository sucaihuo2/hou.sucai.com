<?php

namespace Tian\Controller;

use Think\Controller;

class TaobaoController extends Controller {

    function _initialize() {
        header("Content-type: text/html; charset=utf-8");
        $is_test = false;
        if ($is_test === true) {
            $url_sucaihuo = 'http://www.sucai.com';
            $url_lanren = 'http://www.lanren.com';
            $url_jiuniao_admin = 'http://hou.jiu.com';
        } else {
            $url_sucaihuo = 'http://pay.sucaihuo.com';
            $url_lanren = 'http://pay.lanrenzhijia.com';
            $url_jiuniao_admin = 'http://panel.jiuniao.com';
        }
        $this->url_sucaihuo = $url_sucaihuo;
        $this->url_lanren = $url_lanren;
        $this->url_jiuniao_admin = $url_jiuniao_admin;
    }

    public function lists() {
//         $info = M("taobao_collect")->where("id=33")->find();
//         $rs= get_taobao_content($info['content']);
//         print_r($rs);exit;
        $sql = "is_delete = 0";

        $keywords = trim(I('get.keywords'));
        if ($keywords) {
            $sql .= " AND (title like '%" . $keywords . "%')";
        }

        $is_collect = I('get.is_collect');
        if ($is_collect != '') {
            $sql .= " AND (is_collect = '" . $is_collect . "')";
        }
        $push_id = I('get.push_id');
        if ($push_id != '') {
            if ($push_id == -1) {
                $sql .= " AND (push_id = 0)";
            } else {
                $sql .= " AND (push_id > 0)";
            }
        }

        $count = M("taobao_collect")->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, 500);
        $lists = M("taobao_collect")->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id desc')->select();
        foreach ($lists as $k => $v) {
            $content_length = strlen(strip_tags($v['content']));
            $lists[$k]['content_length'] = $content_length <= 250 ? $content_length : '<b class="red">' . $content_length . '</b>';
            $result_is_collect = '未采集';
            if ($v['is_collect'] == 1) {
                $result_is_collect = '<b class="green">已采集</b>';
            } elseif ($v['is_collect'] == -1) {
                $result_is_collect = '<b class="red">采集失败</b>';
            }
            $lists[$k]['is_collect'] = $result_is_collect;
            if ($v['price'] < 30) {
                $url = "https://www.lanrenzhijia.com/source/" . $v['push_id'] . ".html";
                $urlName = '懒人';
            } else {
                $url = "https://www.sucaihuo.com/source/" . $v['push_id'] . ".html";
                $urlName = '素材火';
            }
            $lists[$k]['push_id'] = $v['push_id'] > 0 ? '<a class="blue" style="text-decoration:underline" target="_balnk" href="' . $url . '">' . $urlName . '</a>' : '未推送';
            $info = M('source')
                            ->field("id")
                            ->where("name ='" . $v['title'] . "' AND taobao_id != '" . $v['collect_id'] . "'")->find();
            $lists[$k]['is_repeat'] = $info ? '<b class="red">已有<a href="/Tian/Source/detail/id/' . $info['id'] . '.html">' . $info['id'] . '</b>' : '暂无';
            $images = json_decode($v['images'], true);
            $lists[$k]['pic'] = "";
            foreach ($images as $v2) {

                $lists[$k]['pic'] .= "<a href='" . $v2 . "' target='_blank' ><img src='" . $v2 . "'></a>";
            }
        }
//        print_r($lists);
        $page = $Page->show();
        $this->assign("page", $page);
        $this->assign("lists", $lists);
        $this->assign("keywords", $keywords);
        $this->assign("is_collect", $is_collect);
        $this->assign("push_id", $push_id);
        $this->display();
    }

    function curl_get($url_curl) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url_curl);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //取消https ssl验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_REFERER, 'https://a.xianyujingling.com/'); //模拟来路
        if (curl_exec($ch) === false) {
            echo 'Curl error: ' . curl_error($ch);
            exit;
        }
        $result = curl_exec($ch);
//      
        curl_close($ch);
        return $result;
    }

    // https://m.tb.cn/h.fMfHBmQ?tk=0joL2gKxdeU
    // http://www.hou.com/Tian/Taobao/collect_goods
    function collect_goods() {
        $url_post = I('post.url');
        $page = I('post.page', 1);
//        echo $page;exit;
//        $url_post = 'https://item.taobao.com/item.htm?&id=657407177046';

        if (empty($url_post)) {
            echo 'url不能为空';
            exit;
        }

        $url = "https://api.api-z.cn/tb_shop?page=" . $page . "&limit=100&access_token=" . $this->access_token . "&tb_goods_url=" . urlencode($url_post) . "";
//        echo $url;
//        exit;
        $strs = $this->curl_get($url);
        $json = json_decode($strs, true);
        if ($json['msg']) {
            echo "<pre>";
            print_r($json);
            echo "</pre>";
            exit;
        }
        $arr = $json['data'];
        $success = 0;
        $error = 0;


        foreach ($arr as $v) {
            $data = array();
            $data['title'] = $v['title'];
            $data['shortUrl'] = $this->getFullUrl($v['detail_url']);
            $data['userNick'] = $v['userNick'];
            $data['price'] = $v['price'];
            $data['picUrl'] = $this->getFullUrl($v['pic_url']);
//            $data['images'] = json_encode($v['imageUrls']);
            $data['collect_id'] = $v['num_iid'];
            $data['content'] = '';
            $collect_info = M("taobao_collect")->field("id")->where("collect_id = '" . $data['collect_id'] . "'")->find();
            if ($collect_info) {
                $error++;
            } else {
                $lastid = M("taobao_collect")->add($data);
                if ($lastid > 0) {
                    $success ++;
                }
            }
        }
        $result = array(
            "success" => $success,
            "page" => $page,
            "total" => count($arr)
        );
//        $str = "成功" . $success . "，失败" . $error;
        echo json_encode($result);
    }

    function getFullUrl($url) {

        if ($url && !strstr($url, 'http')) {
            $url = "https:" . $url;
        }
        return $url;
    }

//http://www.hou.com/Tian/Taobao/onebound_taobao_get
    function onebound_taobao_get($collect_id) {
        $method = "GET";
        $url = "https://api-gw.onebound.cn/taobao/item_get/?key=tel18005151538&&num_iid=" . $collect_id . "&is_promotion=1&&lang=zh-CN&secret=20210422";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_ENCODING, "gzip");
        $str = curl_exec($curl);

        $arr = json_decode($str, true);
        if ($arr['error']) {
            echo "<pre>";
            print_r($arr);
            echo "</pre>";
            exit;
        }

        $result = $arr['item'];
        return $result;
    }

    function onebound_taobao_desc($collect_id) {
        $method = "GET";
        $url = "https://api-gw.onebound.cn/taobao/item_get_desc/?key=tel18005151538&&num_iid=" . $collect_id . "&is_promotion=1&&lang=zh-CN&secret=20210422";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_ENCODING, "gzip");
        $str = curl_exec($curl);
        $arr = json_decode($str, true);

        $result = $arr['item']['desc'];
        return $result;
    }

//http://www.hou.com/Tian/Taobao/update_content
    function update_content() {
        $ids = implode(",", array_filter(explode(",", I('post.ids'))));
//echo 'aaa';
        if ($ids) {
            $lists = M("taobao_collect")->field("id,shortUrl,collect_id")->where("id in (" . $ids . ")")->order("id DESC")->limit(1)->select();
            foreach ($lists as $v) {
                $onebound_info = $this->onebound_taobao_get($v['collect_id']);
                $content = $onebound_info['desc'];
                $userNick = $onebound_info['nick'];
                $url_store = $onebound_info['seller_info']['zhuy'];
                if ($onebound_info && empty($content)) {
//                    echo $content;
                    echo '@@';
                    $content = $this->onebound_taobao_desc($v['collect_id']);
                }
//                echo 'bbb';exit;
                $is_collect = 1;
                if (!$content) {
                    $is_collect = -1;
                }
                M("taobao_collect")->where("id = '" . $v['id'] . "'")->save(array('content' => $content, 'is_collect' => $is_collect, 'userNick' => $userNick, 'url_store' => $url_store));
            }
            $count = M("taobao_collect")->where("id in (" . $ids . ") AND is_collect = 0  AND is_delete = 0")->count();
//            echo M("taobao_collect")->getlastsql();
            echo $count;
        }
    }

    //http://hou.jiu.com/index/Xianyu/pic_local_batch.html
    // 批量生成本地图片 static/img/goods/xtemp/197/codes/
    function getImageHeight($id, $src) {
        $height = getMiddleLogo($src);
        if ($height > 0) {
            $pic_info = getimagesize($src);
            $pic_width = $pic_info[0];

            if ($pic_width > 300) {
                $font_size = 14;
                if ($pic_width >= 500) {
                    $font_size = 18;
                }


                $image = new \Think\Image();
                $image->open($src)->save($src);
            }

            M('source')->where("id = " . $id . "")->save(array("height" => $height));
        }
    }

    public function pic_local_batch($collect_id, $id) {
        if ($id && $collect_id) {
            $info = M("taobao_collect")->field("id,picUrl")->where("id  = '" . $collect_id . "'")->find();

            $dir = "sources/" . getFileBei($id) . $id . "/";

//            $name = basename($info['picurl']);
            $imgNew = $dir . "big.jpg";
            $imgOld = $info['picurl'];

            if (!file_exists($imgNew)) {
                $this->filePut($imgOld, $imgNew);
            }
        }
    }

    public function filePut($imgOld, $imgNew) {
        $dir = dirname($imgNew);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        file_put_contents($imgNew, file_get_contents($imgOld));
    }

    // http://hou.jiu.com/index/xianyu/length_delete.html
    function length_delete() {
        $lists = M("taobao_collect")->field("id,content,title")->where("is_delete = 0")->select();
        $num = 0;
        foreach ($lists as $v) {
            $length = strlen(strip_tags($v['content']));
            if ($v['content'] && $length <= 300) {
                M("taobao_collect")->where("id = '" . $v['id'] . "'")->save(array('is_delete' => 1));
                $num++;
            } else {
                $count = M("taobao_collect")->where("title = '" . $v['title'] . "' AND is_delete = 1")->count();
                if ($count > 0) {
                    M("taobao_collect")->where("id = '" . $v['id'] . "'")->save(array('is_delete' => 1));
                    $num++;
                }
            }
        }
        echo $num;
    }

//http://www.hou.com/Tian/Taobao/push_batch
    function push_batch() {

        $ids = implode(",", array_filter(explode(",", I('post.ids'))));

        if ($ids) {
            $lists = M("taobao_collect")->field("id,shortUrl,collect_id,title,content,price,picUrl,url_store")->where("id in (" . $ids . ") AND push_id = 0")->limit(1)->order("id DESC")->select();

            foreach ($lists as $k => $v) {
                $lists[$k]['content'] = get_taobao_content($v['content']);
                if ($v['price'] <= 30) {
                    $rs = $this->addLanrenSource($v);
                } else {
                    $this->addSucaihuoSource($v);
                }
            }
        }
        $count = M("taobao_collect")->where("id in (" . $ids . ") AND push_id = 0")->count();
        echo $count;
    }

// ALTER TABLE `sucai_pins` ADD `remark` VARCHAR(500) NULL AFTER `is_allegal`, ADD `taobao_id` VARCHAR(200) NULL AFTER `remark`;
    //ALTER TABLE `sucai_pins` ADD `cat_id` INT(10) NULL AFTER `taobao_id`, ADD `language_id` INT(10) NULL AFTER `cat_id`;
    // 
    function addLanrenSource($v) {
        $data = array();
        $data['title'] = $v['title'];
        $data['keywords'] = getKeywords($v['title'], $v['content']);
        $data['description'] = $v['title'];
        $data['points'] = $v['price'];
        $data['content_body'] = $v['content'];
        $data['addtime'] = time();
        $data['is_check'] = 0;
        $data['is_sale'] = 1;
        $data['remark'] = $v['shorturl'] . "\n" . $v['url_store'];
        $data['taobao_id'] = $v['collect_id'];
        $data['typeid'] = 84;
        $languageCat = getLanguageCatLanren($v['title'], $v['content']);
        $data['cat_id'] = $languageCat['cat_id'];
        $data['language_id'] = $languageCat['language_id'];
        $data['logo'] = $v['picurl'];
        $rs = curlPost($this->url_lanren . "/Ajax/addTaobaoSource", $data);

        if ($rs > 0) {
            M("taobao_collect")->where("id = " . $v['id'] . "")->save(array("push_id" => $rs, "push_type" => 'lanren'));
        }
        echo $rs;
    }

    function addSucaihuoSource($v) {
        $data = array();
        $data['uid'] = 1;
        $data['name'] = $v['title'];
        $data['description'] = $v['title'];
//                $rand = rand(10, 20) / 10;
        $xishu = 0.6;
        if ($v['price'] >= 200) {
            $xishu = 0.8;
        } else if ($v['price'] >= 500) {
            $xishu = 1.2;
        }
        $data['points'] = $v['price'] >= 20 ? ceil($v['price'] * $xishu) : 20;
        $data['points_type'] = 1;
        $data['keywords'] = getKeywords($v['title'], $v['content']);
        $data['goods_size'] = rand(5, 20);
        $data['money_install'] = 200;
        $data['time_create'] = time();
        $data['wangpan_url'] = "https://pan.baidu.com/s/" . randomkeys(23);
        $data['wangpan_pwd'] = 'lE5sGiY2Xc8QxfxLQSv1Scy2RdEW8MslhBZLgUtgyFqWsLMpgTyWht4RLE5FWA4R0n7PYXfCMmteALkbnl4ouUuaEDmnZpMcMW62w1jdV2QJSmCYgdPioo80oaIDy0JwnqO/j4omXudaQGgxdT6bJmOBcBx7XlGstYVFWwjOgYI=';
        $data['remark'] = $v['shorturl'] . "\n" . $v['url_store'];
        $data['taobao_id'] = $v['collect_id'];

        $data['details'] = $v['content'];
        $data['addtime'] = time();
        $data['is_show'] = 1;
        $languageCat = getLanguageCat($v['title'], $v['content']);
        $data['cat_id'] = $languageCat['cat_id'];
        $data['color_id'] = $languageCat['color_id'];
        $data['language'] = $languageCat['language_id'];
        $data['lay_id'] = 45; // 布局pc
        $data['development_id'] = $languageCat['development_id'];
        $goods_info = M("source")->field("id")->where("taobao_id = '" . $v['collect_id'] . "'")->find();

        if (empty($goods_info)) {
            $lastid = M("source")->add($data);
            if ($lastid > 0) {
                M("taobao_collect")->where("id = " . $v['id'] . "")->save(array("push_id" => $lastid, "push_type" => 'sucaihuo'));
            }
            $file_path = "sources/" . getFileBei($lastid) . $lastid . "/";
            checkDirExists($file_path);
            $result = $this->pic_local_batch($v['id'], $lastid);
            $logo = $file_path . "big.jpg";

            $this->getImageHeight($lastid, $logo);
        } else {

            M("source")->where("id = '" . $goods_info['id'] . "'")->save($data);
//                 echo M("source")->getlastsql();
        }
    }

    function onebound_taobao_shop_goods($shop_id) {
        $method = "GET";
        $url = "https://api-gw.onebound.cn/taobao/item_search_shop/?key=tel18005151538&&shop_id=" . $shop_id . "&page=1&sort=&&lang=zh-CN&secret=20210422";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_ENCODING, "gzip");
        $str = curl_exec($curl);
        $arr = json_decode($str, true);
        $result = $arr['item']['desc'];
        return $result;
    }

   

    function addLanrenJiuniao($v) {
        $data = array();
        $data['title'] = $v['title'];
        $data['keywords'] = getKeywords($v['title'], $v['content']);
        $data['description'] = $v['title'];
        $data['points'] = $v['price'];
        $data['content_body'] = $v['content'];
        $data['addtime'] = time();
        $data['is_check'] = 0;
        $data['is_sale'] = 1;
        $data['remark'] = $v['shortUrl'] . "\n" . $v['userNick'];
        $data['taobao_id'] = $v['collect_id'];
        $data['typeid'] = 84;
        $languageCat = getLanguageCatLanren($v['title'], $v['content']);
        $data['cat_id'] = $languageCat['cat_id'];
        $data['language_id'] = $languageCat['language_id'];
        $data['logo'] = $v['picUrl'];
        $data['images'] = $v['images'];
//        echo 'aaa';
//        $images = $v['images'];
//        print_r($v);
//        exit;
        $rs = curlPost($this->url_lanren . "/Ajax/addTaobaoSource", $data);

        if ($rs > 0) {
            $push_info = array("id" => $rs, "type" => 'lanren', "collect_id" => $data['taobao_id']);
            $rs = curlPost($this->url_jiuniao_admin . "/index/Signout/updatePush.html", $push_info);
        }
        echo $rs;
    }

    function recieveSource() {
        // echo 444;
        $v = $_REQUEST;
        if ($v['price'] <= 15) {
            echo 987;
            $this->addLanrenJiuniao($v);
        } else {
            // echo 666;
           $rs = $this->addSucaihuoJiuniao($v);
        }
    }

    public function pic_jiuniao($imgOld, $id) {
        if ($id && $imgOld) {

            $dir = "sources/" . getFileBei($id) . $id . "/";
            $imgNew = $dir . "big.jpg";

            if (!file_exists($imgNew)) {
                $this->filePut($imgOld, $imgNew);
            }
        }
    }

    public function pic_jiuniao_rand($imgOld, $id) {
        if ($imgOld && $id) {

            $dir = "sources/" . getFileBei($id) . $id . "/";

            $name = basename($imgOld);
            $imgNew = $dir . $name;
//            $imgOld = $info['picurl'];

            if (!file_exists($imgNew)) {
                $this->filePut($imgOld, $imgNew);
            }
            return $imgNew;
        }
    }

    function getRemotePic($lastid, $imgsJson, $logo) {
        $dir = "sources/" . getFileBei($lastid) . $lastid . "/";
        checkDirExists($dir);
        $images = json_decode($imgsJson, true);
        if (!$images) {
            $images = array($logo);
        }
        $imgNews = array();

        foreach ($images as $v2) {
//            $name = basename($v2);
//            $imgNew = $dir . $name;
            $imgNews[] = $this->pic_jiuniao_rand($v2, $lastid);
        }
        $img_str = "";
        foreach ($imgNews as $v2) {
            $img_str .="<p><img src='https://images.sucaihuo.com/" . $v2 . "'></p>";
        }
        return $img_str;
    }
    
       function getGoodsDescription($title, $content) {
        $fuhaos = [':', '：', ':', ';', '。', ' ', ',', '.',' ','！','!'];

        $min = 30;
        $max = 220;

        $description_last = '';
        $fuhao = "";
        foreach ($fuhaos as $k => $v) {
            $description_substr = substr($content, 0, strpos($content, $v));

            $description_num = strlen($description_substr);
      
            if ($description_num >= $min && $description_num <= $max) {

                $description_last = $description_substr;
                $fuhao = $v;

                break;
            }
        }

        if ($description_last) {
            $length = strlen($description_last);

            $content_substr = substr($content, $length);

            if ($fuhao) {

                $length_fuhao = strlen($fuhao);

                $content_last = trim(substr($content_substr, $length_fuhao));
            }
        }
     
        
        return array("description" => $description_last ? $description_last : $title, "content" => $content_last ? $content_last : $content);
    }
    function addSucaihuoJiuniao($v) {
        echo 'dddd';
        $collect_id = $v['collect_id'];
        $data = array();
        $data['uid'] = 1;
        $data['name'] = $v['title'];
               $description_content =  $this->getGoodsDescription($v['title'], $v['content']);
                   
          
                        $data['description'] = $description_content['description'];
//                $rand = rand(10, 20) / 10;
        $xishu = 1.2;
        if ($v['price'] >= 200) {
            $xishu = 1.4;
        } else if ($v['price'] >= 500) {
            $xishu = 1.5;
        }
        $data['points'] = $v['price'] >= 30 ? ceil($v['price'] * $xishu) : 30;
        $data['points_type'] = 1;
        $data['keywords'] = getKeywords($v['title'], $v['content']);
        $data['zip_size'] = rand(5, 20);
        $data['money_install'] = $v['price'] >= 100 ? 50 : 80;
        $data['time_create'] = time();
        $data['wangpan_url'] = "https://pan.baidu.com/s/" . randomkeys(23);
        $data['wangpan_pwd'] = 'lE5sGiY2Xc8QxfxLQSv1Scy2RdEW8MslhBZLgUtgyFqWsLMpgTyWht4RLE5FWA4R0n7PYXfCMmteALkbnl4ouUuaEDmnZpMcMW62w1jdV2QJSmCYgdPioo80oaIDy0JwnqO/j4omXudaQGgxdT6bJmOBcBx7XlGstYVFWwjOgYI=';
        $data['remark'] = $v['shortUrl'] . "\n" . $v['userNick'];
        $data['taobao_id'] = $v['id'];

        $data['details'] = $description_content['content'];
        $data['addtime'] = time();
        $data['is_show'] = 1;
        $languageCat = getLanguageCat($v['title'], $v['content']);
        $data['cat_id'] = $languageCat['cat_id'];
        $data['color_id'] = $languageCat['color_id'];
        $data['language'] = $languageCat['language_id'];
        $data['lay_id'] = 45; // 布局pc
        $data['development_id'] = $languageCat['development_id'];
        $data['taobao_id'] = $collect_id;
        $goods_info = M("source")->field("id")->where("taobao_id = '" . $collect_id . "'")->find();
        // print_r($data);
        if (empty($goods_info)) {
            $lastid = M("source")->add($data);
            if ($lastid > 0) {
                // 远程更新push_id
                $push_info = array("id" => $lastid, "type" => 'sucaihuo', "collect_id" => $collect_id);
                $rs = curlPost($this->url_jiuniao_admin . "/index/Signout/updatePush.html", $push_info);
                $imgs = $this->getRemotePic($lastid, $v['images'], $v['picurl']);

                M("source")->where("id = " . $lastid . "")->save(array("details" => $data['details'] . $imgs));
            }
            $file_path = "sources/" . getFileBei($lastid) . $lastid . "/";
            checkDirExists($file_path);
            $result = $this->pic_jiuniao($v['picUrl'], $lastid);
            $logo = $file_path . "big.jpg";

            $this->getImageHeight($lastid, $logo);
        } else {

            M("source")->where("id = '" . $goods_info['id'] . "'")->save($data);
//                 echo M("source")->getlastsql();
        }
    }
//http://www.hou.com/Tian/Taobao/recieveSource
  function  test(){
    $title = "php整站程序源码作文网范文文章文学好词好句散文论文门户网站";
    $content = "php整站程序源码作文网范文文章文学好词好句散文论文门户网站 pc+wap 双端 私信联系看演示 【买前说明】 1.程序均为店主自制，比起其他倒卖商家我们更懂技术售后更快捷; 2.购买程序我们均提供免费售后解答及更新； 3.购买前请详细查看演示站测试，程序安装好跟演示站一致； 4.默认拍下为源码价格，php环境需自行配置（装宝塔）；";
    $description_content =  $this->getGoodsDescription($title, $content);
                   
          
    $data['description'] = $description_content['description'];  
    $data['details'] = $description_content['content'];
    print_r($data);
}
}
