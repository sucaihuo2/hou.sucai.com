<?php

namespace Tian\Controller;

use PclZip;

class PsdController extends CommonController {

    public function lists() {
        $sql = "1=1";
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND (name like '%" . $keyword . "%' or keywords like '%" . $keyword . "%')";
        }
        $modal_cat_id = I('get.modal_cat_id', 0, 'int'); //分类筛选
        if ($modal_cat_id > 0) {
            $sql .= " AND cat_id = " . $modal_cat_id . "";
        }


        $count = M('psd')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, C("pagenum"));
        $lists = M('psd')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('id DESC')->select();
        $modal_cats = getDictionarySubSql('modal_cat_id', 'modals_dictionary');
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->assign("modal_cat_id", $modal_cat_id);
        $this->assign("modal_cats", $modal_cats);
        $this->display();
    }

    public function detail() {
        $templatesModel = new \Tian\Model\ModalsModel();
        $id = I('get.id', '0', 'int');
        if ($id > 0) {
            $detail = M('psd')->where("id =" . $id . "")->find();
            if (empty($detail)) {
                $this->error("不存在");
            }
            $jsons = json_decode($detail['content'], true);
            $parameters = json_decode($detail['parameters'], true);
            $detail['logo_url'] = getModalsLogo($id, 'middle', 'psd');
        } else {
            $detail['is_show'] = 1;
            $detail['points'] = 20;

            $detail['logo_big'] = 1;
        }
        if (empty($jsons)) {
            $jsons = array(
                0 => array(
                    "content" => "",
                    "html" => ""
                )
            );
        }
        if (empty($parameters)) {
            $parameters = array(
                0 => array(
                    "paras" => ""
                )
            );
        }
        if (!empty($detail['tags'])) {
            $tagsArr = explode(",", $detail['tags']);
            foreach ($tagsArr as $v) {
                $info = M("modals_tags")->where("id = " . $v . "")->find();
                $tags[] = $info['name'];
            }
        } else {
            $tags = array(
                0 => "",
                1 => "",
                2 => "",
                3 => "",
                4 => ""
            );
        }
        $psd_cat = $templatesModel->getModalsDictionary('psd_cat_id','psd_dictionary'); //psd分类
        foreach ($psd_cat as $k => $v) {
            $psd_cat[$k]['sub'] = M("psd_dictionary")->where("pid = " . $v['id'] . "")->order("ord ASC")->select();
        }
     $psd_colors = getDictionarySubSql('modal_color_id', 'modals_dictionary');
        $psd_jobs = getDictionarySubSql('psd_job_id', 'psd_dictionary');
         $this->assign("psd_colors", $psd_colors);
        $this->assign("psd_jobs", $psd_jobs);
        $next = M('psd')->field("name,id")->where("id > " . $id . "")->order("id ASC")->find();
        $prev = M('psd')->field("name,id")->where("id < " . $id . "")->order("id DESC")->find();
        $this->assign("next", $next);
        $this->assign("prev", $prev);
        $this->assign("psd_cat", $psd_cat);
        $this->assign("detail", $detail);
        $this->assign("mtype", 'psd');



        $this->assign("jsons", $jsons);
        $this->assign("parameters", $parameters);
        $this->assign("tags", $tags);
        $this->display();
    }

    public function detail_post() {

        $id = I('post.id', '', 'int');
        if ($id == 0) {
            $data['uid'] = session("admin_uid");
            $data['is_check'] = 1;
            $data['addtime'] = time();
            $id = M('psd')->add($data);
        }
        $file_path = "psds/" . getFileBei($id) . $id . "/";
        $file_path_demo = $file_path . "demo/";
        checkDirExists($file_path_demo);
        $data['is_original'] = I("post.is_original") == 1 ? 1 : 0;
        $data['is_recommend'] = I("post.is_recommend");
        $data['name'] = trim(I("post.name"));
        $data['cat_id'] = $_POST['cat_id'] ? implode(",", $_POST['cat_id']) : "";
        $data['cat_sub_id'] = I("post.cat_sub_id") ? implode(",", I("post.cat_sub_id")) : "";

        $data['ord'] = I("post.ord", 0, 'int');
        $data['points'] = I("post.points", 0, 'int');
        $data['points_type'] = I("post.points_type", 0, 'int');
        $data['content_first'] = I("post.content_first");
        /*         * **内容**** */
        $json = array();
        $contents = $_POST['contents'];
        $types = $_POST['types'];
        $pics = uploads_pics(I("post.pics"));
        foreach ($contents as $k => $v) {
            if ($v) {
                $json[$k]['content'] = trim($v);
                $json[$k]['type'] = $types[$k];
                $json[$k]['pics'] = $pics[$k];
            }
        }
        $data['content'] = json_encode($json);
        /*         * **参数**** */
        $paras = $_POST['paras'];
        $descriptions = $_POST['descriptions'];
        $defaults = $_POST['defaults'];
        $urls = $_POST['urls'];
        foreach ($paras as $k => $v) {
            if ($v) {
                $parameters[$k]['paras'] = trim(htmlspecialchars($v));
                $parameters[$k]['descriptions'] = trim($descriptions[$k]);
                $parameters[$k]['defaults'] = trim(htmlspecialchars($defaults[$k]));
                $parameters[$k]['urls'] = trim(htmlspecialchars($urls[$k]));
            }
        }
        $data['parameters'] = json_encode($parameters);
        $data['keywords'] = I("post.keywords");
        $data['description'] = trim($_POST['description']);
        $data['logo_big'] = I("post.logo_big");
        $data['source'] = $_POST['source'];
        $data['remark'] = I("post.remark");
        $cat_id = $_POST['cat_id'];
        if (count($cat_id) == 1 && $cat_id[0] == 15) {
            $data['type'] = 1;
        } else {
            $data['type'] = 0;
        }
        $data['tags'] = transferTagsIds($_POST['tag'], 1, 1);

        $data['is_check'] = I("post.is_check", 0, 'int');
        $data['is_recommend'] = I("post.is_recommend", 0, 'int');
        $data['zip'] = $file_path . $data['name'] . ".zip";
        if (I("post.zip") && I("post.zip") != $data['zip']) {
            rename(I("post.zip"), $data['zip']);
        }

        $data['logo_big'] = $file_path . "big.jpg";
        
        if (I("post.logo_big") && I("post.logo_big") != $data['logo_big']) {
//            echo I("post.logo_big")."<hr>".$data['logo_big'];exit;
            rename(I("post.logo_big"), $data['logo_big']);
            $height = getMiddleLogo($data['logo_big']);
            if ($height > 0) {
                M('psd')->where("id = " . $id . "")->save(array("height" => $height));
            }
        }
        if ($id > 0) {


            M('psd')->where("id = " . $id . "")->save($data);

            $state = $data['is_check'] == 0 ? 2 : 1;
            M("temp")->where("original_id = " . $id . " AND mtype =4")->save(array("state" => $state));
        } else {
            
        }
        include_once 'common/detail_post_zip.php';

        //解压压缩文件over
//        getTagsNum(2); //统计标签

        if ($id > 0) {
            deleteHtmlFile($id, 30); //删除缓存
            $this->success('修改成功！', U("Psd/detail", array("id" => $id)));
        } else {
            $this->success('添加成功！', U('Psd/lists'));
        }
    }

}

?>
    