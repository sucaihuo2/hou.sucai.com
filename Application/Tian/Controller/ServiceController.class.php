<?php

namespace Tian\Controller;


class ServiceController extends CommonController {

    public function lists() {
         $s_admin_uid = getAdminId();
        $sql = "1=1";
        
       if($s_admin_uid != 1){
           $sql .= " AND uid = ".$s_admin_uid."";
       }
        $keyword = trim(I('get.keyword'));
        if (!empty($keyword)) {
            $sql .= " AND (name like '%" . $keyword . "%' or keywords like '%" . $keyword . "%')";
        }
        $is_check = I("get.is_check");
        if ($is_check >= 0 && $is_check != '') {
            $sql .= " AND is_check = " . $is_check . "";
        }
        

        $count = M('service')->where($sql)->count();    //计算总数
        $Page = new \Think\Page($count, 20);
        $lists = M('service')->field('sucai_service.id,sucai_service.title,sucai_service.source,sucai_service.price_min,sucai_service.price_max,sucai_service.is_check,sucai_service_dictionary.name')->join('left join sucai_service_dictionary  ON sucai_service_dictionary.id = sucai_service.cat_id')->where($sql)->limit($Page->firstRow . ',' . $Page->listRows)->order('sucai_service.id DESC')->select();
        $this->assign("page", $Page->show());
        $this->assign("lists", $lists);
        $this->assign("keyword", $keyword);
        $this->display();
    }

    public function detail() {
        $id = I('get.id', '0', 'int');
        if ($id > 0) {
            $detail = M('service')->where("id =" . $id . "")->find();
            if (empty($detail)) {
                $this->error("不存在");
            }
            $jsons = json_decode($detail['content'], true);
            $content_problems = json_decode($detail['content_problems'], true);
        }
        if (empty($jsons)) {
            $jsons = array(
                0 => array(
                    "content" => "",
                    "html" => ""
                )
            );
        }
        $cats = M("service_dictionary")->field("id,name")->where("is_check=1")->order("ord ASC")->select(); //服务分类
        $this->assign("cats", $cats);
        $this->assign("detail", $detail);
        $this->assign("mtype", 'service');
        $this->assign("content_problems", $content_problems);
        $this->assign("jsons", $jsons);
        $this->display();
    }
   //服务提交
    public function detail_post()
    {
        $id = I('post.id', '', 'int');
        if ($id) {
            $data['id'] = $id;
        }
        $file_path = "services/" . getFileBei($id) . $id . "/";
           checkDirExists($file_path);
        $data['title'] = I("post.title");
        $data['cat_id'] = I("post.cat_id");

        
        $data['source'] = I("post.source");
        $data['is_check'] = I("post.is_check");
        $data['keywords'] = I("post.keywords");
        $data['description'] = I("post.description");
        $data['content_goods'] = I("post.content_goods");
        $faq = I("post.faq");
        
            $data['content_problems'] = json_encode($faq, true);
                  $data['remark'] = I("post.remark");
            
              /*         * **内容**** */
        $json = array();
        $contents = $_POST['contents'];
        $types = $_POST['types'];
        $pics = uploads_pics(I("post.pics"));
        foreach ($contents as $k => $v) {
            if ($v) {
                $json[$k]['content'] = trim($v);
                $json[$k]['type'] = $types[$k];
                $json[$k]['pics'] = $pics[$k];
            }
        }
        $data['content'] = json_encode($json);
        
        $data['logo'] = $file_path . "big.jpg";
 $data['logo_big'] = I("post.logo_big");
        if (I("post.logo_big") && I("post.logo_big") != $data['logo']) {
      
                rename(I("post.logo_big"), $data['logo']);
//     echo $data['logo_big'];exit;
            $height = getMiddleLogo($data['logo']);
        
//            echo $height;exit;
            if ($height > 0) {
                $src = $data['logo'];
                $pic_info = getimagesize($src);
                $pic_width = $pic_info[0];

                if ($pic_width > 300) {
                    $font_size = 14;
                    if ($pic_width >= 500) {
                        $font_size = 18;
                    }


                    $image = new \Think\Image();
                    $image->open($src)->save($data['logo']);
                }

            }
        }
        if ($id > 0) {
            $res = M('service')->where("id = " . $id . "")->save($data);
            
            $this->error('修改成功！', U("Service/detail", array("id" => $id)));

        } else {
            $res = M('service')->add($data);
            if ($res) {
                $this->success('添加成功！', U('Service/lists'));
            } else {
                $this->error('添加失败！', U('Service/detail'));
            }
        }
    }
    //根据服务ID关联套餐列表
    public function packages_list(){
          $id = I('get.id', '', 'int');
          if($id){
              $res = M('service_taocan')->where("pid = " . $id . "")->select();
              $this->assign('service_packages_list', $res);
              $this->assign('service_id', $id);
          }else{
              echo "调用失败";
          }
          $this->display();
    }
    //创建套餐
    public function create_packages(){
        $pid = I('post.pid');
        if(!empty($pid)){
            $data['name'] = I('post.name');
            $data['ord'] = I('post.ord');
            $data['price'] = I('post.price');
            $data['pid'] = I('post.pid');
            $data['is_check'] = 1; //默认开启
            $res = M('service_taocan')->add($data);
            if(!empty($res)){
                $this->packages_section($pid);
                $msg = array(
                    'code' => 200,
                    'mag' =>"添加成功"
                );
            }else{
                $msg = array(
                    'code' => 400,
                    'mag' =>"添加失败"
                );
            }
            $this->ajaxReturn($msg);
        }else{
            $msg = array(
                'code' => 400,
                'mag' =>"添加失败"
            );
            $this->ajaxReturn($msg);
        }
    }
    //编辑套餐
    public function edit_packages(){
        $pid = I('post.pid');
        $id = I('post.id');
        if(!empty($pid) && !empty($id)){
            $data['name'] = I('post.name');
            $data['ord'] = I('post.ord');
            $data['price'] = I('post.price');
            $w = array(
               'pid' => $pid,
               'id' => $id
            );
            $res = M('service_taocan')->where($w)->save($data);
            if(!empty($res)){
                $this->packages_section($pid);
                $msg = array(
                    'code' => 200,
                    'mag' =>"修改成功"
                );
            }else{
                $msg = array(
                    'code' => 400,
                    'mag' =>"修改失败"
                );
            }
            $this->ajaxReturn($msg);
        }else{
            $msg = array(
                'code' => 400,
                'mag' =>"修改失败"
            );
            $this->ajaxReturn($msg);
        }
    }
    //套餐区间
    public function packages_section($serviceId){
        if($serviceId){
            $sql =  "SELECT MAX(price)as macprice,MIN(price) as minprice FROM sucai_service_taocan WHERE pid =".$serviceId." LIMIT 1";
            $res = M('service_taocan')->query($sql);
            $data['price_min'] = $res[0]['minprice'];
            $data['price_max'] = $res[0]['macprice'];
            $res = M('service')->where("id = " . $serviceId . "")->save($data);
            if($res){
                \Think\Log::write('套餐区间更新成功,套餐ID'.$serviceId,'WARN');
            }else{
                \Think\Log::write('套餐区间更新失败,套餐ID'.$serviceId,'WARN');
            }
        }else{
            \Think\Log::write('套餐区间更新失败,无服务ID','WARN');
        }
    }

    public function order(){
        $count = M('service')->count();    //计算总数
        $Page = new \Think\Page($count, 20);
        $sql = "SELECT a.id,a.order_no,d.title,a.uid,b.`name`,b.price,c.nickname,a.order_money,a.`status`,a.trade_no,a.pay_type,a.addtime,a.time_pay FROM sucai_service_order as a LEFT JOIN sucai_service_taocan as b ON  (a.taocan_id = b.id) LEFT JOIN sucai_user as c ON (a.uid = c.id) LEFT JOIN sucai_service as d ON (a.pid = d.id) ORDER BY  a.id DESC ,a.time_pay desc limit ".$Page->firstRow . ",". $Page->listRows;
        $res = M('service_order')->query($sql);
        $this->assign("page", $Page->show());
        $this->assign('orderlist', $res);
        $this->display();
    }

    public function orderdostatus(){

        $id = I('post.id');
        $do_status = I('post.do_status');

        if(!empty($id)){
            $w = array(
                'id' => $id
            );
            $data['do_status'] = $do_status;
            $res = M('service_orders')->where($w)->save($data);
            if(!empty($res)){
                $msg = array(
                    'code' => 200,
                    'mag' =>"修改成功"
                );
            }else{
                $msg = array(
                    'code' => 400,
                    'mag' =>"修改失败"
                );
            }
            $this->ajaxReturn($msg);
        }else{
            $msg = array(
                'code' => 400,
                'mag' =>"修改失败"
            );
            $this->ajaxReturn($msg);
        }
    }
    //处理人分配
    public function update_processing_person(){
        $id = I('post.id');
        $do_linkman = I('post.do_linkman');

        if(!empty($id)){
            $w = array(
                'id' => $id
            );
            $data['do_linkman'] = $do_linkman;
            $res = M('service_orders')->where($w)->save($data);
            if(!empty($res)){
                $msg = array(
                    'code' => 200,
                    'mag' =>"修改成功"
                );
            }else{
                $msg = array(
                    'code' => 400,
                    'mag' =>"修改失败"
                );
            }
            $this->ajaxReturn($msg);
        }else{
            $msg = array(
                'code' => 400,
                'mag' =>"修改失败"
            );
            $this->ajaxReturn($msg);
        }
    }

}

?>
    