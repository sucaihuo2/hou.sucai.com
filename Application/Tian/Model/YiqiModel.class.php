<?php

namespace Tian\Model;

use Think\Model;

class YiqiModel extends Model {

    protected $tableName = 'pay';

    function getDayLists($datas) {
        $sql_where = $datas['sql_where'];
        $table = $datas['table'];
        $field = $datas['field'];
        $mtype = $datas['mtype'];
        $addtime = $datas['addtime'];
//        $end_date = I("get.start_date") ? I("get.start_date") : date("Y-m-d");
//        $start_date = date("Y-m-d", strtotime("-9 day " . $end_date . ""));
//        $start_time = strtotime($start_date) - 1;
//        $end_time = strtotime($end_date) + 3600 * 24 - 1;

 $start_date = I("get.start_date") ? I("get.start_date") : date("Y-m-d", strtotime("-60 day"));
        $end_date = I("get.end_date") ? I("get.end_date") : date("Y-m-d");
        $start_time = strtotime($start_date);
        $end_time = strtotime($end_date)+ 3600 * 24 - 1;
        $j = 0;
        $pays = array();
        for ($i = $start_time / 3600 / 24; $i < $end_time / 3600 / 24; $i++) {
            $t = date("Y-m-d", $i * 3600 * 24);
            $dates[] = $t;
            $pays[$j]['label'] = $t;
            $pays[$j]['value'] = 0;
            $j++;
        }
//        $start_time_sql = date("Y-m-d H:i:s",$start_date);
//    
//        $end_time_sql = $end_date;
        if($table == '17sucai_orders'){
                $sql = "".$addtime." between '" . $start_date . "' AND '" . $end_date . "'".$sql_where;
        }else{
                $sql = "".$addtime." between '" . $start_time . "' AND '" . $end_time . "'".$sql_where;
        }
//        echo date("Y-m-d",1473060212);
        $lists = M($table)->where($sql)->select();
//        echo M($table)->getlastsql();
//        print_r($lists);
        foreach ($dates as $k => $v) {

            $money = 0;
            foreach ($lists as $v2) {
                $addtimes = $v2[$addtime]+3600*24-1;
                if($table == '17sucai_orders'){
                         $addtimes = strtotime($v2[$addtime])+3600*24-1;
                }
                if ($v == date("Y-m-d", $addtimes)) {
                    if ($mtype == 'count') {
                        $money += 1;
                    } else {
                        $money += $v2[$field];
                    }
                }
            }
            $pays[$k]['value'] = $money;
        }
        return $pays;
    }

    function getMonthLists($datas) {
        $sql_where = $datas['sql_where'];
        $table = $datas['table'];
        $field = $datas['field'];
        $mtype = $datas['mtype'];
        $addtime = $datas['addtime'];
        $pays = array();

        $dates = array();
        $dates[0]['name'] = "本月";
        $dates[0]['start_date'] = date("Y-m-01");
        $dates[0]['end_date'] = time();

        $month_num = 10; //显示几月

        $start_time = strtotime(date('Y-m-01', strtotime('-' . ($month_num - 1) . ' month')));
        $end_time = time();
        $sql = "".$addtime." between '" . date("Y-m-d H:i:s",$start_time) . "' AND '" . date("Y-m-d H:i:s",$end_time) . "' " . $sql_where . "";
        $lists = M($table)->where($sql)->select();
        $t = time() - strtotime(date("Y-m-01"));
        for ($i = 0; $i < $month_num; $i++) {
            $dates[$i]['name'] = date('Y-m', strtotime('-' . $i . ' month')-$t);
            $dates[$i]['start_date'] = date('Y-m-01', strtotime('-' . $i . ' month')-$t);
            $dates[$i]['end_date'] = date('Y-m-t', strtotime('-' . $i . ' month'));
            $pays[$i]['label'] = $dates[$i]['name'];
            $pays[$i]['value'] = 0;
        }

        foreach ($dates as $k => $v) {
            $money = 0;
            foreach ($lists as $v2) {
                $money_date = date("Y-m-d", strtotime($v2[$addtime]));
                if ($v['start_date'] <= $money_date && $v['end_date'] >= $money_date) {
                    if ($mtype == 'count') {
                        $money += 1;
                    } else {
                        $money += $v2[$field];
                    }
                }
            }
            $pays[$k]['value'] = $money;
        }
        return $pays;
    }

  

}
?>

