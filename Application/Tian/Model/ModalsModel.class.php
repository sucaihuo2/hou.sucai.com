<?php

namespace Tian\Model;

use Think\Model;

class ModalsModel extends Model {

    function getModalsDictionary($code, $table = 'modals_dictionary') {//模板字典
        $info = M($table)->field("id")->where("code = '" . $code . "'")->find();
        $lists = M($table)->field("id,name")->where("pid = " . $info['id'] . " AND is_check = 1")->order("ord ASC")->select();
        return $lists;
    }

    function getSourceDictionary($code, $table = 'source_dictionary') {//模板字典
        $info = M($table)->field("id")->where("code = '" . $code . "'")->find();
        $lists = M($table)->field("id,name")->where("pid = " . $info['id'] . " AND is_check = 1 AND id != 237")->order("ord ASC")->select();
        return $lists;
    }

    function getVideoDictionary($code, $table = 'video_dictionary') {//视频字典
        $info = M($table)->field("id")->where("code = '" . $code . "'")->find();
        $lists = M($table)->field("id,name")->where("pid = " . $info['id'] . " AND is_check = 1")->order("ord ASC")->select();
        return $lists;
    }
     function getPintuanDictionary($code, $table = 'pintuan_dictionary') {//模板字典
        $info = M($table)->field("id")->where("code = '" . $code . "'")->find();
        $lists = M($table)->field("id,name")->where("pid = " . $info['id'] . " AND is_check = 1")->order("ord ASC")->select();
        return $lists;
    }

}
?>

