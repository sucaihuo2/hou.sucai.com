<?php

namespace Tian\Model;

use Think\Model;

class StaticsModel extends Model {

    protected $tableName = 'pay';

    function getDayLists($datas) {
        $sql_where = $datas['sql_where'];
        $table = $datas['table'];
        $field = $datas['field'];
        $mtype = $datas['mtype'];
        $addtime = $datas['addtime'];
//        $end_date = I("get.start_date") ? I("get.start_date") : date("Y-m-d");
//        $start_date = date("Y-m-d", strtotime("-9 day " . $end_date . ""));
//        $start_time = strtotime($start_date) - 1;
//        $end_time = strtotime($end_date) + 3600 * 24 - 1;

$start_date = I("get.start_date") ? I("get.start_date") : date("Y-m-d", strtotime("-30 day"));
        $end_date = I("get.end_date") ? I("get.end_date") : date("Y-m-d");
       // $start_date = '2018-10-01';
     //   $end_date = '2018-10-07';
        
        $start_time = strtotime($start_date);
        $end_time = strtotime($end_date)+ 3600 * 24 - 1;;
        $j = 0;
        $pays = array();
        for ($i = $start_time / 3600 / 24; $i < $end_time / 3600 / 24; $i++) {
            $t = date("Y-m-d", $i * 3600 * 24);
            $dates[] = $t;
            $pays[$j]['label'] = $t;
            $pays[$j]['value'] = 0;
            $j++;
        }

        $sql = "".$addtime." between '" . $start_time . "' AND '" . $end_time . "' " . $sql_where . "";
           
        $lists = M($table)->where($sql)->select();
//echo M($table)->getlastsql()."<hr>";
        
        foreach ($dates as $k => $v) {

            $money = 0;
            foreach ($lists as $v2) {
                if ($v == date("Y-m-d", $v2[$addtime])) {
                    if ($mtype == 'count') {
                        $money += 1;
                    } else {
                        $money += $v2[$field];
                    }
                }
            }
            $pays[$k]['value'] = $money;
        }
        return $pays;
    }

    function getWeekLists($datas,$type) {
        $sql_where = $datas['sql_where'];
        $table = $datas['table'];
        $field = $datas['field'];
        $mtype = $datas['mtype'];
        $addtime = $datas['addtime'];
        $pays = array();
        $date = date('Y-m-d');  //当前日期
        $first = 1; //$first =1 表示每周星期一为开始日期 0表示每周日为开始日期
        $w = date('w', strtotime($date));  //获取当前周的第几天 周日是 0 周一到周六是 1 - 6 
        $now_start = date('Y-m-d', strtotime("$date -" . ($w ? $w - $first : 6) . ' days')); //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
        $now_end = date('Y-m-d', strtotime("$now_start +6 days"));  //本周结束日期
        $dates = array();
        $dates[0]['name'] = "本周";
        $dates[0]['start_date'] = $now_start;
        $dates[0]['end_date'] = $now_end;

        $week_num = 10; //显示几周

        $start_time = time() - ($week_num - 1) * 3600 * 24 - (time() - strtotime(date("Y-m-d")));
      
        $end_time = time();
        $sql = " ".$addtime." between '" . $start_time . "' AND '" . $end_time . "' " . $sql_where . "";
  
//        M("user")->where("addtime between '1503809623' AND '1503819623' ")->select();
//        echo M("user")->getlastsql();exit;
        $lists = M($table)->where($sql)->select();
//        echo M($table)->getlastsql();exit;
        $pays[0]['label'] = "本周";
        $pays[0]['value'] = 0;

        for ($i = 1; $i < $week_num; $i++) {
            if ($i == 1) {
                $dates[$i]['name'] = "上周";
            } else {
                $dates[$i]['name'] = "前" . $i . "周";
            }
            $start = 7 * $i;
            $end = 7 * ($i - 1);
            $dates[$i]['start_date'] = date('Y-m-d', strtotime("$now_start - " . $start . " days"));
            $dates[$i]['end_date'] = date('Y-m-d', strtotime("$now_start - " . $end . " days"));

            $pays[$i]['label'] = $dates[$i]['name'];
            $pays[$i]['value'] = 0;
        }

        foreach ($dates as $k => $v) {
            $money = 0;
            foreach ($lists as $v2) {
                $money_date = date("Y-m-d", $v2[$addtime]);
                if ($v['start_date'] <= $money_date && $v['end_date'] > $money_date) {
                    if ($mtype == 'count') {
                        $money += 1;
                    } else {
                        $money += $v2[$field];
                    }
                }
            }
            $pays[$k]['value'] = $money;
        }
        return $pays;
    }

    function getMonthLists($datas) {
        $sql_where = $datas['sql_where'];
        $table = $datas['table'];
        $field = $datas['field'];
        $mtype = $datas['mtype'];
        $addtime = $datas['addtime'];
        $pays = array();

        $dates = array();
        $dates[0]['name'] = "本月";
        $dates[0]['start_date'] = date("Y-m-01");
        $dates[0]['end_date'] = time();

        $month_num = 10; //显示几月

        $start_time = strtotime(date('Y-m-01', strtotime('-' . ($month_num - 1) . ' month')));
        $end_time = time();
        $sql = "".$addtime." between '" . $start_time . "' AND '" . $end_time . "' " . $sql_where . "";
        $lists = M($table)->where($sql)->select();
        $t = time() - strtotime(date("Y-m-01"));
        for ($i = 0; $i < $month_num; $i++) {
            $dates[$i]['name'] = date('Y-m', strtotime('-' . $i . ' month')-$t);
            $dates[$i]['start_date'] = date('Y-m-01', strtotime('-' . $i . ' month')-$t);
            $dates[$i]['end_date'] = date('Y-m-t', strtotime('-' . $i . ' month'));
            $pays[$i]['label'] = $dates[$i]['name'];
            $pays[$i]['value'] = 0;
        }

        foreach ($dates as $k => $v) {
            $money = 0;
            foreach ($lists as $v2) {
                $money_date = date("Y-m-d", $v2[$addtime]);
                if ($v['start_date'] <= $money_date && $v['end_date'] >= $money_date) {
                    if ($mtype == 'count') {
                        $money += 1;
                    } else {
                        $money += $v2[$field];
                    }
                }
            }
            $pays[$k]['value'] = $money;
        }
        return $pays;
    }

    function getYearLists($datas) {
        $sql_where = $datas['sql_where'];
        $table = $datas['table'];
        $field = $datas['field'];
        $mtype = $datas['mtype'];
        $addtime = $datas['addtime'];
        $pays = array();

        $dates = array();
        $dates[0]['name'] = "今年";
        $dates[0]['start_date'] = date("Y-01-01");
        $dates[0]['end_date'] = time();

        $year_num = 10; //显示几年

        $start_time = strtotime(date('Y-01-01', strtotime('-' . ($year_num - 1) . ' year')));
        $end_time = time();
        $sql = "".$addtime." between '" . $start_time . "' AND '" . $end_time . "' " . $sql_where . "";
        $lists = M($table)->where($sql)->select();

        for ($i = 0; $i < $year_num; $i++) {
            $dates[$i]['name'] = date('Y', strtotime('-' . $i . ' year'));
            $dates[$i]['start_date'] = date('Y-01-01', strtotime('-' . $i . ' year'));
            $dates[$i]['end_date'] = date('Y-12-t', strtotime('-' . $i . ' year'));
            $pays[$i]['label'] = $dates[$i]['name'];
            $pays[$i]['value'] = 0;
        }

        foreach ($dates as $k => $v) {
            $money = 0;
            foreach ($lists as $v2) {
                $money_date = date("Y-m-d", $v2[$addtime]);
                if ($v['start_date'] <= $money_date && $v['end_date'] >= $money_date) {
                    if ($mtype == 'count') {
                        $money += 1;
                    } else {
                        $money += $v2[$field];
                    }
                }
            }
            $pays[$k]['value'] = $money;
        }
        return $pays;
    }

}
?>

