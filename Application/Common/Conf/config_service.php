<?php

return array(
    //'配置项'=>'配置值'
    'DB_TYPE' => 'mysql', // 数据库类型
    'DB_HOST' => 'localhost', // 服务器地址
    'DB_NAME' => 'sucai', // 数据库名
    'DB_USER' => 'root', // 用户名
    'DB_PWD' => '123456', // 密码qYvlRGm7e4OLGc7q
    'DB_PORT' => 3306, // 端口
    'DB_PREFIX' => 'sucai_', // 数据库表前缀 
    'DB_CHARSET' => 'utf8', // 字符集
    'MODULE_ALLOW_LIST' => array(
        'Home',
        'Admin',
        'Xuan'
    ),
    'TMPL_EXCEPTION_FILE' => './Application/Home/View/Public/404.html', //./Application/Home/View/Public/404.html
    'URL_MODEL' => 2,
    'LOAD_EXT_FILE' => 'common',
    'SHOW_PAGE_TRACE' => false, //显示调试信息
    'upload_max_size' => 100 * 1024 * 1024,
    'error_operate' => '操作异常',
    'pagenum' => 10,
    'COOKIE_EXPIRE' => 3600 * 24 * 30,
    'modals_file' => array(
        'demo' => 'modals/demo/',
        'temp' => 'modals/temp/',
        'zip' => 'modals/zip/',
        'logo' => 'modals/logo/',
    ),
    'files' => array(
        'avatar' => 'Public/Uploads/avatar/',
    ),
    'pic' => array(
        'types' => 'bmp,jpg,jpeg,png,gif,ico'
    ),
    'pages' => array(
        'site_template' => '4', //模板列表页分页参数位置 必须是最末尾
        'site_js' => 2
    ),
    'URL_ROUTER_ON' => true,
    'URL_ROUTE_RULES' => array(
        'templates/:id\d' => 'Templates/detail',
        'templates/:paras' => 'Templates/index',
        'js/:id\d' => 'Js/detail',
        'js/:paras' => 'Js/index',
        'search' => 'Search/templates',
        'forget' => 'Pwd/find',
        'sendtip' => 'Pwd/send_tip',
        'help/template_post' => 'Help/template_post',
        'help/:paras' => 'Help/index',
    ),
    'HTML_CACHE_ON' => true, // 开启静态缓存
    'HTML_CACHE_TIME' => 3600, // 全局静态缓存有效期（秒）
    'HTML_FILE_SUFFIX' => '.html', // 设置静态缓存文件后缀
    'HTML_CACHE_RULES' => array(// 定义静态缓存规则
        'templates:' => array('Templates/{$_GET.id}_{$_GET.paras}'),
        'js:' => array('Js/{$_GET.paras}'),
        'help:' => array('Help/{$_GET.paras}'),
        'login:' => array('Login/index'),
        'reg:' => array('Reg/index'),
        'index:' => array('Index/index')
    ),
    'SITE_URL' => "http://www.sucaihuo.com/"
);
