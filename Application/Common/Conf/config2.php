<?php

$host = $_SERVER['HTTP_HOST'];

if ($host == 'localhost') {
    $DB_HOST = 'localhost';
    $DB_USER = 'root';
    $DB_PWD = 'root';
    $SITE_URL = "http://localhost/sucai/";
    $version = "";
    $DB_NAME = 'sucai';
    $html = false;
}elseif (strstr($host, 'admin5.sucaihuo.com') != '') { //后台
    $DB_HOST = '139.224.33.250';
    $DB_NAME = 'sucai';
    $DB_USER = 'sucai49';
    $DB_PWD = 'UDo4jJDD7jDSvfqL';
    $SITE_URL = "http://admin5.sucaihuo.com/";
    $version = "";
    $html = false;
} else {

    $DB_HOST = 'localhost';
    $DB_USER = 'sucaihuo_service';
    $DB_PWD = 'iWzfn5hAEa';
    $DB_NAME = 'sucai';
    $SITE_URL = "http://www.sucaihuo.com/";
    $version = "_version13";
    $html = true;
}
$arr = array(
    'DB_TYPE' => 'mysql',
    'DB_HOST' => $DB_HOST,
    'DB_NAME' => $DB_NAME,
    'DB_USER' => $DB_USER,
    'DB_PWD' => $DB_PWD,
    'DB_PORT' => 3306,
    'DB_PREFIX' => 'sucai_',
    'DB_CHARSET' => 'utf8',
    'MODULE_ALLOW_LIST' => array(
        'Home',
        'Tian',
    ),
    'LOG_RECORD' => false, //日志开启
    'URL_MODEL' => 2,
    'LOAD_EXT_FILE' => 'common,system',
    'SHOW_PAGE_TRACE' => false,
    'SITE_URL' => $SITE_URL,
    'upload_max_size' => 100 * 1024 * 1024,
    'pagenum' => 10,
    'COOKIE_EXPIRE' => 3600 * 24 * 7,
    'modals_file' => array(
        'demo' => 'modals/',
        'temp' => 'modals/temp/',
        'zip' => 'modals/',
        'logo' => 'modals/',
    ),
    'js_file' => array(
        'demo' => 'jquery/',
        'zip' => 'jquery/',
        'logo' => 'jquery/',
    ),
    'sites_file' => array(
        'logo' => 'sites/',
    ),
    'source_file' => array(
        'logo' => 'sources/',
        'zip' => 'sources/',
    ),
    'video_file' => array(
        'logo' => 'videos/',
        'zip' => 'videos/',
    ),
     'pintuan_file' => array(
        'logo' => 'pintuans/',
        'zip' => 'pintuans/',
    ),
    'files' => array(
        'avatar' => 'http://demo.sucaihuo.com/other/avatar/dir/',
    ),
    'pic' => array(
        'types' => 'bmp,jpg,jpeg,png,gif,ico'
    ),
    'uploads' => array(
        'upload' => 'uploads/',
        'temp' => 'uploads/temp/',
    ),
    'remote_url' => array(
        "sites" => "http://sites.sucaihuo.com/",
    ),
    'version' => $version,
    'HTML_CACHE_ON' => $html, // 开启静态缓存
    'points' => array(
        'email_check_active' => '0', //邮箱验证激活
        'sign_day' => '20', //每日签到
        'comment' => '5', //评论
        'sign_month' => '200', //连续签到一月
        'paybei' => '10',
        'invite' => '500'
    ),
    'month_discount' => 0.5,
    'money' => array(
        'paybei' => '1', //火币
        'award_top_percent' => 20, //返回上级佣金百分比
        'withdraw_handling_charge_percent' => 20, //手续费百分百
        'withdraw_lowest_money' => 100, //最低可提现金额
    ),
    'privateKey' => '*&09%$_s@?ddt?ghhthh',
    'DEFAULT_FILTER' => 'htmlspecialchars,addslashes',
    'url_yanshi'=>'http://yanshi.sucaihuo.com'
);

return $arr;
