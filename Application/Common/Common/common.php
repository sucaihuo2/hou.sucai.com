<?php

function getRefundState($fuhao) {
    if ($fuhao == 0) {
        $word = "<b class='red'>支出</b>";
    } else {
        $word = "<b class='green'>收入</b>";
    }
    return $word;
}

function getPointsState($type) {
    if ($type == 0) {
        $word = "<span style='color:#333'>待审核</span>";
    } elseif ($type == 5) {
        $word = "<span style='color:#FF6600'>已完成</span>";
    } elseif ($type == 1) {
        $word = "<span style='color:red'>已审核</span>";
    } else {
        $word = "<span style='color:#FF0000'>审核失败</span>";
    }
    return $word;
}

function addPoints($mtype, $money, $uid, $status, $fuhao, $symbol = 0, $tid = 0, $is_vip = 0, $points_type = 0) {
    //8864,905
    if ($money > 0 && !in_array($uid, array(1)) && $uid > 0) {
        $data['addtime'] = time();
        $data['uid'] = $uid ? $uid : session("userid");
        $data['mtype'] = $mtype;
        $data['money'] = $money;
        $data['status'] = $status ? $status : 0;
        $data['fuhao'] = $fuhao ? $fuhao : 0;
        $data['symbol'] = $symbol;
        $data['tid'] = $tid;
        $data['is_vip'] = $is_vip;
        $data['points_type'] = $points_type;
        $lastid = M("points")->add($data);
        if ($lastid > 0) {
            $field_money = ($points_type == 1) ? 'huobi' : 'money';
            if ($fuhao == 1) {
                if (in_array($uid, array(905))) {
                    if (!in_array($mtype, array("comment", "day_sign"))) {
                        return;
                    }
                }
                M('user')->where("id=" . $data['uid'] . "")->setInc($field_money, $data['money']); //setInc加
                //给上级返佣金
                $userinfo = M("user")->field("uid_top")->where("id = " . $uid . "")->find();
                $uid_top = $userinfo['uid_top'];
                if ($uid_top > 1) {
                    $to_money = $data['money'] * C("money.award_top_percent") / 100;
                    M("points")->where("id = " . $lastid . "")->save(array("to_uid" => $uid_top, "to_money" => $to_money));
                    M('user')->where("id=" . $uid_top . "")->setInc($field_money, $to_money); //setInc加
                }
            } else {
                if ($is_vip == 1 && $symbol == 0) { //只有是会员且非原创不扣积分
                } else {
                    M('user')->where("id=" . $data['uid'] . "")->setDec($field_money, $data['money']); //setDec减
                }
            }
        }
        return $lastid;
    }
}

function check_remote_file_exists($url) {
    $curl = curl_init($url);
// 不取回数据
    curl_setopt($curl, CURLOPT_NOBODY, true);
// 发送请求
    $result = curl_exec($curl);
    $found = false;
// 如果请求没有发送失败
    if ($result !== false) {
// 再检查http响应码是否为200
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($statusCode == 200) {
            $found = true;
        }
    }
    curl_close($curl);

    return $found;
}

function getUserAvatar($id) {
    $logo_rs = __APP__ . "/Public/images/avatar.jpg";

    if ($id) {
        $logo = C("files.avatar") . $id . ".jpg";

        if (check_remote_file_exists($logo)) {
            $logo_rs = $logo;
        }
    }
    return $logo_rs;
}

function threeLogin() {
    $arr = array(
        0 => array(
            "code" => "qq",
            "logo" => "qq"
        ),
        1 => array(
            "code" => "renren",
            "logo" => "qq"
        ),
        2 => array(
            "code" => "sina",
            "logo" => "qq"
        ),
    );
    return $arr;
}

function getTempalteState($state, $name_short, $reason) {
    if ($state == 1) {
        $word = "<b class='green'>审核通过 <a href='" . __APP__ . "/website/" . $name_short . "'></a></b>";
    } else if ($state == 2) {
        $word = "<b class='red'>采集成功 <a href='" . __APP__ . "/website/" . $name_short . "'></a></b>";
    } else if ($state == -1) {
        $word = "<b style='color:#666'>审核未通过：" . $reason . "</b>";
    } else {
        $word = "<b style='color:#ccc'>待审核</b>";
    }
    return $word;
}

function getJqueryDemo($id) {

    return "http://yanshi.sucaihuo.com/" . C("js_file.demo") . getFileBei($id) . $id . "/demo/";
}

function getJqueryDemoUrl($detail) {
    if ($detail['demo_url'] != '') {
        $url = $detail['demo_url'];
    } else if ($detail['ord'] == 99) {
        $url = $detail['source'];
    } else {
        $url = getJqueryDemo($detail['id']);
    }
    return $url;
}

function getJqueryDemoImages($id, $type = 'big') {
    $img = "http://yanshi.sucaihuo.com/jquery/" . getFileBei($id) . $id . "/" . $type . ".jpg";
    return $img;
}

function getJqueryBigImage($id, $type = 'big') {
    $img = "http://yanshi.sucaihuo.com/jquery/" . getFileBei($id) . $id . "/" . $type . ".jpg";
    return $img;
}

function getModalsBigImage($id, $type = 'big') {
    $img = "modals/" . getFileBei($id) . $id . "/" . $type . ".jpg";
    return $img;
}

function getSourceBigImage($id, $type = 'big') {
    $img = "sources/" . getFileBei($id) . $id . "/" . $type . ".jpg";
    return $img;
}

function getVideoBigImage($id, $type = 'big') {
    $img = "Video/" . getFileBei($id) . $id . "/" . $type . ".jpg";
    return $img;
}

function getSoftBigImage($id, $type = 'big') {
    $img = "Soft/" . getFileBei($id) . $id . "/" . $type . ".jpg";
    return $img;
}

function getCourseBigImage($id, $type = 'big') {
    $img = "Course/" . getFileBei($id) . $id . "/" . $type . ".jpg";
    return $img;
}

function getSitesBigImage($id, $type = 'big') {
    $img = "sites/" . getFileBei($id) . $id . "/" . $type . ".jpg";
    return $img;
}

function getWeiboImages($id, $type = 'big') {
    $img = "jquery/" . getFileBei($id) . $id . "/" . $type . ".jpg";
    return $img;
}

function getTableInfo($mtype, $field = 'table') {
    switch ($mtype) {
        case "1":
            $table = 'modals';
            break;
        case "2":
            $table = 'js';
            break;
        case "3":
            $table = 'sites';
            break;
        case "12":
            $table = 'tools';
            break;
        case "15":
            $table = 'source';
            break;
        case "20":
            $table = 'js';
            break;
        case "21":
            $table = 'topic';
            break;
        case "30":
            $table = 'video';
            break;
        case "50":
            $table = 'pintuan';
            break;
        case "60":
            $table = 'course';
            break;
    }
    $arr = array("table" => $table);
    return $arr[$field];
}

function getHtmlMtype($mtype) {
    switch ($mtype) {
        case "1":
            $table = 'templates';
            break;
        case "2":
            $table = 'js';
            break;
        case "15":
            $table = 'source';
            break;
        case "30":
            $table = 'video';
            break;
        case "50":
            $table = 'pintuan';
            break;
        case "60":
            $table = 'course';
            break;
    }
    return $table;
}

function getFileInfo($mtype) {
    switch ($mtype) {
        case "1":
            $table = 'modals';
            break;
        case "2":
            $table = 'jquery';
            break;
        case "3":
            $table = 'sites';
            break;
        case "5":
            $table = 'topic';
            break;
        case "15":
            $table = 'source';
            break;
        case "20":
            $table = 'jquery';
            break;
        case "30":
            $table = 'video';
            break;
        case "50":
            $table = 'pintuan';
            break;
        case "60":
            $table = 'course';
            break;
    }
    return $table;
}

function getTan($id) {
    if ($id > 0) {
        $comment = M("comment")->field("uid")->where("id = " . $id . "")->find();
        return "回复 " . getUidUrl($comment['uid']) . "";
    }
}

function getUidUrl($uid) {
    return "<a class='blue'  target='_blank' href='" . __APP__ . "/space/uid/" . $uid . "' >" . getUserName($uid) . "</a> ";
}

function getSpaceUserUrl($uid) {
    return __APP__ . "/space/uid/" . $uid;
}

function getNickname($name, $nickname) {
    if (empty($nickname)) {
        $nickname = $name;
    }
    return $nickname;
    if ($nickname == '素材火管理员') {
        return $nickname;
    } else {
        return hideStar($nickname);
    }
}

function getNoHideNickname($name, $nickname) {
    if (empty($nickname)) {
        $nickname = $name;
    }
    return $nickname;
}

function getUserName($uid) {
    if ($uid > 0) {
        $info = M("user")->field("name,nickname,vip_time")->where("id = " . $uid . "")->find();
        if ($info['vip_time'] > time() or $uid == 1) {
            return "<span class='red'>" . getNickname($info['name'], $info['nickname']) . "</span>";
        } else {
            return getNickname($info['name'], $info['nickname']);
        }
    }
}

function getTopicRule($paras) {
    $parasArr = explode(",", $paras);
    $id = $parasArr[0];
    $para = $parasArr[1];
    $mtype = $parasArr[2];
    if ($mtype == 'edit') {
        
    } else {
        if ($id > 0) {
            $html = "topic/" . getFileBei($id) . $id;
        } else if ($id == '' && $para == '') {
            $html = "topic/index";
        } else {
            $html = "topic/paras/" . $para;
        }
    }

    return $html;
}

function getUploadsCatsType($type) {
    $types = array(
        "0" => "段落",
        "1" => "html",
        "2" => "js",
        "3" => "css",
        "4" => "标题",
        "6" => "PHP",
        "7" => "DIV",
        "8" => "原样输出",
    );
    if ($type >= 0 && $type != '') {
        return $types[$type];
    } else {
        return "代码类型";
    }
}

function uploads_file($file, $type = '.jpg') {
    if (strstr($file, "/temp/")) {
        $path = "uploads/course/" . date("Y") . "/" . date("m") . "/" . date("d") . "/";
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $name_new = $path . time() . rand(1, 999) . $type;
        if (file_exists($file)) {
            rename($file, $name_new);
        }
    } else {
        $name_new = $file;
    }
    return $name_new;
}

function uploads_pics($pics, $type = '.jpg') {
    foreach ($pics as $k => $v) {
        if ($v) {
            $pics_sub = array_filter(explode(",", $v));
            foreach ($pics_sub as $k2 => $v2) {
                $name_new = "";
                if (strstr($v2, "/temp/")) {
                    $path = "uploads/course/" . date("Y") . "/" . date("m") . "/" . date("d") . "/";
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }
                    $name_new = $path . time() . rand(1, 999) . $type;
                    if (file_exists($v2)) {
                        rename($v2, $name_new);
                    }
                } else {
                    $name_new = $v2;
                }
                $pics_sub[$k2] = $name_new;
            }
        } else {
            $pics_sub = '';
        }
        $pics[$k] = $pics_sub;
    }
    return $pics;
}

function transferTagsIds($tagsArr, $mtype, $database = 0) {
    $table = "js_tags"; //1
    if ($mtype == 1) {
        $table = "modals_tags";
    } elseif ($mtype == 30) {
        $table = "video_tags";
    } elseif ($mtype == 15) {
        $table = "source_tags";
    } elseif ($mtype == 50) {
        $table = "pintuan_tags";
    }
    $tag_i = 0;
//    if ($database == 1) {
//        $table = "sucai_" . $table;
//    }
    foreach ($tagsArr as $v) {
        if (!empty($v)) {
            if ($database == 1) {

                $info = M($table)->field("id")->where("name = '" . $v . "'")->find();
                if (empty($info)) {
                    $tag_i++;
                    $tag_max_ord = M($table)->max('ord') + $tag_i;
                    $tag_lastid = M($table)->add(array("name" => $v, "ord" => $tag_max_ord, "is_check" => 1));
                } else {
                    $tag_lastid = $info['id'];
                }
            } else {
                $info = M($table)->field("id")->where("name = '" . $v . "'")->find();
                if (empty($info)) {
                    $tag_i++;
                    $tag_max_ord = M($table)->max('ord') + $tag_i;
                    $tag_lastid = M($table)->add(array("name" => $v, "ord" => $tag_max_ord, "is_check" => 1));
                } else {
                    $tag_lastid = $info['id'];
                }
            }

            $tags[] = $tag_lastid;
        }
    }
    $rs = !empty($tags) ? implode(",", $tags) : "";
    return $rs;
}

function getSymbol($symbol) {
    $rs = "积分";
    if ($symbol == 1) {
        $rs = "积分";
    }
    return $rs;
}

function getPointsUnit($symbol) {
    $rs = "积分";
    if ($symbol == 1) {
        $rs = "火币";
    }
    return $rs;
}

function getPointsType() {
    $arr = array(
        "day_sign" => "每日签到",
        "comment" => "评论",
        "help_template" => "扒模板",
        "modals" => "下载模板",
        "video" => "下载视频",
        "soft" => "下载软件",
        "pintuan" => "拼团",
        "js" => "下载js",
        "source" => "下载源码",
        "bind_three" => "绑定第三方",
        "email_check_active" => "邮箱激活",
        "pay" => "在线充值",
        "month_sign" => "每个月连续签到",
        "weibo_qq" => "分享微博",
        "withdraw" => "提现",
        "reg" => "注册",
        "renpay" => "人工充值",
    );
    return $arr;
}

function getPointsTitle($detail) {
    $userid = getUserid();
    $title = "未知";
    $mtype = $detail['mtype'];
    $money = $detail['money'];
    $yongjin_symbol = $yongjin_name = "";
    if ($userid == $detail['to_uid']) {
        $money = $detail['to_money'];
        $yongjin_symbol = "<span class='yongjin_symbol'>佣金</span>";
        $yongjin_name = "<span style='color:#5188a6;margin:0 3px 0 0'>" . getUserName($detail['uid']) . "</span>";
    }
    if ($detail['tid'] > 0 && in_array($mtype, array("js", "modals", "source", "video"))) {
        $table = $detail['mtype'];
        $info = M($table)->field("name")->where("id = " . $detail['tid'] . "")->find();
        if ($table == 'modals') {
            $table = 'templates';
        }
        $link = __APP__ . "/" . $table . "/" . $detail['tid'];
        if ($info['name']) {
            if ($detail['fuhao'] == 1) {
                if ($detail['is_vip'] == 1 && $detail['symbol'] == 0) {
                    $title = getUserName($detail['uid_to']) . "下载<a href='" . $link . "' target='_blank'>【" . $info['name'] . "】</a>" . getConsumType($detail['fuhao']) . "<span class='money'>" . $money . "</span>" . getPaySymbol($detail['points_type']);
                } elseif ($detail['is_vip'] == 1 && $detail['symbol'] == 1) {
                    $title = getUserName($detail['uid_to']) . "下载<a href='" . $link . "' target='_blank'>【" . $info['name'] . "】</a>" . getConsumType($detail['fuhao']) . "<span class='money'>" . $money . "</span>" . getPaySymbol($detail['points_type']);
                } else {
                    $title = getUserName($detail['uid_to']) . "下载<a href='" . $link . "' target='_blank'>【" . $info['name'] . "】</a>" . getConsumType($detail['fuhao']) . "<span class='money'>" . $money . "</span>" . getPaySymbol($detail['points_type']);
                }
            } else {
                if ($detail['is_vip'] == 1 && $detail['symbol'] == 0) {
                    $title = "下载<a href='" . $link . "' target='_blank'>【" . $info['name'] . "】</a>" . getConsumType($detail['fuhao']) . "<span class='money'>0</span>" . getPaySymbol($detail['points_type']) . "<span style='text-decoration:line-through;color:#999;margin-left:6px'>" . $money . getPaySymbol($detail['points_type']) . "</span>";
                } elseif ($detail['is_vip'] == 1 && $detail['symbol'] == 1) {
                    $huobi_title = "";
                    if ($detail['points_type'] == 0) {
                        $huobi_title = "<span style='text-decoration:line-through;color:#999;margin-left:6px'>" . ($money * 2) . getPaySymbol($detail['points_type']) . "</span>";
                    }
                    $title = "下载<a href='" . $link . "' target='_blank'>【" . $info['name'] . "】</a>" . getConsumType($detail['fuhao']) . "<span class='money'>" . $money . "</span>" . getPaySymbol($detail['points_type']) . $huobi_title;
                } else {
                    $title = "下载<a href='" . $link . "' target='_blank'>【" . $info['name'] . "】</a>" . getConsumType($detail['fuhao']) . "<span class='money'>" . $money . "</span>" . getPaySymbol($detail['points_type']);
                }
            }
        } else {
            $title = "标题丢失";
        }
    } else {
        $points_types = getPointsType();
        if (array_key_exists($mtype, $points_types)) {
            if ($mtype == 'month_sign') {
                $firstday = date("Y-m-d", $detail['addtime']);
                $lastday = date("Y年m", strtotime("$firstday -1 month"));
                $title = $lastday . "月连续签到20次以上获得" . "<span class='money'>" . $money . "</span>" . getSymbol($detail['symbol']);
            } else {
                $title = $points_types[$mtype] . getConsumType($detail['fuhao']) . "<span class='money'>" . $money . "</span>" . getPaySymbol($detail['points_type']);
            }
        } else {
            $title = $mtype . "暂无该类型";
        }
    }
    return $yongjin_name . $title . $yongjin_symbol;
}

function getConsumType($fuhao) {
    $rs = "消耗";
    if ($fuhao == 1) {
        $rs = "获取";
    }
    return $rs;
}

function getWithdrawState($state) {
    $arr = array(
        0 => "待审核",
        1 => "<span style='color:#FF6600'>审核通过,待付款</span>",
        10 => "<span class='green'>审核完成，已付款</span>",
        -1 => "<span class='gray'>拒绝通过</span>",
    );

    if ($state == '') {
        return $arr;
    } else {
        return $arr[$state];
    }
}

function getWithdrawMoney($huobi) {
    $rs = 0;
    if ($huobi > 0) {
        $rs = $huobi * (100 - C("money.withdraw_handling_charge_percent")) / 100;
    }
    return $rs;
}

function getMtype($mtype) {
    if ($mtype == 2) {
        $rs = 'js';
    } elseif ($mtype == 3) {
        $rs = 'site';
    } elseif ($mtype == 12) {
        $rs = 'tools';
    } elseif ($mtype == 21) {
        $rs = 'topic';
    } elseif ($mtype == 15) {
        $rs = 'source';
    } elseif ($mtype == 20) {
        $rs = 'php';
    } elseif ($mtype == 30) {
        $rs = 'video';
    } elseif ($mtype == 50) {
        $rs = 'pintuan';
    } else {
        $rs = 'templates';
    }
    return $rs;
}

function is_vip($vip_time) {
    return $vip_time > time() ? 1 : 0;
}

function getPayPoints($detail) {
    $rs = $detail['points'];
    if ($detail['symbol'] == 1) {
        $rs = $detail['huobi'];
    }
    return $rs;
}

function getPaySymbol($symbol) {
    $rs = "积分";
    if ($symbol == 1) {
        $rs = "火币";
    }
    return $rs;
}

function getMtypeTable($mtype, $file = '') {
    if ($mtype == 1) {
        $table = 'modals';
    } else if ($mtype == 2) {
        if ($file == 1) {
            $table = 'jquery';
        } else {
            $table = 'js';
        }
    } else if ($mtype == 3) {
        $table = 'sites';
    } else if ($mtype == 12) {
        $table = 'tools';
    } else if ($mtype == 15) {
        $table = 'source';
    } else if ($mtype == 20) {
        $table = 'js';
    } else if ($mtype == 21) {
        $table = 'topic';
    } else if ($mtype == 30) {
        $table = 'video';
    } else if ($mtype == 50) {
        $table = 'pintuan';
    }
    return $table;
}

function getVipPoints($userid, $points) {

    if (getUserVip($userid) > 0) {
        $points = $points * 2;
    }
    return $points;
}

function addlog($title, $mtype) {
    $data['title'] = $title;
    $data['mtype'] = $mtype;
    $data['addtime'] = time();
    M("log")->add($data);
}

function check_sign($userid) {
    $rs = false;
    if ($userid > 0) {
        $userinfo = M("user")->field("check_sign")->where("id = '" . $userid . "'")->find();
        if ($userinfo['check_sign'] == md5($userid . "_sucaihuo")) {
            $rs = true;
        }
    }
    return $rs;
}

function getSourceDemo($id) {
    $info = M("source")->field("demo_url,is_iframe")->where("id = " . $id . "")->find();
    if ($info) {
        if ($info['is_iframe'] == 1) {
            $url = "/demo/" . $id . "";
        } else {
            $url = $info['demo_url'];
        }
    }
    return $url;
}

function getMtypeInfo($id, $mtype) {
    $table = getTableInfo($mtype);
    if ($table != 'tools') {
        $detail = M($table)->field("name,tags,uid")->where("id = " . $id . "")->find();
        return $detail;
    }
}

function getMtypeField($id, $mtype, $field = 'name') {
    if ($id > 0) {
        $table = getTableInfo($mtype);
        $detail = M($table)->field($field)->where("id = " . $id . "")->find();
        return $detail[$field];
    }
}

function getMtypeHref($id, $mtype) {
    if ($mtype == 12) {
        $info = M("tools")->field("code")->cache(true)->where("id = '" . $id . "'")->find();
        $id = $info['code'];
    }
    return __APP__ . "/" . getMtype($mtype) . "/" . $id;
}

function getMtypeLogo($id, $mtype) {
    if ($mtype != 21) {

        return getModalsLogo($id, 'middle', getMtypeTable($mtype, 0));
    }
}

function getCommentToUser($id, $mtype, $pid) {
    if ($pid == 0) {
        $info = getMtypeInfo($id, $mtype);
    } else {
        $info = M("comment")->field("uid")->where("id = " . $pid . "")->find();
    }
    return array("nickname" => getUserName($info['uid']), "uid" => $info['uid']);
}

function getMtypeTitle($mtype) {
    if ($mtype == 2) {
        $rs = '网页特效';
    } elseif ($mtype == 3) {
        $rs = '精选网址';
    } elseif ($mtype == 15) {
        $rs = '网站源码';
    } elseif ($mtype == 30) {
        $rs = '视频教程';
    } elseif ($mtype == 50) {
        $rs = '拼团';
    } else {
        $rs = '网站模板';
    }
    return $rs;
}

function getModalsLogo($id, $thumb = 'middle', $file = 'modals') {
    if ($id > 0) {
        if ($file == 'tools') {
            $info = M("tools")->field("code")->cache(true)->where("id = '" . $id . "'")->find();
            return "http://demo.sucaihuo.com/other/tools/images/" . $info['code'] . ".png";
        } elseif ($file == 'sites') {
            return C("remote_url.sites") . getFileBei($id) . $id . "/" . $thumb . ".jpg";
        } else {
            if (in_array($file, array("js", "modals"))) {
                return "http://yanshi.sucaihuo.com/" . C("" . $file . "_file.logo") . getFileBei($id) . $id . "/" . $thumb . ".jpg";
            } else {
                return __APP__ . "/" . C("" . $file . "_file.logo") . getFileBei($id) . $id . "/" . $thumb . ".jpg";
            }
        }
    }
}

function getModalsDemo($id, $html, $url_demo) {
    if ($url_demo) {
        return $url_demo;
    } else {
        return "http://yanshi.sucaihuo.com/" . C("modals_file.demo") . getFileBei($id) . $id . "/demo" . $html;
    }
}

function getZipDownload($table, $id, $files_save) {
    $file = C("" . $table . "_file.zip") . getFileBei($id) . $id . "/" . is_Gb2312($files_save);
    if (in_array($table, array("js", "modals"))) {
        $file = "http://yanshi.sucaihuo.com/" . C("" . $table . "_file.zip") . getFileBei($id) . $id . "/" . getZipMd5($id) . "/" . $files_save;
    }
    return $file;
}

function getZipMd5($id) {
    $privateKey = '*&09%$_s@?ddt?ghhthh';
    return md5(Sha1(md5(md5($id . $privateKey))) . "_ABC*");
}

function getDays($starttime, $endtime) {
    $days = 0;
    $now = time();
    if ($endtime > $now) {
        $days = ceil(($endtime - $now) / (3600 * 24));
    }
    return $days;
}

function getPintuanPercent($points_pinhas, $points_market) {
    $percent = 0;
    if ($points_pinhas > 0 && $points_market > 0) {
        $percent = sprintf("%.2f", ($points_pinhas / $points_market) * 100);
    }
    return $percent;
}

function hideStar($str) { //用户名、邮箱、手机账号中间字符串以*隐藏 
    $first = substr($str, 1);
    $length = mb_strlen($str, 'utf8');
    if (!eregi("[^\x80-\xff]", $first)) {
        $rs = msubstr($str, 0, 1, 'utf-8', false) . "***" . msubstr($str, $length - 1, 1, 'utf-8', false);
    } else {
        $rs = msubstr($str, 0, 2, 'utf-8', false) . "***" . msubstr($str, $length - 2, 2, 'utf-8', false);
    }
    return $rs;
}

function checkMuma($img) {
    $status = 0;
    $tips = array(
        "0" => "文件没问题",
        "5" => "文件有毒",
        "-1" => "文件没有上传"
    );
    if (file_exists($img)) {
        $resource = fopen($img, 'rb');
        $fileSize = filesize($img);
        fseek($resource, 0);
        if ($fileSize > 512) { // 取头和尾
            $hexCode = bin2hex(fread($resource, 512));
            fseek($resource, $fileSize - 512);
            $hexCode .= bin2hex(fread($resource, 512));
        } else { // 取全部
            $hexCode = bin2hex(fread($resource, $fileSize));
        }
        fclose($resource);
        /* 匹配16进制中的 <% ( ) %> */
        /* 匹配16进制中的 <? ( ) ?> */
        /* 匹配16进制中的 <script | /script> 大小写亦可 */
        if (preg_match("/(3c25.*?28.*?29.*?253e)|(3c3f.*?28.*?29.*?3f3e)|(3C534352495054)|(2F5343524950543E)|(3C736372697074)|(2F7363726970743E)/is", $hexCode)) {
            $status = 5;
        }
    } else {
        $status = -1;
    }
    $rs = $tips[$status];
    if ($rs == '文件有毒') {
        unlink($img);
    }
}

function getMtypeUrl($table) {
    if ($table == 'modals') {
        $table = 'templates';
    } else if ($table == 'sites') {
        $table = 'site';
    }
    return $table;
}



?>
