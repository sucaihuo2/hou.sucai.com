function getUrl(strs) {//获取参数
    var url = $("#SITE_URL").val() + "/" + strs;
    return url;
}
function goUrl(url) {
    if (url == -1) {
        history.go(-1);
    } else {
        document.location.href = url;
    }
}
function addNames() {
    var tr = "<tr><td class='td_left'></td>\n\
<td><input name='name[]' type='text' class='common_txt'  value=''/>\n\
<a onclick=$(this).parent().parent().remove()>删除</a></td></tr>";
    $('#tr_name').before(tr);
}
function targetDelTable(id, is_delete) {
    $("#modal_del").modal('toggle');
    $("#modal_del").attr('data-id', id);
    var table = $("#modal_del_table").attr("data-table");
    $("#modal_del").attr('data-table', table);
    $("#modal_del").attr('data-delete', is_delete);
    if (is_delete == 1) {
        $("#modal_del").find(".modal-body").html("确定要恢复吗?")
    } else {
        $("#modal_del").find(".modal-body").html("确定要删除吗？")
    }
}
function targetStateTable(id, state) {
    $("#modal_state").modal('toggle');
    $("#modal_state").attr('data-id', id);
    var table = $("#modal_del_table").attr("data-table");
    $("#modal_del").attr('data-table', table);
    $("input[name=state_box][value=" + state + "]").attr("checked", true);
}
function in_array(search, array) {
    for (var i in array) {
        if (array[i] == search) {
            return true;
        }
    }
    return false;
}
function clearEmptyArr(arr) {
    var length = arr.length;
    var newArr = [];
    for (var i = 0; i < length; i++) {
        if (arr[i] != '') {
            newArr.push(arr[i]);
        }
    }
    return newArr;
}
function uniQueue(array) {
    var arr = [];
    var m;
    while (array.length > 0) {
        m = array[0];
        arr.push(m);
        array = $.grep(array, function(n, i) {
            return n == m;
        }, true);
    }
    return arr;
}
function choseRadio(area_id, field, title) {
    showModal('#myModal');
    $('#myModal').data('modal', field);
    $('#myModal').attr('data-area', area_id);
    $('#modal-title').text(title);
    $.post(getUrl("Box/choseRadio"), {
        field: field
    }, function(data) {
        $("#choseRadio").html(data);
    })
}
function changeOrd(obj, table, id, field) {
    var width = 50;
    if(field =='source'){
        width= 300;
    }
    var val = obj.text();
    var c = obj.parent("td");
    obj.parent("td").html("<input type='text' style='width:"+width+"px;' onFocus=this.select()  onblur=changeOrdConfirm($(this),'" + table + "'," + id + ",'" + field + "') value='" + val + "' />");
    c.children("input").focus();
}
function changeField(obj, table, id) {
    var val = obj.text();
    var c = obj.parent("td");
    obj.parent("td").html("<input type='text' style='width:50px;' onFocus=this.select()  onblur=changeFieldConfirm($(this),'" + table + "'," + id + ") value='" + val + "' />");
    c.children("input").focus();
}
function changeFieldConfirm(obj, table, id, field) {
    $.post(getUrl("Ajax/changeFieldConfirm"), {
        id: id,
        table: table,
        val: obj.val(),
        field: field
    }, function(data) {
        obj.parent("td").html("<a onclick=changeField($(this),'" + table + "'," + id + ",'" + field + "')>" + obj.val() + "</a>");
    })
}
function changeOrdConfirm(obj, table, id, field) {
    $.post(getUrl("Ajax/changeOrdConfirm"), {
        id: id,
        table: table,
        ord: obj.val(),
        field: field
    }, function(data) {
        obj.parent("td").html("<a onclick=changeOrd($(this),'" + table + "'," + id + ",'" + field + "')>" + obj.val() + "</a>");
    })
}

function delPic(id, table, field) {
    if (confirm("确定要删除吗")) {
        $.post(getUrl("Ajax/delPic"), {
            table: table,
            id: id,
            field: field
        }, function(data) {
            $('#td_img').remove();
        })
    }
}
function yes(id, table, field) {
    $.post(getUrl("Ajax/yes"), {
        id: id,
        table: table,
        field: field
    }, function(data) {
        if (data == "是") {
            $('#' + field + '_' + id).html("<a onclick=yes(" + id + ",'" + table + "','" + field + "')>" + data + "</a>");
        } else {
            $('#' + field + '_' + id).html("<a onclick=yes(" + id + ",'" + table + "','" + field + "')><span style='color:red'>" + data + "</span></a>");
        }
    })
}
function no(id, table, field) {
    $.post(getUrl("Ajax/no"), {
        id: id,
        table: table,
        field: field
    }, function(data) {
        if (data == "是") {
            $('#' + field + '_' + id).html("<a onclick=no(" + id + ",'" + table + "','" + field + "')>" + data + "</a>");
        } else {
            $('#' + field + '_' + id).html("<a onclick=no(" + id + ",'" + table + "','" + field + "')><span style='color:red'>" + data + "</span></a>");
        }
    })
}
function no_download(id, table, field) {
    $.post(getUrl("Ajax/no_download"), {
        id: id,
        table: table,
        field: field
    }, function(data) {
        if (data == "是") {
            $('#' + field + '_' + id).html("<a onclick=no_download(" + id + ",'" + table + "','" + field + "')>" + data + "</a>");
        } else {
            $('#' + field + '_' + id).html("<a onclick=no_download(" + id + ",'" + table + "','" + field + "')><span style='color:red'>" + data + "</span></a>");
        }
    })
}
function getColumnHighcharts(id, categories, data, type) {
    if (type == 1) {
        type = "line";
    } else {
        type = "column";
    }
    $(id).highcharts({
        chart: {
            type: type
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            title: {
                text: ''                  //指定y轴的标题
            }
        },
        credits: {
            enabled: false // 禁用版权信息
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'top',
            x: 0,
            y: -10,
            floating: false,
            borderWidth: 0
        },
        series: data
    });
}
function getPieHighcharts(id, data) {
    $(id).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
//                dataLabels: { 显示类别
//                    enabled: false
//                },
                showInLegend: true
            }
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'top',
            x: 0,
            y: -10,
            floating: false,
            borderWidth: 0
        },
        credits: {
            enabled: false // 禁用版权信息
        },
        series: data
    });
}
function goOrderUrl(mtype) {
    var year = $("#year").val();
    var month = $("#month").val();
    var day = $("#day").val();
    var type = $("#type").val();
    if (year == 0) {
        month = day = 0;
    }
    var shop_id = $("#shop_id").val();
    $.post(getUrl("Ajax/goOrderUrl"), {
        year: year,
        month: month,
        day: day,
        type: type,
        mtype: mtype,
        shop_id: shop_id
    }, function(data) {
        goUrl(data);
    })
}
function statics_set(control, mod) {
    var names = '';
    var vals = '';
    $("#modal_set").find("select").each(function() {
        var name = $(this).attr("data-name");
        var val = $(this).val();
        names += name + ",";
        vals += val + ",";
    })
    $.post(getUrl("Ajax/statics_set"), {
        control: control,
        mod: mod,
        names: names,
        vals: vals
    }, function(data) {
        goUrl(data);
    })
}
function yearChange() {
    var year = $("#set_year").val();
    var month = $("#set_month").val();
    var day = $("#set_day").val();
    $.post(getUrl("Ajax/yearChange"), {
        year: year,
        month: month,
        day: day
    }, function(data) {
        $("#set_month").html(data);
        $("#set_day").html('<option value="0">请选择日</option>');
    })
}
function monthChange() {
    var year = $("#set_year").val();
    var month = $("#set_month").val();
    var day = $("#set_day").val();
    $.post(getUrl("Ajax/monthChange"), {
        year: year,
        month: month,
        day: day
    }, function(data) {
        $("#set_day").html(data);
    })
}
function more_del(obj) {
    var modal = obj.parents(".modal");
    var id = modal.attr("data-id");
    var table = modal.attr("data-table");
    var is_delete = $('#modal_del_table').attr("data-delete");
    if (id == 0) {
        var ids = '';
        $("input[name='ids']").each(function() {
            if ($(this).prop("checked") == true) {
                ids += $(this).val() + ",";
            }
        });
        if (ids == '') {
            alert("请选择！");
            return false;
        }
    } else {
        ids = id + ","
    }
    $.post(obj.attr("data-url"), {
        ids: ids,
        table: table,
        is_delete: is_delete
    }, function(data) {
        location.reload();
    })
}
function checkState(obj) {
    var modal = obj.parents(".modal");
    var id = modal.attr("data-id");
    var table = $("#modal_del").attr("data-table");
    var state = $("input[name=state_box]:checked").val();
    $.post(obj.attr("data-url"), {
        id: id,
        table: table,
        reason: $("#reason").val(),
        state: state
    }, function(data) {
        location.reload();
    })
}
function checkTags(obj) {
    if (obj.prop("checked") == true) {
        if ($('#modal_tags').attr('data-tags').indexOf(',' + obj.val() + ',') == '-1') {
            $('#modal_tags').attr('data-tags', ',' + $('#modal_tags').attr('data-tags') + obj.val() + ',');
        }
    } else {
        if ($('#modal_tags').attr('data-tags').indexOf(',' + obj.val() + ',') != '-1') {
            $('#modal_tags').attr('data-tags', $('#modal_tags').attr('data-tags').replace(',' + obj.val() + ',', ','));
        }
    }
}
function choseTagsBoxConfirm() {
    var id = $("#modal_tags").attr('data-id');
    var tags = $("#modal_tags").attr('data-tags');
    $.get(getUrl("Box/choseTagsBoxConfirm"), {
        id: id,
        tags: tags
    }, function(data) {
        $("#modal_tags").modal('hide');
        $("#tag_" + id).text(data);
        $("#tag_" + id).attr('data-tags', tags);
    })
}
function sitemapCat(id) {
    var sitemap_cat = $("#sitemap_cat").val();
    $.get(getUrl("Ajax/sitemapCat"), {
        id: id,
        sitemap_cat: sitemap_cat
    }, function(data) {
        location.reload()
    })
}
function changeLang(id, lang_id) {
    $.post(getUrl("Ajax/changeLang"), {
        id: id,
        lang_id: lang_id
    }, function(data) {

    })
}
function changeMtypes(id, mtype) {
    $.post(getUrl("Ajax/changeMtypes"), {
        id: id,
        mtype: mtype
    }, function(data) {

    })
}
function addTags() {
    var tr = "<input name='tag[]' type='text' class='common_txt tag'  value=''/>\n\
<a onclick=$(this).prev('input').remove();$(this).remove()>删除</a>";
    $('#td_tags').append(tr);
    getTagAutocomplete();
}
function removeContent(obj) {
    obj.parent("td").parent("tr").remove();
}
function addParameter(obj) {
    var tr = "<tr><td class='td_left'>参数设置 <a onclick=addParameter($(this))>+增加</a></td>\n\
    <td><label>参数：</label><input type='text' name='paras[]' class='paras'/>\n\
        <label>描述：</label><input type='text' name='descriptions[]' class='descriptions'/>\n\
        <label>默认：</label><input type='text' name='defaults[]' class='defaults'/>\n\
        <label>链接：</label><input type='text' name='urls[]' class='urls'/>\n\
        <a onclick=removeContent($(this))>删除</a></td></tr>";
    obj.parent("td").parent("tr").after(tr);
}
function sendToBaidu(url) {
    $.post(getUrl("Ajax/sendToBaidu"), {
        url: url,
    }, function(data) {
        alert(data);
    })
}
function getMiddleLogo(id, mtype) {
    $.post(getUrl("Ajax/getMiddleLogo"), {
        id: id,
        mtype: mtype
    }, function(data) {
        alert(data);
    })
}
function getShortUrl(url_short,demo_url) {
    var url_short = $("#"+url_short).val();
    $.post("/Tian/Ajax/getShortUrl", {url_short: url_short},
    function(data) {
      
        $("#"+demo_url).val(data.url_short);
    }, "json")
}
function getWangpanUrl() {
    var wangpan_short = $("#wangpan_short").val();
    var length = wangpan_short.length;
    var wangpan_url = httpString(wangpan_short);

    var splits = wangpan_short.split("提取码：");

        var wangpan_pwd = (splits[1]).substr(0, 4);
    $("#wangpan_url").val(wangpan_url);
    $("#wangpan_pwd").val(wangpan_pwd);
}
function httpString(s) {
    var reg = /(http:\/\/|https:\/\/)((\w|=|\?|\.|\/|&|-)+)/g;
    //var reg = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;  
    //var reg=/(http(s)?\:\/\/)?(www\.)?(\w+\:\d+)?(\/\w+)+\.(swf|gif|jpg|bmp|jpeg)/gi;  
    //var reg=/(http(s)?\:\/\/)?(www\.)?(\w+\:\d+)?(\/\w+)+\.(swf|gif|jpg|bmp|jpeg)/gi;  
    var reg = /(https?|http|ftp|file):\/\/[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]/g;
    //var reg= /^((ht|f)tps?):\/\/[\w\-]+(\.[\w\-]+)+([\w\-\.,@?^=%&:\/~\+#]*[\w\-\@?^=%&\/~\+#])?$/;  
    //v = v.replace(reg, "<a href='$1$2'>$1$2</a>"); //这里的reg就是上面的正则表达式  
    //s = s.replace(reg, "$1$2"); //这里的reg就是上面的正则表达式  
    s = s.match(reg);
    console.log(s)
    return(s)
}
function update_cache(id,mtype){
   
     $.post(getUrl("Ajax/update_cache"), {
        id: id,
        mtype:mtype
    },
    function(data) {
        if(data == 'success'){
            alert('删除成功');
        }else{
            alert('删除失败，文件位置：'+data);
        }
    })
}