window.onload = function() {
    var hidemobilebtn = document.getElementById("hidemobile");
    var showmobilebtn = document.getElementById("showmobile");
    var mobileframe = document.getElementById("mobileFrame");
    if (!hidemobilebtn)
        return false;
    if (!showmobilebtn)
        return false;
    if (!mobileframe)
        return false;
    hidemobilebtn.onclick = function() {
        mobileframe.style.display = "none";
        showmobilebtn.style.display = "block";
    }
    showmobilebtn.onclick = function() {
        this.style.display = "none";
        mobileframe.style.display = "";
    }
}
document.getElementById("mobileFrame").getElementsByTagName("iframe")[0].onload = function() {
    this.style.width = "274px";
}